---
title: "Colour in Illustrator"
linktitle: "Illustrator: colour"
date: 2021-08-19T09:18:27+01:00
draft: false
photoswipe: false
toc: true
categories: ["illustrator"]
tags: ["illustrator", colour]
summary: How to use colour in Illustrator
---

## Colour

1. Top L of the tool bar is the swatches panel.
2. Shift click this to get colour (sliders) panel.
3. Shift click these to move all 3 slider together thus making lighter or darker
4. Use F6 to toggle the color panel.
5. In the top right use the drop down to NOT only choose the colour mode. Also invert the colour or choose it's complement.
6. Use `Edit > Edit Colors > Blend` horizontally to blend a line of objects from the first and last color.


## Making Global colors

1. Select an object.
2. From the swatches menu use the RH drop down to *add a new swatch*.
3. In the dialogue box that pops up tick the check box, `Global Color`. The swatch has a small white triangle on it.
4. Now if you change that colour all other object using that SWATCH, will change too.



## The Swatches Panel

Comes with the default colours initially. 

To get rid first use the top right drop down to `Add used colors`, then choose `Select All Unused`. Click the bin to delete these.

You can switch mode to `HSB` if easier.

To create a palette from an image first choose the color from the image using the eyedropper (`I`) tool. Then click the new swatch icon.
These can then be added to a group. Select them and choose add to group.

## Recolor artwork

{{< music/yt "ul0flcHS1MA" >}}

https://www.youtube.com/watch?v=ul0flcHS1MA


Many things you can do with this. Great vid by Yes, I'm a Designer

{{< music/yt "u25xsb8243Y" >}}


Used to reduce the number of colours. 1 colour will be a range of tints of the same hue.

To recolor first select object.

`Edit > Edit Colors > Recolor Artwork`


There are two tabs: `Edit` and `Assign`


To reset changes click the eyedropper on the top R.