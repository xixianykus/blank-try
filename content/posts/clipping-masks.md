---
title: "Illustrator: Clipping Masks"
date: 2021-08-19T08:48:19+01:00
draft: false
photoswipe: false
categories: ["illustrator"]
tags: ["illustrator"]
summary: "The different types of clipping masks in Illustrator"
---

## Clipping mask

To hide outside the Artboard there's a short cut in CC2019 (finally).

Otherwise use either:

1. A clipping mask. Make a rectange the same dimensions as the artboard. Align vert and horizontal. Make sure this is on top. Select everthing and press Ctrl + 7

2. Make the rectangle again, align it and add a 1000px stroke aligned to outside (from the stroke panel)



There are 3 types of mask:

### 1. Layer Clipping Masks
 
1. Add a shape at the top of the stack. Don't select any objects, just have the layer stack heading highlighted (eg. Layer 1). 
2. Click on the Layer name block to highlight it.
3. Click at the bottom of the layers panel. This leaves everything editable. 
4. To release make sure the main layer is highligted and click the icon at the bottom of the layers panel.Selecting the mask object won't work, unless you just delete it.

NB 1. When applying your clipping shape needs to be the top object in the layer. But once made it doesn't matter where in the stacking order it is.  
NB 2. Options from the object menu won't work.


A different way to make these is choosing the 'Draw Inside' icon at the bottom on the tools palette. This allows the fill colour of the clipping object to be at the bottom.

### 2. Opacity Masks

These are made in the transparency panel. 

First select the object/s you wish to mask.

Method 1

1. Double click the icon to activate it.
2. Uncheck the 'Clip' box so it becomes white. 
3. Paste a shape or gradient into the mask.
4. Click the LH image icon to exit the mask editing mode.

Method 2

Place a gradient object above object/s you want to mask
2. Select everything
3. In the transparency palette click 'Make Mask'



### 3. Clipping masks

These can be made in two different ways. 

The first way is called Draw Inside. Select the shape you want to use then click the R most icon at the bottom of the tool palette or press Shft + D until selected.

This is to clip an object or photo. 
  a) add a shape above the object you want clipped.
  b) `Ctrl + 7` or `Object > Clipping Mask > Make`
This will form a new group and the object doing the clipping will lose any stroke or fills that are applied to it.

 `Ctrl + Alt + 7` to release the mask.

 To edit the photo or object inside the mask use the direct selection tool OR if there are several / lots of object use Object > Clipping Mask > Edit contents




To mask an object or group (group objects first if there is more than one).

1. create the mask as an object where you want it.
2. Cut it to clipboard.
3. On the Transparency panel double the make mask. Clck to uncheck clip mask.
4. Paste in front. 
5. make sure new object is black to completely mask object/s
6. Click icon next to mask icon to return to 

## Letter clipping mask


This is a little more tricky

1. Type out your text
2. optional: Create outlines
3. optional: Ctrl + 8 to make compound path
4. Place objects below text
5. Select all
6. Ctrl + 7 to clip