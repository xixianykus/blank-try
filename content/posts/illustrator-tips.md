---
title: "Illustrator Tips"
date: 2021-08-18T15:41:52+01:00
draft: false
categories: ["Web dev"]
tags: ["illustrator"]
toc: true
summary: "Big list of Adobe Illustrator tips inc. shortcuts, guides, artboards, rotate, reflect, reshape, scale, shear, perspective grid, width tool and gradient mesh."
---


## SHORTCUTS

`Ctrl + 1` = Zoom to 100% view  
`Ctrl + 2` = Lock selected  
`Ctrl + 3` = Hide selected  

`Ctrl + 5` = Convert selected to guides  

`Ctrl + 7` = Make clipping mask from top selected object  
`Ctrl + 8` = Make compound path


### Type

- **Kerning**: place cursor between two characters, hold `Alt` and use L and R arrow keys.
- **Tracking**: same as above but without the cursor. Just select whole type block.
- **Leading**: as tracking but use the up and down keys.


### Tools

`V` = Move tool (black arrow)  
`A` = Arrow tool or direct selection

`P` = Pen tool  
`Shft + ~` = Curvature tool (other Pen tool). Holding Alt creates a corner point

`Shft + E` = The Eraser tool. Hold down Alt and drag to create a whole rectangle of erasing.

`C` = The scissor tool

`E` = Transform tool


`B` is the Brush tool  
`Shft + B` = Blob brush

`R` = Rotate tool  
`O` = Reflect tool

`W` = Blend tool  
`Ctrl + Alt + B` is for Blend (select two objects first)

Zoom - hold down `Alt` and use the mousewheel



`Ctrl + Shft + B` = hides the bounding box.

`Ctrl + G`  / `Ctrl + Shft + G` = Group / Ungroup

`Ctrl + 2` is lock the selected object  
`Ctrl + Alt + 2` is unlock all

`Ctrl + 5` is Make Guides  
`Ctrl + ;` toggles hiding and showing guides

`F2`, `F3`, `F4` = Cut, Copy Paste = NOW Expand Appearence, Expand

`Ctrl + E` = Expand

`F5` = toggles the brushes panel  
`F6` = the colour panel  
`F7` = the layers panel  
`F8` = Create new symbol

`Ctrl + Shft + A` to deselect everything

`Ctrl + Alt + P` = Document setup 

`Shft + o` = Artboard mode (choose esc or any tool to exit, eg moVe tool)

`Ctrl + Y` = wireframe view

`Ctrl + Alt + B` = Blend objects



## Template layers

You can make a layer into a template layer by double clicking it and ticking the checkbox. This locks the layer and dims it too (you can change the dimming though). This is handy for tracing over artwork.

## Isolation mode

Double click to isolate an object and double click outside of it to exit

 OR

Press `Esc` to exit isolation mode

## Expand and Expand appeareance

If an object, like a line, has just a stroke, using `Object > Expand` will make it into a solid object. Shortcut: `Ctrl + £`

Expand Appearance will make everything into flat solid shapes


## Break apart art work

1. Draw with a series of strokes
2. Edit strokes width and brush type
3. `Object > Expand Appearance` to convert to shapes
4. `Object > Live Paint > Make`
5. `Object > Live Paint > Expand`

You can now delete the bits you do not want.

## Offset path

Can be found in the `Object > Path > Offset Path` menu. 

This replicates a path and it's either slightly bigger or smaller (using negative values).

### Reset the Bounding Box

Because of live shapes (like rounded corners) you can't always reset the bounding box by right clicking.

SOLUTION: Use `Object > Shape > Expand Shape`   first.


Hiding the Bounding box

`Ctrl + Shft + B` toggles the bounding box on and off

## Rectangular Grid tool

is found under the line tool.

Good for different designs esp. Logos

The grid can be used with or without a stroke but if locked you need a stroke to see it OR use Make Guides (`Ctrl +8`)


### Click and drag

Click and drag and use the L and R arrow keys to add more or fewer squares

Use `x` and `c` while dragging to move the columns to the L or R

Use `v` and `f` to push the rows up or down


### Click on the artboard

If you click in the top left the grid will start from there. (this can be changed to any corner)


## Text on a Path

Use the right tool and click on the path. To flip the the text so it's not upside down there are 3 vertical bars bisecting the path the text is on. Use the one without a square and drag it to the side of the path you want the text on. The other two bars are used to set the boundries of the text.

### Mold text (or anything)

To change text to a certain shape first set the text up well using tracking (`Alt + up/down arrow keys`), kerning and letter spacing. Create a top shape. You can smooth it with the Pencil>smooth tool (Press `n` for pencil then hold down `Alt`) or better use `Object > Path > Simplify`. Then select all objects use `Ctrl + Alt + C` (`Object > Envelope Distort > Make with top object`).

You could also use `Make with Mesh` using `Ctrl + Alt + M` or  
`Make with Warp` = `Ctrl + Alt + Shft + W`

### With Live Paint Bucket

After making your grid press K for the live paint bucket tool. Click a colour in the swatches then click in any cell to fill that cell with the colour. The L and R and up and down arrow keys can be used to change through the colours.

## Guides with multiple artboards

1. Use the Artboard tool: `Shft + o`
2. Select the artboard
3. Drag out the guide
4. Use the align panel to centre it.

## Colouring the artboard background

You can colour the background for the whole document to any colour you like. 

Go to `File > Document Setup` or use the shortcut `Ctrl + Alt + P` (P for *preferences*).

Check the box *Simulate colored paper* then click in the colour box to bring up the colour picker.


## Rotation

Use the rotate tool (`R`).

Click the object you want rotated.

Alt click where you want the centre of rotation to be.

*OR*

Double click the tool button to bring up the dialogue and rotate from the centre 

Type the no. of degrees, say 30.

Click copy.

Click `Ctrl + D` to repeat


### To Rotate around a specific point

1. Click `R` for the tool
2. Hold down alt and click where you want your rotation centre to be.
3. The dialogue pops up too.


### To Rotate a pattern

Hit `R`
`Alt` click outside the object
In the dialogue make sure Transform patterns is ticked, but not objects



### To Rotate a guide

Enable rulers Ctrl + R and drag out a guide
Unlock it `Ctrl + Alt + ;`
Hit `R`
Click or Alt click the guide to rotate manually or from the dialogue box


### Rotate Anchor points

This works on end points...

As above but click the anchor point

## Reflect tool

This can be tricky for straightforward reflections.

For something reflected along the vertical axis:

Press `O` for the tool. (Reminder: O is symmetrical in every direction)

Click anywhere on the vertical line where you want the reflection to take place

Click on your object, holding down the Alt key (to duplicate it) and the Shft key (to keep it at the right angle).

## Transform tool

The shortcut for this is `E`. Without modifer keys this is much like the move tool. Click and drag a corner to scale the object. Do the same with `shft` key and the proportion is constrained. 

To reshape first click a corner or line **then** hold down `Ctrl` and drag. This changes the perspective.

NB. With text only some transforms work. Hit `Ctrl + Shft + O` to create **o**utlines of the text.


## Reshape tool

This is found under the scale tool. It's not popular but works like this. On an open path click and drag. It adds an anchor point which you drag to change the shape.

On a closed path either:

a) click to create anchor point, release, use the direct selection tool to select the point, then drag OR
b) select two points on your path first, then click in between and drag like on an open path.


## Scale Tool

Shortcut is `s`. Resizing object is generally best done with the move tool (`v`). However hit `s` then `enter` to see the scale tool options and you can scale precisely to a percentage value. You also have the option to choose whether to scale strokes, rounded corners and effects.

If you click and drag you choose the centre point of where to scale from.


## Shear tool

This lives under the shape tool. Like that if you select it and hit enter you can perform a shear manually. Otherwise click to set your *origin* then click and drag elsewhere to *shear* your object.


## Symbol Sprayer

1. Select a symbol in the symbol palette.
2. double click for the options


### Options

![Illustrator's symbol sprayer options](/img/../../../../static/img/symbol-sprayer-options.png)

The diameter is the maximum size the sprayer will reach, like brush size. Can be changed on the fly using square bracket keys.

The `intensity` is the speed the symbols are produced. 

`Symbol set density` is how tightly they are together.


These tools work on individual *symbol groups* which must be selected first.

**Sprayer** sprays the symbols or removes them when holding down `Alt`.

**Shifter** moves the symbols around.

**Scruncher** pulls the symbols in a symbol group together. Press `Alt` to 'unscrunch'?

**Sizeer** makes the symbols larger or with `Alt` smaller.

**Spiner** makes the symbols spin

**Screener** will add transparency to the symbols.

**Stainer** will colour the individual symbols within the group the same colour as the current fill colour. Holding down adds more colour. Press `Alt` will undo the colouring.

**Styler** can apply or remove graphic styles to the symbols

## Live Paint

 Graphic Design How To  
{{< music/yt "n8Sp_6yS1mk" >}}

To use:
1. Select multiple objects first.
2. Hit K for the paint bucket tool.

Or select multiple objects and go to `Object > Live Paint > Make` or  `Ctrl + Alt + X`



1. Live paint bucket hit `K`
2. Options, hit return. You can check the strokes here (a stroke must be added to an object for this to work first.)
3. Switch to eyedropper, hold down `Alt`
4. Cycle through swatches with the L and R arrow keys.


Live paint selection tool:  `Shft + L`

This allows you to select multiple selections then choose a swatch and change all those selected simultaneously.


To use with strokes....

1. Make sure Paint Strokes are enabled in the options (press `K` for Live Paint then Enter for options)
2. Choose the colour AND width you want for your stroke
3. Move the tool onto the stroke you want to change. When the cursor icon changes to a paintbrush just click

**NB. You CAN'T:** Use the width tool or stroke profiles using Live Paint.



## Live Shapes

https://creativepro.com/working-with-live-shapes-in-adobe-illustrator/

Primarily for:  
- rounding corners
- changing number of sides of a polygon
- reducing an ellipse by a slice


https://helpx.adobe.com/uk/illustrator/using/reshape-with-live-corners.html

To round corners click the corner widget and drag.  
To invert the round corner shape first drag and let go. Then hold alt and click the widget. Click again for flattened corners

OR

Click, drag and use the up or down arrow keys


To adjust just one corner click it once, release, then click and drag

To adjust several but not all corners: click one then `shift` click the next. Then drag.

To change the number of sides of a polygon slide the other widget along the bounding box

To reduce a circle or ellipse by a wedge from the centre drag the widget around. With smart guides enabled you see what angle you've cut out.


To convert a live shape to an ordinary shape: `Object > Shapes > Expand Shape`  
This allows you to reset the bounding box.


## Image Trace

Paths: Dragging the Paths slider to the right makes the paths fit tighter to the original “shape” (more anchor points), whereas dragging to the left makes the paths fit looser (less anchor points).

Corners: Dragging the slider to the right makes more corner points, and dragging the slider to the less makes more smooth points.

Noise: Suppose that the background of an image you were tracing contained some unwanted content such as artifacting or dust/spots. By increasing the Noise value (dragging the slider to the right), you can exclude some of that content from the tracing result. The pixel value of Noise refers to the smallest pixel area (not just width or height) that will be traced, and not ignored (see Figure 11).


## Width tool

Adjusts stroke width

Only works on open paths (use scissor tool, press `c`, to cut a path open)

`Shift + w` to access.

Click and drag to add points and choose the width

## Scissor tool

The scissor tool, `c`, is used to cut paths. It can be used on closed or open paths. Just click on the path to make a cut. The tool requires accuracy to work and avoid getting a warning box. 



## Gradient Mesh

Yes I'm a Designer on gradient mesh applied to opacity masks with texture...

{{< music/yt "JvMvfpJOb0U" >}}


Shortcut: `U`


## Perspective Grid and tool


The perspective grid can have 1, 2 or 3 point perspectives

The two tools are

Perspective Grid tool - `Shft + P`  
Perspective Grid Selection tool - `Shft + V`

To hide the perspective grid press `Ctrl + Shft + I` (invisible)


## 10 Illustrator tips


1. Average `Ctrl + Alt + J` is like Join but works where join doesn't to bring points together.
2. ??
3. `Window > New Window` 
4. ??
5. Global color swatch for outlining drawings
6. Pen Tool, use the space bar to move the current point while still drawing.
7. Smooth tool
8. Rounding corners - use direct selection tool to choose a point and round it.
9.  Live paint bucket
10. ??

{{< music/yt "2ycw20MaugA" >}}


## Symmetry

To paint or create symmetrically:

1. Define your axis, say with a guide
2. Make sure at least one object is touching your line of symmetry
3. Group your objects
4. Use `Effect > Distort` and `Transform > Transform`:
	1. Reflect around the X axis (for face or vertical symmetry line)
	2. 1 copy
	3. choose the middle, outside point to reflect on in the little square icon.

Now any thing you add to the group is reflected on the other side.

## Obama style 1

{{< music/yt "Cye3VaLo0Go" >}}

by Gareth David Studio


In Photoshop:

1. cutout the head and shoulders
3. Change to greyscale (`Ctrl + Shft + U`)
3. Adjust contrast with levels. Aim to get a good contrast between hair and eyes
2. save as PSD

In Illustrator

1. Place PSD file
2. `Object > Rasterize`
3. `Effect > Artistic > Cutout` setting: 2, 5, 1
4. `Object > Rasterize` (again)
5. `Image > Trace` (black and white) use threshold
6. `Expand`
7. Deselect all
8. Magic wand to select white
9. Press `delete`
10. Use the Eraser tool carefully over jagged lines to smooth them.
11. Delete unwanted small details
12. Add in new lines where needed eg. the jaw line. and expand and merge

(These two steps are the most time consuming)


13. Setup different layers beneath the main layer in the colours you want
14. Use the eraser tool again to remove sections of each layer
15. For the lines layer, because it is hard to bring back, use and opacity mask instead with a black opacity mask (keep clip checked) use a white brush to reveal the lines

## Links

### General learning

1. [Creatnprocess](https://www.youtube.com/watch?v=ul0flcHS1MA) Good text effects, graphic and logo design
2. [Graphic Design How To](https://www.youtube.com/c/GraphicDesignHowTo/videos) Youtube general search. Lots of good Illustrator videos and other stuff. Eloquently described.
3. [The Width tool](https://www.educba.com/width-tool-in-illustrator/) a comprehensive guide.
4. [Laura Coyle Youtube channel](https://www.youtube.com/channel/UCJvIT9BmwPyqfPahsMMfN6g)
5. [Jeremy Mura](https://www.youtube.com/channel/UCsKxAGJpOi-tsJK18MQ9Fbg) - Graphic designer from Austrlia / logo, branding, graphic design, freelancing
6. [Tutvid](https://tutvid.com/) - a graphic designer, nice and fast and to the point, though sometimes a bit long winded.
Also photoshop, Lightroom, retouching
7. [Brad Colbow](https://www.youtube.com/user/thebradcolbow/search?query=illustrator) - lots of stuff not just Illustrator. He is an illustrator / artist. (eg. How to pick colours/ contrast)
8. [Teela Cunningham](https://www.youtube.com/channel/UC7LfMHlnCxEJWZ-m5BN7_ig)
9. [Yes I'm a Designer](https://www.youtube.com/channel/UCT_of6HCtVZFpnnnLUeAGYA) - good and up to date Youtube channel on Illlustrator and design generally.
10. [Spoon Graphics](https://blog.spoongraphics.co.uk/) - comprehensive site with tutorials/free stuff/ articles etc.
11. [Graphic Extras](https://www.youtube.com/channel/UCjEw2WIz2dp1Y_z9_LJ2-aQ) - up to date, Ilustrator, Photoshop, Affinity designer
graphicxtras.com
12. [T&T Tutorials](https://www.youtube.com/channel/UCQzyK-fyhqUKUUgY3S50gpw) - did a good 42m vid about 3D (Memphis design)

### Inspiration

1. [Dribbble](https://dribbble.com/) (with 3 b's) has a lot of good stuff on. To display work on Dribbble you need an invite. First upload a bunch of stuff. Then, when you have some good quality work, do a search for [invites](https://dribbble.com/search/shots/recent?q=invite) (sort by latest, not popularity). Apply for an invite from one of the offers. See [this article](https://www.webdesignerdepot.com/2013/11/the-ultimate-guide-to-everything-dribbble/ "web designer guide to Dribbble") for more info.
2. Behance is an Abobe web site for showcasing all kinds of digital art.


### Other stuff

1. [Art of Symbols](https://jonathanhaggard.github.io/Art-of-symbols-site/) is all about different symbols. good site.
2. [Happy Hues](https://www.happyhues.co/) - for good colours and swatches

### Links - resources

1. [Busheezy](https://www.brusheezy.com/) for brushes
2. [Free Vectors](https://www.freevectors.net/) seems to have the free and paid for clearly separated




## Fave artists

- Die Antwoord
- Vault 49
- Buro Destruct
- 123Klan
- Autumn Whitehurst
- HR Gieger
- Jasper Goodall