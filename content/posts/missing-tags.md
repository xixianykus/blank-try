---
title: "Missing Tags"
date: 2021-06-09T13:12:47+01:00
draft: false
photoswipe: false
categories: ["hugo"]
tags: ["hugo"]
summary: "Story of the missing tags"
---

So one day I found all my tags from the tags page were missing. They still appeared in summaries and you could click on the individual links to get to a taxonomy term page which was fully populated with links to pages with the same terms.


I decided to add the following to the config file and it fixed it.

```toml
[taxonomies]
  tag = "tags"
  category = "categories"
```

But the mystery is why did they disappear in the first place. I don't think the site should require the above lines as these two taxonomies are generated by default.

Weird!
