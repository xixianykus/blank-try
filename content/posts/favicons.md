---
title: "Favicons"
date: 2022-05-04T09:28:39+01:00
draft: false
photoswipe: false
unfinished: true
categories: ["svg"]
tags: ["svg"]
summary: "How to create favicons"
---

Favicons really help distinguish and find a tab but they can be fiddly to make, especially from SVGs.

Fortunately there are a couple of services that make it quick and easy.

## Free Favicon maker

The free favicon maker by Formito allows you to quickly make letter based favicons. You can make a favicon using the full range of Google fonts, change the colour of the background and font, change the font size to get a custom font.

Once generated you can download as an SVG file, a PNG file or as a `<link>` code containing inline SVG code.


## Real Favicon Generator

Real Favicon Generator is better for more custom logos. 

First make a logo in a vector program like Inkscape or Illustrator and save it as an SVG.

Go to the [SVG Favicon Generator](https://realfavicongenerator.net/svg-favicon/) and upload your image. Two images are generated, `favicon.svg` and `favicon.png` which you download to somewhere in your project. Then just copy the `<link>` code for the `<head>` section of your HTML pages and alter the paths so they point to your images.