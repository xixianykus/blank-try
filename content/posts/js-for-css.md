---
title: "JS for CSS"
date: 2021-10-30T18:19:19+01:00
draft: false
unfinished: true
categories: ["css"]
tags: [Javascript]
summary: "Javascript for modifying CSS"
---

Here a quick way to toggle a class on and off of an element.

```html
<div class="content" onclick="this.classList.toggle('x')">
</div>
```

This will add the class of `x` to the element when it's clicked and remove it when it's clicked again.

```html
<div class="content x">
</div>
```

You can then add some CSS to the class of `x`:

```css
.x {
    color: red;
}
```

This toggle method can be used in a function. The below toggles the class of `change` on or off.

```js
function hamburger(x) {
    x.classList.toggle("change");
}
```

See [this example in action](https://js-blog.pages.dev/playground/hamburger/).

## Links to explore...

[Dev.to article](https://dev.to/karataev/set-css-styles-with-javascript-3nl5)
[My codepen](https://codepen.io/Synphod/pen/gOmGBZR?editors=0110) on changing theme colours with JS