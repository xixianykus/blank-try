---
title: "Patterns and Texture in Illustrator"
linktitle: "Illustrator: Patterns & Texture "
date: 2021-08-19T09:12:10+01:00
draft: false
photoswipe: false
categories: ["illustrator"]
tags: [illustrator, halftone, texture]
summary: "How to create Patterns in Illustrator"
---

## Pattern mode

You can create a pattern in either in:
- `Object > Make pattern` OR 
- in a document and then `Object > Pattern > Make Pattern`

1. choose a size for you pattern size
2. choose a layout, grid, brick, hex etc.


To Edit a pattern double click in the swatches palette.

The Move Tool (`v`)

Double click for option. If transform 


Teela tut  
{{< music/yt "ITRZ75OKrG0" >}}  

{{< music/yt "rANxCdJsTYU" >}}

1. Colour your artwork first.

2. Select it and choose Object > Pattern Make.


This makes the pattern. You can edit it using the Pattern Option panel and directly in the image box.



## Using Patterns

1. When you resize an object the pattern resizes too. To avoid this:

	A. Double click the move tool and make sure scale patterns is unchecked

	B.
	i) Just click the pattern in the swatches panel when the resize is done.
	ii) Use the scale tool (S) making sure 'Transform Patterns' in the options is unchecked. Double click it, input a percent value in uniform. Check preview.

2. To move the pattern within it's object hold down the ` key (below Esc) while dragging. NB The bounding box appears to move but when you let go it snaps back to where it was).

3. You can rotate patterns within objects using the Rotate tool (R). Just tick Transform Patterns within the tool options. Easiest method is to double click and input an angle.

## Symbols

Create symbols by dragging into the symbol palette.

Hold down Alt and drag over an existing symbol and the will change including all instances of it.


Dynamic symbols can be recoloured individually without changing all instances


## Halftone pattern large

This method can be used on gradients, meshes or blends (anywhere where a colour fades to another).

You first create a raster then convert it to vector.

1. Create a circle
2. Fill with b&w radial gradient (dark in the centre)
3. Effect > Pixelate > Color Halftone
4. Set the max size in pixels and all other settings to the same angle (eg. 45 degrees)
5. This creates a raster image so use Object > Expand Appearence . This will open up the Image Trace bar.
6. Use the default option just to make the options panel available. Click on this to open.
7. Uncheck the preview at the bottom then...
8. In the advanced set:
	- paths to 100%
	- corners to 100%
	- noise to the minimum (1)
9. Check preview and if OK hit Trace.
10. Hit the expand button on the Image Trace bar

You now have a group of objects that you can colour as you wish.

Don't use as a background as it's now a cliche.

![Halftone pattern](../photoswipe/Halftone-2.png)

## Add Shapes from font files



## Shading

1. duplicate object and paste in front. Use same colours but in a gradient in multiply mode

2. Add an inner glow through the appearance panel: stylize (hold shift to change blur in 10px increments)


Another method is to use the Knife tool

1. Select the object to shade
2. Draw a line through the shaded section
3. Select the new section and change the colour

Tips: Offset the shading slightly from object to object

## Hatching 1

Text is sometimes hatched with horizontal lines. Here's how

1. Type your text
2. Type two horizontal lines with the line tool below the text. One can have a thick stroke and one a thin one.
3. Blend the lines together with the blend tool. Double click the tool while selected to change the number of steps
4. Use the rectangle tool to choose the colour of your text and draw a rectangle bigger than the text below the lines
5. Select all and press Ctrl + 7 to make a clipping group.
6. Add two strokes one to make the stroke appear it doesn't reach the edge and one outer, normal one.


## Hatching 2

Method 1

1. Use the blend tool to create the hatching from 2 45 deg lines.

2. Use Expand on the hatching lines

3. Type out words, create outlines Ctrl + Shft + o and convert to a compound shape

4. Duplicate and pull out of the way.

5. Select word and hatching and create a clipping mask Ctrl + 7

6. Put the top word back in place then offset with the Arrow tool a few pixels down and left.


![Rainbow pattern with Live Paint](../photoswipe/Rainbow-Live-Paint-6.png)


## Gradient texture

Zimri

{{< music/yt "YFxFgMp2_jk" >}}

If you want a gradient texture on an object....

1. Dupicate and paste in front
2. Add a gradient using the tool button and adjust using the gradient tool (G)
3. Reduce the opacity on the white end of the gradient to zero.
4. Then use Effect > Texture > Grain
5. Choose a grain type, and adjust intensity and contrast
6. Reduce opacity to around 20%

You can still edit the gradient after