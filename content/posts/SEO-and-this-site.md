---
title: "SEO and This Site"
date: 2021-05-20T10:34:48+01:00
draft: false
categories: ["SEO"]
tags: [SEO, hugo]
summary: "How this site tested for SEO"
extLinks:
- ['https://www.goauditor.com/', Go Auditor, SEO test]
- ['https://web.dev/', Web Dev, Google site for devs]
- ['https://gohugo.io/templates/robots/', Go docs on robots.txt]
- ['https://www.freecodecamp.org/news/what-is-open-graph-and-how-can-i-use-it-for-my-website/', Open Graph protocol, by FreeCodeCampe]
---

- [Domain Authority](#domain-authority)
- [Page Authority](#page-authority)
- [Page Speed](#page-speed)
- [Robots.txt](#robotstxt)
  - [1 In the config file](#1-in-the-config-file)
  - [2 With a template](#2-with-a-template)
  - [3. In the static directory](#3-in-the-static-directory)
- [Open Graph Tags](#open-graph-tags)

So I [tested this site](https://www.goauditor.com/audit/35f60a62c73e6191) for it's SEO optimization just to see how it did. I used [Go Auditor](https://www.goauditor.com/), an online test and it did surprsingly well considering I haven't done anything SEO wise and don't even expect the site to be found.

## Domain Authority

For this I scored 92/100. What is it?

> Domain Authority (DA) represents the score for your entire website and is provided by Moz. Like Page Authority, this score can have a maximum value of 100, and factors in metrics such as the amount of links you have. DA is a measure of your overall domain strength.
>
> [Go Auditor](https://www.goauditor.com/)

Could it be better?

> Moz suggests that the best way to improve this score is to focus on your link profile. You will need to understand Google's Penguin algorithm and how it can affect your website. This most important thing about Penguin is to make sure your website's links are predominately brand keywords and NOT heavy on anchor text keywords.
>
> [Go Auditor](https://www.goauditor.com/)

So that was interesting. I do have a lot of outward links, but crucially for real world SEO zero incoming ones. 

> **Inbound lins** - The URL or domain you're auditing doesn't have any backlinks. Obtaining authority links with high relevance is essential to SEO and should be a top priority.
>
> [Go Auditor](https://www.goauditor.com/)

Ah well... Next...

## Page Authority

Only a mere 41 of 100 and a reason why is no page description meta tag.

> Page Authority (PA) is a metric, provided by Moz, that predicts how well your page will rank in Google's search results. The score is based on a 100 point scale and uses different variables such as the amount of links you have to calculate the score. For higher ranking pages, PA will typically be higher than your Domain Authority.
>
> [Go Auditor](https://www.goauditor.com/)

What needed apparently is:
> The meta description tag should provide a compelling description of your page where you have an opportunity to include keywords to increase CTR and ideal lengths are between 150-160 characters.
>
> [Go Auditor](https://www.goauditor.com/)

Since my pages already have summaries all I should need is to add these to a meta>description tag in the head. Easy.

```go-html-template
{{- with .Summary }}
  <meta name="description" content="{{ . }}">
{{ end -}}
```

Other meta tags I could add are *keywords* and *author* but I'm never too sure how useful these are but would add these to frontmatter variables if I was interested in SEO for this site.

I also got a warning about *keywords* not matching the page url. Whilst my h1 heading and url are the same I assume this means those words are not used in the content.

## Page Speed

The final two assessments were for page speed, one for desktop and one for mobile. I passed both of these scoring 98/100 and 89/100 respectively for these. 

Go Auditor says:

> This is not your page load time, but rather your Google PageSpeed. It factors in many optimization metrics such as caching of your website, how you treat your CSS and JS, and many other variables such as optimized images. The best possible score is 100.
>
> You will definitely want to check out [Web.dev](https://web.dev) to test and improve your score, as this is a critical factor in your on-page SEO efforts.
>
> [Go Auditor](https://www.goauditor.com/)

## Robots.txt

So I was warned about not having one of these. In Hugo there are several ways to achieve this:

1. Through the config file
2. with a template
3. In the static directory
   
### 1 In the config file

These are added using one line to your config file:

```toml
enableRobotsTXT = true
```
### 2 With a template

Alternatively you can overwrite the built in robots template with one of your own. Simply put it in `/layouts/robots.txt` or in the theme's `layouts` folder.

In here you could add some code:

```go-html-template
User-agent: *
{{ range .Pages }}
Disallow: {{ .RelPermalink }}
{{ end }}
```

Realistically you might want to add some frontmatter to each page and incorporate it into the above template.

### 3. In the static directory

> To create a robots.txt file without using a template:
> 
> 1. Set enableRobotsTXT to false in the site configuration.
> 2. Create a robots.txt file in the static directory.
>
> Hugo copies everything in the static directory to the root of publishDir (typically public) when you build your site.
>
> [Hugo docs](https://gohugo.io/templates/robots/#readout)

## Open Graph Tags

This was a big fat fail as I don't have any. These are tags in the head of each document to help social media sites link to your content.

[Good article](https://www.freecodecamp.org/news/what-is-open-graph-and-how-can-i-use-it-for-my-website/) on FreeCodeCamp.

>
> [Go Auditor](https://www.goauditor.com/)



