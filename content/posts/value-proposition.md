---
title: "Value Proposition"
date: 2022-12-13T18:13:48Z
draft: false
photoswipe: false
unfinished: true
categories: ["css"]
tags: ["hugo"]
summary: "What is a value proposition"
---


A value proposition is a statement that answers **WHY** someone should do business with you.

Not to be confused with:

- brand slogan
- catch phrases
- positioning statement


A value proposition should be compelling and meet these criteria:

1. **Specific** what are the benefits if someone uses your product or service
2. **Pain focused** how will this fix your customers problem or improve their life
3. **Exclusive** how does it set you apart from competition.


> It should tell people what you do, who you do it for and how you do it differently.


## Types of Value Proposition

1. **Company Value Proposition** 
2. **Home Page Value Proposition** big bold statement on web site. 
3. **Category Value Proposition**
4. **Product Value Proposition** each product needs it's own value proposition.
