---
title: "HTML Details"
date: 2021-05-15T07:11:15+01:00
draft: true
categories: ["hugo"]
tags: ["hugo", shortcodes, html]
summary: "Incorporating the HTML details tag into a Hugo shortcode"
---


So this is an experiment incorprating the details tag into a Hugo shortcode.

{{< details "test try" >}}
This is a test of the details tag in a Hugo shortcode.

You can use markdown formatting inside.

But you can use HTML, even when HTML has not been allowed in Markdown in the config file.

## An h2 markdown heading

{{< /details >}}

The code for this....

```go-html-template
<details>
    <summary>{{ .Get 0 }}</summary>
    <div>{{ .Inner | markdownify }}</div>
</details>
```

Using HTML tags appears to break the code

The `.Get 0` is the first parameter of the shortcode.

And `{{ .Inner }}` is the variable for anything between the opening and closing shortcode tags.

{{< details "The next question is about how we use add in Hugo. Can we use it to join strings reliably? What happens if one variable is a string and the next is a number or boolean?" >}}

The first answer is YES we can use `add` to join strings. However ....

{{< /details >}}

