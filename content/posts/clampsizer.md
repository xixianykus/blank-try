---
title: "Clampsizer"
date: 2021-05-03T10:28:38+01:00
draft: false
categories: ["Web dev"]
tags: ["css", typography]
summary: "Cool tool to size fonts with clamp"
extlinks: 
- ["https://piccalil.li/tutorial/fluid-typography-with-css-clamp/", Picalili, a simpler approach with clamp]
- ["https://utopia.fyi/", Utopia, has a similar calculator that produces a stylesheet with a range of values]
- ["https://css-tricks.com/linearly-scale-font-size-with-css-clamp-based-on-the-viewport/", CSS Trick, The same article on clamp and calulating the slope etc. but with bigger type.]
---

This tool allows you to use the css value `clamp` to get a smooth transition from your smallest font size to your largest.

See the [full article](http://design-lance.com/linearly-scale-font-size-with-css-clamp-based-on-the-viewport/) at design-lance.

**IMPORTANT**: slight aside but don't size fonts purely in viewport units. The reason is that these prevent users being able to zoom in on the type since the viewport width is fixed. So which ever method you use make sure that you mix other units with viewport units using a `calc` function like the following does.

{{< clampsizer >}}

