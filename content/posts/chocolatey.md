---
title: "Chocolatey"
date: 2022-03-02T12:41:44Z
draft: false
photoswipe: false
unfinished: true
categories: ["css"]
tags: ["Windows", Package Managers]
summary: "Using the Chocolatey package manager"
extlinks:
- [https://docs.chocolatey.org/en-us/, Official docs, at chocolatey.org]
- [https://community.chocolatey.org/packages, Search for a package, at chocolatey.org]
- [https://www.gitquest.gq/, Choco help file, on GitQuest.gq]
---

Chocolatey is a package[^1] manager for Windows. Most OS's have such a thing. Linux has *Apt-Get*, *yum*, *dnf* and others. Apple Mac's have *Brew*.

Chocolatey allows installation or updates of multiple programs at one time.


Installation is best done through Windows Powershell and run as an administrator. Without admin rights you can still install but see the official docs for how it's done.

## How to...

### Find a package

Programs/apps are kept in single files (basically zip files) called packages.

If you want to see if Chocolatey has a package you want use `choco search` followed by the name of the program you want.

```bash
choco search firefox
```

You don't have to know the exact name of the package. Chocolatey will generate a list of all relevant files that are a rough match of your search term. 

If you want a less fuzzy search you can also use the *exact* name filter `-e` or `--exact`.

```bash
choco search -e firefox
```

### What's installed on my machine?

If you only want to see what is installed by Chocolatey on your machine use `choco list` with the `-l` switch for *local*. 

NB. However from version 2 `choco list` will only search for local installs.

NB. The alias `clist` has been deprecated and won't work after version 2 of Chocolatey.


**choco info** gives information about a particular package

**choco outdated** will list any outdated packages

### Install a package

Use `choco install package-name` (NB. `cinst package-name` is deprecated.)


### Uninstall a package

`choco uninstall package-name` (NB. `cuninst` is deprecated.)

### Update all apps

`choco upgrade all -y`. The `-y` means you won't be prompted to agree to the licence agreements for each app. (NB. `cup` was teh shortcut alias but that is deprecated and won't be used from version 2.)

## How to install a specific version

To install an app of a specific version use the `--version` flag.

```bash
choco upgrade hugo-extended --version 0.122.0
```

Chocolatey won't let you install an older version of an app by default. However this can be done using the `--allow-downgrade` flag or `--force` flag.

See the [official docs](https://docs.chocolatey.org/en-us/choco/commands/upgrade) for more.

## Other package managers

For Windows there is also *winget* which is Microsoft's own package manager which get packages from the MS Store. This is built in to Windows now

There is also [Scoop](https://github.com/ScoopInstaller/Scoop/wiki) which works differently and is more more dev focused: specializes in open source and CLI tools.

[^1]: Programs/apps are kept in single files (basically zip files) called packages.
