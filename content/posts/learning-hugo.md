---
title: "Learning Hugo"
date: 2019-10-30T07:14:10Z
draft: true
categories: ["Web dev"]
tags: ["hugo"]
summary: "Some thoughts on learning Hugo and a bit more unquoted string text just here."
---


It's definitely worth looking inside existing themes to see how people have put together pages.

If there is more here would I get

A 'read more' link at the bootom

of the page

## Links

1. [Official site](gohugo.io)
2. [Mike Dane’s Youtube series](https://www.youtube.com/playlist?list=PLLAZ4kZ9dFpOnyRlyS-liKL5ReHDcj4G3)
3. [Regis Philibert](https://regisphilibert.com/blog/2019/01/from-wordpress-to-hugo-a-mindset-transition/) best blog on Hugo?