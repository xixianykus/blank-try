---
title: "SVG Courses"
date: 2021-06-01T12:24:18+01:00
draft: false
photoswipe: false
categories: ["svg"]
tags: ["svg"]
summary: "A list of resources to learn about SVG"
---


1. [W3 Schools](https://www.w3schools.com/graphics/svg_intro.asp) has a full course along with the try out feature to practic with. They say you need a basic understanding of HTML and [XML](https://www.w3schools.com/xml/default.asp) first.
2. [SVG Ultimate Course](https://geeksgod.com/udemy-free-course/svg-ultimate-course/) a free Udemy course coupon.
3. [SVG Basics for Beginners](https://www.udemy.com/course/svg-basics-for-beginners-concepts-explained-with-examples/) Another free Udemy course (1hr 27m)
4. [Use SVG in CSS and HTML](https://www.freecodecamp.org/news/use-svg-images-in-css-html/) - one page on FreeCodeCamp. See [many more good articles there](https://www.freecodecamp.org/news/search/?query=svg).
5. [CSS Tricks Video Course](https://css-tricks.com/lodge/svg/). Very comprehensive looking, 40 vids by Chris Coyer from 2014.
6. [Tutorialspoint](https://www.tutorialspoint.com/svg/index.htm) have a very thorough and terse course.
7. [MDN](https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial) also have a free, 17 page tutorial.
8. [Flavio Copes In Depth SVG Tutorial](https://flaviocopes.com/svg/)
9. [Javapoint SVG Tutorial](https://www.javatpoint.com/svg-tutorial) - is, in fact, a long list of SVG tutorials with online editor. Terse in places but a full guide.
10. [Full Self Teach Me 2021 course](https://www.youtube.com/playlist?list=PLrz61zkUHJJHFhsK3BKi-G5FjBOsO-aOY "on Youtube") by Amy Dutton

## Shorter articles

1. [SVG Viewport and Viewbox](https://webdesign.tutsplus.com/tutorials/svg-viewport-and-viewbox-for-beginners--cms-30844) by Tutsplus
2. [CSS with SVG](https://www.sitepoint.com/css-with-svg/) a SitePoint article
3. [Cassie Codes](https://www.cassie.codes/) is the website of Cassie Evans who has lots on SVG