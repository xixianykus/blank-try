---
title: "Learn SVG"
date: 2021-06-01T10:26:17+01:00
draft: false
photoswipe: false
categories: ["svg"] 
tags: ["svg"]
summary: "Learn about using and creating SVG's"
extLinks:
- ["https://www.youtube.com/playlist?list=PLrz61zkUHJJHFhsK3BKi-G5FjBOsO-aOY", Self Teach Me youtube course, up to date thorough SVG course]
---

This is a sub-section of the posts directory. I wanted that reflected in the main menu as a dropdown of posts.
