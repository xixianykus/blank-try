---
title: "SVG Basics"
date: 2021-06-01T08:15:50+01:00
draft: false
photoswipe: false
categories: ["svg"]
tags: ["svg"]
summary: "Basic SVG coding. How to set up sprites and use SVG in day to day use."
css:
- ".search-icon {fill: red; width: 40px; outline: solid 1px pink;}"
- ".rectangle {fill: blue;}"
extLinks:
- ["https://www.youtube.com/playlist?list=PLrz61zkUHJJHFhsK3BKi-G5FjBOsO-aOY", Self Teach Me youtube course, An up to date thorough SVG course]
- [https://www.sitepoint.com/css-with-svg/, Sitepoint on CSS with SVG, Good intro to the basics]
- [https://css-tricks.com/svg-sprites-use-better-icon-fonts/, CSS Tricks on SVG Sprites, Old but worth reading]
- [https://www.creativebloq.com/features/the-complete-guide-to-svg/6, Creative Bloq on Sprites, a 2018 article and part 6 of an SVG series.]
- [https://24ways.org/2014/an-overview-of-svg-sprite-creation-techniques/, SVG sprite creation techniques, An old but very well written article on SVG sprites]
---

{{< svg new-sprite "rectangle" >}}

{{< svg "sprite" "magnifying-glass" >}}

So some basic SVG coding...

{{< html class="svg__example" >}}

<svg>
    <rect fill="green" height="140" width="300" rx="23" />
</svg>

{{< /html >}}

```svg
<svg>
    <rect fill="green" height="140" width="300" rx="23" />
</svg>
```

The sizes here are very important. That's because there is no `viewbox` declared and when that's the case default viewbox values are used which are 300px x 150px. A `<rect>` object can also take `x` and `y` attributes which set the position of the top left corner from the top left corner of the canvas.

```svg
<rect x=20 y="90" width="200" height="70" />
```

{{< html class="svg__example" >}}
      <svg width="165" height="165">
         <circle cx="80" cy="80" r="75" stroke="red" stroke-width="2" fill="cadetblue" />
      </svg>
{{< /html >}}


## Usage

SVG's can be used in several different ways:

1. The code can be stored in a file, eg. *my-image.svg*, then used with an image tag: `<img src="my-image.svg" alt"some text about the image">` or:
```css
   .myelement {
  background-image: url("mybackground.svg");
}
```
2. They can be used inline. For instance for a CSS background:
```css
.mysvgbackground {
  background: url('data:image/svg+xml;utf8,<svg xmlns="https://www.w3.org/2000/svg" viewBox="0 0 800 600"><circle cx="400" cy="300" r="50" stroke-width="5" stroke="#f00" fill="#ff0" /></svg>') center center no-repeat;
}
```

or directly in the HTML:

```svg
<svg width="155" height="155">
   <circle cx="75" cy="75" r="75" stroke="red" stroke-width="2" fill="cadetblue" />
</svg>
```

## SVG sprites

However a common and great way to use SVG's, particularly with sets of icons is to create a sprite that has all of them in one file. This file is kept somewhere, say where your images are kept and you can link to each image within the sprite separately. Once one image is called the sprite file is cached by the browser making all the other SVG's within it instantly accessible.

Creating sprites can be done manually (slow), using an app or when collecting svg's from *some* icon web sites.

For more on SVG sprites see [Sara Soueidan's article](https://24ways.org/2014/an-overview-of-svg-sprite-creation-techniques/ "from 2014 but very well written and still relevant").

She describes 3 different sprite methods:

1. All sprites kept as `<symbols>` in one SVG sprite. This is either stored on the page or as a separate svg file and accessed using the SVG `<use>` tag.
2. Sprites are stored in a CSS file and accessed direcly within the CSS. as background images. This method has major drawbacks: you cannot style the SVG's, animations are restricted to those 'defined inside the SVG` and hover effects don't work.
3. Create the icons together on a canvas and use the `viewbox` to choose the image needed in a given situation. This technique most closely resembles that of the old way of using PNG and GIF sprites. It's bit more advanced though. You can use `view` to define `viewbox` 'view' by name: `<view id='instagram-icon' viewBox='64 0 32 32' />`. this can then be used in an image tag: 
   
   ```html
   <img src='sprite.svg#instagram-icon' alt="Instagram icon" />
   ```

### Syntax

To create a sprite just add the `svg` code of each image to a file and change the `<svg>` tags to `<symbol>` tags. Each `<symbol>` needs a unique `id` too so it can be referenced when called.

Next wrap all these images in one `<svg>` tag. Make sure the opening tag contains the XML links:

```svg
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">

  <symbol id="youtube" viewBox="0 0 24 24">
      <path d="M19.615 3.184c-3.604-.246-11.631-.245-15.23 0-3.897.266-4.356 2.62-4.385 8.816.029 6.185.484 8.549 4.385 8.816 3.6.245 11.626.246 15.23 0 3.897-.266 4.356-2.62 4.385-8.816-.029-6.185-.484-8.549-4.385-8.816zm-10.615 12.816v-8l8 3.993-8 4.007z"/>
  </symbol>

  <symbol>
    ... etc.
  </symbol>

</svg>
```

You could wrap all the `<symbol>` elements in a pair of `<defs>` tags too. The definition tags mean the content isn't rendered until called later using the `<use>` elements. However `<symbol>` elements aren't rendered either since this whole SVG is one collection of icons it will probably never be rendered on a page as it is. And if it was since none of the elements have position values they'd all end up on top of one another.


To use one of the images (now symbols) in an HTML page you create an SVG and use the `<use>` tag to reference the image with a hyperlink:

```svg
<svg class="icon icon--red" width="24" height="24" viewBox="0 0 24 24">
  <use href="social.svg#facebook"></use>
</svg>
```

Using `class` attributes is optional but very helpful to target the SVG's from with your CSS.


An up to date and thorough guide to SVG sprites by Amy

{{< music/yt "LgfLpEHqgGU" >}}

## Using CSS with SVGs

You can override the SVG code using CSS because the CSS has a higher specificity than the inlined SVG values. 