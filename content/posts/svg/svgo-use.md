---
title: "SVGO Use"
date: 2021-09-12T16:53:18+01:00
draft: false
photoswipe: false
categories: [svg]
tags: [SVG]
summary: "How to use SVGO"
---


## Official Help
This is from the help file (`svgo --help`)

Usage: svgo [options] [INPUT...]

Nodejs-based tool for optimizing SVG vector graphics files

Arguments:
  INPUT                      Alias to --input

## Options:

| flag added to svgo | function |
|:---------------------------|:----------------------------|
| -v, --version              | output the version number |
| -i, --input <INPUT...>     | Input files, "-" for STDIN |
| -s, --string <STRING>      | Input SVG data string |
| -f, --folder <FOLDER>      | Input folder, optimize and rewrite all *.svg files |
| -o, --output <OUTPUT...>   | Output file or folder (by default the same as the input), "-" for STDOUT |
| -p, --precision <INTEGER>  | Set number of digits in the fractional part, overrides plugins params |
| --config <CONFIG>          | Custom config file, only .js is supported |
| --datauri <FORMAT>         | Output as Data URI string (base64), URI encoded (enc) or unencoded (unenc) |
| --multipass                | Pass over SVGs multiple times to ensure all optimizations are applied |
| --pretty                   | Make SVG pretty printed |
| --indent <INTEGER>         | Indent number when pretty printing SVGs |
| -r, --recursive            | Use with '--folder'. Optimizes *.svg files in folders recursively. |
| --exclude <PATTERN...>     | Use with '--folder'. Exclude files matching regular expression pattern. |
| -q, --quiet                | Only output error messages, not regular status messages |
| --show-plugins             | Show available plugins and exit |
| -h, --help                 | display help for command |