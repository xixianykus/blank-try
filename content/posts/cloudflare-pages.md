---
title: "Cloudflare Pages"
date: 2023-11-01T08:50:03Z
draft: false
unfinished: true
categories: ["Web dev", dev, CDN]
tags: [hosting, Cloudflare, Netlify]
summary: "A short review of Cloudflare's service for static sites and a comparison with Netlify."
---

In the world of hosting for statics sites, particularly those that use a static site generator, three big names come to mind: Netlify, Vercel and Cloudflare. Like many I've been a long time fan of Netlify. I like them because they innovated such hosting services, they have great documentation and it's very easy to set up a CMS I use, Decap CMS (formally Netlify CMS) using Netlify Identity.

However recently when trying to set up a new account on Netlify for a client I found I was locked out. Apparently I was flagged up as *fraudulent* and couldn't use the service. I did try to appeal this but with no luck. And I have no idea what I did that set off the Netlify bots that prevented me from opening a new account. This was all very disempowering and does not feel good and I was forced to use an alternative and so I chose Cloudflare. The sign up was straightforward and I was not seen as some kind of risk.

## Cloudflare vs Netlify

So here is a quick comparison of using Cloudflare and Netlify.

First some basic differences. Cloudflare is a much bigger company than Netlify and has many more servers from which to serve your site as part of their CDN service. This means potentially faster load times because the server can be nearer to a potential user. In terms of the free tier where Netlify allows a generous monthly bandwidth allowance of 100Gb Cloudflare's has no limit whatsoever. For me that was not such a big issue as the project I was setting up was unlikely to get anywhere near Netlify's allowance. Perhaps more significant is the amount of build time you get. This is the total amount of time your static site generator runs on their servers each month. Netlify allows 300 minutes whilst Cloudflare's is less. For me this is more significant though still not that important as I use the Hugo static site generator which is perhaps the fastest of all SSG's. However for larger sites this time limit can add up and if using a slower SSG like say Jekyll on a large site the amount of build time could become an issue.

Setting up with Cloudflare was pretty easy, comparable with Netlify. One thing is once you've signed up don't hit the *websites* menu item. You need to head to *Pages* further down the menu. Follow the instructions to link your static site's GitHub or GitLab repo and you can quickly get a new site online. A couple of points here. First the Cloudflare default is to choose all repos in your Git account. This was not what I wanted so I had to choose the specific repo I wanted to use. Secondly the free url it selects for you choose a subdomain based upon the name of your project in your GitHub/GitLab account.


## Custom domains

If you're using your own domain setting this up I found a little confusing on Cloudflare, compared with Netlify. I moved my Nameservers (NS records to Cloudflare) as I thought this might make things easier and they provide a very fast service. That was fine, mainly just adding Cloudflare's two NS addresses in the fields on my domain registrars NS section. However setting up the domain of the site from here proved more tricky. Firstly you cannot set an `A` record for your route domain when using Cloudflare Pages it seems. Instead you create a `CNAME` record which points to the the Cloudflare Pages domain. You then have to set up something called bulk redirects too for both this and a www subdomain.


## Dcap CMS

### Links

[A Step by Step Guide to Self-hosting Decap CMS](https://www.njfamirm.ir/en/blog/self-hosting-decap-cms/)