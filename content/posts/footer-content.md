---
title: "Footer Content"
date: 2021-09-05T09:47:55+01:00
draft: false
photoswipe: false
categories: [layout]
tags: [layout]
summary: "What content can go in the footer"
extlinks:
- ["https://fireart.studio/blog/10-best-examples-of-website-footer-designs/", FireArt Studio's list, ]
---

What kind of info can be stored in the footer?

These should be grouped in a logical way.

1. Contact details
   1. address
   2. phone
   3. email
2.  contact form
3. Social media links (these are not *share* links)
   1. Facebook
   2. Linked In
   3. Twitter
   4. Google+
   5. Tumblr
   6. Pinterest
   7. [GitHub](https://github.com/)
   8. [GitLab](https://gitlab.com/)
   9. BitBucket
   10. [Codepen](https://www.codepen.io/)
   11. Dev.to
   12. [Hashnode](https://hashnode.com/)
   13. Twitch.tv
   14. [Mastodon](https://mastodon.social/)
   15. [Micro blog](https://micro.blog/)
   16. Discord
4. Other owned sites like a blog
5. RSS feed
6. Sign up link (to email list?)
7. Sitemap link
8. copyright info
9. Terms of serice
10. Terms of use - general rules and guidelines on using a website or your products. 
11. Privacy policy: how a company will use and protect their sensitive data and other information.
12. A menu of the website's main sections/pages
13. a search box
14. link to the top of the page
15. login link

## FireArt best practices:

1. Try to create a great visual hierarchy,
2. Be attentive to the white spaces, and
3. Separate your footer from the main content.
