---
title: "VS Code Shortcuts"
date: 2021-10-27T22:53:21+01:00
draft: false
photoswipe: false
categories: ["css"]
tags: ["VS Code"]
summary: "A few shortcuts I don't use, until now."
---

There are lots of shortcuts in VS Code.

These are some I should use more than I do.

`Ctrl + D` press after selecting something and this will select the next instance along with another cursor. Can be pressed repeatedly.

`Ctrl + Shft + L` selects all instances of the current selection. This is the same as `Ctrl + F` followed by `Alt + Enter`.


`Ctrl + L` selects the whole line

`Ctrl + Shft + K` to delete a line

`Ctrl + ,` to access the settings

`Ctrl + T` find by symbol. Search your project for CSS class or ID name or a JavaScript function name or variable.

`f5` debugs the file

`Ctrl + Shft + [` will fold the code block

`Ctrl + Shft + ]` will unfold the code block

More shortcuts: `Ctrl + K, Ctrl + S`

`Ctr + Shft + F` - open the search sidebar to **F**ind/search in all project files.


## Conflicting system wide shortcuts

Sometimes a system wide shortcut will override those in VS Code. For example for me I couldn't use the `Ctrl + Shft + L` shortcut because it started performance logging for my AMD graphics card software. Often you can go to the offending app and turn off or change the keybinding. However in this case it wasn't possible and could only be done by going into the Windows registry editor and changing it there.

`KEY_LOCAL_MACHINE\SOFTWARE\AMD\DVR`

Set `ToggleRsPerfRecordingHotkey` to  `none`.