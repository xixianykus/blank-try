---
title: "Exiftool and Image Metadata"
date: 2021-08-07T06:47:44+01:00
draft: false
photoswipe: false
categories: ["images"]
tags: ["hugo", metadata]
summary: "My notes on using EXIFtool, a small app for editing metadata"
extlinks:
- [https://www.exiftool.org/, Exif Tool, Official page with docs and download]
- ["https://adamtheautomator.com/exiftool/", Everything You Ever Wanted to Know about ExifTool, an in depth article]
---

## Usage

- read through the meta data of an image: `exiftool filename.png`
- to add `ImageDescription` to an image: `exiftool -ImageDescription="whatever you want goes here" filename.png`




Exiftool is a small command line program for editing meta data. For me this was useful for adding and editing metadata in images that could be accessed by Hugo (static site generator) when creating image galleries. When image galleries are large and you generate them with Hugo you need somewhere to be able to pull data relevent for each image from. There are no doubt several options. 

One obvious place is the file name. You can access this using `{{ .Name }}` but to get a nice caption you might need to replace hyphens with spaces and get rid of the filename extension (eg. .jpg). Well you can do all that with Hugo functions like `replace` and `[substring](https://learnhugo.ml/posts/substring/)` but it would easier if you didn't have to.

Hugo can pull EXIF data from images but not, according to the specs at least, IPTC data. EXIF data is what comes from the camera giving info about time and date of an image, the shutter speed, aperture etc. You can also store copyright details in the camera which can also be stored in EXIF. The IPTC data is more from a person used for subjective info like the title, a caption and key words.

In fact some of this IPTC data can be pulled out by Hugo if you know where.

First to access the EXIF data you use `{{ .Exif }}` from there we need the tags and then a specific tag.

Here's how we could pull out the description:

```go-html-template
{{ with .Exif }}
  {{ .Tags.ImageDescription }}
{{ end }}
```

