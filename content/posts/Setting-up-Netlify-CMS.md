---
title: "Setting Up Decap CMS"
date: 2021-10-17T19:18:37+01:00
draft: false
unfinished: true
categories: ["css"]
tags: ["netlify", CMS, github, hugo, Decap]
summary: "How to set up Decap CMS with a Hugo site"
toc: true
extlinks:
- [https://www.netlifycms.org/docs/add-to-your-site/, Netlify CMS setup page, A step by step guide from the official docs.]
- [https://www.netlifycms.org/docs/hugo/#getting-started-with-netlify-cms, Quick cut and paste Hugo setup, Another part of the official docs]
---


## How it works

[Netlify CMS](https://www.netlifycms.org/) is runs as a single page web app on your site that can access and edit the content sections of the site's Git repo. Because this part of the site does not need to be processed by Hugo these files are kept in the `static` folder. A folder at `/static/admin` can then be accessed at `www.example.com/admin` on your site. The simple CMS page, `index.html` is actually a React app that uses React code from a CDN or kept locally.

## What you need

You need an unbuilt static site on GitHub, GitLab or BitBucket that is built and hosted by Netlify. These can all be used for free.

## Demo version

If you want to get an idea of whta the CMS is like there is online demo version that doesn't even require a login.

[Netlify CMS demo](https://cms-demo.netlify.com/)

## Setup in brief

Here are the main steps to setup the CMS when hosted on Netlify:

### On your site

1. Add a folder called `admin` to your project's `static` folder and add two files: an `index.html` file and `config.yml` (NOT `config.yaml`).
2. Add two `<script>` tags to this `index.html` file. The first, for a netlify identity widget, is added to the `<head>` section. The second, the react script for the CMS, is added to the `<body>` of your page. The netlify one is:
   
```html
    <script src="https://identity.netlify.com/v1/netlify-identity-widget.js"></script>
```
   For the CMS you can use either of the following:
```html
<script src="https://unpkg.com/netlify-cms@^2.0.0/dist/netlify-cms.js">
<script src="https://cdn.jsdelivr.net/npm/netlify-cms@^2.0.0/dist/netlify-cms.js">
```
3. Add the script tag for the Netlify Identity widget (above) to the `<head>` of your site's main `index.html` page. (This can also be done using Netlify's [script injection feature](https://www.netlify.com/docs/inject-analytics-snippets/))
4. Add another short script just before the closing `<body>` tag of your site's main `index.html` page:

```js
<script>
  if (window.netlifyIdentity) {
    window.netlifyIdentity.on("init", user => {
      if (!user) {
        window.netlifyIdentity.on("login", () => {
          document.location.href = "/admin/";
        });
      }
    });
  }
</script>
```

### On Netlify

1. Enable Git Gateway. From the Netlify interface for your website go to `site configuration` (the second menu item). From here click on `identity` in the vertical menu then press the `Enable Identity` button. 
2. Under registration press `configure` and choose whether you want `Open` or `Invite only`.
4. Scroll down to services and click on `enable git gateway`.
5. Next click on `integrations` in the main menu then on `identity`. invite at least one user to be able to login into the CMS. The email provided will get a login link which will take you to the CMS where you'll be prompted for a password of choosing.
6. Setup the `admin/config.yml` file. See the [Netlify CMS config](/posts/netlify-cms-config) article for how to do this.

---

## More detailed info

On the hosting side you need to set up authentication so that the CMS can access and make commits to your repo.

Although Netlify CMS is an open source project it is, at this time, easier to set up with a Netlify hosting account and is slightly better for some features if kept on GitHub.

The Netlify account makes it simple to set up authentication from GitHub. You can find this in the Netlify interface under the `Site Settings > Identity > Services` section of the specific site (see below).

### One click install

You can use a template specific to your static site generator and install with just a few clicks from [here](https://www.netlifycms.org/docs/start-with-a-template/).

However setting up with a Hugo site that is already built and hosted by Netlify is fairly easy.

### In your site's project folders

You need to:
1. Add an `admin` folder to the `/static/` folder of your site.
2. Add two files to this, and `index.html` file and a `config.yml` file.

If the `index.html` file has a link the Netlify CMS script at a CDN you don't need to do anything else.

If you don't want to use a CDN for Netlify CMS it's also possible to install it using NPM.

## From Netlify

You can generate the access token from within the Netlify interface. From the site's settings (far right end of the menu) click on `Idenity` then `Services` to find the `Git Gateway` section. Click on the `enable Git Gatewy` button. Very easy and that's it done!

### From GitHub

But you can also generate a new token from GitHub. Go to your GitHub account settings in the top right dropdown. From there choose *Developer settings* near the bottom of the list. Here you can choose *Personal access tokens* and press the *Generate new token* button. Copy this immediately as you cannot reaccess it again. You then need to paste it into your Netlify project settings.

To generate a PAT (personal access token) from GitHub click on your profile pic (top right) and choose settings. These are different from the settings for each repo. From there scroll down a click on the the *Developer settings* button near the bottom.

![GitHub developer settings button](/images/github-dev-settings.png)

From here you can choose Personal Access Tokens and click generate a new token.

![GitHub Personal Access Tokens section](/images/github-pat-section.png)

Add a note and set the expiration date and click the sections on the form.

### Expiration of access tokens

From GitHub:

> As a security precaution, GitHub automatically removes personal access tokens that haven't been used in a year. To provide additional security, we highly recommend adding an expiration to your personal access tokens.

More on [Netlify Identity](https://docs.netlify.com/visitor-access/identity/#enable-identity-in-the-ui) in Netlify docs

[Creating a personal ccess token](https://docs.github.com/en/authentication/keeping-your-account-and-data-secure/creating-a-personal-access-token) on GitHub

## Adding Hugo shortcode menu to the CMS UI

There's another script that adds a menu dropdown to the rich text options of the CMS UI.

```html
<script src="https://sharadcodes.github.io/hugo-shortcodes-netlify-cms/dist/hugo_shortcodes_netlify_cms.js"></script>
```

If you add this to the `static/admin/index.html` file BELOW the CMS script in the `<body>` section of the page.

Then in the UI for editing a page you get this selection:

![Hugo shortcodes dropdown menu](/img/netlify-cms-hugo-shortcodes.jpg)

The link above is a Javascript file made up of different blocks for each shortcode and uses the `CMS.registerEditorComponent` for each block. Here is the code for Hugo's built in `<figure>` element shortcode:

```js
CMS.registerEditorComponent({
    id: "figure",
    label: "Figure",
    fields: [{
            name: "title",
            label: "Figure Title",
            widget: "string"
        },
        {
            name: "src",
            label: "Figure SRC",
            widget: "string"
        },
    ],
    pattern: /{{< figure src="([a-zA-Z0-9-_ ]+)" title="([a-zA-Z0-9-_ ]+)" >}}/,
    fromBlock: function(match) {
        return {
            title: match[1],
            src: match[2],
        };
    },
    toBlock: function(obj) {
        return `{{< figure src="${obj.src}" title="${obj.title}" >}}`;
    },
    toPreview: function(obj) {
        return `<figure><img src=${obj.src} alt=${obj.title}><figcaption>${obj.title}</figcaption></figure>`;
    },
});
```

See the [GitHub page](https://github.com/sharadcodes/hugo-shortcodes-netlify-cms) for this code.


## GitHub GraphQL API

One of the new beta features that can be used is the new [GitHub GraphQL API](https://www.netlifycms.org/docs/beta-features/#github-graphql-api).


See also: [Netlify CMS Configuration](/posts/netlify-cms-config/)