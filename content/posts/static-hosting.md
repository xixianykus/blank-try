---
title: "Static Hosting"
date: 2023-04-08T17:44:53+01:00
draft: false
photoswipe: false
unfinished: true
categories: ["dev"]
tags: ["hosting"]
summary: "Overview of static hosting providers"
---

For simple static hosting Netlify are hard to beat Cloudflare. But if you want to go beyond simple static hosting you can either go for a good all in one service like Render or Netlify or Cloudflare or get a second service for say database hosting...

{{< data static_hosting >}}