---
title: "Things I Cant Do in Hugo"
date: 2021-05-28T04:46:37+01:00
draft: true
photoswipe: false
categories: ["hugo"]
tags: ["hugo"]
summary: "A list of things I've not been able to do in Hugo yet"
---


## List of same tagged pages

On a page with tags I want to list other pages with the same tags.

Or should I use related content instead?

Tried this on CSSGeek.


## List of pages that are drafts

On the drafts page I want a list of pages that are drafts, ie `draft: true`, with their links.



