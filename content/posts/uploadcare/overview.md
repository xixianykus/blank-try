---
title: "Uploadcare overview"
date: 2021-12-08T10:19:43Z
draft: false
photoswipe: false
unfinished: true
categories: ["css"]
tags: ["hugo"]
summary: "The different ways of using Uploadcare"
extlinks:
- [https://uploadcare.com/docs/delivery/, Uploadcare docs, Section on delivery types]
---

So there are 3 main ways to use [Uploadcare]:

### 1. Storing files on their server

This method you can upload your files to them, get a link to the image, and add it to your web page. The link can contain additional information telling Uploadcare to process the image a certain way, like size, crop, format, colour etc..

### 2. By Proxy

You can store your images anywhere on the web where you can access them with an `http` link. This typically might be on your website, in the `image` folder for instance. Or could be anywhere else, Dropbox, AWS etc..

Using this method you use a combined link which tells [Uploadcare] where your file is. The link can also contain instructions to transform the image to a specific size, format, crop etc.. It will then pull that file back to it's servers then serve the modified version of it to the device that is requesting it.


### 3. Adaptive Delivery

This is similar to above but a small JS file added to the `<head>` of the page which is used to to tell Uploadcare how to process the image.

The `src` attribute in image tags is replaced with `data-blink-src`

> Adaptive Delivery is a JavaScript tool that requests correctly sized and compressed images from Uploadcare CDN based on page layout and the visitor’s screen.







[Uploadcare]: https://www.uploadcare.com/