---
title: "Uploadcare"
date: 2021-12-05T21:04:22Z
draft: true
unfinished: true
categories: ["css"]
tags: ["hugo", images]
summary: "This section is about using Uploadcare "
---

[Uploadcare](https://uploadcare.com/) is one of many image hosting CDNs. I like this one for several reasons:

1. It has a generous free tier
2. Easy to use
3. Good documentation
4. Works with Netlify CMS (the best git based, open source, headless CMS).

## Why use an image hosting CDN?

If you're using a static site generator like Hugo and using Netlify or Cloudflare for free hosting of a static site you might be wondering why would you then use a separate image hosting CDN? Netlify has a huge bandwidth limit of 100gb per month and Cloudflare's is unlimited. One of the many benefits of Jamstack sites.

Uploadcare is one of many image hosting CDN's. The bandwidth is well down Netlify and Cloudflare at 30gb for the free tier and considering that images usually make up the the bulk of one's bandwidth this might not at first seem like such a great idea.

Image CDN's offer services that regular CDN's do not. That is image processing. Now, if you're like me, and familiar with image editing tools like Photoshop this might not seem such an attractive idea. Getting great images will usually always be better done by a skilled human vs a computer algorithm too. But one thing algorithms can do reasonably well is resizing images. And in a world where screen sizes can vary from small mobile phones to giant 4k monitors getting the perfect size for every screen can be a huge pain. 

The conventional way to deal with this huge variation in screen sizes is to use the HTML `<picture>` element along with the `srcset` attribute to serve up different sized images for different screen sizes. Aside from different screen sizes you may also want to use different image formats. Google's `webp` produces smaller files than `jpeg` but not all browsers (notably Safari) can render `webp` so you need both if you want to offer optimum performance.

Now all this is starting to add up to a lot of work for just one image. With Hugo's built in image processor you can automate much of the work but you will end up with a lot of images stored in your git repo.

## Keeping images out Git repos

Another good move is to keep images out of git repos. Why? Because git is designed to track changes in text files which it does exceedlingly well. But when it comes to changed images it can't track those the way it does with a text file. Instead if you change an image it just keeps every changed version of the whole image. Your git repository can become bloated with files that git was never really meant to deal with.

Using an image CDN is one way of avoiding this but it's not the only way. Atlassian produced Git LFS, software that keeps images out of the main repo. Instead the repo just uses tiny pointer code to p