---
title: "GDPR"
date: 2023-12-30T05:39:05Z
draft: false
photoswipe: false
unfinished: true
categories: [privacyc]
tags: ["GDPR"]
summary: "An overview of the GDPR rules and what you need to comply"
---

The GDPR or General Data Protection Regulation is an EU law that came into effect in 2018.

It's often associated with Cookie banner notices however that is wrong. Cookie banner requirements come from an earlier, 2002, EU law: the ePrivacy directive.

## The Privacy Notice

The privacy notice much contain the 7 following things:

1. What personal data do you collect.
2. Why do you collect this data and what you are going to use the data for? There are 6 legitmate uses.
3. Who do you share personal data with? You don't have to provide names of companies but you do need to provide categories of third party services you use.
4. How is personal data protected?
5. Info on how an individual may exercise their rights: how to they get their data removed
6. Who or where should individuals contact with questions. Also tell site visitors they have the right to complain.
7. When was this privacy notice last changed?


## Cookie consent for Hugo

There is all the code and instructions for adding a cookie banner to Hugo websites at Hugo codex.


## Testing cookie banners

When testing cookie banners you may wish to edit preferences in your browser. In Firefox open the dev tools and click on the **Storage** section. Under *cookies* choose the domain you are using then right click a cookie entry to get options on 