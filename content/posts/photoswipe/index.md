---
title: "Photoswipe Image Gallery"
linktitle: Photoswipe
date: 2021-05-21T16:06:15+01:00
draft: false
categories: ["hugo", JS Library]
tags: ["hugo", leaf bundle, lightbox, image gallery, illustrator]
summary: "This is a page for experimenting with image galleries"
# layout: image-gallery
photoswipe: true
extLinks:
- ['https://planetozh.com/projects/lightbox-clones/', Full list of JS Lightboxes, ]
- ['https://photoswipe.com/documentation/getting-started.html', Photoswipe, Getting started page]
---

This is a gallery made with [Photoswipe JS](https://photoswipe.com/documentation/getting-started.html) and Hugo's page resources. All the images are in a leaf bundle and accessed using 

```go-html-template
{{ range .Page.Resources.ByType "image" }}
```

This is used in a shortcode (`ps_thumbs.html`).

I need to get the button working to activate the gallery or even better via clicking the images.

NB. Version 5 of Photoswipe is now in beta. Would be good to switch to that one. (Think you need to fork the project and switch to a different branch).

## Some vector images

{{< ps_thumbs >}}

## To Do

1. Make images load from thumbnails
2. Stop loading on start.
3. Use image processing on thumbnails to shrink actual size.
4. Can you use different aspect-ratio images when using the range function?
5. Try other themes
6. Animation ?
7. Try the Photoswipe options
8. Upgrade to v.5 beta

Could download files and concatenate but probably not worth bothering with for this site.

## Photoswipe: the good and the bad

What I like:

1. Full screen option
2. different skins
3. Navigate via the keyboard
4. It's free
5. Responsive, works great for mobile.
6. Reasonable documentation. Could be better but it's not bad at all.


What I don't like:

1. You have to put height and width values in for each image which means you can't access with a `range function`. See [Responsive Images](https://photoswipe.com/documentation/responsive-images.html).
2. The images don't stretch to the full height or width of the screen (Maybe this could be changed in CSS?).


## How's it done?

The idea behind this is for a very easy to create gallery on any page you want. To add gallery to any single page (leaf bundle) you simply: add your images to the bundle (folder) of your page, turn on Photoswipe in your frontmatter: `photoswipe: on`, add a shortcode for a thumbnail gallery 

So what was required for the Photoswipe gallery?

1. Two CSS files accessed by `<link>` files in the `<head>` of your page: a main one and another, swappable one for a theme or skin. I used them from a CDN.
2. Two JS files, again linked in the head of the page. 
3. Some HTML in your page.
4. Some JS in your page that links to the images you want to display

Then on the Hugo side we need:

1. A *leaf bundle* with your content page named `index.md` and the images you want in your slide show.
2. A variable in the frontmatter to add the Photoswipe code to the pages you want to use it on.
3. A *partial* with HTML code in 3 above.
4. A shortcode which generates thumbnails of your images on the page.
5. Some CSS to style that shortcode.

Code from the shortcode for this:

```go-html-template
<button onclick="gallery.init()">Open gallery</button>

<div class="images">
   {{- range .Page.Resources.ByType "image" }}
   <figure>
       <img src="{{ .Permalink }}" alt="{{ replace .Name "-" " " }}" onclick="gallery.init()">
       <figcaption>{{ replace .Name "-" " " }}</figcaption>
   </figure>
   {{ end -}}
</div>
```


## Other Lightbox galleries

Here are some other alternatives. (for CDN use [cdnjs.com](https://cdnjs.com/))

1. [Light Gallery](https://www.cssscript.com/responsive-lightbox-gallery-pure-javascript-css3-lightgallery/) looks extremely capable and has loads of options and plugins. In fact this maybe it's downside: it's complicated. However it looks like you can just set up a basic option with one CSS file and one JS file.
2. [baguettebox js](https://openbase.com/js/baguettebox.js) is small file sizes

