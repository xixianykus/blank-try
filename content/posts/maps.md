---
title: "Maps"
date: 2021-09-05T22:06:11+01:00
draft: false
photoswipe: false
categories: [maps]
tags: [maps]
summary: "How to embed maps into a web page"
extLinks:
- [https://leafletjs.com/examples/quick-start/, Leaflet JS setup instructions, Official page on Leaflet JS site]
---

There are multiple services to get maps on a web page for free. This covers some of them as well as several services for getting or converting [coordinates](#getting-coordinates).

## Free Maps

### Open Street Map Method

This seems easy and reliable. Go to [Open Street Map](https://www.openstreetmap.org/) and then:

1. From the RH menu choose “share”
2. Where it says “Link or HTML” choose HTML. 
3. Copy the `<iframe>` code into your web page.
4. Customize with CSS. You can delete the height/width of the iframe and use CSS units in either the style tag or an external stylesheet.
5. A quick way to get coordinates is from [EPSG.io](https://epsg.io/). You can also get these from OSM too.

The main downside is that you can't add things to the map like labels etc.. That's where the javascript libraries come in handy.

Here's a mapy of Sheffield city centre showing the cycle routes:

{{< osm-map >}}

### Leaflet JS

This is one of several JavaScript libraries that use Open Street Map and allows more control and features on the map such as markers.

So it's a little more involved but not too hard, especially if you're using the library from a CDN.

1. Link to the Leaflet JS CSS file. This could be via a CDN or locally.
2. Link to the Leaflet JS javascript file, again via CDN or locally. MUST be after the CSS link.
3. Add a `<div id="map"></div>` to your page where you want your map to appear.
4. Add a height value for the above `<div>` in the CSS somewhere.
5. Add the script tags to the bottom of the page with code for the map.
6. Get an access token if needed and add to the `accessToken:` key.
7. Set the coordinates and zoom level in the above script.
8. Set flags etc., again in the script above.

#### The code

The link code for the latest version of Leaflet JS can be found on [leafletjs.com](https://leafletjs.com/examples/quick-start/), or another CDN like [CDNJS](https://cdnjs.com/libraries/leaflet) or [Skypack](https://www.skypack.dev/view/leaflet).

The JavaScript code below goes at the bottom of the page. This example use Hugo's `with` to access the `map` frontmatter array and `index` to access 3 values from a frontmatter array called `map`. The first is the longitude value, the second latitude and the third is the name for a pop up marker. You could also use another value the a zoom level for the map which is set to `"16"` in the code below.

```go-html-template
{{ with .Params.map }}
<script>

  var map = L.map('map').setView(["{{ index . 0 }}", "{{ index . 1 }}"], "16");
  
  L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(map);
  
  
  
  var marker = L.marker([ {{ index . 0 }} ,  {{ index . 1 }} ]).addTo(map)
  .bindPopup("<b>{{ index . 2 }}</b>").openPopup();
  
</script>
{{ end }}
```


### Mapbox

Mapbox can be found at [mapbox.com](https://www.mapbox.com/)

This is a paid service though there is a free tier and you don't require anything but an email address and a password. In this you get 50,000 free loads per month.

You access the account via an access token which you get from Mapbox.com and embed in the JS on your page.

### Open Layers

Used this a bit in the past but not recently so ... coming soon.


### Bing Maps

You can embed maps from Bing fairly easily. Like Open Street Map you select your map, pick generate code and get an `<iframe>` to embed in your page. The problem is you cannot easily resize them the way you can with OSM embedding. There is a lot of extra stuff you can do but you need a Microsoft account and more and compared to the other options it doesn't seem worth it. Personally sticking to open source is the preferred choice where possible.

## Getting coordinates

Once you have a map set up you will need to get the coordinates of your locations to set up the map. There are several websites good for this.

With Open Street Map you simply right click on spot on the map you want coordinates for then choose *show address* and the coordinates appear in the left hand pane.

[epsg.io](https://epsg.io/) is another way to obtain coordinates.

If you need to convert coordinates to a different system, for instance Ordinance Survey grid ref or postcodes to latitude and longitude the  [nearby.org.uk](https://www.nearby.org.uk/coord.cgi) is a good service.

It's also easy to get coordinates from google maps. Right click and a menu appears with the coordinates a the top. Click these to have them added to the clipboard.