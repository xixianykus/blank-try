---
title: "Draft"
date: 2021-05-27T11:16:33+01:00
draft: true
photoswipe: false
categories: ["css"]
tags: ["hugo"]
summary: "A list of draft pages on this site"
---


{{< draft >}}


## The goal

The goal was to create a list of pages that have `draft: true` set in their frontmatter. This frontmatter variable is used in two places. First on each page that is set to draft for easier identification. And secondly to produce a list of all the pages on the site that are are set to *draft*.

The list is created in a shortcode so can be used in any content file. However there's a couple of things that weren't obvious to me at first and I couldn't get the list to work or produce results.

## First try

The first time I got this working the code looked like this:

```go-html-template
<ul>
{{ range site.RegularPages }}
    {{ if .Params.draft }}
        <li><a href="{{ .RelPermalink }}">{{ .Page.Title }}</a></li>
    {{ end }}
{{ end }}
</ul>
```

I wasn't entirely happy with this. It worked fine but I just felt there must be a simpler way that was more like a regular `range` function.

I used some standard code from the [learnhugo.ml](https://learnhugo.ml) site but these either caused errors, produced no result or listed every file.

```go-html-template
<ul>
    {{ range (.Pages.ByParam "draft") }}
    <li>{{ .Title }}</li>
    {{ end }}
</ul>
```

This one produced an error so I changed `.Pages.ByParam` to `.Site.Pages.ByParam`.

## SUCCESS

This is the final working version.

```go-html-template
<ul>
    {{ range where .Site.Pages ".Page.Params.draft" true }}
    <li><a href="{{ .RelPermalink }}">{{ .Title }}</a></li>
    {{ end }}
</ul>
```

## Adding to the individual pages

This part was easy. To add the word *draft* to the top of draft pages I just used this simple code in the `header.html` partial:

```go-html-template
{{ with .Params.draft -}}
    draft
{{ end -}}
```

I have a shortcode, `draft.html` to create the listing.

There is also some code directly in the `single.html` to avoid any problems with the context in the shortcode.

**Code to count the pages**

```go-html-template
{{$srp := site.RegularPages }}
{{ range $srp }}
    <div><a href="{{ .Permalink }}">{{.Title }}</a></div>
{{ end }}

{{ len $srp }}
```






## Stuff I've learned

 1. `.Pages` is an alias to `.Data.Pages.` It is conventional to use the aliased form .Pages. [docs](https://gohugo.io/variables/page/#pages)

2. Page-level `.Params` are only accessible in lowercase. [docs](https://gohugo.io/variables/page/#page-level-params)

3. from within a shortcode `range .Site.Pages` gave a different result to `range site.Pages`, the function. Perhaps to do with this is from a shortcode?

One attempt that doesn't show anything:

```go-html-template
<ul>  
    {{ range where site.Pages "draft" "true" }}
        <li><a href="{{ .Permalink }}">{{ .Title }}</a></li>
    {{ end }}
</ul>
```

Try run from a partial instead?

## Clean this mess

1. single.html
2. shortcodes/draft.html
3. this page