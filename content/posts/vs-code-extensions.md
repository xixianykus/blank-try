---
title: "VS Code Extensions"
date: 2022-03-02T09:05:44Z
draft: false
photoswipe: false
unfinished: true
categories: ["dev"]
tags: ["VS Code"]
summary: "My current VS Code extentions"
---

My currently installed VS Code extentions. Could probably remove some of these. Do I really need Beautify[^1] AND Prettier?

## Installed

- **Beautify css/sass/sass/less**
- **Better TOML** handy for editing TOML such as in the Hugo config file.
- **Bookmarks** you can add *book*marks to a line of code with `Ctrl + Alt + K`.
- **CSS Peek**
- **File Utils** very quick way to create, delete, rename or open files
- **Formatting toggle** turns formatting on and off with one click
- **GitKraken Authentication provider** not sure I need this at the moment.
- **GitLens** - helps see changes in Git quickly and in the page.
- **HTML Tag Wrap** - quick way to wrap tags around a selection. Sometimes better than the default Emmet Wrap but not always. I use both.
- **Hugo Language and Syntax support** - the best Hugo extention by far IMO.
- **Insert Date String** - quickly insert the current time and data with `Ctrl + Shft + i`.
- **Live Server** - hardly use this server as Hugo and other things have their own these days.
- **Markdown All in One** - Fab!! Markdown is so simple why do you need an extention for it? This does so much like formatting markdown tables, paste a url over selected text to create a link and much more.
- **Open in Browser** 
- **Prettier**
- **SVG**
- **VS Code icons** custom icons.
- **YAML**

## Currently disabled

- **Beautify**
- **Code Spell Checker**
- **Frontmatter**
- **Indent Rainbow**
- **Manta's Stylus supremacy**
- **Red Hat Commons**
- **Spell Right**
- **Spellchecker for source code and text**
- **Stylelint**
- **Stylus**
- **Svelte for VS Code**


[^1]: Beautify is now deprecated coz it's no longer maintained.