---
title: | 
  Smashing Web Design
  Business Model
date: 2021-05-20T06:10:06+01:00
draft: false
unfinished: true
categories: ["business"]
tags: ["business"]
summary: "Notes from an article in Smashing Mag"
extLinks:
- ['https://www.smashingmagazine.com/2021/03/web-design-clients-fast-part1/', Web Design Clients part 1, in Smashing Mag]
- ['https://www.smashingmagazine.com/2021/04/web-design-clients-fast-part2/', Web Design Clients part 2, in Smashing Mag]
---


{{< img src="smashingmag.png" alt="not mush" style="float:right; border-radius: 20px;" >}}

This is based on [two articles](https://www.smashingmagazine.com/2021/03/web-design-clients-fast-part1/) by Sitejet in Smashing Mag.

## Main idea

The central idea in this is to think of your business as offering a service, ie. something ongoing, as opposed to a product. This will better create a regular income, aka MRR or monthly recurring revenue.

> This service model is a mindset shift, to be sure, but it’s a smarter way of doing business. To succeed, start thinking in terms of a recurring service — not a one-and-done product.
>
> Sitejet

## Break even number

The first step is to work out your break even number. How much does it cost to run your business.

There are one off fees, like a computer, and regular fees, hosting, snipcart, forms etc.

## Have multiple tiers

Having different plans will give a broader scope. Minimum is 2, basic and another one.

> We’ve done well with three plans priced at $30, $50, and $100 USD per month.
>
> To differentiate your premium tiers, add advanced features like more storage, faster updates, extra subpages, detailed traffic statistics, multilingual support, and more.
>
> Sitejet

Don't charge by the hour. As you get better you should get faster at your work.

1.  basic plan might be a pre-made theme with x amount of customizations and x updates per month.
2.  custom programming
3.  custom, from scrach web site.


## Marketing

It's best to focus on how a web site will help grow your customer's business and lead to more sales rather than trying to sell the technical aspects of it.

## Vertical

It's better to find a narrow niche and focus exclusively on that at first. Something you're knowledgeable or passionate about: non-profits or environment or climbing etc.

