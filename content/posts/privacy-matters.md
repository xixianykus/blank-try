---
title: "Privacy Matters"
date: 2022-11-18T07:38:47Z
draft: false
photoswipe: false
unfinished: true
categories: ["css"]
tags: ["privacy", encryption, China, security]
summary: "This is a transcript of a Youtube video of a talk by privacy advocate Sun Knudsen"
---


hi everyone

I'm Sun. I'm a technologist and a
security researcher. Over the past few
months I started to embrace *silence*.

What is silence? As we all know silence is the
absence of sound and noise. For instance
silence is also the absence of mention
so you know on social media when people
tagged you and everything even when you
don't want to so people are associating you with
things so they're breaking your silence.

Silence is closely related to privacy
and I'm quite obsessed with privacy.

Recently I bought a pair of noise
cancelling headphones and I used to I go
in cafes once in a while to work and
they helped me realize that silence is
noticeable when noise is muted.

Who has tried noise cancelling headphones?

For me when I walk in a cafe it's
not that noisy right. I put them on and
when I remove them holy like


Well the same thing applies to privacy
so privacy is only crave when when it
1:23
has me lost and that's exactly what's
1:27
happening in China right now so in China
1:29
people are using cameras with facial
1:31
recognition track citizens everywhere
1:34
and that's only the tip of the iceberg
1:37
it's quite terrifying so essentially in
1:40
the lower cities Chinese people are
1:43
ranked and they're identified with an ID
1:46
and in some cities if you cross a red
1:49
light you'll get a ticket sent to you
1:53
and they'll withdraw the scheme
1:55
automatically okay and and a lot of
1:59
people when I mentioned stuff like this
2:00
they're like well you know I don't go
2:04
against the law right so that doesn't
2:05
fight me and that's all cool until you
2:08
look at Hong Kong right because in Hong
2:10
Kong people are fighting for democracy
2:11
something that we all take for granted
2:13
here and they have to hide from all
2:16
those cameras and that's one of the
2:17
reasons why they have umbrellas okay
2:20
and and they're also using telegram to
2:23
coordinate their their manifestations
2:27
and stuff like this so they're also
2:29
using cryptography and I'll talk more
2:31
about that in a second so back to our
2:34
nice little comfortable montreal right
2:36
so we're in Canada here everything is
2:38
fine right like we're a butyl it's tough
2:41
well you guys I'm worried about that see
2:44
a word oh wow and so I'm close allies
2:47
that East is that there yes
2:51
so we're using credit cards with loyalty
2:54
plans to buy stuff because we get 2%
2:56
back that's quite amazing we're using
3:01
Amazon we don't even see em robbery yes
3:05
we're using Amazon and buy stuff and
3:08
guess what you cannot even delete your
3:10
purchase history for Amazon right can't
3:13
even if you want to
3:14
we're exchanging very personal things
3:16
with our friends and family on social
3:18
media or using Google to search all
3:21
Google searches are tied to our identity
3:23
probably forever right everything the
3:25
most intimate we're googling is
3:27
tied to our identities and we're using
3:29
Chrome to browse right I'm not but I'm
3:31
sure you are
3:34
and millions if not billions of websites
3:38
are running something called Google
3:39
Analytics so that's a really helpful
3:41
tool for marketers right well it's an
3:43
even more helpful tool for Google and
3:46
DNS a and we're using Google Maps and
3:49
God forbid if I wasn't using Google Maps
3:51
ovule honest but the promise by default
3:53
Google is tracking our every move all
3:56
over the world and so they know exactly
3:58
where most of us are going and
4:01
apparently there are ways to disable
4:02
this but this I mean a lot of news broke
4:05
that they're not even when you're
4:07
deliberately telling you not to track
4:08
you they're still tracking you sorry
4:12
that was a lot and then
4:14
leave it off yeah and then we're using
4:17
this beautiful little and I'm not again
4:19
by working a lot of people are using
4:22
this so that you can tell that you're
4:24
beautiful
4:25
a little AI box there with a smile
4:28
hey Alexa play this trap you know mute
4:31
the lights you know we're about to get
4:33
intimate and most of all is that this
4:38
thing is listening right and if it
4:40
actually is a cut more about that in
4:42
segments and all of this technology is
4:45
aggregating an enormous amount of their
4:48
private information on us and it's
4:50
storing it forever in huge datasets to
4:55
target ads to us so they say and I mean
4:59
that's prom by itself over consumption
5:01
but it's not today stock
5:02
the problem is when ad tech goes to mass
5:05
surveillance and that is exactly what
5:08
Edward Snowden broke in 2013 when a lot
5:12
of NSA documents were neat one of the
5:15
programs that shed light on is called
5:17
prism so essentially prism as that
5:19
program that allows the government when
5:21
they have some level of suspicion on you
5:24
it could be that you're in the same
5:26
family than someone that is perhaps
5:28
related to a therapist perhaps you know
5:31
it allows the NSA direct access to all
5:36
of the data sets of Apple Facebook
5:39
Google and others so all of those
5:42
private things were saying are to the
5:45
disposal of governments and think Hong
5:47
Kong okay because if we think about it
5:50
here right now it's fine I think United
5:53
States Donald Trump Hong Kong you know
5:56
and that's only wanted the program so
5:59
another program is called the upstream
6:01
servants program so what they're doing
6:03
is a lot of governments
6:05
Canada they're installing tapping
6:07
devices on all of the backbones of the
6:09
Internet's so when you send an email to
6:11
someone abroad for instance in the UK a
6:14
hundred percent sure that that email is
6:17
essentially soared so that if ever
6:20
someday someone has something against
6:22
you they can go in this dataset and pull
6:24
it out so we would think that when that
6:28
broke in 2013 everything are change
6:30
right because we care about privacy who
6:32
cares about privacy
6:34
who cares about freedom who cares about
6:36
democracy can lift your hand so the
6:43
drama is nothing changed and I won't go
6:45
into detail but your Samsung TV is
6:47
listening to what you're saying the
6:49
American elections were hacked using the
6:52
data of 50 million people on Facebook
6:54
shopping malls are tracking our faces
6:58
Google was tracking us even if we tell
7:00
it not to employees of Amazon are
7:03
listening to what we're saying in our
7:05
living rooms
7:06
Australia has stands strong cryptography
7:09
and that's breaking figured freedom of
7:11
speech for journalists so yeah and this
7:15
is all happening in country like Canada
7:17
in the United States and Australia so
7:19
countries that are you know like safe
7:21
right so perhaps we like good privacy
7:25
laws because government is still
7:26
involved in mass surveillance and and
7:29
according to Edward Snowden there's a
7:31
program called the five lines so it's
7:33
really a Canada
7:34
Canada and the United States are among
7:37
the worst countries in the world when it
7:38
comes to privacy we're not that far
7:40
behind China actually so what would the
7:44
audience feels like there's never enough
7:45
time in a day so that develops in
7:51
ourselves a strong craving for
7:52
convenience right so it's pretty cool
7:54
that our fridge has a camera and knows
7:56
when we're running out of milk and
7:57
orders the milk automatically it's
7:59
pretty cool that when you receive a
8:00
package from Amazon because we love
8:02
buying stuff it's pretty cool that we
8:04
have a ring doorbell and we can actually
8:06
let the Amazon employee in and put its
8:08
package in the house right well all of
8:11
this technology well it's true price so
8:14
the true price of convenience is freedom
8:16
were essentially feeding nation-states a
8:20
huge amount of information and it's
8:22
perfectly fine when it's perfectly fine
8:24
until it's not and the problem is like
8:28
they say for the icebergs that are
8:29
melting right we passed the point well
8:32
we definitely have passed the point when
8:34
it comes to how we use technology now we
8:37
need to slow down and that's very
8:40
important we need to let go some of this
8:41
convenience to reclaim our right to
8:43
privacy and therefore democracy they
8:47
could use this before starting to guaran
8:48
so that shopping mall I mentioned
8:50
earlier in Canada well they they stopped
8:53
the face tracking software if no one had
8:56
leaked it it would still be running but
8:57
at least there's some hope and oh
9:00
there's a link there sir about that so
9:01
something that's very strangest San
9:03
Francisco banned facial tracking
9:05
software in San Francisco so it's quite
9:08
interesting that one of the epicenters
9:10
effect in the world is actually banning
9:12
the use of their own technology so I
9:14
think we should listen and and never
9:16
accept this so no ringing doorbells no
9:20
google homes no LX eyes turn off Siri
9:23
and stuff like that
9:26
so here are a few things that I do that
9:28
you guys can do as well to reclaim some
9:30
of your privacy whoops so if something
9:32
is free ask why if something is free you
9:35
are the product so when you're using
9:36
Facebook or Google I mean you're being
9:39
productize
9:41
you can access the internet from a
9:43
country with better privacy laws so when
9:45
I use my Mac I'm always in Sweden so
9:49
that's a country that has much better
9:50
privacy legislation and when you're in
9:53
Sweden you're under the GDP are so
9:55
companies are obliged to have your
9:58
explicit consent to track you it's a
10:01
little bit full of to be honest but
10:03
it's better okay so good country is to
10:07
terminate your internet for Iceland
10:09
Switzerland and Sweden
10:11
please use Firefox instead of chrome
10:14
Chrome is a huge tracking device set and
10:17
in Firefox works very well you know and
10:20
then you can use extensions HTTPS
10:23
Everywhere and privacy badger to help
10:25
mitigate tranq and even more I recommend
10:28
using DuckDuckGo instead of Google
10:30
Google searches is very invasive of your
10:33
privacy so I recommend using DuckDuckGo
10:36
instead of messaging people on facebook
10:39
Messenger you can use signal that's it's
10:41
endorsed by Edward Snowden Snowden sorry
10:43
signal is an encrypted messaging
10:46
platform you can also do in cryptid
10:48
video calls
10:48
so it's end-to-end encrypted meaning you
10:51
can't have a man in the middle attack on
10:53
that and consider everything you go on
10:56
facebook Messenger and Google public
10:58
domain because really you never know
11:00
who's going to see this stuff okay so if
11:03
you don't want to say it out loud in the
11:04
street definitely don't say it say it on
11:06
those platforms and most importantly you
11:09
sure you care about privacy because this
11:12
honestly is a pivotal point in history
11:14
where we have all of us together we have
11:16
to decide that we're saying no to Chrome
11:18
we're saying no to Google we're saying
11:20
no to Facebook if we don't do it we're
11:23
essentially digging our own grave and
11:27
as far as privacy goes and so I
11:30
published some research on medium and
11:34
I'm also starting a YouTube channel so
11:36
if you guys care about privacy you can
11:38
go and follow those things out
11:39
Publishing more information to help you
11:41
guys transition to that is it's not far
12:10
and it will push
12:16
a little negative both conveniens first
12:20
Longines questions I can see it's Jake
12:24
and Elizabeth announce the unusually


## Links

1. [Sun Knudsen](https://sunknudsen.com/)'s website has a huge range of articles.