---
title: "Progressive Web Apps"
linktitle: "Notes on PWA's"
date: 2021-08-04T18:24:03+01:00
draft: false
unfinished: true
photoswipe: false
categories: ["css"]
tags: [PWA, responsive design]
summary: "Some notes on creating Progressive Web Apps."
extlinks:
- ["https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps", MDN docs on PWAs]
- ["https://dev.opera.com/articles/installable-web-apps/", Opera's Installable Apps, This is nicely written, easy to understand]
- ["https://web.dev/progressive-web-apps/", WebDev guide, Google's equivalent of MDN]
- ["https://www.pwabuilder.com/imagegenerator", PWA icon generator]
- ["https://itnext.io/pwa-splash-screen-and-icon-generator-a74ebb8a130", PWA Asset Generator]
- ["https://developers.google.com/web/tools/workbox/", Workbox, Googles JS library to create service workers]
---

## aka A2HS or Add to Home Screen

To turn your web site into a progressive web app you need 3 things:

1. A set of **icons** for the home screen of different mobile devices
2. A site **manifest.json** file with info about your 'app' linked from the `<head>` of your site.
3. A **service worker** to do work like caching files etc.

The site also needs to have an `https` connection.

## Icon set

You need icons for all different mobile devices and screen and resolution sizes.

1. 192px x 192px
2. 512px x 512px

Create an your icon and quickly get a wide range of icons made from [PWA Builder](https://www.pwabuilder.com/imagegenerator) stored into a zip and downloaded into your project.


## Service Workers

These don't have to be created manually. There is a JS library called Workbox that even has a CLI to set up your service workers




Here's a good introductory vid on PWA's by [Fireship.io](https://fireship.io/)

{{< music/yt "sFsRylCQblw" >}}