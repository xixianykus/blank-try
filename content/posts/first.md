---
title: "About this site"
date: 2019-10-30T07:13:57Z
draft: false
tags: ["hugo"]
categories: ["Web dev"]
summary: "A few random questions & notes on this site"
---

Found this site/folder not working because the theme was not added to the config file. Duh

It uses the blank theme.

The home page and CSS don's seem to be working.


## Questions

1. Why doesn't the content of _index.md show up on the home page?
   1. Ans: Because it uses a home page template. Add `{{ .Content }}` to the top of that template to fix.
2. Why no top level menu? It's in the header and referenced in the config.toml
   1. In the config.toml file `[[main.menu]]` should have been `[[menu.main]]`