---
title: "Chartist JS"
date: 2021-05-09T08:39:34+01:00
draft: false
categories: ["JS Library"]
tags: ["graphs", Chartist JS, javascript]
summary: "First try with Chartist JS"
chartistjs: true
extlinks:
- ["https://gionkunz.github.io/chartist-js/index.html", Chartist JS, The main site for the library]
---

Finger strength of max-hang on a 20mm edge for 5 seconds

{{< charts/firstchart >}}


Here is the code for the first graph. Note the last line where you have to include each block (data and options in this case).

```js
var data = {
    // A labels array that can contain any sort of value
    labels: ['Ben Moon', 'Magnus Mitbo', 'Steve McClure', 'Jim Pope', 'Max Milne', 'Will Bosi', 'Alex Megos'],
    // Our series array that contains series objects or in this case series data arrays
    series: [
    [101, 106, 115, 116, 118, 130, 132]
    ]
};

var options = {
    low: 100,
    high: 140,
    horizontalBars: true,
    reverseData: true,
    axisY: {
        offset: 70
    }
}

// Create a new bar chart object where as first parameter we pass in a selector
// that is resolving to our chart container element. The Second parameter
// is the actual data object.
new Chartist.Bar('.ct-chart', data, options);
```