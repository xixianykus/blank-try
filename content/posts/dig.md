---
title: "DiG"
date: 2023-11-02T08:04:55Z
draft: false
photoswipe: false
unfinished: true
categories: [dev, CLI]
tags: ["DNS"]
summary: "Basic use of the DiG command for getting info on DNS records"
---

The DiG command typically comes with Unix/Linux based operating systems so usually built in with Mac computers. On Windows it can be used with Git Bash for Windows.

Help can be found using `dig -h` plus, on Unix/Linux systems, you can get the manual using `man dig`.

## Uses

DiG (Domain Information Groper) is a command-line tool used to perform DNS lookups and retrieve information about a domain name or IP address. It can be used to verify the authenticity of a website, check for malware or phishing activity, and gather information about a website's infrastructure.

## Syntax

The basic syntax is

```bash
dig server name type
```

If you use `dig example.com` it will retrieve only the `A` records for the domain. This is the same as `dig example.com A`.

You can do the same for any other records:

```bash
dig example.com mx
dig example.com aaaa
dig example.com txt
```

You can also look for all records using the keyword `any` how

If you wish to use a specific DNS server, such as Google's at `8.8.8.8` use the `@` symbol like so:

```bash
dig @8.8.8.8 example.com mx
```

## Reverse lookups

To look for the domain of an IP address use the `-x` flag.

```bash
dig -x 93.184.216.34
```

## Tracing the route

You can also get the whole route of a DNS request using the `+trace` command.

```bash
dig example.com +trace
```


## What are SOA records?

You'll often come across SOA records. What are they?

> The DNS ‘start of authority’ (SOA) record stores important information about a domain or zone such as the email address of the administrator, when the domain was last updated, and how long the server should wait between refreshes.
>
> All DNS zones need an SOA record in order to conform to IETF standards. SOA records are also important for zone transfers.
> 
> From [Cloudflare docs](https://www.cloudflare.com/en-gb/learning/dns/dns-records/dns-soa-record/).

## nslookup

nslookup is another useful command line tool for getting info about `NS` records.

```bash
nslookup
set type=SOA
example.com
```