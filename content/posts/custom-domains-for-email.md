---
title: "Custom Domains for Email"
date: 2023-10-29T11:56:34Z
draft: false
photoswipe: false
unfinished: true
categories: ["css"]
tags: ["dns", email]
summary: "Ways to use your custom domain name with email for free"
---


## Zoho

[Zoho](https://zoho.com/) is a service like Gmail but based in India. It has an excellent track record for many years and is a well established service.

They offer an email account option to use your own domain name for up to 5 users. The account has a maximum storage capacity of 5Gbs per inbox, which is reasonable but not outstanding.

Another downside, for some, is that there is no POP3/IMAP access so you have to access email via their web portal.

## Improv MX

[ImprovMX](https://improvmx.com/) has a free tier for redirecting emails sent to your custom domain to your email account inbox. However you cannot send any emails from your custom domain, ie. there's no SMTP service on the free tier. You can only use one custom domain and can have up to 25 email aliases on that. Attachments are limited to 10mbs.

## Forward Email

[Forward Email](https://forwardemail.net/en) is a company that specializes in email forwarding and has a great free tier that works with your own domain or domains.

## Cloudflare email forwarding

[Cloudflare's email routing](https://blog.cloudflare.com/introducing-email-routing/) looks interesting though its only in beta currently and so there is [a waiting list](https://www.cloudflare.com/en-gb/email-forwarding-waitlist-sign-up/) to access the free service.

## MX Route

[MX Route](https://accounts.mxroute.com/i) offers for life email service for unlimited domains and accounts but with a 10Gb total storage limit and limited to 300 outbound emails per hour. This costs around $129. The service is basic but they seem like an honest and down to earth bunch with a good, no BS attitude.