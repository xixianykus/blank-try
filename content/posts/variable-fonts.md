---
title: "Variable Fonts"
date: 2021-11-10T15:13:31Z
draft: false
photoswipe: false
unfinished: false
categories: ["css"]
tags: [css, typography]
summary: "Links to stuff on variable fonts"
css:
- "@import url('https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@100..900&display=swap');"
- "h1, h2 {font-family: 'Roboto Slab', monospace; font-weight: 760;}"
- "h2 {font-size: 2.1em;}"
---


Variable fonts are built with different *axes* that can vary depending on the settings. For instance instead of having several different font files for condensed, normal, expanded etc. a variable font file *may* have a width axes that allows the single font to be set at different widths. 

The most common axis is probably *weight* which allows a font to vary from thin strokes to thick. With one font file on a web app you can vary the thickness simply using CSS.

Variable fonts have been pushed by Google, Apple, Microsoft and Adobe which means support is good.

## Compression

When using variable fonts on the web it is better if they are in the more compressed (smaller) file format of  WOFF2.

You can convert fonts at either [FontSquirrel](https://fontsquirrel.com/) or [Everything Fonts](https://everthingfonts.com/).

The process is generally but not always straightforward. If you have problems with the WOFF2 format you can always use the original format of the font file, for example `.ttf` for True Type fonts.


## Adding a self hosted font file

If you have a font file on your site somewhere you can add it via CSS using `@font-face`.


```css
@font-face {
    font-family: 'Libre Franklin';
    src: url('fonts/LibreFranklinRoman.woff2');
    font-style: normal;
}
@font-face {
    font-family: 'Libre Franklin';
    src: url('fonts/LibreFranklinItalic.woff2');
    font-style: italic;
}
```

NB. Use the same name to avoid having to call a different `font-family` name just when using the italic version.

You need to define both `normal` and `italic` font-styles.

Then in the CSS you can add the variable's axes features like so:

```css
p {
    font-weight: 400;
}
```

Although the `font-weight` property has been around a long time in CSS it is only recently that it can work with variable fonts. A method that works with older browsers is to use `font-variation-settings`.

```css
p {
    font-variation-settings: "wght" 400;
}
```


## High and low level CSS properties

The W3C ask devs to use the high level properties generally and only use low level properties if we need to. However the low level property is/was available before the high level properties (although not in Safari).

High level properties (with low level counterparts):

1. `font-weight` - `wght`
2. `font-style` - `ital` and `slnt`
3. `font-stretch` - `wdth`
4. `font-optical-sizing` - `opsz`


Low level property:

1. `font-variation-settings`


The high level settings can be used with `font-variation-settings` like so:

```css
p {
    font-variation-settings: 'wght' 900, 'wdth' 100;
}
```

However the main use for `font-variation-settings` is for custom axes (values) of a variable font, such as a drop shadow or x-height. Rather than use the above it's better to use:

```css
font-weight: 900;
font-stretch: 100%;
```

The `font-stretch` property can also take adjectives like `condensed`, `expanded` etc.. Again these will vary from font to font.


## Real world usage

Firstly the limits of font-size and weight settings are set in the font by the designer. If the maximum weight value is set at 700 then putting 900 in will not do anything.

Also fonts may require a specific number for a specific look. For example a font might use the weights of 40, 50, 60 etc. to set the type at different sizes. Another font may use values from 1 to 1000. You can find these by using the different instances on [Axis-Praxis](https://axis-praxis.org/)


## font-style

The font-style property can be used to set either:

1. The italic level. This *officially* goes from `0` to `1`. A value of 0 is straight up.
2. The slant value. This *should* go from `-90` to `90` which represent degrees. Most font designers however will limit the range here. For instance a font might just go from zero to -15. A value of `0` means no slant at all.

Also note that not all font designers will use the official specs. The Bitcount font has slant values from 0 to 1000.

It's assumed you don't use both italic and slant and the brower will determine which is which based on the value given.

## font-optical-sizing

This property sets the minimum and maximum thickness or thinness of a font's lines on fonts that have this built in. When fonts are displayed at large sizes you can use a big difference in thickness. But at smaller sizes the text will be more readable if the thinnest parts are not too thin.

As always values will depend on the font.

The preferred method when available is:

```css
p {
    font-optical-sizing: 72;
}
```

otherwise:

```css
p {
    font-variation-settings: 'opsz' 72;
}
```


## Custom axes

In CSS these are accessed with `font-variation-settings`. The four letter value is always in capitals.

One way to get to see all the different axes of a font is through Firefox dev tools.


## Adding fonts from Google Fonts

The font used for headings on this page, Roboto Slab, is loaded from Google fonts using the `@import` rule in the page's `<style>` sheet in the `<head>` of the document.

Generally when choosing a variable font Google encourages you to pick only those weights you need. However it's possible to choose a range of font weights like so:

```css
@import url('https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@100..900&display=swap');
```

Note the two dots between the weights: `Roboto+Slab:wght@100..900`. Also note the use of `wght` which is the same 4 letter code used above for `font-variation-settings` above.

When a range of weights is called using this method you should be able to use any weight in the range:

```css
h1, h2 {
    font-family: 'Roboto Slab';
    font-weight: 760;
}
```


## Youtube vid

A big long vid. Skip to the last section (1.29.40) for a quick recap on most of the above.

{{< music/yt "9IFqv5uVP_c" >}}


## Links

1. [Variable Fonts](https://variablefonts.io/) is an excellent source of info with links to other good resources too.
2. [axis-praxis](https://www.axis-praxis.org/) has a number of online variable fonts that can be adjusted with their variable settings. You can also upload and thus demo you're own font files.
3. [V-Fonts](https://v-fonts.com/)
4. [Spectral font](https://spectral.prototypo.io/) is a *parametric* font.
5. [Fontview](https://github.com/googlefonts/fontview) is a tool to open variable fonts to read instances etc. for Mac and Linux
6. [font-display property](https://nooshu.com/blog/2020/02/23/improving-perceived-performance-with-the-css-font-display-property/) How a font stack can load with properties `block`, `fallback`, `swap`, `optional` and `auto`.