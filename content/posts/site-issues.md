---
title: "Site Issues"
date: 2019-11-02T10:41:20Z
draft: false
categories: ["Web dev"]
tags: ["hugo"]
summary: "Fixes and problems for this site."
---

**If you fix write how below.**

See also: READLOCAL.md

### The websites title wouldn't show

In the home page template (layouts/index.html) the site title would crash the system.

FIX: The text case was wrong. Missed the capital off of *title*. **.Site.title** should have been **.Site.Title**



### Post list not showing summary

Instead it was just showing a bunch of text.

FIX: Added the Hugo version number to Netlify and in a new netlify.toml. I also changed summary to description.



### No main menu

No main menu is appearing yet defined in partials/header. Yet it works in blank2

FIX: `[[main.menu]]` should have been `[[menu.main]]` in config file.




### Content of _index.md not showing

was not showing up on the home page. **Reason:** There was a  layout file (`layouts/index.html` ) responsible for that page instead of the default `list.html` page. This required ```{{ .Content }}``` to be added to that page to pull the content from _index.md.

