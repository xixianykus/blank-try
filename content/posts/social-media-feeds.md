---
title: "Embedding social media feeds"
linktitle: "Social Media Feeds"
date: 2021-09-04T06:11:37+01:00
draft: false
photoswipe: false
categories: ["css"]
tags: ["twitter", tumblr, API]
summary: "Adding social media feeds to a web page"
extlinks:
- ["https://help.twitter.com/en/using-twitter/embed-twitter-feed", Embed a Twitter feed, "Twitter’s instructions on embedding any feed."]
- ["https://finlay.tumblr.com/post/529010691/embed-tumblr-into-your-website", Adding Tumblr via its API, Good post by Finlay Craig]
- ["https://www.bloggermint.com/2012/01/easiest-way-to-embed-rssatom-feed-on-any-web-page/", Adding RSS feed using JQuery, A quick way to add a feed]
- ["https://itnext.io/a-beginners-guide-to-using-the-twitter-api-839c8d611b8c", Twitter API for beginners, Simple instructions on how to use the Twitter API]
- ["https://rapidapi.com/blog/how-to-use-the-twitter-api/", The Twitter API in 4 Easy steps, Another tutorial on using the Twitter API]
- ["https://www.tumblr.com/docs/en/api/v2#what-you-need", What you need for Tumblr API, official documentation by Tumblr]
---

## Twitter

Twitter makes it very easy. You can either get a list of a single feed or a [public list](https://help.twitter.com/en/using-twitter/twitter-lists).

For either you:

1. go to [publish.twitter.com/](https://publish.twitter.com/) 
2. Put in the url of the feed you want to embed
3. Optional: click on 'set the customization options' to choose the dark or light theme and set the width if want to.
4. Click **Copy Code** and paste into your page.

When I first tried this only the link to the feed showed up (the first line fallback option below). The reason was an extension I was using blocked the external feed. I tried it in a different browser and it worked.

Once you have the code you can easily change themes from dark to light and change the width by directly editing the code.

```html
<a class="twitter-timeline" data-width="400" data-theme="dark" href="https://twitter.com/GretaThunberg?ref_src=twsrc%5Etfw">Tweets by GretaThunberg</a>

<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
```

## Tumblr

NOT SURE THIS METHOD WORKS STILL. It looks like Tumblr may have blocked access to b when it's embedded in a web page. You CAN get the feed directly in a browser tab though.

So this is based on [an article](https://finlay.tumblr.com/post/529010691/embed-tumblr-into-your-website) by Finlay Craig.

The Tumblr api can be accessed like so replacing *username* with the name of the user whose feed you want.

```html
https://username.tumblr.com/api/read
```