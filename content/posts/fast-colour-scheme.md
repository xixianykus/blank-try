---
title: "Fast Colour Scheme"
date: 2022-02-14T05:50:46Z
draft: false
unfinished: false
categories: ["css"]
tags: [design, colour]
summary: "Notes based on a Kevin Powell video on generating web site colour schemes."
---

The main ideas to find a colour scheme fast are:

1. Start in black and white. This makes you look at the layout in a more objective way. Getting the layout and structure right is more easily done without the distraction of thinking about colour.
2. Introduce just one colour. This could be taken from an image in the photo. In a program like Adobe XD or using CSS custom properties you can then apply this and and change it anyway you like.
3. You now essentially have 3 colours: black, white and your chosen colours. 
4. More colours, like more fonts, are harder to get them to work together.
5. Rather than using different colours use tones of your single colour.
6. Introduce a second (fourth inc. black and white) to draw the eye: to buttons headings etc..

## Online tools

Also mentioned in the video are online tools:

1. [mycolor.space](https://mycolor.space/)
2. [color.adobe.com](https://color.adobe.com/)


{{< music/yt "mq8LYj6kRyE" >}}