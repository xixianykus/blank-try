---
title: "RSS Bridge"
date: 2022-09-06T18:12:36+01:00
draft: false
photoswipe: false
unfinished: true
categories: ["RSS"]
tags: ["rss"]
summary: "RSS Bridge is an app used to create RSS feeds from sites that don't have them."
---

> Dear so-called "social" websites.
>
> Your catchword is "share", but you don't want us to share. You want to keep us within your walled gardens. That's why you've been removing RSS links from webpages, hiding them deep on your website, or removed feeds entirely, replacing it with crippled or demented proprietary API. FUCK YOU.
>
> You're not social when you hamper sharing by removing feeds. You're happy to have customers creating content for your ecosystem, but you don't want this content out - a content you do not even own. Google Takeout is just a gimmick. We want our data to flow, we want RSS or Atom feeds.
>
> We want to share with friends, using open protocols: RSS, Atom, XMPP, whatever. Because no one wants to have your service with your applications using your API force-feeding them. Friends must be free to choose whatever software and service they want.
>
> We are rebuilding bridges you have willfully destroyed.
>
> Get your shit together: Put RSS/Atom back in.
>
> [RSS Bridge](https://github.com/RSS-Bridge/rss-bridge)