---
title: "Perspective Grid"
date: 2021-12-19T22:08:54Z
draft: false
unfinished: true
toc: true
categories: ["css"]
tags: ["illustrator"]
summary: "How and where to use Illustrator's Perspective Grid tools."
---


## Do you need it?

The perspective grid is not difficult to learn but it can be clunky to use and in many cases you might be better off not using it at all. If you only have a small number of objects that need perspective you might be better without it using perhaps the transform tool (`e`) instead. But the more objects you have the more useful it becomes.

## Getting started

The perspective grid tool is fairly simple but counter-intuitive to use.

You can choose it from the tools menu or use the shortcut `Shft + p`

![From the menu](menu-flyout.jpg)


## Vanishing points

If you click the perspective grid tool, `shft + p`, a default 2 point perspective grid appears. This can be changed via the `View > Perspective grid` menu. Choose either `Define Grid..` or one of the presets under `One`, `Two` or `Three Point Perspective` options.

You can have 1, 2 or 3 vanishing points.

![1 vanishing point](vanish-1.png)

With 1 vanishing point a cube will have 2 sides visible but one of these will be face on to the viewer, so a perfect square as if drawn using the rectangle tool.

![2 vanishing points](vanish-2.png)

With 2 vanishing points the view is similar to an isometric grid such as is often used in pixel art. A cube can have 2 vertical faces visible both shrinking away from the viewer. One shrinks to a point to the left the other to a point on the right. The vertical corners of the cube remain parallel to one another. 

![3 vanishing points](vanish-3.png)

A third vanishing point adds vertical perspective. If you're drawing has a large building viewed from the bottom the walls of the building will converge with height so this third vanishing point is essential. 

## Tools

As soon as you do this a grid appears on the page. There are several ways to change things on this grid:


| Tool                           | What it does                                                                          | Shortcuts                          |
| :----------------------------- | :------------------------------------------------------------------------------------ | :--------------------------------- |
| The perspective grid tool      | is used to open and edit the grid                                                     | `Shft + p`                         |
| The perspective selection tool | is used to move and adjust objects whilst they maintain their perspective on the grid | `Shft + v` hold `alt` to duplicate |
| From the main menu             | which contains multiple sub menu options                                              | `View > Perspective Grid`          |
| Perspective grid widget        | You can change the plain you are working on, or turn it off, by pressing              | `1`, `2`, `3` or `4`               |
| Move through a plane           | You can move objects through their plane by holding down `5` while dragging them      | `5` hold `alt` to duplicate        |
| Hide the grid and widget       | If the perspective grid tool is active you can also hide the grid by pressing `Esc`.  | `Ctrl + Shft + I`                  |
| Undo                           | changes to the grid using undo,                                                       | `Ctrl + Z`                         |

You can also use regular tools like the pen and just click points on the guides to get what you want too.

![From the main menu view menu](view-menu.png)

## Setting up the grid

The first stage is to set up the grid. Once you start placing objects on the grid you can't really change it. Well you can change it but all the objects you've placed on it won't change with the changes on the grid which means you'll have to edit each one, point by point, which is time consuming.


The first thing to do is to decide how many perspective vanishing points you want. This is done from the main menu `View > Perspective Grid > One point perspective`. When you choose from here any earlier changes you made to the grid will be lost which is why it's important to start with this step.

The difference between these is like looking at a cube. You see either one, two or three sides of a cube.

Alternatively you can create your grid from the dialogue box `View > Perspective Grid > Define Grid...`

![Define Perspective Grid dialogue box](define-grid-dialogue.png)

The options are fairly obvious. Once set up you can save the values as a preset and recall them for any other documents you want the same grid.


## Patterns

If you use a pattern to fill one of your objects it won't have perspective applied.

Instead you would firstly expand the pattern `Ctrl + E`. You might need to do this several times. Then use the Perspective Selection tool to move it onto the grid.

Another method uses `Envelope Distort`:


> You can fake it, create 2 same size rectangles.  
> Fill one with a pattern and place the other into the perspective grid.  
> Send the one with the pattern to the back.  
> Set the Object > Envelope Distort > Envelope Options... to: Distort Pattern Fills.  
> Select both rectangles and choose `Object Envelope Distort > Make with Top Object`.   
> 
> from [Tom Fredericks](https://community.adobe.com/t5/illustrator-discussions/pattern-don-t-follow-perspective-grid/m-p/11953770)


## Adding text

To add text to a perspective grid first type it out normally. Then with the Perspective Selection tool active (`Shft + v`) choose a plane to add the text to by pressing `1`, `2` or `3`. Drag the text onto the plane and resize holding down `Shft`

To edit text that has been placed on a perspective grid you need to go into isolation mode. Double click the text to enter isolation mode. You might want to press `Esc` once to hide the highlighter around it. Then edit as normal.

Another way to change the colour of text is via the Appearence panel. Click your text, go to the Appearence panel and add a new fill. This fill can be coloured from here.

You can also add effects from the `Effect` main menu.


## Video

Concise 10 min video showing how to use the Perspective Grid tools by Primo Graphics...

{{< music/yt "dVV9GiffmEQ" >}}