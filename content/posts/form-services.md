---
title: "Form Services"
date: 2023-10-29T16:40:18Z
draft: false
photoswipe: false
unfinished: true
categories: ["email"]
tags: ["SaaS"]
summary: "Synopsis of forms for static sites"
---


Some of the more interesting form services.

| Service                                                         | Max per month | Other                                                                                                                           |
| :-------------------------------------------------------------- | :------------ | :------------------------------------------------------------------------------------------------------------------------------ |
| [Formspree](https://formspree.io/)                              | 50            | Limited antispam on the free tier. The big name in forms but probably not the best. Promoted by Cloudflare.                                                                      |
| [Form Submit](https://formsubmit.co)                            | Unlimited    |  Very easy setup and no registration required. Great service with lots of options, inc. sending forms to multiple email services, hiding your email from spam. I found there was a delay of about 10 minutes before receiving the email at the given email address.                                                  |
| [Air Form](https://airform.io/)                                 | Unlimited     | Similar to Form Submit with an easy setup and no registration required.                                                         |
| [Formspark](https://formspark.io)                               | 250           | Limited free tier. Paid tier is a one off of $29 forever.                                                                       |
| [Form Taxi](https://form.taxi/)                                 | 40            | Send to up to 5 email address. Multiple antispam protections, monthly archive, custom success page.                             |
| [Get Form](https://getform.io/)                                 | 50            | Limited free tier.                                                                                                              |
| [Fab Form](https://fabform.io/)                                 | 100           | Limited free tier. Paid tier is a one off of $29 forever.                                                                       |
| [Basin](https://usebasin.com/)                                  | 50            | 1 form endpoint, 50 submissions /mo, 90-day data retention, basic spam protection, AJAX support, and Zapier integration.        |
| [Jotform](https://www.jotform.com/)                             | 100           | Up to 5 forms with 100 fields each. Includes Jotfrom branding                                                                   |
| [Netlify](https://netlify.com/)                                 | 100           | Good easy to setup and no branding. Several layers of optional antispam controls. Can only be used for sites hosted by Netlify. |
| [Serverless Formsy](https://github.com/lexoyo/serverless-forms) | Unlimited     | A free, self hosted solution that can be set up with one click on Heroku.                                                       |


