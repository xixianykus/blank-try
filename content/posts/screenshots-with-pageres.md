---
title: "Screenshots With Pageres"
title: "Screenshots w Pageres"
date: 2023-03-14T09:56:04Z
draft: false
photoswipe: false
unfinished: false
categories: ["CLI"]
tags: ["screenshot", tools, npm]
summary: "How to use Pageres: a CLI program for grabbing screenshots of any website."
---

[Pageres](https://github.com/sindresorhus/pageres-cli) is CLI tool that grab a screenshot of any website and in any size or format. Whether you want a mobile version or full size page you can choose the width you want and you can choose whether you want the whole page (from header to footer) or just what's visible in the viewport, ie. 'above the fold'.

The app was created by [Sindre Sorhus](https://sindresorhus.com/) who has created 1000 npm packages plus many other things.

You can install the tool globally with [npm](/posts/npm/):

```bash
npm install --global pageres-cli
```

## Usage

For a screenshot Pageres takes one obligitory argument and that is the url of the site you wish to screenshot:

```bash
pageres https://blank-try.netlify.app/posts/npm/
```



This will take a screenshot of the whole page as a PNG and save in the current folder. The docs say the default size is 1366x768 but as you can see below the height is irrelevant as it gets all of the page *unless* you use the `--cropped` or `-c` flags (see below).

You can also take multiple screenshots of different sites with one command. Just add multiple urls along with multiple resolutions.

```bash
pageres http://example.com https://learnhugo.ml/ 500x800 1024x768

or

pageres [http://example.com 500x800] [https://learnhugo.ml --crop --darkMode --format=jpg]
```

![blank-try](/img/blank-try.netlify.app!posts!npm-810x1080.png)

If you want a different resolution just add it as a argument in the format `1280x800` for example.

If you want just the above the fold part of the page, you add the `--crop` flag which can also be written as `-c`. And you can specify the size of your outputted image using the `--scale=` flag. 

```bash
pageres https://morelearning.ml/ 1280x800 --scale=0.5 --crop
```

![More Learning website](/img/morelearning.ml-1280x800-cropped.png)

You can even specify that you want the dark mode version of a site with the `--darkMode` flag.

```bash
pageres https://morelearning.ml/ 1024x768 --darkMode
```

![More Learning in dark mode](/img/morelearning.ml-1024x768-cropped.png)

## Wrapping up

Pageres is an extremely capable free tool if you need to get screen shots of any website on the web or on your own machine. There are many options you can read about on the [Pageres home page](https://github.com/sindresorhus/pageres-cli). I've only looked at those that I find most useful. In addition you can get screenshot of elements on a page 

These are:

| command/flag                                                  | what it does                                                                                                                    |
| :------------------------------------------------------------ | :------------------------------------------------------------------------------------------------------------------------------ |
| `pageres --help`                                              | The help file which is very useful                                                                                              |
| `pageres http://example.com/`                                 | Grabs the *full* page at the default resolution of 1366x768                                                                     |
| `pageres http://example.com/ 900x1080`                        | Grabs the *full* page at specified resolution                                                                                   |
| `pageres http://example.com/ 900x1080 --crop`                 | Grabs the top part page cropping at 1080 pixels. Can use `-c` instead.                                                          |
| `pageres http://example.com/ --darkMode`                      | Screenshot of the page in dark mode (if it has one set)                                                                         |
| `pageres http://example.com/ --format=jpg`                    | Change the output format to JPEG from the default PNG                                                                           |


Somewhat less useful for me include:

| command/flag                                                  | what it does                                                                                                                    |
| :------------------------------------------------------------ | :------------------------------------------------------------------------------------------------------------------------------ |
| `pageres http://example.com/ --filename=website-1`            | Specify you're own file name. If you want the date or url set by Pageres us `<%= url %>` or `<%= date %>` after the equals sign |
| `pageres http://example.com/ --css=body {color: blue;}` | Apply custom CSS.                                                                                                               |
| `pageres http://example.com/ --css=custom.css`                | Use a custom CSS file.                                                                                                          |
| `pageres http://example.com/ --selector=footer`               | Choose a DOM element and just get an image of that                                                                              |
| `pageres http://example.com/ --hide=footer`                   | Hides a specific element. You can use this flag multiple times to hide multiple elements                                        |


Type `pageres --help` for the full list or see the [pageres github page](https://github.com/sindresorhus/pageres-cli).