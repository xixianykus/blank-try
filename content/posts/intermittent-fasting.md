---
title: "Intermittent Fasting"
date: 2023-01-05T06:18:39Z
draft: false
photoswipe: false
categories: ["health"]
tags: ["health"]
summary: "3 ways to begin intermittent fasting"
extlinks:
- [https://www.shortform.com/pdf/fast-feast-repeat-pdf-gin-stephens, "Summary of Feast, Fast, Repeat", at Shortform.com]
---

![Feast, Fast, Repeat](/img/feast-fast-repeat.jpg)
{.float-right}

Gin Stephens is an author on intermittent fasting. She has written two books, *Delay don't Deny* (2016) and *Fast Feast Repeat* (2022). Her preferred approach is is using *time restricted eating* which is where you reduce your *eating window*&mdash;the time in which you eat each day&mdash;to just a few hours each day. For instance you might eat only between the 4 hours from 3pm to 7pm. This would be a 20/4 protocol where you fast for 20 hours and eat within the 4 hour window.

One of the most important things for her is the idea of the *clean fast*. This means that during the time you are fasting you don't have anything that could could cause an insulin spike and this includes even things like low or zero calorie substances like sugar free chewing gum or drinks with artificial sweetners. In short you can have water, black tea or black coffee. The bitter taste of tea and coffee is what makes them OK. Keeping to this makes it easier to fast for longer.

To begin fasting she suggests using something she calls the *FAST start*. This is a period of 28 days where your body gets used to intermittent fasting. The main idea is that during this time you body will deplete all it's glycogen stores and be forced to switch to it's fat burning mode where keytones from fat are used for energy. Because of the typical 3 meals per day plus snacks we generally eat our bodies aren't used to using their natural fat burning metabolism. So the 28 day *FAST start* is a period to train our bodies to adapt. 

She has 3 slightly different ways of enacting this *FAST start*:

1. The Easy Does it approach
2. The Steady Build approach
3. The Rip off the Band Aid approach


## The 3 approaches

### 1. Easy Does It

| Period       | Window length | Protocol | Notes                                                                                      |
| :----------- | :------------ | :------- | :----------------------------------------------------------------------------------------- |
| Days 1 - 7   | 12 hours      | 12/12    | Low carb ease in breakfast, low carb ease in lunch, regular dinner                         |
| Days 8 - 14  | 10 hours      | 14/10    | Low carb ease in breakfast or early low carb ease in lunch, low carb snack, regular dinner |
| Days 15 - 21 | 8 hours       | 16/8     | Low carb ease in lunch, regular dinner                                                     |
| Days 16 - 28 | 6 hours       | 18/6     | Low carb ease in lunch OR low carb snack, regular dinner                                   |

Gin says:

> In this approach, you’re starting out in week 1 with three meals a day in a twelve-hour eating window. Notice that this plan includes some low-carb “ease-in” meals that will help your body lower insulin levels (our bodies release less insulin in response to lower-carb meals) while still eating three times a day at first. 
> 
> Have a low-carb breakfast and a low-carb lunch, and then eat the type of dinner that you’re used to (dinner doesn’t need to be low-carb unless you have already been living a low-carb lifestyle; remember that other than these low-carb ease-in meals, we aren’t changing what we eat during the FAST Start, only when we eat).
> 
> Each week, you tighten up your eating window by a couple of hours, until you finally end up with a window of about six hours containing either two meals or a snack and a meal.
>
> Gin Stephens in Eat, Fast, Repeat


### 2. Steady Build


| Period       | Window length | Protocol | Notes                  |
| :----------- | :------------ | :------- | :--------------------- |
| Days 1 - 7   | 8 hours       | 16/8     | Lunch, dinner          |
| Days 8 - 14  | 7 hours       | 17/7     | Lunch OR snack, dinner |
| Days 15 - 21 | 6 hours       | 18/6     | Lunch OR snack, dinner |
| Days 16 - 28 | 5 hours       | 19/5     | Snack,  dinner         |

Of this approach she says:

> In this approach, you start off skipping breakfast on day 1, and BOOM! You’re doing it! Stick to clean-fast-approved beverages all morning, and eat your typical lunch followed by your typical dinner within an eight-hour eating window.
>
> Each week, you shorten your eating window by an hour, and you end up in week 4 with an eating window of five hours that contains a snack and a meal.
> 
> Gin Stephens in Eat, Fast, Repeat


### 3. Rip off the Band Aid

| Period       | Window length | Protocol | Notes                  |
| :----------- | :------------ | :------- | :--------------------- |
| Days 1 - 7   | 6 hours       | 18/6     | Lunch, dinner          |
| Days 8 - 14  | 6 hours       | 18/6     | Lunch OR snack, dinner |
| Days 15 - 21 | 5 hours       | 19/5     | Lunch OR snack, dinner |
| Days 16 - 28 | 4 hours       | 20/4     | Snack,  dinner         |


> In this approach, you are maximizing your fasting time starting on day 1, with a six-hour window and therefore eighteen hours of daily fasting. The first week you skip breakfast and eat two meals, and over the course of weeks 2 and 3, you can choose to either eat lunch or transition lunch into more of a snack. In week 4, you end up with an eating window of four hours that contains a snack and a meal.
> 
> Gin Stephens in Eat, Fast, Repeat