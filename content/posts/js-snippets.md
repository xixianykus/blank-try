---
title: "JavaScript Snippets"
linktitle: "JS Snippets"
date: 2024-05-03T09:14:31+01:00
draft: false
photoswipe: false
unfinished: false
categories: [JavaScript]
tags: [JavaScript, VS Code]
summary: A list of useful JavaScript snippets from within VS Code
style: |
  table {
    border-collapse: collapse;
    width: fit-content;
    font-family: var(--mono-fonts);
  }
  th {
    border-bottom: solid 2px currentColor;
  }
  tr:nth-child(2n + 2) {
    background-color: #0004;
  }
  th, td {
    padding: 0.5em;
    vertical-align: top;
  }
  td:first-child {
    font-weight: 500;
    font-size: 1em;
  }
  td:nth-child(3) {
    whitespace: pre;
    font-family: var(--mono-font);
    font-size: 0.9em;
    color: goldenrod;
  }
---

The VS Code snippets for JavaScript are less obvious than the CSS or HTML emmet ones but they can save a lot of typing. And unlike AI assistants they're faster and predictable (once you know them.)

So here's a partial list of some of the most useful .. first published on YAMLsite.


| snippet | what it does       | code produced                                                        |
| :------ | :----------------- | :------------------------------------------------------------------- |
| al      | alert()            | alert('msg');                                                        |
| co      | confirm()          | confirm('msg');                                                      |
| pm      | prompt()           | prompt('msg');                                                       |
| ae      | addEventListener   | document.addEventListener('load', function (e) { <br>// body<br> }); |
| clg     | console.log        | console.log()                                                        |
| gi      | getElementById     | document.getElementById('id');                                       |
| qs      | querySelector      | document.querySelector('selector');                                  |
| qsa     | querySelectorAll   | document.querySelectorAll('selector');                               |
| fn      | function           | function methodName (arguments) { <br> // body <br> }                |
| afn     | anonymous function | function(arguments) { // body }                                      |
| anfn    | arrow function     | (params) => { <br><br>}                                              |
| nfn     | named function     | const name = (params) => { <br><br>}                                 |
| fre     | forEach() loop     | array.forEach(currentItem => {<br><br>});                            |
| sto     | setTimeout         | setTimeout(() => {<br><br>}, delayInms);                             |
| sti     | setInterval        | setInterval(() => { <br><br> }, intervalInms);                       |
| ca      | classList.add()    | document.classList.add('class');                                     |
| ct      | classList.toggle() | document.classList.toggle('class');                                  |
| cr      | classList.remove() | document.classList.remove('class');                                  |
| sa      | setAttribute()     | document.setAttribute('attr', value);                                |
| ra      | removeAttribute()  | document.removeAttribute('attr');                                    |
| ga      | getAttribute()     | document.getAttribute('attr');                                       |
| cel     | createElement()    | document.createElement(elem);                                        |
| jp      | JSON.parse         | JSON.parse(obj);                                                     |
| js      | JSON.stringify     | JSON.stringify(obj);                                                 |

See the [full list]




[full list]: https://marketplace.visualstudio.com/items?itemName=xabikos.JavaScriptSnippets