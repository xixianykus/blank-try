---
title: "How to Learn New Things"
date: 2021-12-01T09:51:35Z
draft: false
unfinished: false
categories: ["Web dev", dev]
tags: ["learning"]
toc: true
summary: "A 10 step programme to learn any new thing, like a programming language for instance."
extlinks:
- [https://simpleprogrammer.com/about-simple-programmer/, Simple Programmer, "John Z Sonmez's personal site w lots of good info."]
css:
- "img[alt='Soft Skills book'] {transform: rotate(-5deg); box-shadow: 5px 5px 5px #0003; width: max(20vw, 300px); margin-right: 4vw;}"
---

So this page is based on ideas from the excellent book Soft Skills by John Z. Sonmez. The subtitle gives more insight into the book: *the software developer's life manual*.

This section of the book explains that devs need to be really good at learning because new things are coming along all the time so to keep one's skills current knowing how to learn is really important. However this method can be used to learn anything.


There are 10 steps. The first 6 you need only do once. The last 4 are repeated as needs be. You might need all 10 and Sonmez encourages one to change these as one needs to.

![Soft Skills book](/img/soft-skills-2.jpg)
{.float-right}

## The 10 Steps

1. Get the big picture
2. Determine scope
3. Define success
4. Find resources
5. Create a learning plan
6. Filter resources
7. Learn enough to get started
8. Play around
9. Learn enough to do something useful
10. Teach


### 1. Get the big picture

This is about getting an idea about the whole topic: what it involves and what you're going to learn. You may well not need everything but getting an overview allows you to choose the 20% that you really need to know.

Do research on the internet or from books, skim reading to find out what the topic involves.

### 2. Determine scope

When you have an idea of the *breadth* of the topic you are learning the next stage is to break it down into manageable pieces. You can't learn everything at once and probably won't need to anyway. So you have to decide which pieces you want to learn. 

Being specific and goal driven is helpful. From the book:

| Original topic    | Properly scoped topic                                                     |
| :---------------- | :------------------------------------------------------------------------ |
| Learn C#          | Learn the basics of C# language needed to create a simple console app     |
| Learn Photography | Learn to shoot portrait pictures                                          |
| Learn Linux       | Learn how to set up and install Ubuntu and how to use it's basic features |

The book goes on to say:

> You might be tempted to make your scope bigger and less focused, because you want to learn about the different subtopics in your topic area, but  resist the temptation and try to be as focused as possible. You can only learn one thing at at time. You can always come back later...

You can also decide what to learn depending on how much time you have. If you have a lot of time you can learn more but if not you need to be stricter and maintain a tight focus.

### 3. Define success

You need to know when you have accomplished your learning. So define a list of specific criteria before starting.

Again from the book

| Bad success Criteria                    | Good success criteria                                                                                |
| :-------------------------------------- | :--------------------------------------------------------------------------------------------------- |
| I can take good pictures with my camera | I can go through all the features of my camera, describe what they are and why and when to use them. |
| I can learn the basics of C#            | I can build a small app in C# that makes use of all of the major language features                   |
| I can build web pages in HTML           | I can create my own homepage that displays my resume and sample work on the internet using HTML 5    |
| I know some basic Javascript            | I know how different ways create Javascript interactions that change the CSS of HTML elements.       |



The books says: 

> Just make sure it's something that you can evaluate at the end of the process to be sure you met the objective.

### 4. Find resources

This one is easy. Find as many resources for learning as you can. Don't worry about quality for now. Just get links to content whether blogs, tutorials, videos or even books specific to what you want to learn.

### 5. Create a learning plan

Most subjects are taught in specific logical order. By skimming through some of the above you can get an idea of what order to learn in. This is a bit like creating the contents of a book, a list of the different chapters and/or sub-chapters. This is your learning plan.

### 6. Filter resources

So this is going back through your list in step 4 and selecting a small subset of resources to focus on. Which ones are best suited to you. Who are the best teachers? Which books get the best reviews?

---

The next four steps are to be repeated for each topic or subtopic you want to learn. The basic idea is:

**Learn, Do, Learn, Teach.**


### 7. Learn enough to get started

The key word is *enough*. The idea is you learn just enough to get going. The common mistakes are not learning enough first or spending too much time pouring over the documentation before getting started.

### 8. Play around

This is a better way of learning because by trying things out you discover errors, create questions for yourself and when you figure out the answers will remember them much better than dry reading of the documentation.

> For a new technology or programming language you might create a small project during this step and test things out.

Make a list of the questions you have so you can go through them and find the answers in step 9.

### 9. Learn enough to do something useful

Answering the questions you formulated in step 8. Use the resources amassed in step 4 to find answers but you can play and experiment more as required. Keep an eye on your success criterea defined in step 3.

> Each module you master should in some way move you forward toward your final destination.

### 10. Teach

> if you want to learn a subject in depth, if you really want to gain understanding about as subject, you have to teach it. There's no other way.

You can teach in a variety of ways from blog posts to posting online videos or teaching a group of devs, helping out on forums or other sites like dev.to.



