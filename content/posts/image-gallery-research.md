---
title: "Hugo gallery"
date: 2021-05-18T10:34:22+01:00
draft: false
unfinished: true
categories: ["css"]
tags: ["hugo", javascript, image processing, image gallery]
summary: "Hugo gallery is an old image page generator. Plus alternatives"
---


## Hugo Gallery

[Hugo Gallery](https://github.com/icecreammatt/hugo-gallery) is an image generator that will create a new folder with markdown files for each image in a named folder.

Version 1.0 was released in 2016 and not been updated since.

Appears simple to use:

`hugo-gallery <Source Path> <Destination Section> <Title> [BaseUrl]`

Real example:

`hugo-gallery static/images/vacation-photos hawaii "Hawaii Trip"` will produce a new folder called *hawaii* at `/content/hawaii`.

### The bad

There are [no install instructions](https://github.com/icecreammatt/hugo-gallery/issues/13). It's built primarily in Go with some shell scripts so you probably need Go installed as a minimum.

There is [some help](https://discourse.gohugo.io/t/hugo-gallery-setup-help/26276) in the forums but still does not say if it requires Go to be installed.

Though from one commentator on GitHub it looks like it does:

> I already realized that I have to run `go build hugo-gallery.go`
>
> [link](https://github.com/icecreammatt/hugo-gallery/issues/13#issuecomment-391731101)


## Some Image Gallery software

1. [Photoswipe](https://photoswipe.com/) is tested here. ([CDN link](https://www.cdnpkg.com/photoswipe)).
2. [BaguetteBox JS](https://openbase.com/js/baguettebox.js) looks worth trying, available via [CDN](https://cdnjs.com/libraries/baguettebox.js)
3. [Photo Grid Gallery](https://www.cssscript.com/photo-grid-gallery/)
4. [Smart Photo](https://www.cssscript.com/accessible-mobile-friendly-image-viewer-smartphoto/), similar to Photogrid.
