---
title: "Responsive Design"
date: 2021-05-04T05:47:55+01:00
draft: false
categories: ["css"]
tags: ["css", responsive design]
summary: "Notes about making this site responsive"
---

This site is now pretty responsive.

## Code Blocks

The last main step was making the code blocks collapse using `white-space: pre-wrap`. (Interestingly I'd tried using `white-space: wrap` which is wrong. I was thinking of flexbox value. It's actually `normal`). There is also the value of `pre-line` which appeared to unindent any indentation, a different way to save space. But I went with `pre-wrap` and reduced the font size too which was on the large size.

## Mobile Menu

The reponsive side menu was straightforward enough. It was based on a [tutorial](https://www.w3schools.com/howto/howto_js_side-nav.asp) from W3 Schools and used minimal Javascript.
The JS is used to change the CSS width of the container from 0 to 100%.

```js
/* Open the side-nav */
function openNav() {
    document.getElementById("mySidenav").style.width = "100%";
  }
  
  /* Close/hide the side-nav */
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }
```

These are accessed through a simple `onClick` attributes:

```html
<a href="javascript:void(0);" onClick="openNav()" class="icon">

<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
```

Another change was simply hiding some elements using a class of `desktop`. These use `display: none` until the viewport get's big enough to fit them in.

## Heading sizes

I was surprised that the heading sizes didn't change from the clamp size set in `rem` and `vw`'s, even when this was set in `:root` and headings were set in `em`'s or percentages.

```css
:root {
font-size: clamp(1rem, 0.9120rem + 0.2817vw, 1.25rem);
}
```

## Ctrl + Shft + M

Great shortcut in Firefox to switch the viewport to responsive mode and back. Works in Chrome but only when the dev tools are open.

## Responsive Changes

To sum up different methods to make a site responsive here is a list:

1. Hide elements / information blocks entirely. Mobile users probably won't need everything and too much info becomes clutter and probably won't be read.
2. Lay things out vertically rather than horizontally.
3. Scalable font-sizing using `clamp` and the [clamp tool](/posts/clampsizer/).
4. Use media queries, though don't overuse. Instead other methods like flexbox and grid to change layouts more smoothly.
5. Links should be `display: block` for touch screens. Adding some padding will make the contact area larger too.
6. Make images responsive using `max-width: 100%` or `width: 100%` (this can enlarge the image too).
7. Use different sized images using `<picture>` with `srcset`.


