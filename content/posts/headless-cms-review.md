---
title: "Headless CMS Review"
date: 2021-10-27T22:03:19+01:00
draft: false
photoswipe: false
categories: [CMS]
tags: ["hugo", Netlify CMS, CMS]
summary: "Jon D Jones best 5 headless CMSs"
css:
- ".warning {border: solid 4px currentColor; padding: 1em}"
extlinks:
- [https://www.jondjones.com/, Jon D Jones, Good site with lots of info on Jamstack, Netlify, CMS etc.]
- ["https://www.youtube.com/watch?v=_VwurOVFRZY", Stop using Wordpress in 2021, Eloquent Youtube vid by ODi productions]
---

**Note on Hugo**  
Unfortunately, Hugo cannot create Hugo pages (i.e. with permalinks) from Data yet. If you want to use an API-based CMS wwith Hugo you are limited to only displaying the content in a page. By [OlafG1 on Reddit](https://www.reddit.com/r/Strapi/comments/ns5eh3/hugo_integration/)
{.warning}


This is based on a video by Jon D Jones (see below).

## 1. [Contentful](https://www.contentful.com/)

1. SaaS
2. Uses a REST API
3. There is a free tier, single space for up to 5 users.
4. Lots of plugins to integrate with other services like Shopify
5. best design, sleek and minimal

Note after the free tier there is a big price jump to $500 pcm.


## 2. [Strapi](https://strapi.io/)

A CMS for people who hate SAS. Open source. There are only about 10 or 12 self-hosted CMS's including Wordpress.

> Strapi is hands down the best option out there.
> 
> Jon D Jones on non-SaaS CMS's

Self hosted is free. Requires a database and other [build requirements](https://docs.strapi.io/developer-docs/latest/setup-deployment-guides/deployment.html). Strapi has a [list of hosting providers] some which have free tiers such as [Render]

There is also a cloud based option in the pipeline.

Limited plugins but include GraphQL, email...

Claim to be the most customizable CMS too.

[Strapi tutorial] by Laith Harb


## 3. Contentstack

This is for enterprise level CMS, cost around 100k per year.

## 4. [Storyblok](https://www.storyblok.com/)

1. Inline editing experience
2. SAS but has a free plan: “Free forever: for developers, freelancers and small businesses” for one *seat* (user). Additional *seats* are &euro;9 pcm.

## 5. [Netlify CMS](https://www.netlifycms.org/)

1. Git powered only. 
2. Easy install.  Choose your static site generator and click install. This will create a Github repo with all the files plus hosted on Netlify.


## What about [Prismic] and [Sanity]?

> I like [Sanity] and [Prismic], however, the marketplace/thrid-party integration support its no where as good in them.  This means more dev effort if you want to integrate things like shopify, optimizely, google analytics etc...  Contentful's environment feature is also better than the others IMHO.  As a dev team we use the environments on every build.  Most of the clients we have deemed Contentful/Sanity have preferred Contentful's UI, however, this last point is more subjective.
>  
> Jon D Jones

- **[Prismic]** *community* tier is free with unlimited api calls and 100gb of CDN bandwidth pcm.
- **[Sanity]** free tier is more restrictive. 500k CDN api requests, 100k api requests, 10gb bandwidth and 5gb assets, 3 non-admins, unlimited admins. (api, CDN and bandwidth is doubled with Syntax offer - for how long though?)


## Other Git backed CMS's

1. [Publii](https://getpublii.com/) is a desktop app that allows you to make changes locally before committing to origin.
2. [Cloudcannon](https://cloudcannon.com/) is a cloud based CMS. It has a free for devs option that allows 3 users. It's also advertised and an 'all in one platform for modern static sites' so can be used to do everything. Once off the free tier it costs $250 pcm.
3. [Tina](https://tina.io/) is an inline editor. It currently still in development. It is cloud based but currently free and sounds like it will continue with a free tier later. Tina connects to git repos via a Graph QL API.

## Other API driven CMS's

## SaaS (Caas)

SaaS is Software as a Service and CaaS is Content as a Service

1. [Graph CMS] The free forever tier allows 1m API calls and 100gbs of data. All based on GraphQL.

### Open Source self-hosted

2. [KeystoneJS] is another open source alternative to Strapi that runs on GraphQL. It's free and so requires hosting somewhere.
3. [Cockpit] another open source offering. Requires PHP 7.1 or more, SQLite or MongoDB, Apache with mod/rewrite enabled.

{{< music/yt "aDFgrOlHR_s" >}}


[list of hosting providers]: https://docs.strapi.io/developer-docs/latest/setup-deployment-guides/deployment.html#hosting-provider-guides
[Render]: https://render-web.onrender.com/
[Strapi tutorial]: https://www.youtube.com/watch?v=vcopLqUq594
[Prismic]: https://prismic.io/
[Sanity]: https://sanity.io/
[Graph CMS]: https://graphcms.com/
[KeystoneJS]: https://keystonejs.com/
[Cockpit]: https://getcockpit.com/