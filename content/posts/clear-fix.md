---
title: "Clear Fix"
date: 2021-06-08T19:53:13+01:00
draft: true
photoswipe: false
categories: ["css"]
tags: ["css", clearfix]
summary: "A short summary about the page"
---

I intend to write summat about the `clearfix` method of escaping floated elements but for now here's some links:

1. [W3 Schools](https://www.w3schools.com/Css/css_float_clear.asp)
2. [What does clearfix do in CSS](https://stackoverflow.com/questions/9543541/what-does-the-clearfix-class-do-in-css) stackoverflow