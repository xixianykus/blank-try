---
title: "5 Golden Graphic Design Rules"
date: 2021-10-22T16:55:12+01:00
draft: false
unfinished: true
categories: ["graphic design"]
tags: [graphic design]
summary: "Rules always used by Yes I'm a Designer"
extlinks:
- ["https://www.youtube.com/watch?v=rfmJI8BqxC8&list=PL0EqspLrOAxRX_IeXFX1jcaqHKCc45IJx", 10 part graphic design theory series, by Yes I’m a designer]
---

This one of a ten part video series by [Yes I'm a Designer](https://yesimadesigner.com/) on Design theory. (Full video and link to the series below)

## 1. Contrast

1. Helps viewer focus on the most important details
2. Separates elements and perhaps add depth
3. Low frequency backgrounds (eg blurred, one color, tone, hue etc.)

For more on contrast see the other 2 videos: [Contrast pt.1](https://www.youtube.com/watch?v=N5cmtv3WlWI), [Contrast pt.2](https://www.youtube.com/watch?v=nEl2mT3Da4c)

## 2. Diagonals

Diagonal lines help break up the rigid rectangular formats of webpages, pages, posters, cards etc.

Using italics can help legibilty as they're straight up when placed on a diagonal.

Look for diagonal lines in images too.

## 3. Balance

For narrow aspect ratio vertically you generally need centered designs (though this doesn't mean perfectly symmetrical). Wider formats allow left or right alignments creating tension. Don't rely on the most obvious ways of achieving balance. The more subtle ways are usually more effective.

## 4. Depth

Everything is 2D whether print or screen. Establishing depth makes images more interesting - like the viewer can enter the image.

Out of bounds images where part of the image breaks out of the frame.

Make images merge into the background colour thus placing them in the back with the text in front.

Avoid *kissing details*. This is where to elements touch each other. It's not clear which is supposed to be in front or which is in the back.


## 5. Unity and harmony

1. repeating elements
2. Rhythm - alternate size and colour, angle of rotation and other parts of repeating elements
3. closure - when part of an image is missing your brain can automatically fill in the gaps
4. text formatting
5. grouping


{{< music/yt "66rpI8oePyw" >}}