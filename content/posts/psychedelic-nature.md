---
title: "Psychedelic Nature Symbols"
date: 2021-08-27T09:22:11+01:00
draft: false
photoswipe: false
categories: ["css"]
tags: ["illustrator"]
summary: "List of symbols for psychedelic art and nature"
---

I like the psychedelic style posters of the sixties and beyond. This style was based on the much earlier art nouveau style. Some posters were almost copies of art nouveau posters. One of the biggest differences was the extremely bright and lurid colours used psychedelic art compared with the more muted and desaturated tones of art nouveau. The psychedelic style evolved and is definitely it's own style. Here is a list of symbols and styles that are commonly used in this style.

## Psychedelic symbolism

1. Bright fluro colours
2. the eye
3. a third eye
4. grotesque and exaggerated facial features
5. Merging: one thing merging into another
6. sunburst
7. fractals
8. splashes
9.  curvy lines (ornament and art nouveau styles)
10. frames (implied, with somoe padding or actual)
11. melting objects
12. mushrooms
13. Type: rounded and distorted, amorphous
14. skulls

## Nature symbols

1. birds
2. butterflies
3. trees
4. flowers
5. the sun
6. hills and landscape features

