---
title: "Email Services"
date: 2021-11-20T10:16:29Z
draft: false
photoswipe: false
unfinished: true
categories: ["css"]
tags: ["hugo"]
summary: "Some notes on email services"
---

When looking for an email service the main things to consider are:

1. Free to use.
2. POP 3 and preferably IMAP too.
3. Can you use your own domain name.
4. Encryption

It's hard if not impossible to find all of these in one deal. There are few free services that off POP3 or IMAP access and the encrypted services do not.

There are other factors to consider too. The importance of these will depend on your use case.

- How much storage?
- How many emails can you send per day?
- Maximum size for attachments
- Message filters
- autoreply 
- auto forwarding (useful if you move to a different service)
- built in encryption / works with other systems
- number of aliases
- use you're own domain
- green energy
- discount for non profits
- country where based (some have better privacy laws than others)
- desktop app
- mobile app

### [Proton](https://proton.me/mail)

Great interface, it has been around a while, has a free tier of 500mb storage. Easy to use PGP Encryption. The biggest encrypted email service. Since emailing other Proton users is automatically encrypted this fact is more useful the more people that use it.

There are limits on the free account: you can only have 3 folders and only send 150 messages per day.

No POP or IMAP access with a free account. The free account has a 500mb storage limit that can be upped to 1Gb if you get the mobile app. Can't use your own domain on the free tier.

An account also includes cloud storage and limited use of their VPN (can only use a few servers and cannot use bittorrent).

The standard pricing is &euro;4 per month (&euro;48 per year) for one user with a 5gb storage limit and 5 email aliases. Alternatively there is the *Professional* tier at &euro;6.25 (&euro;75 per year) which allows up to 5000 inboxes and a 5gb storage limit for each.

### [Tutanota](https://tutanota.com/)

Based Germany Tutanota offer a good free tier. They sport their own very strong encryption where the subject line is encrypted too. Perhaps the most secure service to communicate to other Tutanota users. But cannot use general PGP. 1Gb of free storage.

### [Skiff](https://skiff.com)

Recommended by [PC Mag](https://www.pcmag.com/reviews/skiff) but no pop3 or IMAP but there is auto forwarding. Free plan allows up to 10Gb and message limit of 200 per day. Uses Web 3.0 and is decentralised.

### [Posteo](https://posteo.de/en/)

Another German based service promising 100% green energy and a very green ethos (inc. no flights, organic veggie lunches). No free tier but costs 1EU pcm. 2Gbs of storage (then 0.25EU per Gb pcm). 2 Aliases plus more at 0.10EU per alias per month.

### [Preveil](https://www.preveil.com/)

Another top rated PC Mag recommendation.

### [StartMail](https://startmail.com/)

No free tier. Payment details needed for trial run too. Unlimited email aliases, pop 3 and IMAP access. Based in the Netherlands. No calendar or cloud storage.

### Migadu

This service allows you to host many accounts with different names. There is a maximum number of sent and recieved emails of 20 and 200 on this tier.

They used to have a free tier but now it's $19 per year for the *micro* tier. Free to try.

### [Zoho](https://zoho.com/)

You can use the *Free Forever* tier which does not include POP or IMAP any more. You can however use your own domain. There is a wizard to take you through the process. You might need to set up with the business plan at first then skip the payment step.

### [Mailo](https://mailo.com/)

Mailo allows 5 email aliases on the free tier from one email account. No IMAP or POP3 access on the free tier.

### GMX

GMX.com allows 10 email aliases, 65gb of storage, allows large attachments, has POP3 and IMAP.

## Alternatives

Another way is to use the email forwarding service [improvemx](https://improvmx.com/).

The free tier (which they pledge to keep free) allows only one domain name but 25 aliases. You have to first delete any previous MX records then replace them with two of their's.

Email attachments are limited to 10mbs.