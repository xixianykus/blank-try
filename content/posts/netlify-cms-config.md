---
title: "Netlify CMS config"
date: 2021-08-19T12:14:50+01:00
draft: false
categories: [CMS]
tags: ["Netlify", CMS, GitHub, npm, Decap]
toc: true
summary: "Setting up the config settings for Decap CMS"
extlinks:
- ["https://www.netlifycms.org/docs/configuration-options/", CMS confg options, from the official docs]
- ["https://jamstack.com/headless-cms/", Jamstack CMS List, Jamstack.com's and .org’s list of headless CMS’s]
---

The configuration file, `static/admin/config.yml`, can be fairly easy to quite complex depending on your site and what you want to do.

The first parts are straightforward enough but it's when you get to the `collections` section that thing *may* get trickier because there are a lot of petential options.

## Playing with the CMS config

It's much better if you can experiment with the settings on a local machine. This can be done using the *Netlify CMS Proxy Server*. The Netlify CMS docs say this is only in beta so far. I've used this with Hugo and found it worked pretty well for the most part. If the worst comes to the worst you just restart the servers (yes two of them).

It works by running two servers simultaneously: one for your site and one for the CMS. Changes made appear in your local version of the site only.

### Setting up the Netlify CMS Proxy server

First you need [Node/NPM](/posts/npm) installed on your machine. 

1. Add a new first line at the top level (ie. no indenting) to your CMS's `config.yml`[^1] file:

```yaml
local_backend: true
```

2. Make sure you are NOT running the Hugo server for your site. Then from a terminal set in the root of your project you type:

   ```bash
   npx netlify-cms-proxy-server
   ```

   You may be prompted to install the server so just hit `y` and it installs and starts in a few seconds. This starts on port 8081. 

3. Leave it running and open another terminal window and use that to start `hugo server` (again in the root of your project).

4. In your browser go to `localhost:1313` (assuming Hugo server is using that port number) add `/admin` to the address, `localhost:1313/admin`, and it you should be automatically logged in to the CMS if `config.yml` file is set correctly. See the Configuration section below.

### Using multiple config files

The default file for the CMS configuration is at `static/admin/config.yml`. However you can use a different file located anywhere if you add a link to it in the `<head>` of your CMS's `index.html` file.

```html
<link href="path/to/config.yml" type="text/yaml" rel="cms-config-url">
```

This means that you can experiment with different layouts and options while keeping 

## Configuration

So below is some code from the config file for Netlify CMS. This is found in the `/static/admin/config.yaml` file.

Note to allow new `pages` to be added in this configuration you have to remove the `files` entries and instead use a `folder` entry the same as `blog`. You cannot have both `files` and `folder` in the same *collection*.

You should be able to access this from localhost by simply adding `/admin` to the url. ie `localhost:1313/admin`. You can also add `local_backend: true` to the YAML file to allow the local repo to be accessed and edited by the CMS.

The option for `publish_mode: editorial_workflow` doesn't seem to work the way it appears in [the demo](https://cms-demo.netlify.com/#/workflow) for some reason.


```yaml
backend:
  name: git-gateway
  branch: master # Branch to update (optional; defaults to master)
display_url: https://example.netlify.app  # displays the url in the CMS interface
site_url: https://example.netlify.app # used by the CMS for various things
publish_mode: editorial_workflow # hopefully will allow the use of draft edits
media_folder: "static/images"
public_folder: "/images"  # this where images are found in the published site. img src= use this path.
collections:
  - name: 'blog'
    label: 'Blog'
    folder: 'content/blog'
    create: true
#    slug: '{{year}}-{{month}}-{{day}}-{{slug}}'
    slug: '{{slug}}'
    editor:
      preview: false
    fields:
      - { label: 'Title', name: 'title', widget: 'string' }
      - { label: 'Publish Date', name: 'date', widget: 'datetime' }
      - { label: 'Summary', name: 'summary', widget: 'string' }
      - { label: 'Body', name: 'body', widget: 'markdown' }
  - name: 'pages'
    label: 'Pages'
    files:
      - file: 'content/_index.md'
        label: 'Home Page'
        name: 'home'
        fields:
          - { label: 'Title', name: 'title', widget: 'string'}
          - { label: 'Body', name: 'body', widget: 'markdown'}
      - file: 'content/about.md'
        label: 'About Page'
        name: 'about'
        fields:
          - { label: 'Title', name: 'title', widget: 'string'}
          - { label: 'Body', name: 'body', widget: 'markdown'}
```

## Open Authoring

It's possible to allow the CMS to be open to anyone with a GitHub account. The changes they make are committed to a new branch to be accepted later. More [here](https://www.netlifycms.org/docs/open-authoring "Open authoring page on NetlifyCMS.org").

## Editing the CSS

You can add a stylesheet to the head of the `static/admin/index.html` file. Finding the elements to target can be tricky. They will typically require `!important` to be added.

Here's some CSS with *some* of the interface targetted:

```css
<style>
  @media screen and (prefers-color-scheme: dark) {
        body,
        #nc-root,
        .css-2oej7z-ToolbarContainer {
          background-color: rgb(34, 92, 252) !important;
        }
        .css-y7r3-AppHeader {
          background-color: rgb(56, 109, 253) !important;
        }

        /* background for new posts */
        .css-f3a0ud-NoPreviewContainer-card-splitPane {
        background-color: rgb(36, 98, 168) !important;}
      }

      /* the boxes */
      .css-5wgw3f-ListCard-card,
      .css-1hvrgvd-CollectionTopContainer-card-cardTop,
      .css-1gj57a0-SidebarContainer-card {
        background-color: rgb(40, 128, 228) !important;
      }
      
      .eab48an0:hover {
        background-color: rgb(101, 160, 228) !important;
      }
      h1, h2, h3, h4, h5, h6, p {
        color: rgb(209, 244, 247) !important;
        }

      /* New blog button */
      .css-1qfk5ut-CollectionTopNewButton-button-default-gray {
      background-color: rgb(255, 81, 0) !important;
      transition: .5s;
      }
      .css-1qfk5ut-CollectionTopNewButton-button-default-gray:hover {
      background-color: rgb(170, 26, 26) !important;
      }
</style>
```

[^1]: The default path for the CMS in Hugo is `/static/admin/config.yml` though the location can be changed.