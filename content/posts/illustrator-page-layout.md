---
title: "Illustrator Page Layout"
date: 2022-09-06T20:37:57+01:00
draft: false
photoswipe: false
unfinished: true
categories: ["graphic design"]
tags: ["illustrator"]
summary: "Getting round Illustrators lack of page grid"
---


With a page layout program like In Design it's very easy to create a grid for the page with multiple columns and gutters.

Unfortunately with Illustrator there is no such tool. It's designed for illustrations after all, not designing pages.

Here's a way to distribute 4 same sized items squarely on the page (artboard). The four items could be any repeating artwork or could just be rectangles to create a page grid. Unfortunately this is harder than using In Design but if you want a page grid in Illustrator this works a treat.

NB. If you use the distribute horizontal centers option Illustrator will place the two end objects right on the edge of the page. And if you try to align to a key object everything is centred in the middle.

![Four objects aligned on a page](/img/four-objects.jpg)

## The method

First a bit of maths.

Assume we're laying out 4 items on A4 paper (297mm wide).

Measure the width of one object. Call this width *x*. Now multiply this by 4. We need to add to that the width of 4 gaps. That is the 3 gaps between each item plus 2 half gaps at each end.

The width of the page for an A4 is 297mm. So 297mm - 4*x* gives you the total amount of space you have left over. Let's call this *y*.

With 4 objects there will be 3 gaps between them and 2 half gaps at either end. Since the ends will probably only want to be half as big as the gaps we can divide *y* by 4 to get the size of the 3 gaps between the objects. And the final quarter left over can be divided in two and used for the spaces at either end.

Next we need a new artboard. This needs to have a width of 297mm - *y*/4. Place the 4 objects on this artboard and press the distribute horizontal centers button. Select all 4 and group them together (Ctrl + G) and drag them back to the first artboard. Use the center vertical and center horizontal buttons to put them in the centre of the page. Finally delete the temporary artboard and you're done. 
