---
title: "Theme Switcher"
date: 2021-05-29T06:15:44+01:00
draft: true
photoswipe: false
categories: ["css"]
tags: ["hugo"]
summary: "A short summary about the page"
extlinks:
- ['https://medium.com/@haxzie/dark-and-light-theme-switcher-using-css-variables-and-pure-javascript-zocada-dd0059d72fa2', Dark & Light Theme Switcher, inc. how to persist via local storage.]
- ['https://stackoverflow.com/questions/8796107/how-to-make-changeable-themes-using-css-and-javascript', Stackoverflow answers, several solutions]
---

{{< codepen3 id="gOmGBZR" height="700" >}}

