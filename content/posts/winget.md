---
title: "Winget"
date: 2022-11-26T06:41:49Z
draft: false
photoswipe: false
unfinished: true
categories: ["Windows"]
tags: ["Windows", Package Managers]
summary: "How to use Winget, Windows built in package manager"
extlinks:
- [https://winstall.app/, Winstall, a way to browse apps available with Winget ]
---

Winget is a package manager that comes with Windows 10.

You can quickly see most[^1] apps installed on your machine with `winget list`. This will show you which apps have upgrades available.

Alternatively use `winget upgrade` to see only those apps that have upgrades available.

To upgrade all packages just use the `--all` flag: `winget upgrade --all` though BEWARE, it's not always accurate. 

You don't need to upgrade from an elevated prompt though doing so bypasses the Windows confirm dialogue box.

For help with any command use the `--help` flag after the command. For example: `winget show --help`.


| command   | description                                                       |
| :-------- | :---------------------------------------------------------------- |
| install   | Installs the given package                                        |
| show      | Shows information about a specific package                        |
| source    | Manage sources of packages                                        |
| search    | Find and show basic info of packages                              |
| list      | Display installed packages (inc. those not installed with Winget) |
| upgrade   | Shows and performs available upgrades                             |
| uninstall | Uninstalls the given package                                      |
| hash      | Helper to hash installer files                                    |
| validate  | Validates a manifest file                                         |
| settings  | Open settings or set administrator settings                       |
| features  | Shows the status of experimental features                         |
| export    | Exports a list of the installed packages                          |
| import    | Installs all the packages in a file                               |
| help      | Show these commands                                               |


## Winstall

[Winstall](https://winstall.app/) is a website listing all apps available with Winget. You can quickly select a bunch of apps, hit the *Generate Script* button and you get a simple batch script you can paste into the command line and download the apps.


[^1]: This doesn't show *all* apps installed with Chocolatey, though may show some of them.