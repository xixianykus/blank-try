---
title: "Svelte Notes"
date: 2022-01-02T18:50:14Z
draft: false
unfinished: true
categories: ["css"]
tags: ["Svelte"]
summary: "Some initial notes on using Svelte"
---

## What is it?

Svelte works in a similar way to JavaScript frameworks like React and Vue. But it's not a framework it's a compiler. This means when building it works in a similar way the final website or project is compiled down to plain HTML, CSS and JavaScript. This makes it faster than other *frameworks*. It also has a really nice, easy to learn syntax.

## Setting up

There are several ways to set up Svelte but the best probably at the moment is to use [Vite](https://vitejs.dev/).

Go your projects or website directory and type:

```bash
npm init vite@latest
```

You choose a name for your project and that will be the name of the folder created to house it in.

You then select the *framework* to use and choose `svelte` or `svelte-ts` if you want to use TypeScript.

Next change directories to that of your new project:

 ```bash
 cd new-project-name
 npm install # to install dependencies
 npm run dev
 ``` 

 Your new project should be running on `localhost:3000`.

 ## Directories

 You should then have directories and files like this:

```bash {linenos=false}
├── .vscode
├── node_modules/
├── public/
│    └── favicon.ico
├── src/ # source code - where you develop your app
│    └── svelte.png
├── .gitignore
├── index.html  # Main app file
├── jsconfig.json
├── package-lock.json
├── package.json
├── README.md
└── vite.config.js
```

See [MDN](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Svelte_getting_started#application_structure) for a detailed description of what the files are for.

In the `src` folder is a folder called `lib` and in this is a file called `App.svelte` and `main.js`

The `main.js` has the following:

```js
import App from './App.svelte'

const app = new App({
  target: document.getElementById('app')
})

export default app
```

In this case the whole of the Svelte app is inside an HTML element with the `id` of `app`. This should be in the `index.html` file: `<div id="app">`


## Sources for learning Svelte

[decode Lesson 3](https://www.youtube.com/watch?v=4xQpg0_CkdQ&list=PLVvjrrRCBy2KpGl3-s_ELqKd4hiNCN6yz&index=3) 24 lessons on Youtube.

- [Official Svelte tutorial](https://svelte.dev/tutorial) is pretty good with interactive coding.
- [Decode 24 videos on Youtube](https://www.youtube.com/watch?v=MgOpRVTFBa8&list=PLVvjrrRCBy2KpGl3-s_ELqKd4hiNCN6yz)
- [MDN](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Svelte_getting_started)
- [Free Code Camp: learn-svelte-in-5-minutes](https://www.freecodecamp.org/news/learn-svelte-in-5-minutes/)
- [Learn with Jason and Rich Harris](https://www.learnwithjason.dev/let-s-learn-svelte), 1hr 20 min video course
- https://www.asquero.com/tutorial/learn-svelte/ links to the best free courses
- [Build a todo app with svelte 3](https://freshman.tech/svelte-todo/)
- https://dev.to/alessandrogiuzio/my-svelte-journey-n17 personal dev.to series  about learning Svelte
- [freeCodeCamp course](https://www.youtube.com/watch?v=vhGiGqZ78Rs) on Youtube (54mins)
- [Traversy Media Crash course](https://www.youtube.com/watch?v=3TVy6GdtNuQ). 1hr 20 mins, October
- [Svelte with Graph QL](https://hasura.io/learn/graphql/svelte-apollo/introduction/) and intro to both technologies at hasura.io
- [Build a Svelte Landing Page](https://www.eternaldev.com/blog/getting-started-with-svelte-by-building-a-landing-page/)