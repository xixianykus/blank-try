---
title: "3d Text in Illustrator"
linktitle: "Illustrator: 3D text"
date: 2021-08-19T09:06:41+01:00
draft: false
photoswipe: false
categories: ["illustrator"]
tags: ["illustrator"]
summary: "Different methods for creating 3D text in Illustrator"
---

## 3D extrude

You can map art to 3d shapes but so far it seems a little clunky to apply.

First you have to make your art as a symbol. If you want a repeating pattern first make your pattern THEN apply it to a rectangle (or other shape. Then store that as a symbol.

You then have to edit the 3D via the appearance panel (for the whole group) then find the face you wish to apply the art to and apply it to that.


### IMPROVING IT


To start editing the text, go to Object > Expand Appearance. This will take the 2D text and make paths for the 3D text.

MORE AT: https://www.webfx.com/blog/web-design/a-guide-to-creating-3d-text-in-adobe-illustrator/

## 3d text like 123 Klan

### METHOD 1 - Appereance panel

{{< music/yt "jHphDQrwdzw" >}}

Perhaps the best, from Spoon Graphic tutorial....

1. Clear both the stroke and the fill from the text

2. In the appearence panel add a new fill layer to the text. Drag it beneath the text. Add a stroke colour.

3. Use the Transform to move it 1px on x and y. 

4. Choose the number of copies to fix the thickness


Another method is without stroke. Have 3 fill layers, one for the text face, one for the first bit of 3d (so looks like a stroke) and the third for the deeper part of the 3D effect.



### METHOD 2 Using the 3D effect

Use 3D extrude. Then expand appearance and maybe expand too.


### METHOD 3 - Using blends

1. Type out text and convert to outlines
2. Make two copies in the same place (Ctrl + C, Ctrl + F)
3. Hide the top copy and move the bottom copy to where you want the shadow
4. Blend the middle and bottom copies together. (Select both and press Ctrl + Alt + B)
5. Double click the blend tool to set the specified number of steps. Around 20 to 50 is usually good.
6. Expand the appearance (Object > Path > Expand Appearance)
7. Use the pathfinder paletter to merge all copies
8. Get rid of jagged edges: Go into outline mode (Ctrl + Y)
9. Zoom in and use the lasso tool to select all points you want to get rid of.
10. Go to Object > Path > Remove Anchor Points
11. Make the top layer visible


### METHOD 4 - Using a drop shadow

{{< music/yt "owDNq9dNulk" >}}

Remove fill from Characters in the Appearance palette and add to the Type section (above)


1. Add a drop shadow
2. Expand and Expand Appearance
3. Pathfinder merge
4. Delete some points with the pen and holes with the group selection tool

Type out text. (use a heavy or black font)

Make 2 copies of this with black fill and stroke

Separate these as you want the shadow block to appear. Create Outlines in both

Use the blend tool to blend them using maybe 10 or 20 steps (The jagged edges will be taken care of later and limiting the number will speed up the process).

Use object expand appearance

Use the pathfinder to blend together to one

Use the pen tool to delete the points on the middle step

IF you used many steps in the blend...

	use the lasso to select then delete those points
	use Ctrl J to rejoin the open path
