---
title: "DNS Records"
date: 2021-10-26T08:12:08+01:00
draft: false
unfinished: false
toc: true
photoswipe: false
categories: [Web dev]
tags: ["DNS"]
summary: "All about DNS records, NS records, TXT records, nameservers, CNAME and more."
extlinks:
- [https://dnschecker.org/, DNS Checker, For getting all records of a domain name]
- [https://mxtoolbox.com/, MX Toolbox]
- [https://network-tools.com/nslookup/, Network Tools NS Lookup, Another way to get some DNS records.]
- [https://www.cloudflare.com/en-gb/learning/dns/what-is-dns/, "What is DNS?", "Cloudflare's docs are a good source of clear info"]
---

DNS records point domain names like a web address to the right web server or email server etc. There are different kinds of DNS record but the main ones are `A` records for web servers and `MX` records for mail servers.

There are various tools to look up these records. [MX Toolbox](https://mxtoolbox.com/) enables you to look up records for mail servers.


## NS Records

NS records point to your 'control centre'. That is where all the other records live. To change any other records you have to make those changes where your nameserver (NS) is hosted. Typically, if you buy a domain, they will host the NS records for you. However you can change this if needs be.

They're usually two lines that look something like these GoDaddy example:

`ns11.domaincontrol.com`
`ns12.domaincontrol.com`

For Cloudflare they might look like:

`jeff.ns.cloudflare.com`
`ann.ns.cloudflare.com`

There are always 2 nameserver records and sometimes there can be more.

## A records

An `A` record points to your website's hosting servers. The 'A' stands for *address* and it points to the ip address of the domain. When `example.com` is typed into an browser address bar the `A` record points it to an associated IPv4 address. (A different record exists where IPv6 is used. These are `AAAA` records.)

There are usually two fields for an `A` name. One for the domain and one for the ip address:

`www.example.com`
`123.538.4.1`

An `@` in the domain name field means the 'root' domain, so `example.com` in the above example.

You can have different `A` records for different sub domains which points to a different IP address OR go to the same IP but routed to a different web page.

Using `A` records you can have different subdomains with different hosting providers and different IP's.

A more complex example including the `@` and subdomains `mail` and `docs`:

```
Domain / Name
"@", "example.com", "mail", "docs"

IP address
187.459.23.482
```

NB. You must set up your site first BEFORE changing the A records to point to the new server.

**Tip:** write down the old A record IP address before making any changes to it in case you make a mistake and need to change back.

## AAAA

Also called quad A records these are the same as `A` records except they're used with an IP version 6 address.

## CNAME records

CNAME records refer to an alias or redirect for a specific site or subdomain.

All `CNAME` records must point to a domain, never to an IP address.

The two fields are:

1. Name: www
2. Destination: `@` or `"example.com"`

This points the www version of your domain to the straight version. These can also be used for email redirects.

Using 

`Name: *`
`@`

The asterick is wildcard for all subdomains. In this case all subdomains will point to your root domain.

NB. Some DNS hosts don't recognize the `@` symbol so you have to type out your full domain.


## MX records

`MX` stands for *mail exchange* and these tell an email where it's supposed to go: they point to your email hosting provider. 

Typically you have between 2 and 5 MX records for handling email.

If your email is hosted by G Mail, Zoho or other large providers the MX records will be probably be the same as everyone esle's.

The `MX` records have the following fields:

1. Name / Domain `"@" "example.com"`. This must be an `A` record, not a `CNAME` record for instance.
2. Mail server might have multiple entries: `"mx.zoho.com", "mx2.zoho.com", "mx3.zoho.com"`
3. Priority. There are usually several email servers and this value tells the email to go to say host1 first but if it's not available then send it to host 2. The values are relative to each other. With 3 MX records the values of 1, 2, 3 will be the same as 7, 18, 133.  takes an interger from 0 to 66,000. Usually it's 1, 10, 20, 50 etc. They can also be equal to one another: 20, 20, 20 for instance.
4. TTL Stands for Time To Live. Set to `automatic`.

## SPF, DKIM and DMARC records

These records are vital for preventing spam or phising attacks being sent from your domain. If this were to happen your domain could be blacklisted, a process that is very hard or impossible to reverse.

The service [EasyDmarc](https://easydmarc.com/) allows you to quickly check the SPF records of any domain.

## SPF Sender Policy Framework records

This allows mail providers like Yahoo or Zoho, to detect and prevent unauthorized mail sent on your domain's behalf. This allows you to specify one or more IP addresses which can send mail on your domain's behalf.

The syntax looks like this:

```bash
"v=spf1 include.spf.protection.outlook.com -all"
```

There are 3 parts to this.

`v=spf1` This is the spf version number. This should always be included.

`include.spf.protection.outlook.com` There 2 parts here. The `include` is the *mechanism* whilst the `spf.protection.outlook.com` bit is called the *directive*.

The last part, `-all`, also has two parts. The hyphen is the *qualifier*, whilst the word `all` is the directive. Here the qualifier is a fail so that any emails not part of the the qualified list will fail. Using a tilde, `~` instead of a hyphen is a *soft fail*. The email will be allowed but marked as *suspicious*. A question mark `?` is neutral and is saying your spf record is saying nothing about passing or failing. The email is thus accepted.

The hypen or fail option is always recommended.

### Mechanisms and directives

| Mechanism | Applies when..                                 | Example                                        |
| :-------- | :--------------------------------------------- | :--------------------------------------------- |
| `a`       | Authorize servers by domain name               | `a:autodicover.outlook.com -all`               |
| `mx`      | Authorize servers by another MX record         | `mx:inbound-smtp.us-east-1.amazonaws.com -all` |
| `IP4`     | Authorize servers by IPv4 address OR range     | `IP4:i192.168.0.4/22 -all`                     |
| `IP6`     | Authorize servers by IPv6 address OR range     | `IP6:i2001:db8;f168::/22 -all`                 |
| `include` | Authorize third party email services by domain | `include.spf.protection.outlook.com -all`      |

SPF records are usually created as TXT records though some providers may have separate SPF record choice.

Once created they're validated using an online tool such as [MX Toolbox](https://mxtoolbox.com/).


## DKIM or Domain Keys Identified Mail

Generally these don't need to be set unless you're setting up your own mail server. They are set by your email provider.

These records are used to prevent a message being altered in transit using a private / public key pair. The public key is stored as the DKIM record.

If the DKIM signature is valid the email will go to your inbox otherwise to spam or junk mail.


## DMARC or Domain-based Message Authentication Reporting and Conformance

This is used to determine the authenticity of an email message.

SPF and DKIM records must be set first before using.

```bash
"v=DMARC1;p=reject;"
```

The syntax is a series of *tags* separated by semi-colons. The first tag is the version number 

| Tag   | Required? | What it does                                                 | Example                              |
| :---- | :-------- | :----------------------------------------------------------- | :----------------------------------- |
| `v`   | Y         | The version of DMARC used. Currently there is only version 1 | `v=DMARC1`                           |
| `p`   | Y         | Policy or What to do with messages that fail DMARC           | `p=reject`, `p=none`, `p=quarantine` |
| `sp`  | N         | Like `p` but for subdomains                                  | `sp=reject`                          |
| `pct` | N         | Percentage of emails the policy applies to                   | `pct=100`                            |


{{< yt S6id_BPFHcY >}}

## TXT records

`TXT` records associate text with your domain and can be used with many things. For instance you can use them to verify with Google that you own your domain. They can be used with email verification or automation services to help deliverabilty.

There are two fields: Name and Content

The name might be your domain. Content could be many different bits of text

Name: `example.com`
Content: `google-site-verification-3450ww3497#`

## Pitfalls

If you are changing your NS records make sure your new host has all of your domain's existing DNS records FIRST (before changing the NS records.) If using Cloudflare they will automatically obtain all the DNS records with a site. If using another provider you can use [MX Toolbox](https://mxtoolbox.com/) to obtain some DNS records. You can't get CNAME records without knowing where they are. (Cloudflare accounts for DNS records are free and very easy to do.)

In Cloudflare you would:

1. Add your domain name to get the DNS entries added
2. Change the A record to the new website IP address (if needed)
3. Change the NS records at your domain registrar to point to your Cloudflare account.

## Simple site switchover

Just change the A name record from the old IP address to the new one.

## The 2 ways to change a website to a new hosting provider

1. Change the `A` name ip address
2. Change the `NS` servers

## Checking DNS Propagation

When DNS records are first set up or moved or changed it can take a while for the changes to propagate through the internet.

One way it to see how its going is use the free service [DNS Checker](https://dnschecker.org/). Simply type in your url, select which record you wish to check (typically the A or CNAME record for example) and you can see where things are at with your site.

## Find out More with DiG

On Linux based platforms (inc Git Bash for Windows), you can use the tool DiG to find out more about the configuration of your domain.

```bash
dig example.com CNAME
```

The above will give information about the status of the current site's DNS record, which IP address it points to and more.

{{< music/yt "YV5tkQYcvfg" >}}