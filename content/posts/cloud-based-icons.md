---
title: "Cloud Based Icons"
date: 2021-12-23T08:33:36Z
draft: false
photoswipe: false
unfinished: false
categories: ["svg"]
tags: ["icons", svg]
summary: "Setting up icons with free cloud based services"
extlinks:
- [https://www.w3schools.com/icons/fontawesome5_intro.asp, W3 Schools guide to Font Awesome, Another way to easily add icons to a website]
- [https://www.lambdatest.com/blog/its-2019-lets-end-the-debate-on-icon-fonts-vs-svg-icons/, Icon fonts vs SVG icons]
- [https://github.com/fontello/fontello/wiki/How-to-use-fontello-fonts-in-your-project, How to use Fontello in your project, The official docs on GitHub]
---

Setting up icons can be a bit tiresome at times but using cloud based services can make it very efficient.

Two used here are [Fontastic.me](https://app.fontastic.me/) and [Fontello.com](https://fontello.com/). Both can create an icon font file and fontastic can create an SVG sprite containing all your icons. Both allow you to select only those icons you need so you're not downloading anything extra.


## [Fontastic](https://app.fontastic.me/)

Fontastic is the easiest way to create and deploy SVG sprites[^1] I've found.

1. Go to Fontastic and sign up for an account.
2. Select the icons you want by clicking (the outline turns yellow).
3. Hit the *customize* tab and choose the letters and names you want associated with each icon.
4. Hit the *publish* tab and then the SVG Sprite sub tab and click publish.
5. Copy `<script>` code for file and add it to the `<head>` of your document.
6. Below in the *Icons reference* section select and copy the code for each icon and paste it into your HTML files.
7. Add some CSS. An easy way to do this is add another class name to the `<svg>` tags, for intance `icon`.
```svg
<svg class="icon icon-links">
   <use xlink:href="#icon-links"></use>
</svg>
```
8. Style your icons particularly the size and colour.
```css
.icon {
    fill: currentcolor;
    width: 1em;
    height: 1em;
    margin-right: .7ch
}
```

## [Fontello](https://fontello.com/)

Fontello is very similar to Fontastic but less commercial; there are no paid options at all and nothing to sign up to.

Rather than add a link to the `<head>` you download a kit which includes multiple font and CSS files and  a config.json file.

Unfortunately there is no SVG sprite option. There are 5 different font versions:

1. .eot
2. .svg which is an [SVG font](https://design.tutsplus.com/articles/what-is-an-svg-font-all-about-svg-fonts--cms-35110) file. [^1]
3. .ttf standard True Type.
4. .woff
5. .woff2 better compression and smaller file size than `.woff`.

In the file I used which comprised of 7 icons the `SVG` and `woff2` were the smallest, both at 4k.

So compared to [Fontastic](https://app.fontastic.me/) it's a bit more work to set up, you're hosting the icons yourself so a tiny bit more bandwidth but you're not dependent on an external source.

|               | Fontello          | Fontastic        |
| :------------ | :---------------- | :--------------- |
| **Icon sets** | Font Awesome      | Font Awesome     |
|               | Entypo            | Octicons         |
|               | Typicons          | Cittadino        |
|               | Iconic            | Streamline Icons |
|               | Modern Pictograms | Foundation Icons |
|               | Meteocons         | Entypo           |
|               | MFG Labs          | Steadysets       |
|               | Maki              |                  |
|               | Zocial            | Linecons         |
|               | Brandico          | Just Vector      |
|               | Elusive           | Social Icons     |
|               | Linecons          |                  |
|               | Web Symbols       |                  |
| **Cost**      | Completely free   | Free tier        |
| **CDN**       | No                | Yes              |
| **SVG**       | SVG font file[^2] | SVG sprite       |


## More

[Flaticon](https://flaticon.com/) is a big commercial site. The main pro here is the huge range of icons. But amongst the numerous downsides are:

- lots of advertising that can be hard to separate from the actual Flaticon site
- you cannot download SVG's and SVG sprites unless you have a paid account.
- so basically it's just PNGs.

See [Icon sources](/links/#icons) on the links page.


[^1]: SVG Sprites are SVG files that contain multiple images, in this case icons. With one http request all you icons are cached by the browser to be used throughout a website or app.
[^2]: SVG fonts are [currently considered](https://caniuse.com/?search=svg%20fonts) deprecated and only work on Safari browser.