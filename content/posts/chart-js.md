---
title: "Chart JS"
date: 2021-05-09T14:44:51+01:00
draft: false
categories: ["javascript", JS library]
tags: ["graphs", javascript]
summary: "My first graph with Chart JS and notes on basic use."
chartjs: true
extlinks:
- ["https://www.chartjs.org", Chart JS, The main site for Chart JS w lots of good info.]
- ['https://www.youtube.com/c/LevelUpTuts/search?query=chartJS', Level Up, 11 part series by Scott Tolinski]
---

So this is set up using Chart JS (rather than Chart*ist*).

{{< charts/chartjs >}}


This seems really good. Easy to use and super responsive. It animates on loading, the labels turn sideways and the scale changes automatically when responsive. 

There also seems to be more tutorials for this.

## How to Scale

The basic HTML looks like this.

```html
<div class="graph-wrapper">
    <canvas id="latticetest" width="16" height="9"></canvas>
</div>
```

The width and height of the `<canvas>` element are ratios. They change the shape of the graph but not it's size and so it will still use up all of the available width.

To scale it use a wrapper instead and scale that.

```css
.graph-wrapper {
    width: min(100%, 700px);
}
```
I used a `min()` value which select the lowest value of two. Here the graph will take up 100% of the space until it reaches 700px wide. When there is more than 700px 
    