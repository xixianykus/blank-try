---
title: "npm"
date: 2021-09-11T21:00:00+01:00
draft: false
photoswipe: false
toc: true
categories: ["dev"]
tags: ["npm", cli, javascript, JS]
summary: "How to use npm"
extlinks:
- [https://npmjs.com/, Search official npm]
- [https://docs.npmjs.com/, Official npm docs]
- ["https://nodejs.org/en/download/", Node JS download, "Inc. npm, the official site"]
- [https://nodesource.com/blog/an-absolute-beginners-guide-to-using-npm/, "Absolute Beginner's Guide to Using npm", an in depth guide]
- [https://www.impressivewebs.com/npm-for-beginners-a-guide-for-front-end-developers/, npm for beginners, really detailed guide for frontend usage.]
---

Here's a brief how to for basic npm commands...

## Get help from the command line

There's built in help: `npm -h` will give some general command info as well as a list of all the commands. For specific help on a command it's `npm <command> -h`.

You can also search the official docs: `npm help-search <command>`


## npm init

This is typically the first thing you want to do. This will set up the current folder as a project. 

NB. If you install without first using `npm init` npm will first see if the current folder is an npm project. If it's not it will check the parent folder and use that if that folder already has a `package.json` file. If it doesn't then the current folder will be used and `package.json`, and `package-lock.json` files will be created along with a `node_modules` folder containing whatever package you installed with npm.

So although it can work out without using `npm init` it's usually best to use it first to explicitly set the folder for your project.


So `npm init` will create just one file, a `package.json` file, which is a manifest for the project. A Q & A is launched to help you create the file. To skip the Q & A and just use the default settings use `npm init --yes` or `npm init -y`. The file can be easily edited at any time should want to change anything.

## Installing a package

First, if you want to find out more about a package, you can open the packages home page using `npm home <package-name>`.

You may want to search for a package too, for instance to check you have the right name. Use `npm search`. `npm find` and `npm s` are aliases for `npm search` too.

To install a program use: `npm install <package-name>`. This will install into the current directory. You can also use the `i` alias for install: `npm i <package-name>`.

This will add your package or module to a folder called `node-modules`. It will also create a `package-lock.json` to your project.

You can also add the `--save` or `--save-dev` flags with `npm install`. These add the package to your `package.json` although `-save` happens anyway. The `--save-dev` will save it a a dev dependency meaning it's only used for development rather than production. The shortcut version is `-D` (capital letter).

```bash
npm i <package-name> --save-dev
```

## Uninstalling

Just use `npm uninstall <package-name>`

To see what packages are installed use `npm ls`.

### .gitignore

The `node_modules` can quickly grow very large. If you're using git it's usual to add this folder to the `.gitignore` file. Everything needed for the project is referenced in the `package.json` file so the `node_modules` directory can be recreated anywhere. Running `npm i` or `npm install` will also create the `node_modules` folder with all the packages needed by the project listed in `package.json`.

### global installs

Some packages, such as CLI tools, are better to be installed globablly. Use either the `--global` or the `-g` flag: `npm install -g <package name>`.

If you don't add the `-g` flag the package will be installed into the directory you are in.

To see which npm packages you have installed (on Windows) press the Windows key + R (for *run*) and type `%appdata%` (case insensitive). Click on `mpm` folder and for more info click on `node-modules`. Alternatively you can run `npm ls -g --depth 0` to print out all node modules globablly installed.

### import or require?

Once installed you'll need to get the package into your main js file. This can be done usine either `require` or `import`. The former is for CommonJS modules and the latter is for ES6 modules.

```js
const chalk = require('chalk');
```

For ES6 modules:

```js
import chalk from 'chalk';
```

If your `package.json` has this line in it: `"type": "module",` you should be able to use ES modules and thus use `import` and `export`.

Here's an example `package.json` file used by Vite:

```json
{
  "name": "vite-project",
  "private": true,
  "version": "0.0.0",
  "type": "module",
  "scripts": {
    "dev": "vite",
    "build": "vite build",
    "preview": "vite preview"
  },
  "devDependencies": {
    "vite": "^3.0.7"
  },
  "dependencies": {
    "alpinejs": "^3.10.3",
    "open-props": "^1.4.13"
  }
}
```

[More](https://learn.coderslang.com/0014-how-to-import-and-use-npm-modules/) on import and require at coderslang.

### for CSS...

You might typically link to the CSS file using the CSS `@import`. 

```CSS
@import "open-props/normalize";
```

This will look in `node_modules` for a folder called `open-props` and file called `normalize.css` there. You typically don't need the file extension for this.


### See installed packages

Use `npm list` for local, `npm list -g` for global packages.

## Uninstalling

Use the `uninstall` command: `npm uninstall <package-name>`

## Updating


To update a local package: `npm update <package-name>`. If you just type `npm update` it will update ALL local packages.

To update a global package: `npm update <package-name> -g` or simply `npm update -g` for all global packages.

You can check which packages need updating by running `npm outdated -g`

## Version?

Just add `-v`: `npm -v` or to see which version of Node, `node -v`



## Other things...

### yarn and pnpm

These are two alternative to npm and both have pros and cons. See the page on [yarn and pnpm](/code/yarn-install/)

### nvm

...stand for Node Version Manager. This is used to switch between different versions of Node.js.


Good tutorial by Jessica Chan...

{{< music/yt "P3aKRdUyr0s" >}}