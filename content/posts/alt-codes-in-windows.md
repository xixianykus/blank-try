---
title: "Alt Codes in Windows"
date: 2021-05-21T10:13:25+01:00
draft: false
categories: ["windows"]
tags: ["windows", emoji]
summary: "My struggle to use alt codes in Windows"
extLinks:
- ['https://altcodeunicode.com/', Alt Code Unicode, a great source of info]
- [https://www.w3schools.com/charsets/, W3 Schools Character sets, Very comprehensive lists]
---

The *alt codes* are a great, quick way to access special characters directly from the keyboard. All you do is hold down the left Alt key and type specific numbers on the numeric keypad. Release the Alt key and you have your special characters.

Unfortunately for me only some of them work. I wanted to access the drawing character which have long lines like this:

├── index.md
     └── img

I can cut'n'paste them but cannot seem to access them using the Alt key method.

[Information online](https://tweaklibrary.com/alt-codes-are-not-working-on-windows-10/) suggested adding `EnableHexNumpad` to `Computer\HKEY_CURRENT_USER\Control Panel\Input Method` in the registry editor but that didn't solve the problem for me (even after restarting the computer).

It seems there are two sets of characters (well at least 2). The original set by IBM and Windows Code Page 1252. This latter set always begins with a zero. For example `Alt + 0147` will produce an opening double quote: “ á

## The Emoji keyboard

{{< img src="emoji-keyboard.png" alt="emoji keyboard" style="float: right; width: min(100%, 320px);" >}}

There are two ways to access specific characters without the Alt codes. The most convenient is the so called *Emoji keyboard* which has far more than just emojis. Quick to access just press the `Windows key + ;`. When you click on a character it is pasted straight to where your cursor is.


⊢ ❄ 🌈 👀

It annoys me that I can't access the characters I want using the Alt codes but this is actually not a bad method. And it seems that you do NOT even need to enable emojis in the config file with: `enableEmoji = "true"`

💛✝ 🆎 ✔ 🐔 🐓 💪 🙏 💩 📌 🗻 🌧 ☀

Some line characters found under the `symbols > maths characters ∞` set:

−∣ ⊣ ⊦ ⊢ — - ⊺ T◌

Maths symbols: Ⅰ ⅼ ∣ − ⊤ ⊢ ⊥ ⊣ ⊦ ⊩ — _ ¯ ‾

 |— ⊢ ⊦ ⊺ Ͱ-

## The Windows Character Map

So this is the old way. I think the Windows character map has been around since at least Windows 3.1. To access this:

1. press and hold the Windows key with R (for *run*). 
2. Type *charmap* and press enter. 
3. Click on the character, 
4. press select, 
5. Press copy 
6. and finally paste into your document. 
   
This method is much slower and has fewer available characters. The one advantage is you can choose which font to use and thus see which characters are available with that font.

## W3 Schools

Finally there is [W3 schools list of character maps](https://www.w3schools.com/charsets/). This is a very thorough set of pages covering pretty much everything from maths symbols to emoji to arrows and much more.

