---
title: "Baguette Image Gallery"
date: 2021-08-07T21:13:24+01:00
draft: false
baguette: true
categories: [images]
tags: ["hugo", image gallery, lightbox]
summary: "Trying out Baguette Box JS image gallery."
extlinks:
- [https://github.com/feimosi/baguetteBox.js, BaguetteBox JS]
- [https://webjs.net/en/p/simple-and-easy-to-use-lightbox-with-baguettebox-js-778.html, Web JS article, how to use Baguette Box]
---

For code on this see the [Baguette page](/code/baguette-box-js/) in the `/code` section.

Currently just setting up. Need to use `baguette` shortcode and get images from just either the `design` folder or `set-1` in a different gallery.

Also need to see how to get a masonry layout going with CSS/JS ? and shrink images to thumbnail sizes.

As these are PNG's is there any data fields that can be written to?

(This uses the `experiments/bag.html` shortcode rather than the `baguette.html` one.)

{{< experiments/bag >}}


## Link

[Baguette Box in Code](/code/baguette-box-js/)