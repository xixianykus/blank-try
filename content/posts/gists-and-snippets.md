---
title: "Gists and Snippets"
date: 2021-05-12T12:21:22+01:00
draft: false
categories: ["hugo"]
tags: ["hugo", git, github, gitlab]
summary: "Experimenting with embedding Github gists and GitLab snippets."
---

So this page is using the built in Hugo shortcode `gist`. It inserts the github script tag to pull the data from a GitHub gist. The file used is a list of links written in markdown.

This means you don't need to rebuild the site to update the gist. A simple pagre refresh will do.

It also means you have no control over the HTML or styling of the gist's content.

{{< gist xixianykus 6ba32bd9a0d0cb449ad25a7db95ea3d9 >}}

## Alternatives

Instead of using markdown use HTML. You can add a css file to the gist but obviously this would be the same in every web site.

Another way might be to is to embed a git repo in your page.

There are different methods: you could use an `iframe` but the styling could not be used from the site in which is is embedded.

Maybe pull the repo like you do with a theme using git clone or git submodules. Submodules is always up to date (after a site rebuild I assume). This could be incorporated into a partial or shortcode.

## GitLab Snippet

Just testing this html snippet

{{< gitlab_snip 2119432 >}}

What's interesting is that it has picked up the CSS background colour for the `pre` and or `code` tags.

To allow these to get the embed code they have to be saved as *public* in GitLab.

## GitLab markdown

{{< gitlab_snip 2106546 >}}



## Links about this page

1. [Hugo docs on gist shortcode](https://gohugo.io/content-management/shortcodes/#gist)
2. [GitLab docs on Snippets](https://docs.gitlab.com/ee/user/snippets.html)

<script src="https://gist.github.com/xixianykus/6ba32bd9a0d0cb449ad25a7db95ea3d9.js"></script>