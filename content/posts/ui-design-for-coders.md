---
title: "UI Design for Coders"
date: 2022-01-24T05:39:23Z
draft: false
photoswipe: false
unfinished: true
categories: [graphic design, layout]
tags: [layout, design, typography]
summary: "My notes from the video by Design Course, Gary Simon."
---

## 5 Design parameter

**Good design** requires:

- Colour and contrast
- white space
- scale
- visual hierarchy - direct the user
- typography


## Objective design vs Subjective

It's good to break design up into these two halfs.

Objective design is things that are right or wrong design. Examples include:

- poor contrast
- confusing visual hiearchy

Subjective design could include things such as colour palette and design choices based on beauty etc..


{{< music/yt "0JCUH5daCCE" >}}
