---
title: "Social Share Buttons"
date: 2022-12-20T16:34:29Z
draft: false
photoswipe: false
unfinished: true
categories: ["css"]
tags: ["social media"]
summary: "Creating social media share buttons."
---

Social media share buttons allow a visitor to your page to quickly share that page on their social media feed. So unlike social media links - which link to *your* social media accounts - these don't require a social media account by the website, only the user.

The key thing about each button is the link.

Some examples are below though a more complete list can be found on [Crunchify.com](https://crunchify.com/list-of-all-social-sharing-urls-for-handy-reference-social-media-sharing-buttons-without-javascript/) 
    
Facebook  
`https://www.facebook.com/sharer.php?u=[post-url]`

Twitter  
`https://twitter.com/share?url=[post-url]&text=[post-title]&via=[via]&hashtags=[hashtags]`

Pinterest  
`https://pinterest.com/pin/create/bookmarklet/?media=[post-img]&url=[post-url]&is_video=[is_video]&description=[post-title]`

Pocket  
`https://getpocket.com/save?url=[post-url]&title=[post-title]`

WhatsApp  
`https://api.whatsapp.com/send?text=[post-title] [post-url]`


In each of these you need to change the square brackets with either the URL of the page, the title of the page, hashtags used or whether it's a video in the case of Pinterest.

## Colours

When creating the buttons you can find accurate colours for different brands at colours from [brandcolors.net](https://brandcolors.net/)

## Icons

Icons are usually best saved as SVG's and in a single sprite to minimize http requests. A list of icon providers can be found on the [links page](/links/#icons).

## Mastodon

Adding a share link to Mastodon is a little more complex since there are multiple Mastodon instances each with a different URL. [One solution](https://www.bentasker.co.uk/posts/documentation/general/adding-a-share-on-mastodon-button-to-a-website.html) is to ask the user which they use with some Javascript first.

[Another example](https://codepen.io/bipoza/pen/XWbegOM) on Codepen uses an online script creating modals with customizable images and texts.