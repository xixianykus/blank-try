---
title: "Brush, Pencil tools & Strokes in Illustrator"
linktitle: "Illustrator: Brush, Pencil & Strokes"
date: 2021-08-19T09:57:57+01:00
draft: false
photoswipe: false
categories: [illustrator]
tags: ["illustrator"]
summary: Using the brush and pencil tools in Illustrator and manipulating strokes
---


## Brush and Pencil tools

The pencil tool is good for sketching. Holding down Alt changes it to the Smooth tool.

It can redraw the line too. Set option by doulbe clicking. You can't use brushes with it though you can add brush effect to the strokes after.

The brush tool is similar but applies predefined artwork to the strokes in combination with the brushes palette.

## Brushes

Brushes use the stroke colour to work. Shortcut: `b`, whereas the blob brush too (`Shft + b`) paints with fills rather than strokes.

The following are sub-types of the regular *brush tool*, rather than *blob brush*.

### Pattern Brush

The shape of the brush is repeated along the stroke.

Good for making multi coloured object into a a brush, for a rope or a snake, for example.

1. Design your pattern and make so it goes (would paint) from L to R 
2. Drag it to the brushes palette and choose 'Pattern Brush' to save it.
3. Select the path you wish to stroke and then click on the brush.

![Example of pattern brush effect](../photoswipe/8a-8-shadows.png)


### Art Brush

The shape of the brush is stretched from one end of the path to the other.

Lots of possibility with this. Make from a shape or multiple shapes of just one colour.

If set to colour by tint then the colour can be set in the stroke panel. The size is also set by the stroke size.


### Calligraphy brushes

...to do

### Stipple brushes

... to do



### Closing a Pencil path

When the path is almost closed a little circle symbol appears. Let go and it closes.

## Strokes

Strokes can be modified with the width tool: Shft + W

You can save a stroke profile by selecting it then from the stroke palette clicking the little icon at the bottom.


### Dotted lines

To make dotted lines:

1. In the stroke panel click the checkbox for dashed line
2. Enter zero for the first dashed
3. Add anything for your gap
4. Choose round caps