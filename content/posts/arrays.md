---
title: "Arrays"
date: 2020-08-24T08:41:52+01:00
draft: false
categories: ["Web dev"]
tags: ["hugo"]
summary: "How to create and access arrays in Hugo"
fish: ["shark", "herring", "squid", "chair"]
watercreatures:
 - ["shark", "herring", "squid", "chair"]
 - ["starfish", "sand dollar", "nautilous", "anemone"]
 - ["orca", "dolphin", "sperm whale", "manatee"]
extLinks:
 - ["https://regisphilibert.com/blog/2017/04/hugo-scratch-explained-variable/", "Scratch"]
 - ["https://gohugo.io/functions/dict/#readout", "dict by Regis"]
 - ["https://learnhugo.ml/posts/arrays/", Arrays at Learn Hugo]
---

Arrays in the frontmatter (YAML) can be written as:

```yaml
fish: ["shark", "herring", "squid", "chair"]
```

or

```yaml
fish:
- shark
- herring
- squid
- chair
```

To access an item of an array from Hugo template use `with` and `index`:

```go-html-template
{{ with .Params.fish }}
  {{ index . 1 }}
{{ end }}
```

You can also have arrays of arrays. For example:

```yaml
watercreatures:
 - ["shark", "herring", "squid", "chair"]
 - ["starfish", "sand dollar", "nautilous", "anemone"]
 - ["orca", "dolphin", "sperm whale", "manatee"]
 ```

 So how do we access these?

 ```go-html-template
{{ with .Page.Params.watercreatures }}
  {{ index . 1 2 }}
{{ end }}
```

The first index, 1, selects the second array. The second number, 2, selects the third item of the array.

How do we get a list of all of them? in order?

## Using the `arrays.html` shortcode:

{{< arrays >}}