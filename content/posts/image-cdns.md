---
title: "Image CDNs"
date: 2021-11-23T11:17:45Z
draft: false
photoswipe: false
unfinished: true
categories: ["CDN"]
tags: [CDN, image processing]
summary: "A list of options for hosting and processing images on the web"
---

Using a CDN to host images has several advantages. For git based sites keeping images out of the repo is good because every time they are changed git keeps a full version of the image. If the images don't change perhaps that doesn't matter. But the other advantage is having the CDN make image transformations for you thus making an image of multiple sizes and aspect ratios for different screens.

The only problem is which service to choose from? Below is a list of different services that have a free tier. A few considerations to be taken into account are cost, integration, and features. Many of these services offer a free tier but some are much better than others. If you're using a particular piece of software like a CMS you might want a direct integration. For instance Cloudinary and Uploadcare offer direct integration with Netlify CMS. This means you can choose images and upload new images directly to their servers from the CMS itself. Such integrations are nice but not absolutely necessary. It depends on your project and how you intend to add images to the site.

There are various features but the most important for most projects is image resizing for responsive design. Most services offer image resizing in one form or another. You add height and/or width to the CDN image url and the resized image is generated on the server ready to use. You can add such sizing to HTML elements like `<picture>` and `srcset` to let the browser choose the right image based on the user's screen size.

Another similar feature is auto formatting. The CDN will automatically choose the  best format for the browser. So for example while Chrome and Firefox can use the `webp` format Safari cannot and so will need a jpeg version of the image. 

However an even simpler way to this is with what Uploadcare call [Adaptive Delivery](https://uploadcare.com/docs/delivery/adaptive-delivery/#adaptive-delivery). This is much simpler. You just link to the full size image on the CDN server and when a user requests the image one of the right size is generated on the fly.

It's also worth considering that there are other ways to create multiple files. Adobe Photoshop allows saving images in different qualities in multiple sizes in one go.

Hugo also has it's own built-in image processor too. But these services are pretty handy so here is a list 

1. [Netlify Large Media](https://docs.netlify.com/large-media/overview/) - integrates with Netlify CMS, 100gb bandwidth
2. [Uploadcare](https://uploadcare.com/) - free forever inc. unlimited transformations, 30gb bandwidth, 3gb storage, Netlify CMS plugin
3. [Cloudinary](https://cloudinary.com/) - credit based free tier but with a max of 20gb bandwidth. Integrates with Netlify CMS.
4. [ImageKit.io](https://imagekit.io) - free forever tier w 1000 transformations pcm and 20gb storage and 20gb bandwidth
5. [Gumlet.com](https://gumlet.com) - 1gb bandwidth, recommeded by [travis.media](https://travis.media/)
6. [Imgix.com](https://imgix.com/) - Always free, 1000 origin images, infinite transformations, 'bandwidth included' ??
7. [Sirv.com](https://sirv.com/) - 500mb storage (we only have about 135mb so far) and 2gb bandwidth. Zoomable images will have Sirv branding on them. Easy to set up?
8. [Statically.io](https://statically.io/docs/using-images/) is a free CDN for hosting open source projects. 
9. [Git LFS](https://www.atlassian.com/git/tutorials/git-lfs) - not a service but a software, a plugin for Git. Used by Netlify's Large Media service but can be used on it's own.
10. None of the above - Just keeping image files in the regular places of your site also has some advantages too.