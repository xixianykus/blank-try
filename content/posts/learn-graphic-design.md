---
title: "Learn Graphic Design"
date: 2021-10-15T19:16:25+01:00
draft: false
photoswipe: false
categories: [graphic design]
tags: [graphic design]
summary: "How to learn graphic desing by Gareth David"
extlinks:
- [https://www.youtube.com/channel/UC64eec0UYHxflyEWgyZOvLA, Gareth David Youtube, lots of good tutorials homing in on each of these topics]
---


So the main steps are:

## 1. Understanding graphic design

Understanding visual elemnents and design principles.

The visual elements are:

- line
- colour
- shape
- texture
- space
- form
- typography

The design principles are:

- contrast
- heirarchy
- alignment
- balance
- proximity
- repetition
- simplicity
- function

## 2 Familiarise yourself with the tools and equipement

Two types of tools: software and practical

## 3. Collect and get inspired

Use a scrapbook and on a computer a scrapbook folder to collect work you like. Pinterest is another good option.

Take pictures of things you like when out and about.

The aim is to learn what others have done before you.

> Keep a sketchpad with you at all times. Great ideas can hit you at any time.

Collect creative books. Not just about design but type, web all sorts of design books.

All of this will get your creative juices flowing, improve design theory and technical knowledge.

## 4. Practice and build skills

Practice:
- drawing - the basic skill of expression and communication. Bring ideas from your mind to a piece of paper you can start to practice the *design process*. You don't have to be amazing but get good at *drafting*, getting your ideas onto paper.
- design - practice: have a challenge eg. 
  1. design a logo for a local business.
  2. choose typeface/s, colours and create a theme.
  3. Design a poster with the logo to promote the business.
  4. how would it work digitally? As a web site
  5. First BEFORE using the computer, do research, seek inspiration and sketch out ideas first. Get at least 3 or 4 good ideas first.
- software skills - once completed the above practice with the software

Repeat the above process. The more you do the more work you will have to show for it.

## 5 Seek education and apply your skills

3 options:

1. self teaching
2. Online courses
3. traditional route: study at university

### Self teaching

the pros:

1. the cheapest
2. more resources on the web everyday
3. free livestreams
4. loads of books available
5. loads of free tutorials available

the cons:

1. requires own motivation and commitment to see it through
2. you can feel misguided and lack direction
3. hard to get feedback and criticism from peers
4. hard to build a body of work
5. lack of goals and set times
6. can be lonely without others

### Online courses

1. variety of courses available
2. cheaper than uni
3. learn while working and throughout your career
4. more focus, aim and direction than self teaching
5. taught by professionals who have working experience

### university

the cons

1. Really expensive


## 6. Make mistakes and learn from them

One big part of development is discovery, testing what you have learned, pushing yourself further and making mistakes.

> The problem is not making mistakes. It's not learning from them.
> The more mistakes you make the more you will learn and improve in the future

## Conclusion

The best method depends on your circumstances and what is best for you.


{{< music/yt "DP9M8SCQsR0" >}}


Also many more like this such as:

{{< music/yt "zYoTo0TfVjg" >}}



