---
title: "SVG Notes"
date: 2021-05-04T12:30:43+01:00
draft: false
categories: ["css", svg]
tags: ["hugo", svg, css]
summary: "Some notes on SVG's"
extlinks:
- ["https://piccalil.li/tutorial/relative-sizing-with-em-units/", Picalili, A guide to sizing with ems including SVGs]
- ["https://www.youtube.com/watch?v=TBYJ2V1jAlA", Viewport and Viewbox, By Kevin Powell]
- [https://flaviocopes.com/svg/, Flavio Scopes page, A thorough intro to SVG]
---


So I added the Hugo logo to the footer and also to a page. First thing is I needed two copies: a shortcode version to put in the content section and a partial for the footer (which is itself another partial).

You can target the SVG with CSS but the sizing is a little odd. I added an `id` attribute to the svg itself so I could target that:

```css
svg#hugo {
    width: 4rem;
    height: 1rem;
}
```

SVG's have a *viewbox* AND a *viewport*. The CSS controls the size of the latter which is a window crops the image.

The viewBox was set in this svg logo with `viewBox="0 0 1493 391"`. The first two numbers are the starting coordinates of the viewbox. The second two are the width and height.

The size of an image comes from the relationship between the viewport and viewbox. If the viewbox size is lowered it has the effect of zooming in.

> The viewport is the size of the viewable area.

> The viewbox controls what is inside that viewable area.

