---
title: "Schema"
date: 2022-12-26T11:11:12Z
draft: false
photoswipe: false
unfinished: true
categories: ["css"]
tags: ["schema", "structured data"]
summary: "How to optimize and customize search results using structured data."
extlinks: 
- [https://schema.org/, schema.org, the official site listing schema types and a good introduction to using schema]
- [https://developers.google.com/search/docs/appearance/structured-data/intro-structured-data#structured-data, Google developers, Google article on structured markup]
---


Adding structured data to a website can make the content make more sense to search engine bots. It can also be used to improove the site in search results by including a logo, images, ratings, search bar or links to different sections of the site.

There are many types of schema and even more sub-types. The sub-types are at the bottom of each page on [schema.org]




## Recommended schema types

There are over 600 schema types. Here are 9 recommended ones:

1. [Organization](https://schema.org/Organization) includes *name*, *link to your logo*, *description* and *address*.
2. [Website](https://schema.org/WebSite) allows a site search box on your site to appear in search results. However you need Google *authority* to have a high enough ranking for this to work.
3. [Product](https://schema.org/Product) is for eCommerce websites.
4. [AggregateRating](https://schema.org/AggregateRating) displays ratings and a review snippet in the search results.
5. [BlogPosting](https://schema.org/BlogPosting)
6. [FAQPage](https://schema.org/FAQPage) can add some questions in the search results.
7. [Event](https://schema.org/Event)
8. [Recipe](https://schmema.org/Recipe)
9. [VideoObject](https://../schema.org/VideoObject)



## Google's guidelines

There are many guidelines both general and for specific schema. Some of the general ones include:

- Content must be visible
- Markup must be complete
- Schema should be placed on the page that it applies to (not all on the home page OR every schema on all pages)

Here's an example from Google of the main Hugo site, https://gohugo.io

![Google result](google-hugo.png)

## Formats


### HTML microdata

The method adds HTML microdata tags to HTML elements.

### RDFa

### JSON LD

This is Google's recommended format. It's added to the `<head>` of a page using script tags with the type for JSON LD:

```html
<script type="application/ld+json">
    // code here...
</script>
```

Unlike Microdata and RDFa there is no need to edit HTML tags.


You can add schema to your markup via a plugin, manually or use a schema generator such as one on [TechnicalSEO.com](https://technicalseo.com/tools/schema-markup-generator/)


## Testing






[schema.org]: https://schema.org/