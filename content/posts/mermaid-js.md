---
title: "Mermaid JS"
date: 2021-12-09T16:52:37Z
draft: false
photoswipe: false
unfinished: true
categories: [JS library]
tags: [JS, markdown, Mermaid, Hugo]
mermaid: true
summary: "How to install Mermaid JS which creates diagrams from simple code in markdown files."
css:
- "div.mermaid {isplay: inline-block}"
extlinks:
- [https://discourse.gohugo.io/t/use-mermaid-with-code-fences/17211, Using code fences in Hugo with Mermaid, a post in Hugo forums]
- [https://mermaid-js.github.io/mermaid/, Mermaid JS official docs, The Mermaid JS website]
---

## What is it?

Mermaid JS is a Javascript library that allows diagrams to be added easily to web pages with some simple code. Because it's so capable there is actually a lot of code to learn but like most languages since you probably won't use most of it you just learn what you need which typically might just be *flow charts*. Pie charts are useful too.


## Installing

So MermaidJS works by taking specially formatted code inside a `<div>` tag with a class of `mermaid`. This code is interpreted by Mermaid JS which creates the diagram.

To install you simply add the code to your site using a CDN or downloading the JS file to your site. You add an initialisation script too:

```html
{{ with .Params.mermaid }}
    <script src="https://cdn.jsdelivr.net/npm/mermaid/dist/mermaid.min.js"></script>
    <script>
        mermaid.initialize({ startOnLoad: true });
    </script>
{{ end }}
```




To create a flow chart or other Mermaid diagram you just add your Mermaid code inside of a div tag like so:

So here we go:

```html
<div class="mermaid">
  flowchart LR
    a --> b
</div>
```

Et voila. Here is a Mermaid diagram using a Hugo shortcode to add the `<div>` tag:

```html
{{⪡ html class="mermaid" ⪢}}
  flowchart LR
    a --> b
{{⪡ /html ⪢}}
```

{{< html class="mermaid" >}}
flowchart LR
a --> b
{{< /html >}}


## Use without shortcodes

It's possible to use Mermaid with code fences (backticks) directly in a Markdown file with the addition of a small amount of Javascript to the `<script>` tag above.

```js
window.onload = function() {
    mermaid.init(undefined, ".language-mermaid");
};
```

Here is a simple version of the output. I could (might) change the CSS for `code.language-mermaid` too if I feel the need to get rid coloured background or do something else like left aligning or whatever.

```mermaid
flowchart LR
a --> b
```

The slightly weird thing is that the chart appears to be coloured by the syntax highlighting.

## A Pie chart

Here's a pie chart using code fences (backticks):

```mermaid
pie
title My big pie chart
  "pastry" : 1
  "filling": 2
```

And with a short code:

{{< html class="mermaid" >}}
pie
title My big pie chart
  "pastry" : 1
  "filling": 2
{{< /html >}}

NB. By default Mermaid will align the diagram in the center of the parent element (in this case a `<div>` tag). To change this to left aligned I tried using `display: inline-block` to `div.mermaid`. However this cut off the legend of the chart.
