---
title: "Artist vs Illustrator vs Designer"
date: 2021-11-10T09:05:08Z
draft: true
photoswipe: false
unfinished: true
categories: [graphic design]
tags: [graphic design]
summary: "How to get started with a piece of graphic design"
---

## An Artist

So for this article I'm ignoring the fact that the label *artist* is often applied to many different fields and limit its meaning to the more traditional use of a visual designer, someone who creates images by painting, drawing or digitally.

So what is different about an artist? For me an artist is someone who is actually coming up with his/her own ideas. They're not given a brief or task of any sort to fulfill. The person may feel strongly about something, it could be war, or love, or something trivial that amuses them. Unlike either the illustrator or graphic designer the artist has a truly blank canvas.


## An illustrator

An illustrator is a person who is given a task to perform. There is a specific end goal in mind. We need an image for the cover of this book for instance. The book is already written, all the ideas are fully formed and the illustrator has to translate these into an image. So the illustrator's task is extremely limited compared to the artist. However if an artist has decided on a goal or mission for themselves they are then essentially working as an illustrator too. Of course some artists don't necessarily have a goal at all. They may just be experimenting with images to see what they get. They may be guided only by their subconscious too. 

## Graphic designer

Like an illustrator a graphic designer has a preset task to perform. It can be more or less limited but for instance a designer may have some or even all of the following:

1. A logo
2. a mission statement 
3. brand colours
4. brand style guide
5. brand typeface/s
6. brand catchphrase
7. titles
8. copy
9. image/s

When a designer has amassed both the ideas/intent and visual material as well his/her task is likely to be even more limited.

Of course a designer may be tasked with the job of creating the visual elements of a brand. This could be much closer to a *blank canvas* that say putting a web site together would be. However they will still be strongly guided by the intent of the people they are working for, or even if working for themselves, the purpose of what they're trying to communicate. 


## The blank canvas

So the designer is attempting to communicate something. This means there is no necessity at all for the design to be beautiful or attractive. Although such qualities may help get someone's attention in the first place.