---
title: "Cloudflare Pages"
date: 2021-12-26T01:58:24Z
draft: false
unfinished: false
categories: ["css"]
tags: ["hugo", Netlify]
summary: "Cloudflare Pages vs Netlify and using Hugo with CF Pages"
extlinks:
- [https://developers.cloudflare.com/pages/, Cloudflare Pages docs, The official documentation]
- [https://github.com/gohugoio/hugoDocs/blob/master/netlify.toml, An example of a Hugo netlify.toml file, "inc. environment variables"]
---

Cloudflare pages is a free static hosting service similar to [Netlify] and in *some* ways even better. Whereas [Netlify] allows a generous bandwidth allowannce of 100gb per month [Cloudflare] has no limit at all.

[Netlify] deploys sites and apps to it's CDN edge servers. [Cloudflare] does the same but has many more such servers so theoretically even faster.

However it's not an easy battle. Whether those extra edge servers provide any real advantage is debatable. Bandwidth may be less of an issue than build times for some and here the Netlify free tier beats Cloudflare with longer build times. At the moment it is 300 minutes per month. Netlify also has a totally first rate developer experience with fantastic docs and many other services too. Certain things like the open source [Netlify CMS] is particularly easy to set up on Netlify.

Personally I prefer Netlify but I use Cloudflare Pages too and there's no doubt it's an excellent service too so here's a quick bit about using it.

It's free to use so go to [Cloudflare], set up an account and login. Go to the Pages section NOT Websites. From here you connect to your Git provider account (either GitHub or GitLab) and repo for your site/app and Cloudflare will build and deploy the site. Like Netlify free `https` is included and free urls that look like `www.example-name.pages.dev` where *example-name* is whatever name you choose.

## Setting up the Hugo version

When using Hugo you want to set the version number you want your site built with. This is not too hard to find. Go to the Pages section and click on the name of the site you want to edit.

From here click on the `Settings` tab and then in the left hand column click on `Environment variables`. Here you can edit the variables so you'll want to add one, if it's not there already, called `HUGO_VERSION`. The value you give it will be the value of whichever version you wish to use, for instance `0.91.2`. Click `save` and it will be built with that version the next time you make changes to your site. You'll probably want to do the same for the *preview* version too.

See the [official docs page] on Enviroment variables.

![Cloudflare pages interface](cloudflare.png)



[Netlify]: https://netlify.com/
[Cloudflare]: https://cloudflare.com/
[Netlify CMS]: https://netlifycms.org/
[official docs page]: https://developers.cloudflare.com/pages/platform/build-configuration#environment-variables