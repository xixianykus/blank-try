---
title: "Useful Functions for Data"
date: 2021-05-13T10:47:30+01:00
draft: true
categories: ["css"]
tags: ["hugo"]
summary: "Notes on functions that could be useful for organising data in Hugo"
---

## `uniq` 
> Takes in a slice or array and returns a slice with subsequent duplicate elements removed.
>
> Hugo docs

Example:

```go-html-template
{{ uniq (slice 1 2 3 2) }}
{{ slice 1 2 3 2 | uniq }}
<!-- both return [1 2 3] -->
```

## `len`
2. `sort`
3. `range`

## `complement`

>  gives the elements of a collection that are not in any of the others.
>  
> [Hugo docs](https://gohugo.io/functions/complement/)

## `append`

