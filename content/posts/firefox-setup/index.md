---
title: "Firefox Setup"
date: 2022-12-10T10:33:00Z
draft: false
photoswipe: false
# unfinished: true
toc: true
categories: ["Privacy"]
tags: ["Firefox"]
summary: "How to setup Firefox to maximize privacy according to Sun Knudsen"
---


![Firefox logo](firefox-logo..png)
{.float-right .mw30}

## Preferences/Settings

Much of this guide involves changing the preferences. To access these first click the little hamburger icon at the far right side of the address bar. From the dropdown menu choose settings towards the lower part of the menu. You can also get to the same place by typing `about:preferences` in the url box and pressing enter.

After installing follow these steps.

### 1. Make it the default browser

In the first part of the settings make sure you have

`Always check if Firefox is your default browser.` set. Press the `Make default browser button` if not.


### 2. Enable DNS over HTTPS

Scroll down to the bottom of the General section to `Network Settings` and click on the `Settings` button. Towards the bottom make sure `Enable DNS over HTTPS` is checked.

### 3. Change the search

The default search engine is Google because Google pay Firefox for this. In the search section of settings click on the dropdown box to change this to DuckDuckGo. 

Sun also recommends disabling `Search suggestions` by unchecking the the `Provide search suggestions` box in case you accidentally paste a password into the search bar and it is submitted to DuckDuckGo. 

In the `Search shortcuts` he removes everything but DuckDuckGo.


### 4. Privacy and security

The next section under *Enhanced Tracking Protection* click on the `custom` radio button. For *cookies* choose `All third party cookies` option. Sun says that despite the warning of broken websites very few websites are broken.

Just below this *custom* box make sure the *Send web sites a “Do Not Track” signal that you don’t want to be tracked* the *Always* radio button is checked.

Under *Cookies and Site Data* check the box for *Delete cookies and site data when Firefox is closed*. 

The next section *Logins and Passwords* Sun reccomends disabling everything by unchecking *Ask to save logins and passwords for web sites*. Although it's better to keep passwords in a password vault like *Keepass* I find for some sites the security of the password is not that important anyway so choose to use the Firefox password manager on a site by site basis.

For *History* on the dropdown choose *Use custom settings for history* and then check `Clear history when Firefox closes`.

Under *Firefox Data Collection and Use* disable everything. 

Finally in the *sync* section Sun "highly recommends not enabling sync". To make certain this doesn't turn on by mistake in the address bar type `about:config` the accept the risk button. In the input box type `identity.fxaccounts.enabled`. Set this to false using the toggle button on the right hand side then close the tab. When Firefox restarts that whole section will be missing from the *Settings*/*preferences* section.


## Extensions

1. **Privacy Badger** which detects trackers and disables them. Not an adblocker, something Sun is against.
2. **HTTPS Everywhere** Make sure *Encrypt All Sites Eligible* is turned on.
3. **Firefox Multi Account Containers**. This is accessed by right clicking the *New tab* tab. To use first of all create a container for a specific site or set of sites. Give it a name. Then R click the new tab tab and open a tab with your new container. Open the first site you want in that container. Then click on the *Containers* icon at the R end of the address bar and choose your container so the site will always open that container.

Finally open the site and it will ask you if you want to open it in that container. Tick the checkbox to always open in that container if that's what you want.


{{< yt NH4DdXC0RFw >}}

