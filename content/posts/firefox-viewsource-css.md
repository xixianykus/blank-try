---
title: "Firefox Viewsource CSS"
date: 2021-05-08T16:02:42+01:00
draft: false
categories: ["css"]
tags: ["firefox"]
summary: "How to turn the Firefox view source page into a dark mode"
extlinks:
- ["https://davidwalsh.name/firefox-user-stylesheet", Add a User Stylesheet in Firefox, by David Walsh]
---

So the idea was to change the CSS when viewing the source code of a web page in Firefox into a darker theme.

The easiest and best way to do this was to use a plugin called [Nightviewer](https://addons.mozilla.org/en-US/firefox/addon/night-viewer-dark-page-source/).

---

So this requires several steps:

1. Locate the right folder
2. Edit CSS
3. Enable Legacy Profile Customizations
4. Restart Firefox

I couldn't get this to work but anyway here goes.

## 1. Locate the folder

In the firefox address bar type `about:support`

Look for `Profile Folder` and click on `Open Folder`.

Look for the file `userContent.css`

## 2. Edit the CSS

This is the CSS which comes from [Mozilla Support question](https://support.mozilla.org/en-US/questions/1276606):


```css
@-moz-document url-prefix(view-source:) {

	*|*:root {
		background: none !important;
	}

	#viewsource {
		font-family: "Roboto Mono", Inconsolata, "Source Code Pro", SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace, -moz-fixed !important;
		line-height: 1.5em;
		background-image:
		url(https://farm9.staticflickr.com/8536/8680613751_71ef92bbdb_o_d.jpg),
		linear-gradient(to right, #002b36d0, #002b36d0, #000000d0),
		linear-gradient(#00000040 50%, transparent 50%);
		background-size: cover, cover, auto 3em;
		background-position: center, center, top 10px left 0;
		background-attachment: fixed, fixed, scroll;
		background-repeat: no-repeat, no-repeat, repeat-y;
		background-blend-mode: overlay, normal, normal;
		color: #839496;
	}

	pre[id]:before,
	span[id]:before {
		width: 6ch !important;
		color: #586e75 !important;
		margin-left: -7ch !important;
	}
	pre {
		padding-left: 1ch;
		margin-left: 6ch !important;
		border-left: 2px solid #073642;
	}
	pre > :first-child {
		padding-right: 1ch;
	}

	.highlight .start-tag {
	 color: #d33682 !important;
	}
	.highlight .end-tag {
	 color: #d33682 !important;
	}
	.highlight .comment {
	 color: #586e75 !important;
	}
	.highlight .cdata {
	 color: #cb4b16 !important;
	}
	.highlight .doctype {
	 color: #268be2 !important;
	}
	.highlight .pi {
	 color: #268be2 !important; /* orchid -> blue */
	}
	.highlight .entity {
	 color: #b58900 !important; /* violet -> yellow */
	}
	.highlight .attribute-name {
	 color: #93a1a1 !important;
	}
	.highlight .attribute-value {
	 color: #2aa198 !important;
	 text-shadow: 1px 1px 2px black;
	}
	.highlight .markupdeclaration {
	 color: #859900 !important;
	}
	.highlight .error,
	.highlight .error > :-moz-any(.start-tag, .end-tag, .comment, .cdata, .doctype,
	.pi, .entity, .attribute-name, .attribute-value) {
		color: #002b36 !important;
		background-color: #dc322f !important;
	}
}
```

## 3. Enable Legacy Profile Customizations

Open `about:config` and make sure this setting:
`toolkit.legacyUserProfileCustomizations.stylesheets` is set to true


## 4. Restart Firefox

And you're done. Except it hasn't worked for me.