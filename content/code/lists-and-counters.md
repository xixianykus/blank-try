---
title: "Lists and Counters"
date: 2021-09-05T06:25:54+01:00
draft: false
categories: ["css"]
tags: ["css", "::marker", lists]
summary: "Styling lists and the ways of :counter"
extlinks:
- [https://moderncss.dev/totally-custom-list-styles/, Modern CSS article, by Stephanie Eckles]
---

Some notes from an [excellent article](https://moderncss.dev/totally-custom-list-styles/) by Stephanie Eckles at [Modern CSS](https://moderncss.dev/)

## Using emoji for bullets

Emoji are weird things, part font and part image it seems. Because you can use them directly in the source code you can add them using a custom HTML data attribute to any element including list items:

```html
<ul>
    <li data-bullet="😁">
    <li data-bullet="🐒">
    <li data-bullet="🦢">
</ul>
```

You can then use this with the `::before` pseudo element with `content`:

```css
li::before {
    content: attr(data-bullet);
    font-size: 1.25em
}
```

(The way Stephanie does this is by setting each list item to `display: grid`) however the more modern way uses...

## CSS `::marker`

The CSS `::marker` selector is for styling the bullet points or numbers on lists. It's easy to have a different colour bullet than the text:

```css
li::marker {
    color: orangered;
}
```

However `::marker` can also accept `'content` with a data attribute which makes it easier to use those emoji's above:

```css
li::marker {
    content: attr(data-bullet);
    font-size: 1.25em;
}
```

Stephanie recommends adding some left-margin if needed to the `<ul>` to prevent cut off by overflow. Also some left padding for the `<li>` element.

The `::marker` selector can have the following CSS properties:

- `animation-*`
- `color`
- `content`
- `direction`
- `font-*`
- `transition-*`
- `unicode-bidi`
- `white-space`

(I have no clue what `unicode-bidi` is)