---
title: "Quick'n'dirty dark mode"
date: 2021-05-24T21:40:17+01:00
draft: false
categories: ["css"]
tags: ["css"]
summary: "A quick way to get a full dark mode colour scheme in just a couple of lines of code."
extLinks:
- ['https://adactio.medium.com/inlining-svg-background-images-in-css-with-custom-properties-c32adde256c3', Inlining SVG background images with CSS Custom Properties, Good tips by Jeremey Keith]
- ['https://css-tricks.com/dark-modes-with-css/', CSS Tricks article, Good stuff]
- ['https://charlesrt.uk/blog/the-dark-web-rises/', The Dark Web Rises, article on why we need dark web.]
- [https://lea.verou.me/2021/03/inverted-lightness-variables/, Lea Verou's method, A different way to create a dark theme using custom properties on HSL values.]
---

So choosing a dark mode scheme can sometimes take a lot of time, particularly if your colours aren't defined in CSS custom properties. But here's and extremely quick way to get a new colour scheme in just a couple of lines:

```css
@media screen and (prefers-color-scheme: dark) {
    html, img {
        filter: invert(1) hue-rotate(180deg);
    }
}
```

If the user has their OS set to dark mode this CSS will invert and change all the colours. 

Now note that `img` is in there along with HTML. What's that doing there? Well if you just have `html` everything is inverted, including images. Assuming you don't want your images changed then by adding `img` you are changing it twice. First they're converted by the `html` and then they're converted again by `img` which changes them back to how they should be.

## No mobile ??

With this site for some reason the only thing which didn't seem to work was the mobile slide out menu. Not sure of exactly why that was but in the spirit of *quick'n'dirty* I just added a few more characters of code to make sure these changes only happened when the full menu was active: `and (min-width: 1001px)` as in.

```css
@media screen and (prefers-color-scheme: dark) and (min-width: 1001px) {
    html, img {
        filter: invert(1) hue-rotate(180deg);
    }
}
```

If you're not sure what this looks like because you don't have dark mode set on your OS, you're on mobile or maybe I've changed the colour scheme again here's a screen shot:

![quick'n'dirty dark scheme](/img/quick-dark-scheme.png)

## Issues?

Now it's not perfect and there's something going on with Firefox around the bottom curved corner. There is presumably extra processing power needed to change the colours too. 

Another thing is that the scroll bar kept the light scheme too. But overall I think the result is good and at least generates an idea for a new scheme starting point. With CSS custom properties you could quickly take the colours from this scheme and use them in the custom properties in the `@media (prefers-color-scheme)` declaration.

## Going in the whole hog

So I decided to use the colours produced by the CSS filter in a *proper* way. Copying the colours wasn't entirely straight forward. Using Color Picker browser extention could not choose the filter colours and instead picked the orginal ones. No use at all. Luckily I had the screen shot on the page so I *picked* the colours from there. 

After copying the new colours into a root element in my dark scheme media declaration colour I commented out the filter and checked the results.

```css
@media screen and (prefers-color-scheme: dark) and (min-width: 1001px) {
    
    /* html, img {
        filter: invert(1) hue-rotate(180deg);
    } */
    :root {
    --bgc: rgb(57, 24, 31);
    --bgc2: rgb(40, 62, 116);
    --bgc3: #683047;
    --c: #69AFDC;
    }
}
```

### Problem 1: the SVG's colours

The first and expected problem was the svg wavy background colours. This SVG was set inline in the CSS file using 

```css
main {
    background-image: url("data:image/svg+xml...etc.");
}
```

I thought about targetting this with the original filter settings but as it's already part of the CSS file there's no way targetting it (as far as I am aware).

The fill colours in SVGs apparently cannot take CSS custom properties. So what to do? I used [a solution involving more custom properties](https://adactio.medium.com/inlining-svg-background-images-in-css-with-custom-properties-c32adde256c3) of the SVG itself.

First I moved the original SVG into a custom property:

```css
 --bg-image: url("data:image/svg+xml ... etc.);
 ```

 Then I duplicated this line and gave it a different name for the dark theme:

```css
 --bg-image: url("data:image/svg+xml ... etc.);
 --bg-image-dark: url("data:image/svg+xml ... etc.);
 ```

Finally, using the search function of my editor for *fill*, I located the fill colours and changed the values to the colours I wanted. Then I just added this new custom property for the recoloured SVG to a `main` declaration in my dark scheme media query:

```css
main {
    background-image: var(--bg-image-dark);
}
```

NB. It's possible to target SVG's colour in CSS using the `fill` property. However this SVG had multiple fills as well as being part of the CSS file.

### Problem 2: syntax highlighting

The next problem was the syntax highlighting in code blocks. These weren't targetted by CSS file. The colours are generated by Hugo, my SSG, and inserted inline into the page. The obvious quick way was to use the filter again on code blocks.

```css
code {
    filter: invert(1) hue-rotate(180deg);
}
```

The only issue was this changed the background colour too because that was using a CSS custom property already and so was switched in dark mode. One way would be to remove the custom property in `code` declaration and just use a straight hex colour value of the original, light scheme, colour.


## A different method

Lea Verou describes a completely different [method](https://lea.verou.me/2021/03/inverted-lightness-variables/).
