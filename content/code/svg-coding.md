---
title: "SVG Coding"
date: 2021-09-12T15:58:52+01:00
draft: false
categories: [Web dev, svg]
tags: ["SVG"]
summary: "Some code for using SVG's"
---

## Calling a single from a sprite

```svg
<svg class="icon icon-twitter-logo">
  <use xlink:href="/path/to/images/my-sprite.svg#icon-twitter-logo"></use>
</svg>
```
The `xlink:` bit is deprecated apparently.

## CurrentColor

It's common to use SVG icons inside links for menus etc.. But the CSS usually targets either the `color` or `background-color`. An SVG will usually want it's `fill` property changed though, or sometimes the `stroke`. An easy way to this is to set the `fill` color to ``currentColor`. This property will take the colour being used at the time including the colour of the hover state.


## SVG

The `<svg>` is the main container for one or more SVG's.

`<g>`
`<symbol>`
`<title>`

## Using SVG's

There are 7 ways to add SVG's to a web page:

1. Using the `<img>` tag : can't target with CSS so you can't use `:hover` etc.
2. Inline - paste the code straight into the page. Can modify colour using the `fill` attribute or with CSS IF the fill attribute is removed. You can add an `id` or `class` attributes to the `<svg>` tag OR to the `<path>`. Using the `<path>` allows to target different paths on images that use multiple paths.
3. Using the SVG within the CSS file;
   ```css
   section {
      background-image: url(path/to/file.svg) left top no-repeat;
      background-size: 100px 100px; /* important to make the image fit within the container */
      width: 100px;
      height: 100px
      display: block;
   }
   ```

   You can't change the colour with this method.
4. Using CSS masks. 
   ```css
   div {
       width: 200px;
       aspect-ratio: 1 / 1;
       background-color: blue;
       mask-image: url(path/to/image.svg);
       mask-size: 200px 200px;
   }
   ```

   The `mask-image` and `mask-size` properties still require additional  `webkit-` prefix for webkit browsers.

5. You can add SVG code directly into an image tag's source attribute. You have to prefix it first with a data URI.
   ```html
   <img src='data:image/svg+xml;utf8,<svg id="swan" etc...' >
   ```

   Two issues: first there is a conflict of double quotes here. So you have to use single quotes for either the `src` attribute or within the SVG, `<img src='...' >`

   Secondly you cannot use the `#` symbol either, as in colours `#4512a7`;. You can use CSS colour names like `cadetblue` or `hotpink` etc..

   Alternatively you can covert your SVG code to base 64 using a free service like [Base 64 Image](https://base64-image.de)

6. Using the `<object>` tag