---
title: "CSS :not()"
date: 2021-09-04T22:02:56+01:00
draft: false
categories: [css]
tags: ["css"]
summary: "Using the CSS :not() pseudo selector"
---

Inverse of `:is()`

> check out :not(), which does the opposite and excludes the selectors passed to it.  
> -- [Caity Opelka](https://dev.to/imcaity)

## Examples

Here are some examples...

```css
* + *:not(li, dt, h4) {
    margin-top: 1em;
}
```

This selects every element that follows another unless it's a list item, a term
: definition term or an h4 header.

```css
img:not([alt]) {
  outline: 2px solid red;
}
```

This uses an attribute selector and puts an outine around any images that don't have an `alt` attribute.
