---
title: "Baguette Box JS"
date: 2021-08-06T14:57:11+01:00
draft: false
categories: ["Hugo", images, JS Library]
tags: [lightbox, image gallery, image processing, page resources, javascript]
summary: "Another image gallery set up with Hugo"
extlinks:
- [https://github.com/feimosi/baguetteBox.js, BaguetteBox JS]
- [https://webjs.net/en/p/simple-and-easy-to-use-lightbox-with-baguettebox-js-778.html, Web JS article, how to use Baguette Box]
---


So my second Javascript lightbox or gallery is *Baguette Box JS*. I was looking for:
1.  a vanilla JS library that didn't need another library
2.  Did a nice job of displaying images, ran smoothly and was easy to install.
3.  Was customizable for different uses.
4.  Was responsive and mobile (touch) friendly.
    
I think *Baguette Box* ticks the boxes. It has bunch of custom settings that are easy to add, the documentation is fairly simple but it doesn't need any more. Compared to Photoswipe there is no full screen button but full screen can be made the default in the code, so developer set rather than set by the user, so that's a minor downside compared to Photoswipe. 

However there are several upsides.

1. You don't need to add the image size for every image which is great. If you have a lot of different sized images that's pain. In my Photoswipe gallery I made the images the same size.
2. There is a lot less code to deal with (see below).
3. Customization is easy

[Customization](https://github.com/feimosi/baguetteBox.js#customization) comes in two forms. Firstly you can edit the CSS. Secondly you can cut'n'paste some code snippets between some JS brackets. Easy.


## How to integrate Baguette Box with Hugo

The two things necessary for Baguette Box are:

1. JS and CSS files for the Baguette Box to work. Using a CDN these are simply links the `<head>` of the document.
2. Some HTML code for your web page below to your page.

Here are the CDN links for the `<head>` section. I got them both from [CDNJS](https://cdnjs.com/libraries/baguettebox.js).

(You can of course always get the latest files from [BaguetteBox GitHub page](https://github.com/feimosi/baguetteBox.js) and link to those if you prefer.)

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.11.1/baguetteBox.js" integrity="sha512-FbW9...HE9cknTrg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.11.1/baguetteBox.css" integrity="sha512-3eoKq...wDVwg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
```

2. The basic HTML suggested by Baguette Box is:

```html
<div class="gallery">
    <a href="img/2-1.jpg" data-caption="Image caption">
        <img src="img/thumbnails/2-1.jpg" alt="First image">
    </a>
    <a href="img/2-2.jpg" data-caption="Image caption">
        <img src="img/thumbnails/2-2.jpg" alt="Second image">
    </a>
    ...
</div>
```

## Hugo code

Although the above code is simple enough if you've 30 images you don't want to be writing that out over and over again. This is where Hugo comes in.

On the Hugo side we need the images kept somewhere to use and some code to place the images in our HTML code above.

The two possible places to store the images are either:
1. in a leaf-bundle (simply a folder with an `index.md` file for the page) 
2. or subfolder of the top level `/assets` folder. 

I went for the leaf bundle method because I like the fact you can just dump a bunch of images in there, turn on Baguette Box with two words in the frontmatter, and let Hugo do the rest. 

Here is my code. The `.gallery` could be anything you want (no space though). It's just the class name of the `<div>` that contains the gallery.

```go-html-template
<script>
    baguetteBox.run('.gallery');
</script>


<div class="gallery">
    <ul>
        {{- with .Page.Resources.ByType "image" }}
            {{ range . }}
            {{ $thumbs := .Fill "200x200" }}
            <li>
                <a href="{{ .Name }}" data-caption="{{ with .Exif }}{{ .Date.Format "Jan 2006" }} {{ .Tags.Caption }} - {{ end }}{{ replace (replace .Name ".jpg" "" ) "-" " " | title }}">
                    <img src="{{ $thumbs.RelPermalink }}" alt="{{ replace .Name "-" " " }}">
                </a>
            </li>
            {{ end }}
        {{ else }}
            <p>No images for this page.</p>
        {{- end -}}
    </ul>
</div>


<script>
window.addEventListener('load', function() {
    baguetteBox.run('.gallery');
});

</script>
```

So where does this code go in a Hugo site? I put it in a shortcode so I could drop into any page. If you intend to have two or more galleries per page that would require some thought. You could add a parameter to the shortcode that is the class name of your `<div>` so you could use the shortcode more than once. But then you need to decide how to split your images up, otherwise done this way you'll end up with two identical galleries. If you're sticking with a leaf bundle (images and resources in the same folder as the `index.md` page file) you can create subfolders for each gallery and add another parameter to 


## Options

The various options for Baguette Box go curlies the bottom `<script>` tag code:

```js
baguetteBox.run('.gallery', {
    // Custom options
});
```

Like this:

```js
<script>
    window.addEventListener('load', function() {
      baguetteBox.run('.gallery', {
        fullScreen: true,  // default is false
        captions: false,   // default is true
        animation: fadeIn, // default is slideIn, false is the other option
        overlayBackgroundColor: 'rgba(245, 245, 245, .9)' // default is 'rgba(0,0,0,0.8)'
    });
    });
</script>
```

See the [full list of options](https://github.com/feimosi/baguetteBox.js#customization).

