---
title: "Footnotes in Markdown"
date: 2021-09-06T19:01:13+01:00
draft: false
categories: ["Hugo"]
tags: ["hugo", markdown]
summary: "Adding footnotes in markdown"
extlinks:
- ["https://www.markdownguide.org/extended-syntax/#footnotes", Markdown guide on footnotes]
---

So this is something I haven't used much before. You can add footnotes in markdown with some terse code.

To add a footnote[^1] you simply add `[^1]` after the word you want the footnote to appear. Then at the bottom of the page you can reference it using the same but with a following colon `[^1]: and your footnote after`.

This creates a small reference superscript[^super] number and a link to the bottom of the page.

It doesn't matter what you call the ref it will look like this and numbered according to position in the page:

```html
<sup id="fnref:2">
```

Footnote references don't have to be a number[^arse]. The could be anything. The previous one here was written as `[^arse]` in the markdown but still renders as a small 2 link.

## In Hugo

The default markdown renderer in Hugo is the Goldmark renderer. The footnotes at the bottom of the page are created inside a `<section>` element with a class name of `footnotes` and a horizontal line `<hr>` and an ordered list inside:

```html
<section class="footnotes" role="doc-endnotes">
    <hr>
    <ol>
        <li>first footnote...</li>
        <li>second etc..</li>
    </ol>
</section>
```

On this site I hid the `<hr>` element and added a top border to the `<section>` container. A heading was added using CSS pseudo element `::before`.

## Filler text

Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quisquam illum repudiandae velit minus id nihil doloremque distinctio eaque pariatur nobis cum magnam nemo, provident iure ipsam libero ducimus inventore aliquid itaque, totam, maxime commodi eveniet! Nesciunt cumque debitis culpa incidunt distinctio, alias molestias tempore, consectetur quia eaque nihil, mollitia facere.

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia pariatur eius ad, dolor accusamus deleniti doloremque. Voluptates eligendi perferendis blanditiis dolorem nostrum officiis beatae aut qui vero, architecto commodi voluptate maxime nulla corrupti numquam repellat placeat voluptatibus totam voluptatem sit explicabo ad optio! Perferendis dolores quibusdam aliquam nostrum tenetur voluptates soluta corporis deserunt ea vel maiores, magni fuga at, quisquam labore laboriosam. Explicabo voluptatum sed nihil nostrum quam aspernatur quo.

Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam mollitia excepturi laborum culpa voluptate eaque officia non unde similique, consectetur dolorem aliquam aspernatur quae velit dignissimos reprehenderit nisi, exercitationem sequi id fuga praesentium nobis pariatur. Iure, tempore quidem esse blanditiis amet tempora modi architecto commodi.

Explicabo obcaecati iste amet nesciunt hic commodi laudantium placeat eveniet, ea molestiae molestias suscipit ipsa porro eos accusamus unde, cumque, eum pariatur autem vel expedita alias sequi totam similique. Debitis illum eligendi officia, magni voluptatem unde quis, ab atque ullam minus error quam odit laboriosam.

Nesciunt dolores magni facere tenetur enim ullam optio neque debitis facilis. Autem dignissimos rem, eaque ratione obcaecati ut rerum modi, temporibus recusandae reiciendis repudiandae aliquid error tempore dolores minus, nihil quibusdam aperiam! Beatae corrupti facilis ut perspiciatis id atque rem, delectus repellat sit saepe error.

[^1]: Here is the first footnote in the page. Notice the horizontal line above the footnotes is placed automatically.

[^super]: Superscript is created using the `<sup>` tags and these have an ID to link to.

[^arse]: So this is the reference that used the word *arse* instead of a number.

    Here's another paragraph too belonging to the *arse* footnote. Indent subsequent paragraphs with 4 spaces otherwise they will just end up at the bottom of your main text rather than below the footnotes line.