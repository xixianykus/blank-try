---
title: "Text Decoration"
date: 2021-05-04T22:21:45+01:00
draft: false
categories: ["css"]
tags: ["css", blockquote]
summary: "The new CSS text-decoration and related properties"
css:
- "h4 {text-decoration: underline wavy green}"
- "h4 + p {tex-decoration: underline 1px #fff; transition: 0.6s;}"
- "h4 + p:hover {text-decoration: underline 2ex #fff5; text-decoration-skip-ink: none; text-underline-offset: -1.5ex; cursor: pointer;}"
extlinks:
- ["https://every-layout.dev/rudiments/units/#the-ch-and-ex-units", ex and ch units, By Every Layouts Heydon Pickering]
- [https://piccalil.li/quick-tip/add-scroll-margin-to-all-elements-which-can-be-targeted/, Piccalilli, about scroll-margins]
---

Some interesting additions to the ancient `text-decoration` property along with a logical use of `ex` units which measure the height of an *x* character. 

```css
text-decoration: underline; /* also overline, none and line-through */
text-decoration-thickness: 2ex;
text-decoration-color: #c4e4fb;
text-decoration-style: wavy; /* also solid, double, dotted and dashed */
text-decoration-skip-ink: none; /* also auto and all */
text-underline-offset: -2.8ex;
```

#### text-decoration: underline wavy green

On **hover** this demonstrates  
text-decoration-skip-ink: none and  
text-underline-offset: -1.5ex;

## text-decoration

This is short hand for:

| CSS property | Possible values |
|:---|:---|
| text-decoration-line: | underline / overline / line-through |
| text-decoration-style: | solid / dashed / dotted / double / wavy |
| text-decoration-color: | blue |
| text-decoration-thickness: | 2px / from-font / auto

The `text-decoration-thickness` value  `from-font` uses a thickness defined in the font. If the font does not include this information then the value of `auto` is chosen. The `auto` value lets the browser choose the thickness.

## text-decoration-skip-ink

This is not part of `text-decoration`. The default is auto and when set to `none` the underline or overline will go straight through the texts ascenders and descenders.

> specifies how overlines and underlines are drawn when they pass over glyph ascenders and descenders. 
>  
> [MDN docs](https://developer.mozilla.org/en-US/docs/Web/CSS/text-decoration-skip-ink)

## On ex and ch units

> The width, in pixels, of one full line of text is extrapolated from the relationship between the rem-based font-size and ch-based max-width. By delegating an algorithm to determine this value—rather than hard coding it as a px-based width—we avoid frequent and serious error. In CSS layout terms, an error is malformed or obscured content: data loss for human beings.
> 
> [Every Layout](https://every-layout.dev/rudiments/units/#the-ch-and-ex-units)