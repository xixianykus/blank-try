---
title: "Testpage"
date: 2021-05-13T16:42:53+01:00
draft: false
categories: ["Hugo"]
tags: ["hugo"]
weight: 1
summary: "A page for experimenting with Hugo's code"
extlinks:
- ['https://gohugo.io/functions/slice/', slice, Hugo docs]
- ['https://gohugo.io/functions/union/', union, Hugo docs]
- ['https://gohugo.io/functions/intersect/', intersect, Hugo docs]
- [https://gohugo.io/functions/append/, append, Hugo docs]
fauna: [goat, pangolin, echidna, weasel, pangolin, fossa, skunk]
fish: [shark, turbot, sunfish, striped marlin, goat]
birds: [sparrow, red kite, osprey, jackdaw]
goat: gary
shark: Simon
---


## Testing

This uses a shortcode combined with frontmatter variables.

The page has a weight of 1 so it always appears at the top of the sidebar list.

## Lesson 1 .Page

I didn't even know `.Page` was a variable but this wouldn't work without it. I was trying to range through a frontmatter array called `fauna` with `{{ range .Page.Params.fauna }}`. Without the `.Page` the build failed. This was from within a shortcode.

## 2. union

> Given two arrays or slices, returns a new array that contains the elements or objects that belong to either or both arrays/slices.

Note that `union` produces an array that does not contain repeat values if they appear in both arrays or even twice in on array.

There is also `intersect` which lists only those members that are in both arrays.

## 3. Joining two variables

In the frontmatter there are two variables:

```yaml
goat: gary
shark: Simon
```
How can we join these together? You can either make them into one variable using `add` or make them into an array using `slice`.

```go-html-template
{{ $two := add .Page.Params.goat .Page.Params.shark }}
```

To make an array instead just use `slice`

```go-html-template
{{ $two := slice .Page.Params.goat .Page.Params.shark }}
```

## 4. Using `append`

The `append` function is similar to union in that you can add to an array.

Starting with the `$both` variable which has already merged two arrays using `union` this adds a frontmatter variable `goat`.

```go-htmo-template
 {{ $both = $both | append .Page.Params.goat }}
 ```

 You can also add an array with append too:

 ```go-html-template
 {{ $s = $s | append (slice "d" "e") }}
 ```

{{< testpage >}}