---
title: "Using Gitinfo With Hugo"
date: 2021-05-26T20:41:06+01:00
draft: false
categories: ["Hugo"]
tags: ["hugo", git]
summary: "A short summary about the page"
extLinks:
- ['https://gohugo.io/variables/git/#readout', Hugo docs on GitInfo, More details]
---

The idea of being able to access git info from Hugo templates is an alluring one. I wanted to list out all the updates from the commit messages to save doing it manually on the front page. Unfortunately it doesn't work that way.

The {{ .GitInfo }} object in Hugo is purely for use on content files. It provides info from that page's last commit message and info only. It works in line with `{{ .Lastmod }}` which shows the time/date a page was last modified. With `.GitInfo` you can add the author name, author's email, the commit message, the hash and an abbreviated hash.

So unfortunately there is no `{{ .Site.GitInfo }}` to get a full list of changes.

If you want to use it in content pages add something like:

```go-html-template
{{ with .GitInfo }}
This page was last updated on {{ .Lastmod.Format "2 Jan 2006" }} by {{ .Author }}. The commit message states: {{ .Subject }}.
{{ end }}}}
```

Unlike `.GitInfo` the `.Lastmod` can be used with `.Site` to get the last update of the whole site. Just use `{{ .Site.Lastmod.Format "02/01/06" }}` replacing the date formatting with however you would prefer it.

There is also `.LastChange` which the docs say:

> **.Site.LastChange** &ndash; a string representing the date/time of the most recent change to your site. This string is based on the date variable in the front matter of your content pages.
> 
> [docs](https://gohugo.io/variables/site/#site-variables-list)

