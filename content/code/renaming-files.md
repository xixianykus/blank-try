---
title: "Renaming Files"
date: 2021-07-03T10:45:40+01:00
draft: false
categories: ["dev"]
tags: ["windows", Irfanview]
summary: "Some notes on how to batch rename files"
---

## Irfanview

Irfanview is an image viewer and editor but has built in batch renaming that works on any files, not just images.

1. Open a file in Irfanview: press `o`.
2. Use `b` to open the batch rename dialogue box.
3. Click the `batch rename` radio button (top left)
4. Press the `options` button to choose your renaming parameters.
5. Use `$N` for the original name. You can change strings of text to other strings.

Good for changing spaces in files to hyphens.

Changing capitals to lowercase was very hit and miss but a much better way is the following.

## Changing to lowercase using Windows

With a command prompt you can type the following from inside the directory you wish to change the file names:

```bash
for /f "Tokens=*" %f in ('dir /l/b/a-d') do (rename "%f" "%f")
```

Simple to use as above but see [Superuser article](https://superuser.com/questions/65302/is-there-a-way-to-batch-rename-files-to-lowercase) for details on how this works.

