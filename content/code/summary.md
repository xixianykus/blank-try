---
title: "Summary"
date: 2020-08-26T12:54:53+01:00
draft: false
categories: ["hugo"]
tags: ["hugo"]
summary: How the built in .Summary variable is used in the blank theme
extlinks:
- [https://gohugo.io/variables/page/, List of Hugo's page variables]
---

The rather confusingly named *summary* partial is called into the aside and represents each entry listed in list pages.

`.Summary` is also a built in variable that:

> generates summaries of content to use as a short version in summary views. [Hugo docs](https://gohugo.io/content-management/summaries/)

In this, the Blank, theme there is also a template (not a partial) named *summary.html* which contains the following:


```go-html-template

<article>
	<h3><a href="{{ .Permalink }}">{{ .Title }}</a></h3>
	<time>{{ .Date.Format "02.01.2006 15:04" }}</time>
	{{ range .Params.tags }}
	<a href="{{ "/tags/" | relLangURL }}{{ . | urlize }}">{{ . }}</a>
	{{ end }}
	<div>
		{{ .Summary }}
		{{ if .Truncated }}
			<a href="{{ .Permalink }}">Read more...</a>
		{{ end }}
	</div>
</article>
```

This is accessed from the list page with `{{ .Render "summary" }}` on line 12:

```go-html-template
{{ define "main" }}
	<main class="list-page">

		{{ if or .Title .Content }}
		<div>
			{{ with .Title }}<h1>{{ . }}</h1>{{ end }}
			{{ with .Content }}<div>{{ . }}</div>{{ end }}
		</div>
		{{ end }}

		{{ range .Paginator.Pages }}
			{{ .Render "summary" }}
		{{ end }}
		{{ partial "pagination.html" . }}
	</main>
{{ partial "sidebar.html" . }}
{{ end }}
```