---
title: "Codepen Embedding"
date: 2021-05-17T21:13:28+01:00
draft: false
categories: ["Hugo"]
tags: ["hugo", codepen]
summary: "Embedding Codepen pens in Markdown with Hugo"
extlinks:
- ['https://kinson.io/post/embed-codepen/', codepen script shortcode, by Jeremy Kinson]
---

Some experiments embedding Codepen pens in markdown files.

Codepen3 is probably the best. Had to wrap the script in a `<div>` so I could adjust the width.

{{< codepen1 >}}

This embed uses an iframe

{{< codepen2 >}}

This is from codepen3.html which is pretty neat. Just add the id of a pen: `{{⪡ codepen3 id="tGpju" ⪢}}`


{{< codepen3 id="tGpju" >}}

There are other options available...

```go-html-template
{{/* DEFAULTS */}}

{{ $user    := "your_username" }}
{{ $height  := 500 }}
{{ $tab     := "result" }}{{/* html|css|js|result */}}
{{ $theme   := 8862 }}{{/* create on codepen.io */}}

<div style="width: min(750px, 100%)">
    
    <script
        data-slug-hash="{{ .Get "id" }}"
        data-user="{{ or (.Get "user") $user }}"
        data-height="{{ or (.Get "height") $height }}"
        data-default-tab="{{ or (.Get "tab") $tab }}"
        data-theme-id="{{ or (.Get "theme") $theme }}"
        class='codepen'
        async
        src="//codepen.io/assets/embed/ei.js"
    ></script>
</div>
```