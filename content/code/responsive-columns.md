---
title: "Responsive Columns"
date: 2021-08-05T08:28:35+01:00
draft: false
categories: ["css"]
tags: ["css", responsive design]
summary: "How to make columns responsive"
extlinks:
- ["https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Columns/Handling_content_breaks_in_multicol", MDN on column breaks, a good guide]
---

In these days of flexbox and CSS grid it's easy to forget about using CSS columns. But these are a great solution because unlike flexbox and grid on single block of varied content can be made to flow from one column to the next.

This makes them ideal for markdown pages where flexbox or grid would make every element into a flex or grid item. 

## Adding responsiveness

This is pretty easy but something I didn't know.

If you are using CSS columns you can make them responsive simply by setting both the width and number of columns. Instead of using `column-width` and `column-count` just use the shorthand `columns`:

```css
section {
    columns: 300px 3;
}
```

According to [CSS tricks](https://css-tricks.com/almanac/properties/c/columns/):

> The column-count will act as the **maximum** number of columns, while the column-width will dictate the **minimum** width for each column. By pulling these properties together, the multi-column layout will automatically break down into a single column at narrow browser widths without the need of media queries or other rules.

In fact if you're using more than 2 columns it will break down into one less column as required until you get down to just 1 column.

## Column breaks

With columns with headings or other element you typically don't want the columns to break in certain places. There are several CSS properties to help here. One of the most useful is `break-inside` which takes the values of `avoid` and `auto` (the default).

For example if your column layout uses a list, `<ul>`, then you may want to keep each list item together. Simply use:

```css
li {
    break-inside: avoid;
}
```

The useful values for web pages (rather than print) are:

- `auto` - the default. Automatic page/column/region break inside the element
- `avoid` and `avoid-column` - the default. Automatic page/column/region break inside the element

Another two are `break-before` and `break-after`. These have many possible values though most are aimed at either page breaks (so for print material) or regions.

For forcing breaks in columns you only need:

```css
p {
    break-before: column;
}
```

The value of `column` means: always insert a column-break before the element.

Other values here are: 
- `auto` - this the default value and means automatic page/column/region break before the element
- `avoid` - avoid a page/column/region break before the element
- `avoid-column` 	avoid a column-break before the element