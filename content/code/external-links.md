---
title: "External Links"
date: 2020-08-26T13:38:04+01:00
draft: false
categories: ["Web dev"]
tags: ["hugo", arrays]
summary: "How external links stored as an array of arrays and are accessed"
---

In the frontmatter:

```yaml
extlinks:
- ["https://gohugo.io/templates/pagination/", Official docs on Pagination]
```

Which is accessed by the template partial `extlinks.html`:


```go-html-template
{{ with .Params.extlinks }}
<ul>
    {{ range . }}
    <li>
        <a href="{{ index . 0 }}">{{ index . 1 }}</a>
    </li>
    {{ end }}
</ul>
{{ end }}
```