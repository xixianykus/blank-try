---
title: "Clipboard JS"
date: 2021-12-10T12:19:40Z
draft: true
unfinished: true
categories: ["Hugo", JS Library]
tags: ["hugo"]
summary: "How to set up Clipboard JS in Hugo"
extlinks:
- [https://clipboardjs.com/, clipboardJS website, Official pages]
---

{{< cliptest >}}

Here's the code for the above example:

```html
<script>
    new ClipboardJS('.btn');
</script>

<input id="foo" type="text" value="https://github.com/zenorocha/clipboard.js.git">

<!-- Trigger -->
<button class="btn" data-clipboard-target="#foo">
    <img src="/img/clippy.svg" alt="Copy to clipboard">
</button>
```
{.btn}

This can be used on any element as long as the id of the `data-clipboard-target` value matches that of the element AND with the `#` symbol for id: `data-clipboard-target="#foo"`

Try this paragraph
{#clipme}

<button class="btn" id="clipme" data-clipboard-target="#clipme">this para</button>



## How to install

1. Add the script to the `<head>` of the document. So for example from a CDN:
   ```html
   <script src="https://cdn.jsdelivr.net/npm/clipboard@2.0.8/dist/clipboard.min.js"></script>
   <script>new ClipboardJS('.btn');</script>
   ```
2. Instantiate the script:


