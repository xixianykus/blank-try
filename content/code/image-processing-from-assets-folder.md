---
title: "Image Processing From Assets Folder"
date: 2021-05-08T15:33:57+01:00
draft: false
categories: ["Hugo"]
tags: ["image processing"]
summary: "How to get an image from the assets folder"
extlinks:
- ["https://mertbakir.gitlab.io/hugo/image-processing-in-hugo/", Image processing in Hugo, a new article by Mert Bakir - part of a whole series on Hugo]
---

Use this to get an image:

```go-html-template
{{ $src := .Get 0 }}
{{- $image := resources.Get $src -}}

<img src="{{ $image.Permalink }}" title="{{ .Get 1 }}">
```

The first line saves the string used in the first parameter of the shortcode as a variable we called `$src`. In this case the string is `img/dinosaur.webp"`.

The next line uses that variable to retrieve the image using `resources.Get` and store that in a variable `$image`.

Finally the link to that image is added using the `.Permalink` method.

To use the shortcode, stored in the `asset_image.html` file, we write this in our markdown file:

```go-html-template
{{⪡ asset_image "img/dinosaur.webp" "This is a T-Rex" ⪢}}
```


{{< asset_image "img/dinosaur.webp" "This is a T-Rex" >}}


## Image processing

Now here is something I'm unsure about. It looks like when you use image processing running `hugo server` the images aren't actually resized. I think it maybe just injects some CSS to make them appear resized. I think the actual resizing only takes place when the site is published to the the public file using the `hugo` command. But, I'm uncertain at present.


## A better way?

A briefer way might be to incorporate `.GetMatch` as in ` {{ $banner := resources.GetMatch "pathToYourImage" }}` see [Mert Bakir's article](https://mertbakir.gitlab.io/hugo/image-processing-in-hugo/)