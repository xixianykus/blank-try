---
title: ".GetPage Test"
date: 2021-08-03T18:56:34+01:00
draft: false
categories: ["Hugo"]
tags: ["hugo"]
summary: "A short summary about the page"
extlinks:
- ['https://gohugo.io/functions/getpage/', .GetPage from Hugo docs]
- ['https://gohugo.io/content-management/page-resources/', Page Resources]
css:
- ".getpage-updates {height: 45vh; overflow-x: hidden; overflow-y: scroll; font-size: .8em; background-color: #283E74; padding: 1em 1em 1em 40px; width: min(450px, 100%); float: right; border-radius: 40px}"
- ".getpage-update::after {content: 'Use overflow-y: hidden; to hide that bottom scroll bar.'; position: absolute; bottom: -2em;}"
---

{{< experiments/getpage >}}

So here's a quick experiment using `.GetPage`. This can retrieve any page object in your given the right url. You can then get different parts of the page: `.Content`, `.Title`, `.Permalink` etc.

Here I created a new folder `/headless` and added an `index.md` file with the setting `headless: true` in the frontmatter. `headless` is a predefined Hugo variable.

To retrieve the content section of the page I first created a shortcode, `getpage-test.html` and then used `with` like so:

```go-html-template
{{ with .Site.GetPage "/headless/index.md" }}
  <div class="getpage-updates">
      {{ .Content }}
  </div>
{{ end }}
```

The CSS class had some CSS applied through my frontmatter array `css`.

NOTE: This won't work with other pages in that `/headless/` folder. That's because other pages will be page resources and you need to use the 

## Layout on this page

The *Site Updates* box is floated to the right. This method ONLY works if the text on the left side (the content of this page) is long enough to push the bottom of the content section down far enough. A floated element can't do this.

I tried experimenting with grid but for that to work you would need just 2 HTML elements and with the *Site Updates* box being inside `section.content` every paragraph and heading becomes a grid item. So floating seems the only way to go.
