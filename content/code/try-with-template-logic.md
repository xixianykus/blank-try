---
title: "Archetypes with\n Template Logic"
date: 2021-05-23T07:53:32+01:00
draft: false
categories: ["Hugo"]
tags: ["hugo", archetypes]
summary: "An example of how Hugo's template language can be used and run from inside an archetypes file."
---

So this page was generated with the following template code at the bottom of the archetypes/code.md file:

```go-html-template
## Latest Code posts

{{ range first 10 ( where .Site.RegularPages "Section" "code" ) }}
* [{{ .Title }}]({{ .Permalink }})
{{ end }}
```

I couldn't get the spacing of the list items right despite trying with `{{- range -}}` hyphens.

So it produced a list with gaps:

```md
* [Testpage](https://blank-try.netlify.app/code/testpage/)

* [Try With Template Logic](https://blank-try.netlify.app/code/try-with-template-logic/)

* [Page Resources](https://blank-try.netlify.app/code/page-resources/)

* [New Image Shortcode](https://blank-try.netlify.app/code/new-image-shortcode/)

// etc.
```
I've deleted these spaces below because they annoy me.

Presumably you could use HTML if HTML is allowed in the config OR perhaps simply call a shortcode in.

The docs warn:

> the site will only be built if the .Site is in use in the archetype file, and this can be time consuming for big sites.
> 
> [Hugo docs](https://gohugo.io/content-management/archetypes/#create-a-new-archetype-template)

## Latest Code posts

* [Testpage](https://blank-try.netlify.app/code/testpage/)
* [Try With Template Logic](https://blank-try.netlify.app/code/try-with-template-logic/)
* [Page Resources](https://blank-try.netlify.app/code/page-resources/)
* [New Image Shortcode](https://blank-try.netlify.app/code/new-image-shortcode/)
* [Line Breaks
 in Headings](https://blank-try.netlify.app/code/line-breaks-in-headings/)
* [Custom Tags Page](https://blank-try.netlify.app/code/custom-tags-page/)
* [Codepen Embedding](https://blank-try.netlify.app/code/codepen-embedding/)
* [Blockquote Styling](https://blank-try.netlify.app/code/blockquote-styling/)
* [Sidebar Code](https://blank-try.netlify.app/code/sidebar-code/)
* [Image Processing From Assets Folder](https://blank-try.netlify.app/code/image-processing-from-assets-folder/)
