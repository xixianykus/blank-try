---
title: ":target pseudo class"
date: 2021-05-22T20:07:30+01:00
draft: false
categories: ["css"]
tags: ["experiments", css]
summary: "Simple demo of the amazing :target CSS pseudo class. Very cool."
extLinks:
- ['https://www.youtube.com/watch?v=6j5q-hP8sfk', CSS Lightbox vid, by Kevin Powell]
---

This page can be used to practice things with shortcodes. I suppose it could have it's own layout but probably leave that for now.

It will use shortcodes in the `shortcodes/experiments` folder


## Using the CSS `:target` pseudo class



{{< experiments/css_target >}}

