---
title: "Aside"
date: 2020-08-24T19:58:39+01:00
draft: false
categories: ["Web dev"]
tags: ["hugo", "nav"]
summary: "Code from the sidebar"
---

Here is some code from the side bar.

```go-html-template
<aside>
	<div>
		<div>
			<h3>LATEST POSTS</h3>
		</div>
		<div>
			<ul>
				{{ range first 12 (where .Site.RegularPages "Type" "in" .Site.Params.mainSections) }}
				<li><a href="{{ .RelPermalink }}">{{ .Title }}</a></li>
				{{ end }}
			</ul>
		</div>
	</div>
</aside>
```

And this is in the config.toml 

```toml
[params]
  mainSections = ["posts", "code", "duck"]
```

This defines which posts will appear in the sidebar as can be seen in the above code. This makes it easy to include or remove a section from the latest posts.

