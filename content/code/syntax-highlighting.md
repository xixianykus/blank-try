---
title: "Syntax Highlighting"
date: 2019-12-05T17:12:15Z
draft: true
categories: ["Web dev"]
tags: ["hugo"]
summary: "READ about the CSS here"
extlinks: 
- ["https://netlify.com", "Netlify"]
- ["https://duckduckgo.com/", "DuckDuckGo"]
---


Syntax highlighting appears without any config.toml configuration. Just the language added after the first three backticks.

```css
body {
    font-size: 20px;
    font-family: 'courier new', 'segoe ui';
    color: rgb(34, 82, 34);
    background: hsl(351, 70%, 91%);
    display: grid;
    grid-template-columns: 3fr 1fr;
}

header,
footer {
    grid-column: 1 / 3;
    background-color: rgb(171, 195, 247);
    padding: 1em;
}
```

However it's much better configured with a theme. The background can be changed with CSS so pick a colour or tone that's not too far from the original.

```toml
[markup]
  [markup.highlight]
    codeFences = true
    guessSyntax = false
    hl_Lines = ""
    lineNoStart = 1
    lineNos = true
    lineNumbersInTable = false
    noClasses = true
    style = "monokailight"
    tabWidth = 4
```