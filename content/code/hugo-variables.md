---
title: "Hugo Variables"
date: 2021-05-03T13:03:55+01:00
draft: false
categories: ["Hugo"]
tags: ["variables"]
summary: "A list of Hugos most useful variables."
extlinks:
- ["https://gohugo.io/variables/site/", Hugo docs site variables]
---

## Page Variables

{{< svg/hugo-logo >}}

Because the variables are built in to Hugo when accessing them you don't need to start with `.Params`.

**.AlternativeOutputFormats**   contains all alternative formats for a given page; this variable is especially useful link rel list in your site’s <head>. (See Output Formats.)

**.Aliases**  aliases of this page

**.Content** the content itself, defined below the front matter. See also **.RawContent**

**.Data** the data specific to this type of page.

**.Date** the date associated with the page; .Date pulls from the date field in a content’s front matter. See also .ExpiryDate, .PublishDate, and .Lastmod.
**.Description**  the description for the page.

**.Dir**   the path of the folder containing this content file. The path is relative to the content folder.


**.Draft**    a boolean, true if the content is marked as a draft in the front matter.

**.ExpiryDate**    the date on which the content is scheduled to expire; .ExpiryDate pulls from the expirydate field in a content’s front matter. See also .PublishDate, .Date, and .Lastmod.

**.File**    filesystem-related data for this content file. See also File Variables.

**.FuzzyWordCount**    the approximate number of words in the content.

**.Hugo**    see Hugo Variables.

**.IsHome**    true in the context of the homepage.

**.IsNode**    always false for regular content pages.

**.IsPage**    always true for regular content pages.

**.IsSection**    true if .Kind is section.

**.IsTranslated**    true if there are translations to display.

**.Keywords**    the meta keywords for the content.

**.Kind**    the page’s kind. Possible return values are page, home, section, taxonomy, or term. Note that there are also RSS, sitemap, robotsTXT, and 404 kinds, but these are only available during the rendering of each of these respective page’s kind and therefore not available in any of the Pages collections.

**.Language**    a language object that points to the language’s definition in the site config. .Language.Lang gives you the language code.

**.Lastmod**    the date the content was last modified. .Lastmod pulls from the lastmod field in a content’s front matter.

If lastmod is not set, and .GitInfo feature is disabled, the front matter date field will beused.  If lastmod is not set, and .GitInfo feature is enabled, .GitInfo.AuthorDate will beused instead.

See also .ExpiryDate, .Date, .PublishDate, and .GitInfo.


**.LinkTitle**    access when creating links to the content. If set, Hugo will use the linktitle from the front matter before title.

**.Next**    Points up to the next regular page (sorted by Hugo’s default sort). Example: {{with .Next}}{{.Permalink}}{{end}}. Calling .Next from the first page returns nil.

**.NextInSection**    Points up to the next regular page below the same top level section (e.g. in /blog)). Pages are sorted by Hugo’s default sort. Example: {{with .NextInSection}}{{.Permalink}}{{end}}. Calling .NextInSection from the first page returns nil.

**.OutputFormats**    contains all formats, including the current format, for a given page. Can be combined the with .Get function to grab a specific format. (See Output Formats.)

**.Page** The single version has no reference I can find. Best guess is that this is a reference to the page you are in on. I found it useful when accessing frontmatter variables from within a shortcode: `.Page.Paramas.variableName`

**.Pages**    a collection of associated pages. This value will be nil within the context of regular content pages. See .Pages.

**.Permalink**    the Permanent link for this page; see Permalinks

**.Plain**    the Page content stripped of HTML tags and presented as a string.

**.PlainWords**    the slice of strings that results from splitting .Plain into words, as defined in Go’s strings.Fields.

**.Prev**    Points down to the previous regular page (sorted by Hugo’s default sort). Example: {{if .Prev}}{{.Prev.Permalink}}{{end}}. Calling .Prev from the last page returns nil.

**.PrevInSection**    Points down to the previous regular page below the same top level section (e.g. /blog). Pages are sorted by Hugo’s default sort. Example: {{if .PrevInSection}}{{.PrevInSection.Permalink}}{{end}}. Calling .PrevInSection from the last page returns nil.

**.PublishDate**    the date on which the content was or will be published; .Publishdate pulls from the publishdate field in a content’s front matter. See also .ExpiryDate, .Date, and .Lastmod.

**.RSSLink (deprecated)**    link to the page’s RSS feed. This is deprecated. You should instead do something like this: {{ with .OutputFormats.Get "RSS" }}{{ .RelPermalink }}{{ end }}.

**.RawContent**    raw markdown content without the front matter. Useful with www.remarkjs.com (markdown slideshow maker.)

**.ReadingTime**    the estimated time, in minutes, it takes to read the content.

**.Resources**    resources such as images and CSS that are associated with this page

**.Ref**    returns the permalink for a given reference (e.g., .Ref "sample.md"). .Ref does not handle in-page fragments correctly. See Cross References.

**.RelPermalink**    the relative permanent link for this page.

**.RelRef**    returns the relative permalink for a given reference (e.g., RelRef "sample.md"). .RelRef does not handle in-page fragments correctly. See Cross References.

**.Site**    see Site Variables.

**.Summary**    a generated summary of the content for easily showing a snippet in a summary view. The breakpoint can be set manually by inserting `<!-- -->` at the appropriate place in the content page, or the summary can be written independent of the page text. See [Content Summaries](https://gohugo.io/content-management/summaries/) for more details.

**.TableOfContents**    the rendered table of contents for the page.

**.Title**    the title for this page.

**.Truncated**    a boolean, true if the .Summary is truncated. Useful for showing a “Read more…” link only when necessary. See Summaries for more information.

**.Type**    the content type of the content (e.g., posts).
.UniqueID (deprecated)
    the MD5-checksum of the content file’s path. This variable is deprecated and will be removed, use **.File.UniqueID** instead.

**.Weight**    assigned weight (in the front matter) to this content, used in sorting.

**.WordCount**    the number of words in the content.