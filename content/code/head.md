---
title: "Head"
date: 2020-08-24T19:52:07+01:00
draft: false
summary: The head section used by the Blank theme.
---

```go-html-template
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{{ .Title }} | {{ .Site.Title }}</title>
	{{ with .Site.Params.description }}<meta name="description" content="{{ . }}">{{ end }}
	{{ with .Site.Params.author }}<meta name="author" content="{{ . }}">{{ end }}
	<link rel="stylesheet" href="{{ "css/style.css" | relURL }}">
	{{ with .OutputFormats.Get "RSS" -}}
		{{ printf `<link rel="%s" type="%s" href="%s" title="%s">` .Rel .MediaType.Type .RelPermalink $.Site.Title | safeHTML }}
	{{- end }}
</head>
```