---
title: "New Image Shortcode"
date: 2021-05-20T09:46:58+01:00
draft: false
categories: ["Hugo"]
tags: ["hugo", shortcode]
summary: "My new image shortcode"
---

So rather than use the regular markdown code for images (`![alt for image](url-of-image.png)`) I made a simple shortcode called `img.html`.

In particular I wanted to be able to style images, float them left or right, round corners, etc..

You don't have to type the full path, just the file name and as long as it's in the static/img/ folder it will work.

I was going to use positional setting for the src value but it seems shortcode have to be set to positional or named, but not both.

It's interesting that the variables can be used inside the `with` statements, which reduce the context.



```go-html-template
{{ $src := .Get "src" }}
{{ $alt := .Get "alt" }}
{{ $title := .Get "title" }}
{{ $style := .Get "style" }}

<img src="/img/{{ $src }}"
{{ with "style" }} style="{{ $style | safeCSS }}" {{ end }}
{{ with "alt" }} alt="{{ $alt }}" {{ end }} 
{{ with "title" }} title="{{ $title }}" {{ end }}>
```

## Future idea?

Create a similar shortcode but that uses Hugo's image processing to resize images and create a srcset for responsive.