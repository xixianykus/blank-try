---
title: "Setting Up RSS With Hugo"
date: 2021-05-26T20:59:21+01:00
draft: false
categories: ["Hugo"]
tags: ["hugo", RSS, CSS]
summary: "RSS is provided automatically with Hugo. You just need a link"
extlinks:
- ['https://www.w3schools.com/XML/xml_rss.asp', W3 Schools on RSS, Good intro]
- ['https://gohugo.io/templates/rss/', Hugo docs on RSS, the official page]
---

The Hugo docs page on RSS is a little more complicated than it needs to be if all you want is an RSS feed. Hugo does this automatically and if you want to display that on the footer of your site or elsewhere all you have to do is provide a link. The link is simply `https://yoursite.com/index.xml`. You can test it out by simply adding `index.xml` to the end of your site's url or localhost.

Additionally each section will have it's own RSS. Again just add `index.xml` to the end of each section to see it.

Additionally you want a link in the `<head>` of each page linking to that sections RSS. See [the docs](https://gohugo.io/templates/rss/#reference-your-rss-feed-in-head) for the options.

This RSS page looks great in Firefox which provides some nice styling but rather user unfriendly in Chrome based browsers (inc Edge) which provides no CSS at all.

```xml
<rss xmlns:atom="http://www.w3.org/2005/Atom" version="2.0">
<channel>
<title>home page on Blank Try</title>
<link>http://localhost:64421/</link>
<description>Recent content in home page on Blank Try</description>
<generator>Hugo -- gohugo.io</generator>
<language>en-us</language>
<atom:link href="http://localhost:64421/index.xml" rel="self" type="application/rss+xml"/>
<item>
<title>Testpage</title>
<link>http://localhost:64421/code/testpage/</link>
<pubDate>Thu, 13 May 2021 16:42:53 +0100</pubDate>
<guid>http://localhost:64421/code/testpage/</guid>
<description>A page for experimenting with Hugo&rsquo;s code</description>
</item>
etc...
```

If you want to create your own RSS template first know what you're getting into.

This is the default, built in template used by Hugo. You can actually have a few options to change some things in the site config file.

```go-html-template
{{- $pctx := . -}}
{{- if .IsHome -}}{{ $pctx = .Site }}{{- end -}}
{{- $pages := slice -}}
{{- if or $.IsHome $.IsSection -}}
{{- $pages = $pctx.RegularPages -}}
{{- else -}}
{{- $pages = $pctx.Pages -}}
{{- end -}}
{{- $limit := .Site.Config.Services.RSS.Limit -}}
{{- if ge $limit 1 -}}
{{- $pages = $pages | first $limit -}}
{{- end -}}
{{- printf "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?>" | safeHTML }}
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>{{ if eq  .Title  .Site.Title }}{{ .Site.Title }}{{ else }}{{ with .Title }}{{.}} on {{ end }}{{ .Site.Title }}{{ end }}</title>
    <link>{{ .Permalink }}</link>
    <description>Recent content {{ if ne  .Title  .Site.Title }}{{ with .Title }}in {{.}} {{ end }}{{ end }}on {{ .Site.Title }}</description>
    <generator>Hugo -- gohugo.io</generator>{{ with .Site.LanguageCode }}
    <language>{{.}}</language>{{end}}{{ with .Site.Author.email }}
    <managingEditor>{{.}}{{ with $.Site.Author.name }} ({{.}}){{end}}</managingEditor>{{end}}{{ with .Site.Author.email }}
    <webMaster>{{.}}{{ with $.Site.Author.name }} ({{.}}){{end}}</webMaster>{{end}}{{ with .Site.Copyright }}
    <copyright>{{.}}</copyright>{{end}}{{ if not .Date.IsZero }}
    <lastBuildDate>{{ .Date.Format "Mon, 02 Jan 2006 15:04:05 -0700" | safeHTML }}</lastBuildDate>{{ end }}
    {{- with .OutputFormats.Get "RSS" -}}
    {{ printf "<atom:link href=%q rel=\"self\" type=%q />" .Permalink .MediaType | safeHTML }}
    {{- end -}}
    {{ range $pages }}
    <item>
      <title>{{ .Title }}</title>
      <link>{{ .Permalink }}</link>
      <pubDate>{{ .Date.Format "Mon, 02 Jan 2006 15:04:05 -0700" | safeHTML }}</pubDate>
      {{ with .Site.Author.email }}<author>{{.}}{{ with $.Site.Author.name }} ({{.}}){{end}}</author>{{end}}
      <guid>{{ .Permalink }}</guid>
      <description>{{ .Summary | html }}</description>
    </item>
    {{ end }}
  </channel>
</rss>
```

To add CSS you'll have to create a new template..

## Registering

So from W3 Schools:

> Submit your RSS feed to the RSS Feed Directories (you can Google or Yahoo for "RSS Feed Directories"). Note! The URL to your feed is not your home page, it is the URL to your feed, like "https://www.w3schools.com/xml/myfirstrss.xml". Here is a free RSS aggregation service:
>
> [Newsisfree](http://www.newsisfree.com/): [Register here](http://www.newsisfree.com/user/new/)
>
> Register your feed with the major search engines:
>
>    Google - http://www.google.com/submityourcontent/website-owner
>    Bing - http://www.bing.com/toolbox/submit-site-url
>
> Update your feed - After registering your RSS feed, you must make sure that you update your content frequently and that your RSS feed is constantly available.
>
> [W3 Schools](https://www.w3schools.com/XML/xml_rss.asp)


## Link in the `<head>`

This will help search engines find your RSS feed. It's easy in Hugo. Just add this to your head for an RSS link on the home page and section pages:

```go-html-template
{{ with .OutputFormats.Get "rss" -}}
  {{ printf `<link rel="%s" type="%s" href="%s" title="%s" />` .Rel .MediaType.Type .Permalink $.Site.Title | safeHTML }}
{{ end -}}
```