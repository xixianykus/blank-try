---
title: "Youtube RSS"
date: 2022-09-06T22:59:02+01:00
draft: false
categories: ["RSS"]
tags: ["youtube", RSS]
summary: "How to get the RSS feed for a Youtube channel"
---

The feed for a Youtube channel is this `https://www.youtube.com/feeds/videos.xml?channel_id=` followed by the channel ID.

To get the ID go to the channel page and press `Ctrl + U` to view the source code. Search the page for `externalId` and the ID will be right there looking like `UC7_gcs09iThXybpVgjHZ_7g`

You can check the feed url by pasting into a browser tab and looking at the result.