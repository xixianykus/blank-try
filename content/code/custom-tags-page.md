---
title: "Custom Tags Page"
date: 2021-05-18T03:29:02+01:00
draft: false
categories: ["Hugo"]
tags: ["hugo"]
summary: "Notes about the custom tags page"
---

This site uses the Blank theme which uses on list page template for section pages, taxonomy pages and taxonomy term pages. I wanted specific customization of this, to make a basic tag cloud and no pagination.

## How to

Created a new template in `layouts/tags/list.html`. There is [lots of choice](https://gohugo.io/templates/lookup-order/#examples-layout-lookup-for-taxonomy-pages) to choose from. This doesn't affect the [categories page](/categories/) which still uses the `layouts/_default/list.html` page.

The code for the new page is:

```go-html-template
{{ define "main" }}

<main class="list-page">

    {{ if or .Title .Content }}
    <div>
        {{ with .Title }}<h1>{{ . }}</h1>{{ end }}
        {{ with .Content }}<div>{{ . }}</div>{{ end }}
    </div>
    {{ end }}

<ul class="tags-list">
    {{ range .Pages.ByTitle }}
        <li><a href="{{ .Permalink }}">{{ .Title }}</a></li>
    {{ end }}
</ul>
```

## Modify the sidebar

The sidebar was set up to change depending on whether it's section was something or nothing (like home, links and about pages). For the tags page the section is tags. To include this I added an or to the if statement:

```go-html-templates
{{ if or  (eq .Section "") (eq .Section "tags") }}
<h2>Latest Posts</h2>
<ul>...
```

</main>
{{ partial "sidebar.html" . }}    
{{ end }}
```

