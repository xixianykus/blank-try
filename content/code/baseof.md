---
title: "Baseof"
date: 2020-08-26T13:16:48+01:00
draft: false
categories: ["Hugo"]
tags: [printf]
summary: "Code for the baseof and head section template"
---

Code for this site's `<head>` section. 

Interesting is line 14, the RSS section using `printf`.

```go-html-template
<!DOCTYPE html>
<html lang="{{ .Site.LanguageCode | default "en-us" }}">
<head>
	{{ hugo.Generator }}
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>{{ .Title }} | {{ .Site.Title }}</title>
	{{ with .Site.Params.description }}<meta name="description" content="{{ . }}">{{ end }}
	{{ with .Site.Params.author }}<meta name="author" content="{{ . }}">{{ end }}
	<link href="https://fonts.googleapis.com/css2?family=Epilogue:ital,wght@0,300;0,600;1,300&family=Roboto+Mono:wght@400;500&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="{{ "css/style.css" | relURL }}">
	{{ with .OutputFormats.Get "RSS" -}}
		{{ printf `<link rel="%s" type="%s" href="%s" title="%s">` .Rel .MediaType.Type .RelPermalink $.Site.Title | safeHTML }}
	{{- end }}
</head>
<body>
	{{ partial "header" . }}
	{{ block "main" . }}{{ end }}
	{{ partial "test-panel" . }}
	{{ partial "footer" . }}
</body>
</html>
```

