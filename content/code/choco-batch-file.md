---
title: "Choco Batch File"
date: 2022-12-09T17:03:41Z
draft: false
categories: ["Windows"]
tags: ["Chocolatey", "package managers", Windows]
summary: "A batch file to install a list of programs with Chocolatey"
---


Copy & paste the following into a batch file (a file with a `.bat` extension). 

Edit the line 47 to pick the programs you want.

Run the batch file from a terminal with **admin** rights.

```bash
echo OFF

NET SESSION >nul 2>&1

IF %ERRORLEVEL% EQU 0 (

   echo.

) ELSE (

   echo.-------------------------------------------------------------

   echo ERROR: YOU ARE NOT RUNNING THIS WITH ADMINISTRATOR PRIVILEGES.

   echo. -------------------------------------------------------------

   echo. If you're seeing this, it means you don't have admin privileges!

   pause

   echo.

   echo. You will need to restart this program with Administrator privileges by right-clicking and select "Run As Administrator"

   pause 

    echo.

   echo Press any key to leave this program. Make sure to Run As Administrator next time!

   pause

   EXIT /B 1

)


powershell -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "[System.Net.ServicePointManager]::SecurityProtocol = 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

choco feature enable -n=allowGlobalConfirmation

echo Chocolatey is ready to begin installing packages!

pause


choco install brave curl edgedeflector git golang hugo-extended irfanview irfanviewplugins keepass keepass-plugin-favicon keepass-plugin-keetheme lunacy mpc-hc-clsid2 nodejs-lts obsidian qbittorrent signal thunderbird vscode vscode-markdown-all-in-one wget -y

pause
echo.
echo Your installation is complete.
pause

```