---
title: "Blockquote Styling"
date: 2021-05-17T12:22:01+01:00
draft: false
categories: ["css"]
tags: ["css"]
summary: "Quick way to add a big double quote to the start of a blockquote"
extlinks:
- ['https://altcodeunicode.com/', Alt Code Unicode, A nice clear table of characters with a search]
- ['https://isotropic.co/the-best-css-styling-for-blockquotes/', More blockquote styles, Ideas from Codepen]
---

There are various ways to do this but to do it all in the CSS requires using the `::before` pseudo-class and adding the quote using the content property. 

You can add the the quote directly using the alt code 0147 (hold down the Alt key and press 0147 on the number pad section of the keyboard), or you can put the [unicode code](https://altcodeunicode.com/) in which is `\201C`. Unicode characters can be chained together and if you need a space the code is `\0020` which you can also get with the Alt code 32.

Three unicode characters chained together:

```css
::before {
    content: '\25B2\0020\25AC';
}
```



```css
blockquote {
    position: relative;
    margin-left: 0;
}

blockquote::before {
    position: absolute;
    content: '\201C';
    display: inline;
    font-size: 4em;
    left: -1.3rem;
    top: -5px;
}
```

You can also use the HTML `cite` attribute and pull it in using `attr(cite)` in using content in the `::after` pseudo-class:

```html
<blockquote cite="Nelson Mandela">
```

And the CSS would be:

```
blockquote::after {
    content: attr(cite);
}
```

See [this](https://codepen.io/harmputman/pen/IpAnb) Codepen.

However since you cannot add the `cite` attribute in markdown you could either create a shortcode for it or perhaps an easier way to to simply make sure the last line of every blockquote has a source and add it there. Then use CSS to style the last paragraph in a blockquote: `blockquote ::last-child`. This would also allow the cite to be linked too.
