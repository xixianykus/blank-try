---
title: "Image Processing Leaf Bundle"
date: 2021-05-08T03:19:44+01:00
draft: false
categories: ["Hugo"]
tags: ["hugo", image processing]
summary: "A leaf bundle page to experment with Hugo's image processing"
extlinks: 
- [https://gohugo.io/content-management/page-resources/, Page resources, Hugo docs]
- [https://alexlakatos.com/web/2020/07/17/hugo-image-processing/, Image processing, by Alex Lakatos]
- ["https://discourse.gohugo.io/t/how-to-use-image-processing-using-a-short-code/28264/5", How to use image processing in a shortcode, Interesting post from the Hugo forum]
- ["https://nilsnh.no/2018/06/10/hugo-how-to-add-support-for-responsive-images-trough-image-processing-and-page-bundles-3/", Image Processing and Page Bundles, Blog post by Nils Norman Haukås]
- ["https://www.youtube.com/watch?v=2QYpkrX2N48", Kevin Powell on srcset, Excellent vid explaining srcset and how mobile 2x (retina) and 3x screens work]
- ['https://mertbakir.gitlab.io/hugo/image-processing-in-hugo//', Image Processing in Hugo, Good succinct article by Mert Bakir]
---

{{< bundle_image name="girl.webp" alt="some girl from bundle_image.html" caption="a girl from unsplash" >}}

So  trying to get the image processing working. This usually means keeping images in the `/assets/` directory or as a *page resource* in the same folder as a leaf bundle (which is what I'm trying here.)

## Processed images

The processed images have ended up in `/resources/_gen/images/` and there were 23 images in total. This is the default.

> These images are not “Hugo fast” to generate, but once generated they can be reused.
> 
> [Hugo docs](https://gohugo.io/content-management/image-processing/#image-processing-performance-consideration)

To get rid of the excess: `hugo --gc`. The `gc` stands for garbage collection. When I did this the number of images was reduced to 6.

## Adding an image in Markdown

If your image is part of a page bundle then all you neeed is: `![webp image of a girl](girl.webp)`

## Using Photoshop

Since much of the time images will be first processed in Photoshop (to edit and convert from RAW/PSD to jpg) it's possible to export multiple files in one go at different sizes and using a naming convention.

A shortcode could be created incorporating this naming convention into a `<picture>` element (see below).

## The `<picture>` element

According to [W3 Schools](https://www.w3schools.com/tags/tag_picture.asp):

> The <picture> element contains two tags: one or more <source> tags and one <img> tag.
>
> The browser will look for the first <source> element where the media query matches the current viewport width, and then it will display the proper image (specified in the srcset attribute). The <img> element is required as the last child of the <picture> element, as a fallback option if none of the source tags matches.
>
> The <picture> element works "similar" to <video> and <audio>. You set up different sources, and the first source that fits the preferences is the one being used.
>
>  [W3 Schools](https://www.w3schools.com/tags/tag_picture.asp)

The example code they use is: 

```html
 <picture>
  <source media="(min-width:650px)" srcset="img_pink_flowers.jpg">
  <source media="(min-width:465px)" srcset="img_white_flower.jpg">
  <img src="img_orange_flowers.jpg" alt="Flowers" style="width:auto;">
</picture>
```

## srcset Without the Picture Element

You don't need `<picture>` to use the attribute `srcset`. Here it is used just using the `<img>` tag:

```html
<img src="img/cat-500.jpg"
    srcset="img/cat-500.jpg,
            img/cat-1000.jpg 2x,
            img/cat-1500.jpg 3x"
    alt="pet cat">
```

This says use the 500px image normally but on a 2x device use the 1000px one and on a 3x device use the 1500px one.

You can also choose instead of device type the width of the image. That is how many pixels it will be when rendered. This assumes the image width is assigned a percentage value.


```html
<img src="img/cat-500.jpg"
    srcset="img/cat-500.jpg 500w,
            img/cat-1000.jpg 1000w,
            img/cat-1500.jpg 1500w"
    alt="pet cat">
```

(NB. When testing in dev tools it might be handy to disable the cache in the Network tab. Otherwise it will keep the highest pixel version.)

## Retina and other mobile screens

In the old days a pixel in css, eg. `width: 320px;`, meant exactly that. But with the extremely high density of pixel per inch on modern mobile screens that would have rendered something tiny. So with retina screens, aka 2x, this was multiplied by 2. If CSS said 1px that would be rendered as 2px x 2px square, ie. 4px. There are now 3x screens as well. These render a single pixel as 3 x 3 or 9 pixels altogether.

In Chrome dev tools when in responsive mode you can change the DPR (density pixel ratio). To do this you will probably first need to click the 3 vertical dots drop down.


## Getting an image from the Assets folder

In Hugo images must be either in the assets folder or part of a leaf branch.

From the assets folder the following works... ?

## Nils Code that works

This code is from [ Nils Norman Haukås](https://nilsnh.no/2018/06/10/hugo-how-to-add-support-for-responsive-images-trough-image-processing-and-page-bundles-3/)

```go-html-template
{{ $altText := .Get "alt"}}
{{ $caption := .Get "caption"}}
{{ with $.Page.Resources.GetMatch (.Get "name") }}
  <figure>
    <a href="{{.RelPermalink}}">
      <img
        srcset="
          {{ (.Resize "320x").RelPermalink }} 320w,
          {{ (.Resize "600x").RelPermalink }} 600w,
          {{ (.Resize "1200x").RelPermalink }} 2x"
        src="{{ (.Resize "600x").RelPermalink }}" alt="{{$altText}}"/>
    </a>
    <figcaption><p>{{ $caption }}</p></figcaption>
  </figure>
{{ else }}
  could not find image
{{ end }}
```

## Bep's shortcode example

Here is [an example](https://discourse.gohugo.io/t/how-to-use-image-processing-using-a-short-code/28264/6?u=horbes) posted by Bep on the Hugo forum:

```go-html-template
{{- $img := .Page.Resources.GetMatch .Destination -}}

{{- if and (not $img) .Page.File -}}
    {{ $path := path.Join .Page.File.Dir .Destination }}
    {{- $img = resources.Get $path -}}
{{- end -}}


{{- with $img -}}
    {{- $large := $img.Resize "1200x" -}}
    {{ $medium := $large.Fill "726x402" -}}
    {{ $small := $medium.Fill "458x254" -}}
    <figure class="image-caption">
        <img alt="{{ $.Text }}" srcset="
            {{ $small.RelPermalink }} 458w,
            {{ $medium.RelPermalink }} 726w,
            {{ $large.RelPermalink }} 1200w" sizes="50vw" src="{{ $small.RelPermalink }}" />
        <figcaption>{{ with $.Title | safeHTML }}{{ . }}{{ end }}</figcaption>
    </figure>
{{- else -}}
    <img src="{{ .Destination | safeURL }}" alt="{{ $.Text }}" />

{{- end -}}
```

## Using files from /static/

Hugo cannot process files directly from the static folder since by definition that folder is not processed.

However you can *mount* that folder using 3 simple lines of code in the config file:

```toml
[[module.mounts]]
  source = "static/images"
  target = "assets/images"
```
Check out the [rules of module mounts](https://gohugo.io/hugo-modules/configuration/#module-config-mounts) in the docs.


{{< picture name="girl.webp" alt="girl in webp" caption="girl in webp from picture shortcode" >}}
