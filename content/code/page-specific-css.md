---
title: "Page Specific CSS"
date: 2021-05-26T16:23:42+01:00
draft: false
categories: ["Hugo"]
tags: ["hugo", css]
summary: "A short summary about the page"
extLinks:
- ['https://gohugo.io/variables/files/', Hugo docs on .File variable, Lots more there]
---

To target a specific page with CSS or even a whole section's pages add page specific and section specific class names to the body tag. But how?

Add this line to replace your `body` tag. 

Using `.File.ContentBaseName` is Hugo's rather verbose way of getting the filename without it's extention BUT more than that if the file is a leaf bundle then the file name, `index.md` is not much use for adding a class name to the body tag of each page.

Sooo, in that case it grabs the name of the folder instead. Perfect.

```go-html-template
<body class="{{ .File.ContentBaseName | lower }}{{ with .Section }} {{ . }}{{ end -}}">
```

Use `with .Section` because some files, like the home page for example, won't be in a section (most likely). But when they are the section name is used in the body tag too.

Result:

```html
<body class="my-file-name section-name">
```