---
title: "Yarn and pnpm"
date: 2022-02-16T20:34:32Z
draft: false
toc: true
categories: []
tags: [Hugo, cli]
summary: "Installing and updating Yarn and pnmp on Windows"
extlinks:
- [https://yarnpkg.com/getting-started/install, Official Yarn Install, official Yarn website]
- [https://blog.logrocket.com/javascript-package-managers-compared/, Logrocket article, comparing all Node package managers]
---

[Yarn] and pnpm are package managers similar to [npm](/posts/npm.md) but can be much faster for some things. There are several versions of [Yarn], the main ones being 1, 2 or 3.

This method uses *corepack* which is part of Node.js and has been described as a package manager for package managers.

To install first open a terminal and, if in Windows, run

Then simply type 

```bash
corepack enable
```

and hit enter.

## Updating

Check for [Yarn] and [pnpm] versions: 

```bash
yarn -v
pnpm -v
```

For [Yarn] for me it was 1.22. To update to the latest stable version for all your projects go to the root of your projects folder and type:

```bash
yarn set version stable
```

All the subfolders will now use the latest version. At the time of writing that updated me to v 3.1.1.

However outside of your projects folder the yarn version will still be `1.22`. Yarn makes the change by adding a `.yarn` folder containing a `releases` folder with that latest version: `yarn-3.1.1.1.cjs`. This is about 2MB in size.

You can now install with Yarn instead of NPM in any sub directory. However deciding whether to use [Yarn] or [pnpm] or npm may depend on which packages you want to install.

## pnpm

[pnpm] is also enabled when using `corepack enable`. 

This is unlikely to be the latest version so to upgrade use the following, replacing the version number with the latest version number, for [pnpm]

```bash
corepack prepare pnpm@7.9.4 --activate
```

[pnpm]'s main advantage is that it saves disk space by not duplicating dependencies when different projects use the same ones. This can be a be a huge advantage for anyone using the same package in multiple projects. 

[pnpm install with corepack](https://pnpm.io/installation#using-corepack)

Note that not all commands are the same for [pnpm]. For instance:

| npm command            | pnpm equivalent                               |
| :--------------------- | :-------------------------------------------- |
| `npm install`<br>`npm i` | `pnpm install` <br>`pnpm i`<br> `pnpm add <pkg>` |
| `npm run <cmd>`        | `pnpm <cmd>`                                  |

Also note that `pnpx` is deprecated and there is now `pnmp exec`, `pnpm run`, `pnpm start` and `pnpm dix` to use instead.

Use `pnpm --help` to find out more.

## Yarn with Hugo

Hugo pipes will only work with `node_modules` so either use 'npm' or use the [enable the node-modules plugin] in that case.





[enable the node-modules plugin]: https://yarnpkg.com/getting-started/migration#if-required-enable-the-node-modules-plugin

[pnpm]: https://pnpm.io/
[yarn]: https://yarnpkg.com/