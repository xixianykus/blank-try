---
title: "Masonry Layouts"
date: 2021-08-08T06:32:15+01:00
draft: false
categories: ["css"]
tags: ["css"]
summary: "How to create masonry layouts with CSS"
---

## Basic

The easy method is is use some simple CSS

```css
.container {
    columns: 180px, 10; /* 180px is the minimum width, 10 is the max no. of cols */
    column-gap: 5px;
}
/* flexible images */
img {
    width: 100px;
}
```

This lays the content out top to bottom. If you want L to R it can be done [with grid](https://w3bits.com/css-grid-masonry/) and a bit of JS.

The quick way is to use W3 Bits [masonry generator](https://w3bits.com/tools/masonry-generator/) that does everything for you.