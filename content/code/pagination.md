---
title: "Pagination"
date: 2021-05-03T03:39:40+01:00
draft: false
categories: [hugo]
tags: ["pagination", blank theme]
summary: "The code for pagination in the Blank theme"
extlinks:
- ["https://gohugo.io/templates/pagination/", Official docs on Pagination]
---

How the blank theme does pagination.

The Hugo default value for [pagination](https://addons.mozilla.org/en-US/firefox/addon/night-viewer-dark-page-source) is `10`, meaning it will list out the first 10 posts or articles etc. before creating a new page. To change this simply add this line to the config file:

```toml
paginate = 16
```

Here's the code from `pagination.html` partial:

```go-html-template
<div>
{{ if .Paginator.HasPrev }}
	<a href="{{ .Paginator.Prev.URL }}">Previous Page</a>
{{ end }}
{{ .Paginator.PageNumber }} of {{ .Paginator.TotalPages }}
{{ if .Paginator.HasNext }}
	<a href="{{ .Paginator.Next.URL }}">Next Page</a>
{{ end }}
</div>
```

