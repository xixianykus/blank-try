---
title: "Header"
date: 2020-08-24T20:09:15+01:00
draft: false
categories: ["Web dev"]
tags: ["hugo", menus]
summary: "How the header from this theme is."
extLinks:
- [https://learnhugo.ml/posts/menus/, Learn Hugo on Menus]
---

```go-html-template
<header>
	<a href="{{ .Site.BaseURL }}">{{ .Site.Title }}</a>
	{{ with .Site.Menus.main }}
	<nav>
		<ul>
			{{ range . }}
			<li><a href="{{ .URL | relURL }}">{{ .Name }}</a></li>
			{{ end }}
		</ul>
	</nav>
	{{ end }}
</header>
```

And this is in the config.toml 

```toml
[params]
  mainSections = ["posts", "code", "duck"]
```

This defines which posts will appear in the sidebar
