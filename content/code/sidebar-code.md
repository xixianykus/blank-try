---
title: "Sidebar Code"
date: 2021-05-08T20:27:35+01:00
draft: false
categories: ["Hugo"]
tags: [hugo]
summary: "A short summary about the page"
---

Here is the code from the sidebar on the right as it was, more or less, from the blank theme:

```go-html-template
<ul>
    {{ range first 15 (where .Site.RegularPages "Type" "in" .Site.Params.mainSections) }}
        <li><a href="{{ .RelPermalink }}">{{ .Title }}<small> ({{ .Date.Format "2 Jan 06" }})</small></a> </li>
    {{ end }}
</ul>
```

The use of `in` is interesting. According to [Hugo docs](https://gohugo.io/functions/in/) `in`:

> Checks if an element is in an array or slice–or a substring in a string—and returns a boolean.

It goes on to say:

> The elements supported are strings, integers and floats, although only float64 will match as expected.

> In addition, in can also check if a substring exists in a string.

The examples used are: 

```go-html-template
{{ if in .Params.tags "Git" }}Follow me on GitHub!{{ end }}
```

`mainSections` are listed in the config file:

```toml
[params]
  mainSections = ["posts", "code"]
```

## Latest code

The latest sidebar code does two more things.

First it changes the content of the sidebar depending on whether it's a not a section page (eg home page, links, about or tags)&mdash;where it lists all recent posts from all `mainSections` (those listed in the config file)&mdash;or another page,  where it lists the latest posts from that section.

Secondly it creates *active* links for the current page which can be targetted in CSS with an attribute selector: `a[href="#"]**`**.

```go-html-template
<ul>
  {{ if eq .Section "" }}
    {{ range first 15 (where .Site.RegularPages "Type" "in" .Site.Params.mainSections) }}
    <li><a href="{{ .RelPermalink }}">{{ .Title }}<small> ({{ .Date.Format "2 Jan 06" }})</small></a> </li>
    {{ end }}

  {{ else }}
    {{ $currentPage := . }}
    {{ range first 15 (where .Site.RegularPages "Type" "in" .Section) }}
      <li>
            
        {{ if eq $currentPage . }}
          <a href="#">{{ .Title }}<small> ({{ .Date.Format "2 Jan 06" }})</small></a>
        {{ else }}
          <a href="{{ .RelPermalink }}">{{ .Title }}<small> ({{ .Date.Format "2 Jan 06" }})</small></a>
        {{ end }}
                
      </li>
    {{ end }}
  {{ end }}
</ul>
```