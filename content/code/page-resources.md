---
title: "Page Resources"
date: 2021-05-21T09:05:18+01:00
draft: false
categories: ["Hugo"]
tags: ["hugo"]
summary: "How to access Hugo's page resources"
extLinks:
- [https://regisphilibert.com/blog/2018/01/hugo-page-resources-and-how-to-use-them/, Page Resources and How to Use Them, by the excellent Regis Philibert]
- [https://gohugo.io/content-management/page-resources/, Hugo docs page, the official page]
---

A page is saved as a leaf bundle by creating it's own folder and then adding an `index.md` file. When you do this other files can be stored in this folder and they become page resources.

To access these we can use `.Resources` in a similar way to `.Pages`.

```go-html-template
{{ range .Resources }}
  {{ . }}
{{ end }}
```

This will give the following list where there is one image, one text file and one markdown file:

```
profile.webp text.txt Page(/about/about.md)
```

Like using range with pages we could use it like this:

```go-html-template
<ul>
    {{ range .Resources }}
    <li><a href="{{ .Permalink }}">{{ .Name }}</a></li>
    {{ end }}
</ul>
```

This gives the following list with, almost, links to the files:

- profile.webp
- text.txt
- more-words.md
- about.md

Whilst the links to the first two files work the links to the two markdown files actually link to the `index.md` file instead. In fact while `.Permalink` gives the url of the *resource*:

> Resources of type page will have no value.
>
> Hugo docs

This listing can sorted by the type of file: image, text and page for the 4 files here by simply adding `.Resources.ByType "image"`


```go-html-template
<ul>
    {{ range .Resources.ByType "image" }}
    <li><a href="{{ .Permalink }}">{{ .Name }}</a></li>
    {{ end }}
</ul>
```

These are MIME types and the options are: audio, image, text, 

Here is some code from Regis Philibert's site.

```go-html-template
{{ with .Resources.ByType "page" }}
<div class="page">
    {{ range . }}
    <h2>{{ .Title }}</h2>
    <div>
        {{ .Content }}
    </div>
    {{ end }}
</div>
{{ end }}
```

...but if you only want one specific file and only want the content of that file then:

```go-html-template
{{ with .Resources.GetMatch "about.md" }}
<div>
  {{ .Content }}
</div>
{{ end }}
```



## To show an image

...simply bring it's url into an image tag:

```go-html-template
<div>
    {{ with .Resources.GetMatch "*.webp" }}
    <img src="{{ . }}" alt="">
    {{ end }}
</div>
```


```

└── content 
    ├ ─ ─ post
```
vertical line ||

alt 195 �

├ƕƕ$(…—Ð)

    |&mdash;&mdash;