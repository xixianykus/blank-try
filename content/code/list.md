---
title: "List"
date: 2020-08-26T13:13:37+01:00
draft: false
categories: ["Web dev"]
tags: ["hugo"]
summary: "A short summary about the page"
---

The code from the `_default/list.html` page:

```go-html-template
{{ define "main" }}
	<main class="list-page">

		{{ if or .Title .Content }}
		<div>
			{{ with .Title }}<h1>{{ . }}</h1>{{ end }}
			{{ with .Content }}<div>{{ . }}</div>{{ end }}
		</div>
		{{ end }}

		{{ range .Paginator.Pages }}
			{{ .Render "summary" }}
		{{ end }}
		{{ partial "pagination.html" . }}
	</main>
{{ partial "sidebar.html" . }}
{{ end }}
```