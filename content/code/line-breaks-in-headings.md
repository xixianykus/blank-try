---
title: "Line Breaks\n in Headings"
date: 2021-05-20T07:37:17+01:00
draft: false
categories: ["Hugo"]
tags: ["hugo"]
summary: "How I achieved line breaks in headings"
extLinks:
- ['https://stackoverflow.com/questions/3790454/how-do-i-break-a-string-in-yaml-over-multiple-lines', Break a string in YAML, Stackoverflow question]
---

Some of my page headings were quite long so I wanted the choice to choose where to break them.

The headings are in the frontmatter of each page in the `title` key.

To break a title use either: 

1. the `\n`  for *new line* in the text where you want the break title: `title: One line\n into two` or
2. use pipe symbol is used to enforce the breaks and a hyphen to prevent a break at the end.

```yaml
title: |-
  My very long winded
  Extra long Headings
```

NB. keep the spacing the same on each line.

## HTML

In HTML line breaks and white space are ignored. To fix this use the CSS:

```css
h1 {
    white-space: pre-line;
}
```

According to W3 schools:

> Sequences of whitespace will collapse into a single whitespace. Text will wrap when necessary, and on line breaks
>
> [link](https://www.w3schools.com/cssref/pr_text_white-space.asp)

