---
title: "Animated Heart SVG"
date: 2021-05-30T09:25:39+01:00
draft: false
categories: ["animation", svg]
tags: ["svg", css]
summary: "Very cool heart with simple SVG code"
extlinks:
- ['https://viewbox.club/tips/03.SVG_Stroke_Animations.html', Viewbox.club email, on SVG stroken animations]
---

Here's something to motivate improving SVG skills. A cool heart with some very simple coding from [Viewbox.club](https://viewbox.club/tips/03.SVG_Stroke_Animations.html):

{{< svg/heart >}}


```svg
<svg viewBox="0 0 100 100">
	<style>
		.animated-heart {
			fill: none;
			stroke: red;
			stroke-width: 2px;
			stroke-linecap: round;
			stroke-dasharray: 1px;
			stroke-dashoffset: 1px;
			animation: drawHeart 3s ease-out infinite;
		}
		@keyframes drawHeart {
			90%, 100% {
				stroke-dashoffset: 0px;
			}	
		}
	</style>
	<path class="animated-heart" pathLength="1" d="M49.998,90.544c0,0,0,0,0.002,0c5.304-14.531,32.88-27.047,41.474-44.23C108.081,13.092,61.244-5.023,50,23.933  C38.753-5.023-8.083,13.092,8.525,46.313C17.116,63.497,44.691,76.013,49.998,90.544z" />
</svg>
```

To shrink the SVG I wrapped it in a `<div>` and gave it a width value.