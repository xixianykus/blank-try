---
title: "Another"
date: 2021-08-03T20:10:16+01:00
draft: true
---

## Some blurb from another page

I recently came across an [interesting article](https://www.smashingmagazine.com/2019/03/css-alignment/) by Rachel Andrew on alignment in CSS. This coincided with me having problems aligning some things for this site. Specifically the NEXT and PREVIOUS button text was not well vertically aligned and I couldn't align the RSS icon (an inline SVG) with the RSS text in the footer.

The fix for the buttons was:

1. Giving the buttons a fixed height in pixels.
2. Setting the line height to the same height.

In the case of the RSS icon and text I didn't actually figure out the fix but instead stumbled across the fact that having the text under the icon was probably better to what I was after which was aligning the tops. All I had to do then was a quick `text-align: center;`, the best and simplest alignment property going IMO, and I was done.


