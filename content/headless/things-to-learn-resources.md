---
title: "Resources for Things to Learn"
date: 2021-12-02T09:47:29Z
draft: false
summary: Resources for Things to Learn
---


## PWA links

1. [Top 7 Tools for Building a Progressive Web App](https://simpleprogrammer.com/tools-progressive-web-app/)

## Upwork

[How to get jobs on Upwork](https://millo.co/how-to-get-jobs-on-upwork)