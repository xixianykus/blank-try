---
title: "Coming Soon!!"
date: 2021-12-01T09:25:38Z
draft: false
headless: false
summary: A list of new things to learn and add to this site.
---

Making this box integrage with the other list. Use Flexbox probably.

1. Learning to learn
2. Using Uploadcare for images
3. Learning more about [Chart JS](/posts/chart-js.md)
4. Go over some basic [Hugo programming skills](/tags/hugo/)
5. Javascript basics for DOM manipulation
6. [Progressive Web Apps](https://www.w3docs.com/course/progressive-web-apps-pwa-the-complete-guide)
7. Mermaid
8. Creating columns in markdown with a Hugo shortcode. `<--->`
9. SVG sprite creation. Fastest ways.
10. CSS Flexbox patterns
11. CSS Grid patterns
12. SASS (Mike Dane?)



[Resources for the above](/headless/things-to-learn-resources/)