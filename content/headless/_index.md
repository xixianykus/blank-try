---
title: "Headless"
date: 2021-08-03T18:57:51+01:00
draft: false
headless: false
summary: A headless page to embed on the front page. Not required at the moment.
---

## SITE UPDATES

### Latest

1. Adding new [Baguette Box JS image gallery](/posts/baguette-box-js/) and [a code page](/code/baguette/).
2. Added a [Search Page](/search/) based on a lovely simple solution. And [a post about it](/posts/)
3. [The Docs](/docs/) is a new section for documentation about the site. So many features it's easy to forget them or how they work. Link in the footer. (07.08.21)
4. Added `css` option for frontmatter. This is an array that allows CSS rules to be added to the `<head>` section stylesheet from the frontmatter. Also set the config file to allow HTML attributes to be added to HTML blocks like lists, tables, paragraphs etc. (03.08.21)
5. New `html` shortcode. Same as `<div>` but without *markdownify* so can use for HTML tags and SVG. (01/06/21)
6. New `<div>` shortcode for generic CSS on the fly in Markdown. Takes class, id and style attributes. Used it to create this scrollable box. (29/05/21)
7. New *Draft* page with a list of draft articles (not viewable on live site, obviously.) (28/05/21)
8. Added RSS icon to footer (26/05/21)
9. Added next and previous page option in section pages (26/05/21)
10. Added [a dark theme](/code/quick-n-dirty-dark-mode/) (for OS's set to dark mode), first using a CSS filter and then by changing the custom property colours. (25/05/21)
11. Set up and [image gallery](/posts/image-gallery/) using Photoswipe and Hugo's page resources. This included new partial and shortcode: `photoswipe.html` and `ps_thumbs.html`. Photoswipe can be turned on or off in the frontmatter: `photoswipe: off`. (24/05/21)
12. New Youtube and Soundcloud shortcodes. The youtube one, yt.html, uses `aspect-ratio` which is not well supported yet.
13. [Changed About page into a leaf bundle](/about/)
14. [Added meta description tags](/posts/seo-and-this-site/)
15. [A new image shortcode](/code/new-image-shortcode/)
16. [Line Breaks in Headings](/code/line-breaks-in-headings/)
17. [Custom scroll bar](https://www.digitalocean.com/community/tutorials/css-scrollbars) added for FF and webkit browsers. (18/05/21)
18. [smooth scroll](https://moderncss.dev/pure-css-smooth-scroll-back-to-top/) back to top button
19. Site title subtitle
20. [Custom tags page](/code/custom-tags-page/).
21. Music section added.

### Less Late

1. Chartism and Chart js libraries (10/05/21)
8. Added mobile friendly slide in menu.
9. Code block responsivity added.
10. Added some reponsivity (though the code blocks are not responsive)
11. Added Wavy background from [BG Jar](https://bgjar.com/)
12. Embedded the [clamp sizing tool](/posts/clampsizer/).
13. Enabled Git Info for dates and lastmod field.
14. Sorted external links `extlinks` for frontmatter arrays
15. Favicon added.
1. Added fonts Epilogue (main text) and Roboto Mono for code.
6. Added new section for experimenting with: test-panel, which is above the footer
7. Added type and section variables to the footer.
8. Added a new section 'code' for chunks of code from the site
9. Added Params / mainSections to the config file to allow the code section posts appear in the sidebar. The default behaviour is: "If the user has not set this config parameter in their site config, it will default to the section with the most pages."

### Earlier
1. added hugo.Generator
2. changed name of post to Syntax highlighting
3. Changed the site url in config file to netlify.app from netlify.com
4. changed summary to description in frontmatter and summary.html / summary did not work on Netlify.
5. renamed some files so more logical in finding them
6. added netlify.toml with hugo version (maybe also set this on Netlify too)