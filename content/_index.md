---
Title: "home"
birds: ["eagle", "owl", "red kite", "buzzard", "kestrel", "sparrowhawk", "falcon"]
summary: Personal site that's mostly about using the Hugo static site generator, CSS and related web dev technologies.
---

{{< coming-soon >}}

This site was for experimenting, and still is, but also used as a my blog for notes about Hugo static site generator.

It makes use of the latest code that works in the latest stable Firefox and most other up to date browsers.


This is site is based on the blank theme with some modifications in `/layouts/`.

The .git folder is removed from `themes/blank` but ideally you should use submodues for hosting on Netlify.

It's purpose is just for testing some things.

{{< div class="scroll-box" >}}

## SITE UPDATES

### Latest

1. Added breadcrumb nav to single pages (26.12.22)
2. Removed Tags link from main menu and added tags links to tag lists on page 'Tagged in'.(10.11.21)
3. Added a categories link to all tag pages. (10.11.21)
4. Fixed tag links at top of pages which had spaces in and thus broken links (31.10.21)
5. Added the unfinished option for frontmatter so unfinished articles can still be found. Also [a list](/head-2/) of unfinished pages and draft pages. (31.10.21)
6. Added transluscent background to article elements in section pages. Added unfinished variable to frontmatter for posts (31.10.21).
7. Darkened the sidebar for dark mode slightly and lightened the pink link colours. (08.09.21)
8. Put a link to the *Posted in* snippet and used `.Parent` with `.Name` to get the subsections not just parent sections (04.09.21).
9. Added Table of Contents option to list pages too. Split the Links page into a section with a list of my sites. Turned off BaguetteBox captions. (31.08.21)
10. Table Of Contents option added. Use `toc: true` to activate on a page. (26.08.21)
11. Stopped *Page updated* showing when it hasn't been updated. (20.08.21)
12. Adding new [Baguette Box JS image gallery](/posts/baguette-box-js/) and [a code page](/code/baguette/).
13. Added a [Search Page](/search/) based on a lovely simple solution. And [a post about it](/posts/)(09.08.21)
14. [The Docs](/docs/) is a new section for documentation about the site. So many features it's easy to forget them or how they work. Link in the footer. (07.08.21)
15. Added `css` option for frontmatter. This is an array that allows CSS rules to be added to the `<head>` section stylesheet from the frontmatter. Also set the config file to allow HTML attributes to be added to HTML blocks like lists, tables, paragraphs etc. (03.08.21)
16. New `html` shortcode. Same as `<div>` but without *markdownify* so can use for HTML tags and SVG. (01/06/21)
17. New `<div>` shortcode for generic CSS on the fly in Markdown. Takes class, id and style attributes. Used it to create this scrollable box. (29/05/21)
18. New *Draft* page with a list of draft articles (not viewable on live site, obviously.) (28/05/21)
19. Added RSS icon to footer (26/05/21)
20. Added next and previous page option in section pages (26/05/21)
21. Added [a dark theme](/code/quick-n-dirty-dark-mode/) (for OS's set to dark mode), first using a CSS filter and then by changing the custom property colours. (25/05/21)
22. Set up and [image gallery](/posts/image-gallery/) using Photoswipe and Hugo's page resources. This included new partial and shortcode: `photoswipe.html` and `ps_thumbs.html`. Photoswipe can be turned on or off in the frontmatter: `photoswipe: off`. (24/05/21)
23. New Youtube and Soundcloud shortcodes. The youtube one, yt.html, uses `aspect-ratio` which is not well supported yet.
24. [Changed About page into a leaf bundle](/about/)
25. [Added meta description tags](/posts/seo-and-this-site/)
26. [A new image shortcode](/code/new-image-shortcode/)
27. [Line Breaks in Headings](/code/line-breaks-in-headings/)
28. [Custom scroll bar](https://www.digitalocean.com/community/tutorials/css-scrollbars) added for FF and webkit browsers. (18/05/21)
29. [smooth scroll](https://moderncss.dev/pure-css-smooth-scroll-back-to-top/) back to top button
30. Site title subtitle
31. [Custom tags page](/code/custom-tags-page/).
32. Music section added.

### Less Late

1. Chartism and Chart js libraries (10/05/21)
8. Added mobile friendly slide in menu.
9. Code block responsivity added.
10. Added some reponsivity (though the code blocks are not responsive)
11. Added Wavy background from [BG Jar](https://bgjar.com/)
12. Embedded the [clamp sizing tool](/posts/clampsizer/).
13. Enabled Git Info for dates and lastmod field.
14. Sorted external links `extlinks` for frontmatter arrays
15. Favicon added.
1. Added fonts Epilogue (main text) and Roboto Mono for code.
6. Added new section for experimenting with: test-panel, which is above the footer
7. Added type and section variables to the footer.
8. Added a new section 'code' for chunks of code from the site
9. Added Params / mainSections to the config file to allow the code section posts appear in the sidebar. The default behaviour is: "If the user has not set this config parameter in their site config, it will default to the section with the most pages."

### Earlier
1. added hugo.Generator
2. changed name of post to Syntax highlighting
3. Changed the site url in config file to netlify.app from netlify.com
4. changed summary to description in frontmatter and summary.html / summary did not work on Netlify.
5. renamed some files so more logical in finding them
6. added netlify.toml with hugo version (maybe also set this on Netlify too)

{{< /div >}}
