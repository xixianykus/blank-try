---
title: "Privacy Policy"
date: 2023-12-30T09:57:50Z
draft: false
---


This site does not collect any user data. There is no analytics software or any other software used to collect site visitors' personal information.

The site is therefore fully GDPR compliant in this regard but...

## Youtube

Some pages do contain embedded Youtube videos and Youtube makes the setting of cookies on your device mandatory. If you opt out of Youtube's cookies you may not be able to view or play Youtube videos.
