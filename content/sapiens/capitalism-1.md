---
title: "Capitalism 1"
date: 2021-07-17T07:55:21+01:00
draft: false
summary: The Capitalist Creed. First part of the chapter on capitalism from the book Sapiens by Yuval Noah Harari.
---

The Capitalist Creed
====================

MONEY HAS BEEN ESSENTIAL BOTH FOR building empires and for promoting science. But is money the ultimate goal of these undertakings, or perhaps just a dangerous necessity?

It is not easy to grasp the true role of economics in modern history. Whole volumes have been written about how money founded states and ruined them, opened new horizons and enslaved millions, moved the wheels of industry and drove hundreds of species into extinction. Yet to understand modern economic history, you really need to understand just a single word. The word is growth. For better or worse, in sickness and in health, the modern economy has been growing like a hormone-soused teenager. It eats up everything it can find and puts on inches faster than you can count.

For most of history the economy stayed much the same size. Yes, global production increased, but this was due mostly to demographic expansion and the settlement of new lands. Per capita production remained static. But all that changed in the modern age. In 1500, global production of goods and services was equal to about $250 billion; today it hovers around $60 trillion. More importantly, in 1500, annual per capita production averaged $550, while today every man, woman and child produces, on the average, $8,800 a year.[1](Hara_9780771038525_epub_nts_r1.htm#c16-nts001) What accounts for this stupendous growth?

Economics is a notoriously complicated subject. To make things easier, let’s imagine a simple example.

Samuel Greedy, a shrewd financier, founds a bank in El Dorado, California.

A. A. Stone, an up-and-coming contractor in El Dorado, finishes his first big job, receiving payment in cash to the tune of $1 million. He deposits this sum in Mr Greedy’s bank. The bank now has $1 million in capital.

In the meantime, Jane McDoughnut, an experienced but impecunious El Dorado chef, thinks she sees a business opportunity – there’s no really good bakery in her part of town. But she doesn’t have enough money of her own to buy a proper facility complete with industrial ovens, sinks, knives and pots. She goes to the bank, presents her business plan to Greedy, and persuades him that it’s a worthwhile investment. He issues her a $1 million loan, by crediting her account in the bank with that sum.

McDoughnut now hires Stone, the contractor, to build and furnish her bakery. His price is $1,000,000.

When she pays him, with a cheque drawn on her account, Stone deposits it in his account in the Greedy bank.

So how much money does Stone have in his bank account? Right, $2 million.

How much money, cash, is actually located in the bank’s safe? Yes, $1 million.

It doesn’t stop there. As contractors are wont to do, two months into the job Stone informs McDoughnut that, due to unforeseen problems and expenses, the bill for constructing the bakery will actually be $2 million. Mrs McDoughnut is not pleased, but she can hardly stop the job in the middle. So she pays another visit to the bank, convinces Mr Greedy to give her an additional loan, and he puts another $1 million in her account. She transfers the money to the contractor’s account.

How much money does Stone have in his account now? He’s got $3 million.

But how much money is actually sitting in the bank? Still just $1 million. In fact, the same $1 million that’s been in the bank all along.

Current US banking law permits the bank to repeat this exercise seven more times. The contractor would eventually have $10 million in his account, even though the bank still has but $1 million in its vaults. Banks are allowed to loan $10 for every dollar they actually possess, which means that 90 per cent of all the money in our bank accounts is not covered by actual coins and notes.[2](Hara_9780771038525_epub_nts_r1.htm#c16-nts002) If all of the account holders at Barclays Bank suddenly demand their money, Barclays will promptly collapse (unless the government steps in to save it). The same is true of Lloyds, Deutsche Bank, Citibank, and all other banks in the world.

It sounds like a giant Ponzi scheme, doesn’t it? But if it’s a fraud, then the entire modern economy is a fraud. The fact is, it’s not a deception, but rather a tribute to the amazing abilities of the human imagination. What enables banks – and the entire economy – to survive and flourish is our trust in the future. This trust is the sole backing for most of the money in the world.

In the bakery example, the discrepancy between the contractor’s account statement and the amount of money actually in the bank is Mrs McDoughnut’s bakery. Mr Greedy has put the bank’s money into the asset, trusting that one day it would be profitable. The bakery hasn’t baked a loaf of bread yet, but McDoughnut and Greedy anticipate that a year hence it will be selling thousands of loaves, rolls, cakes and cookies each day, at a handsome profit. Mrs McDoughnut will then be able to repay her loan, with interest. If at that point Mr Stone decides to withdraw his savings, Greedy will be able to come up with the cash. The entire enterprise is thus founded on trust in an imaginary future – the trust that the entrepreneur and the banker have in the bakery of their dreams, along with the contractor’s trust in the future solvency of the bank.

We’ve already seen that money is an astounding thing because it can represent myriad different objects and convert anything into almost anything else. However, before the modern era this ability was limited. In most cases, money could represent and convert only things that actually existed in the present. This imposed a severe limitation on growth, since it made it very hard to finance new enterprises.

Consider our bakery again. Could McDoughnut get it built if money could represent only tangible objects? No. In the present, she has a lot of dreams, but no tangible resources. The only way she could get her bakery built would be to find a contractor willing to work today and receive payment in a few years’ time, if and when the bakery starts making money. Alas, such contractors are rare breeds. So our entrepreneur is in a bind. Without a bakery, she can’t bake cakes. Without cakes, she can’t make money. Without money, she can’t hire a contractor. Without a contractor, she has no bakery.

Humankind was trapped in this predicament for thousands of years. As a result, economies remained frozen. The way out of the trap was discovered only in the modern era, with the appearance of a new system based on trust in the future. In it, people agreed to represent imaginary goods – goods that do not exist in the present – with a special kind of money they called ‘credit’. Credit enables us to build the present at the expense of the future. It’s founded on the assumption that our future resources are sure to be far more abundant than our present resources. A host of new and wonderful opportunities open up if we can build things in the present using future income.

If credit is such a wonderful thing, why did nobody think of it earlier? Of course they did. Credit arrangements of one kind or another have existed in all known human cultures, going back at least to ancient Sumer. The problem in previous eras was not that no one had the idea or knew how to use it. It was that people seldom wanted to extend much credit because they didn’t trust that the future would be better than the present. They generally believed that times past had been better than their own times and that the future would be worse, or at best much the same. To put that in economic terms, they believed that the total amount of wealth was limited, if not dwindling. People therefore considered it a bad bet to assume that they personally, or their kingdom, or the entire world, would be producing more wealth ten years down the line. Business looked like a zero-sum game. Of course, the profits of one particular bakery might rise, but only at the expense of the bakery next door. Venice might flourish, but only by impoverishing Genoa. The king of England might enrich himself, but only by robbing the king of France. You could cut the pie in many different ways, but it never got any bigger.

That’s why many cultures concluded that making bundles of money was sinful. As Jesus said, ‘It is easier for a camel to pass through the eye of a needle than for a rich man to enter into the kingdom of God’ (Matthew 19:24). If the pie is static, and I have a big part of it, then I must have taken somebody else’s slice. The rich were obliged to do penance for their evil deeds by giving some of their surplus wealth to charity.

![](images/Hara_9780771038525_epub_057_r1.jpg)

**The Entrepreneur’s Dilemma**

If the global pie stayed the same size, there was no margin for credit. Credit is the difference between today’s pie and tomorrows pie. If the pie stays the same, why extend credit? It would be an unacceptable risk unless you believed that the baker or king asking for your money might be able to steal a slice from a competitor. So it was hard to get a loan in the premodern world, and when you got one it was usually _small_, _short-term_, _and subject to high interest rates_. Upstart entrepreneurs thus found it difficult to open new bakeries and great kings who wanted to build palaces or wage wars had no choice but to raise the necessary funds through high taxes and tariffs.

![](images/Hara_9780771038525_epub_058_r1.jpg)

**The Magic Circle of the Modern Economy**

That was fine for kings (as long as their subjects remained docile), but a scullery maid who had a great idea for a bakery and wanted to move up in the world generally could only dream of wealth while scrubbing down the royal kitchens floors.

It was lose-lose. Because credit was limited, people had trouble financing new businesses. Because there were few new businesses, the economy did not grow. Because it did not grow, people assumed it never would, and those who had capital were wary of extending credit. The expectation of stagnation fulfilled itself.

