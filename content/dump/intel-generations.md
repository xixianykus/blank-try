---
title: "Intel Generations"
date: 2022-09-28T06:55:39+01:00
draft: true
summary: Notes on the different Intel Core generations
tags: [laptops]
---


Most mobile i7s are only two core with 4 threads. Only the higher ones are 4 core with 8 threads in the earlier generations. This changes later though.

|Gen| Name | Nm| Year |Notes |
|:--|:--|:--| :-- |:-- |
|2 | Sandy Bridge| 32nm | 2012 |The higher i7s are quad core|
|3 | Ivy Bridge| 22nm | 2012| ditto |
|4 | Haswell | 22nm | 2013| ditto |
|5 | Broadwell | 14nm | 2014/5 | ditto |
|6 | Skylake | 14nm | 2015 | better CPU and GPU performance and reduced power consumption. All i7's are U series and have only 2 cores |
|7 | Kaby Lake | 14nm | 2017 | H series has 4 cores for both i5 and i7. Faster clock on turbo speeds, improved 3D, and 4k graphics |
|8 | Kaby Lake U series<br> Coffee Lake<br>Amber Lake<br>Whiskey Lake | 14nm | 2018 | All U series has 4 cores for both i5 and i7.<br>All H series have 4 or 6 cores (high i5 and all i7) |