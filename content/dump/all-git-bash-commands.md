---
title: "Help for Git Bash Commands"
date: 2022-09-20T09:34:29+01:00
draft: true
summary: Contents of git-bash help command
tags: [bash, Windows]
---

See [Linux Handbook](https://linuxhandbook.com/a-to-z-linux-commands/) for an excellent A - Z of bash

## CLI help

To get help on any command not listed use `<command> --help` eg. `ls --help`, or typically easier using `less` for example: `ls --help | less`

This is a print out of the `help` command.

The manual pages, using the `man` command, is not available in Git-Bash and neither is the `info` command. However `help` with the `-m` switch, for pseudo manpages, does work. 

```bash
help -m pwd
```

```bash
GNU bash, version 5.1.16(1)-release (x86_64-pc-msys)
These shell commands are defined internally.  Type `help' to see this list.
Type `help name' to find out more about the function `name'.
Use `info bash' to find out more about the shell in general.
Use `man -k' or `info' to find out more about commands not in this list.

A star (*) next to a name means that the command is disabled.

 job_spec [&]                                    history [-c] [-d offset] [n] or history -anr>
 (( expression ))                                if COMMANDS; then COMMANDS; [ elif COMMANDS;>
 . filename [arguments]                          jobs [-lnprs] [jobspec ...] or jobs -x comma>
 :                                               kill [-s sigspec | -n signum | -sigspec] pid>
 [ arg... ]                                      let arg [arg ...]
 [[ expression ]]                                local [option] name[=value] ...
 alias [-p] [name[=value] ... ]                  logout [n]
 bg [job_spec ...]                               mapfile [-d delim] [-n count] [-O origin] [->
 bind [-lpsvPSVX] [-m keymap] [-f filename] [->  popd [-n] [+N | -N]
 break [n]                                       printf [-v var] format [arguments]
 builtin [shell-builtin [arg ...]]               pushd [-n] [+N | -N | dir]
 caller [expr]                                   pwd [-LPW]
 case WORD in [PATTERN [| PATTERN]...) COMMAND>  read [-ers] [-a array] [-d delim] [-i text] >
 cd [-L|[-P [-e]] [-@]] [dir]                    readarray [-d delim] [-n count] [-O origin] >
 command [-pVv] command [arg ...]                readonly [-aAf] [name[=value] ...] or readon>
 compgen [-abcdefgjksuv] [-o option] [-A actio>  return [n]
 complete [-abcdefgjksuv] [-pr] [-DEI] [-o opt>  select NAME [in WORDS ... ;] do COMMANDS; do>
 compopt [-o|+o option] [-DEI] [name ...]        set [-abefhkmnptuvxBCHP] [-o option-name] [->
 continue [n]                                    shift [n]
 coproc [NAME] command [redirections]            shopt [-pqsu] [-o] [optname ...]
 declare [-aAfFgiIlnrtux] [-p] [name[=value] .>  source filename [arguments]
 dirs [-clpv] [+N] [-N]                          suspend [-f]
 disown [-h] [-ar] [jobspec ... | pid ...]       test [expr]
 echo [-neE] [arg ...]                           time [-p] pipeline
 enable [-a] [-dnps] [-f filename] [name ...]    times
 eval [arg ...]                                  trap [-lp] [[arg] signal_spec ...]
 exec [-cl] [-a name] [command [argument ...]]>  true
 exit [n]                                        type [-afptP] name [name ...]
 export [-fn] [name[=value] ...] or export -p    typeset [-aAfFgiIlnrtux] [-p] name[=value] .>
 false                                           ulimit [-SHabcdefiklmnpqrstuvxPT] [limit]
 fc [-e ename] [-lnr] [first] [last] or fc -s >  umask [-p] [-S] [mode]
 fg [job_spec]                                   unalias [-a] name [name ...]
 for NAME [in WORDS ... ] ; do COMMANDS; done    unset [-f] [-v] [-n] [name ...]
 for (( exp1; exp2; exp3 )); do COMMANDS; don>   until COMMANDS; do COMMANDS; done
 function name { COMMANDS ; } or name () { COM>  variables - Names and meanings of some shell>
 getopts optstring name [arg ...]                wait [-fn] [-p var] [id ...]
 hash [-lr] [-p pathname] [-dt] [name ...]       while COMMANDS; do COMMANDS; done
 help [-dms] [pattern ...]                       { COMMANDS ; }
```