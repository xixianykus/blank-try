---
title: "Form Validation"
date: 2022-09-23T16:33:47+01:00
draft: true
summary: Some ways to improve form validation with HTML rather than javascript.
tags: [Kevin Powell]
---


Some ways for form validation using the `pattern` attribute.


<form>

<div><label>Password:</label></div>
<input title="Must be letters only and at least 12 characters" type="password" required pattern="[a-zA-Z]{12,}">

<input type="submit">

</form>


Using a regex pattern in the `pattern` attribute restricts passwords to just lettters:

```HTML
<input type="password" required pattern="[a-zA-Z]+">
```

You can use [regexr.com](https://regexr.com/) to help create and check patterns.

## More in the video

{{< yt s2ThIxm7FyA >}}

<style>
    form {
        display:grid;
        grid-template-columns: auto 1fr;
        grid-template-rows: 1fr auto;
        gap: 0.5em;
    
        margin: 1.5em 0;
        padding: 0.8em 1.2em;
        background: #1115
    }
    input[type="submit"] {
        grid-column: 2 / -1;
        grid-row: 2 / -1;
        max-width: 16ch;
        border: none;
        background: orangered;
        color: white;
        font-size: 0.8em;
        padding: 0.25em 0;
        border-radius: 0.25em;
    }
</style>