---
title: "Jmooring Comment"
date: 2022-08-23T11:58:43+01:00
draft: true
tags: [Hugo, Hugo modules]
---

From [the forums](https://discourse.gohugo.io/t/benefit-of-using-hugo-modules-over-node-modules/36968/8?u=horbes)

To be fair, one can use npm with both public and private repositories, regardless of whether they contain code or content.

Feel free to try it with this test repository:

`npm install https://github.com/jmooring/hugo-content`

Then add this to your site config:

```toml
[[module.mounts]]
source = 'content'
target = 'content'
[[module.mounts]]
source = 'node_modules/hugo-content'
target = 'content'
```

The source repository needs a package.json file, created with `npm init -y`.