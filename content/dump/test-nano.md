---
title: test-nano
date: 2022-09-03T08:30:49+01:00
---

So this is a file to test out using Nano. There is another file called just nano.md here too. 

What does "write out" mean? It means saving the file bizarrely enouth.

So this is a justified paragraph. What does it mean in a text editor context. When I get to the end of this line I'll find out.

So one question might be what's the advantages over Vim?

> The GNU nano, commonly known as ‘nano,’ is an improved project of the Pico text editor, which was released in 1999. Like Vim, Nano also 
comes pre-installed on most Linux systems. The Nano text editor an ideal tool for beginners. On startup, the Nano text editor shows the 
following interface.

## Users

From [LinuxHint.com](https://linuxhint.com/vim_vs_nano/)

Vim is necessary for a Linux system administrator. This text editor is also good for programmers because you can use it to program code in the form of a plain text file that is easy to copy and manipulate. Furthermore, Vim is good for anyone wanting to work with command line text editors often, and even works for writers. Vim is therefore a good program for all users.

Nano is great text editor for beginners who are new to terminal-based text editing. Nano is also useful for those who want to make just 
a few simple edits. If you are not a “perfect” Linux user, then Nano might be for you.
