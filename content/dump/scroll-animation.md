---
title: "Scroll Animation"
date: 2022-09-16T13:36:40+01:00
draft: false
summary: A bit about scroll animation and JS libraries to use
tags: [JavaScript]

---

There are [many JS libraries](https://www.hongkiat.com/blog/scrolling-effects-js-libraries/) for scroll animations, many of which are JQuery plugins.

Here's one that works with [animate.css](https://animate.style/ "Great documentation and now in v4.11") and has no other dependencies. It's kinda free[^1], open source and weighs only 8.23kb.

It's called [wow.js](https://www.delac.io/WOW/docs.html) and you can get it from [github](https://github.com/matthieua/WOW), or npm.

Might set up some of these here sometime but most of the pages on this site are are fairly short.

[^1]: For commercial sites you get a licence which is $29 for a single developer.