---
title: "PC Upgrade"
date: 2022-09-27T08:17:33+01:00
draft: true
summary: Considerations on upgrading my PC
css:
- "h1 {color: #7d567c;--shadow: #8fae99;}"
---

The two main updates would be the CPU and a bigger SSD. The CPU would be an i7, currently available on eBay from £35.

A bigger SSD would encourage update to Windows 11 which has better support for WSL 2 allowing full support for Desktop apps. However because of Meltdown and Spectre Microsoft don't support Windows 11 on any PC's that don't have 8th gen iCore processors and above or equivalent. (The threat of Spectre and Meltdown on AMD cpu's is not clear.)

Current motherboard: Gigabyte Z77-D3H w an Intel socket LGA 1155
Current processor: Intel Core i5 3550


## Processor

The reason is better performance with Adobe apps. Illustrator is a bit slow with 3D. If I was to update these apps and get Win 11 (if I get a new SSD) might need even more CPU power.

For desktop i7 there are 4 suffixes: 

1. [i7-3770](https://ark.intel.com/content/www/us/en/ark/products/65719/intel-core-i73770-processor-8m-cache-up-to-3-90-ghz.html) with runs at 3.4GHz with turbo to 3.9Ghz
2. [i7-3770K](https://ark.intel.com/content/www/us/en/ark/products/65523/intel-core-i73770k-processor-8m-cache-up-to-3-90-ghz.html) which is 77w TDP (same as my i5 3550) with a base rate of 3.5GHz
3. the **i7-3770S** which is 65 w (low power)
4. the **i7-3770T** which is 45w (ultra low power)

All have 8MB cache, use LGA 1155 socket and came out in April 2012.

See [Wikipedia page](https://en.wikipedia.org/wiki/List_of_Intel_Core_i7_processors#%22Ivy_Bridge%22_(22_nm)).

## SSD

These are from about £35 for 500GB and £53 for 1TB.

I could presumably use both SSD's one for Windows and one for WSL-2.

Currently have 5 disks:

1. 240GB SSD for the OS
2. 3TB Toshiba? for the `U:` drive and a small scratch disk `X:`
3. A 250GB drive for Troodon `E:` and *maxstorage* `J:`. Probably not really any use for anything. Has another Windows OS on it and maxstorage partition is empty.
4. A 2TB disk with Coelophysis `F:` drive, T-Storage `T:` and a 40GB scratch disc.
5. The DVD drive

There is also the `M:` drive which is just the CF card accessible via a USB device.

So I can add one new SSD as is. And could remove the old 250GB one too leaving one connection free.


## Plan

Get new CPU first. Run that for a while to test. 
Then get new SSD after that and new OS and apps.
Sell i5 processor