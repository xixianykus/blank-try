---
title: "WSL"
linktitle: "Windows Subsystem for Linux"
date: 2022-08-31T06:25:46+01:00
tags: [wsl, Linux, zsh]
---

## Why?

Many of the better CLI tools, such as Babun, seem to be stopping development.

WSL has now reached version 2 that is far more performant than the first version. It generally gets good reviews from all around.

## Linux and Windows

> Note that Windows "does not know about" the Linux file system, but Linux can access all the files in Windows. By default Windows file system is mounted in Linux at /mnt/c/. I recommand to create a symlink (shortcut) of Documents and Downloads, the most used Windows folders, in Linux home directory. That way, the access to data is simplified.
> [ubuntu-on-windows-wsl](https://dev.to/mhebrard/ubuntu-on-windows-wsl-40po)

```bash
cd
ln -s /mnt/c/Users/hebrardms/Documents Documents
ln -s /mnt/c/Users/hebrardms/Downloads Downloads
```

(The above article also has info on installing zsh and Oh My Zsh).

## Installing

You don't download WSL, simply enable in Powershell, or turn on/off Windows programs, and restart Windows.

Enable both:
1. Windows Subsystem for Linux
2. Virtual machine platform

This is for WSL 2 which is a virtual version on Linux rather than a translation layer used by WSL 1. It it boots up much faster and is more optimized and recommended.


## Linux versions

You need to download and install a version of Linux: 
- Ubuntu (680mbs)
- Debian (80mbs) 
- Alpine (0.8mb)

### Comparison

Debian is superior compared to Alpine Linux with regards to:

- quantity and quality of supported software
- the size and maturity of its development community
- amount of testing everything gets
- quality and quantity of documentation
- present and future security of its build infrastructure
- the size and maturity of its user community, number of people who know its ins and outs
- compatibility with existing software (libc vs musl)

Alpine Linux's advantages on the other hand:

- it has a smaller filesystem footprint than stock Debian.
- its slightly more memory efficient thanks to BusyBox and musl library

[link](https://www.turnkeylinux.org/blog/alpine-vs-debian)

Ubuntu is bigger and comes with GUI that's probably not needed. But it's easier to set up than Debian. Also Ubuntu helped MS designing WSL so hopefully tightly integrated.

## Final setting up..

After installing on the first run you may need to download and install any updates first. This is done like installing anything: download, double click...

The new OS will ask you to create a default UNIX user account: ie. a username and password.

### Checking which version

Open Windows Powershell and type `wsl -l -v` to see which Linux version are installed with which WSL version. You can change the WSL version from here.

Using `lsb_release -a` gives info on LSB modules.

`wsl --shutdown` from Powershell will shut down Linux OS's that are running.

`wsl --set-default-version 2` to set the WSL version.

## Do I need a new version of VS Code to run under Linux?

No. But you need a VS Code extension.

## Choosing the install directory

Can be done with [LxRunOffline](https://github.com/DDoSolitary/LxRunOffline)


## Adding ZSH

NB. This requires git to be installed on Linux.

[Full instructions](https://github.com/xcad2k/videos/tree/main/windows-terminal-powerlevel10k)

1. Choose, download and install a nerdfont in Windows.
2. Update the repo: `sudo apt update`
3. Download ZSH: `sudo apt install zsh -y`
4. Install Oh My ZSH. `sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"`
5. "Do you want to change your default shell to zsh?" enter `Yes`.
6. Install a custom theme: `git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/themes/powerlevel10k`


## Help

 * Documentation:  https://help.ubuntu.co
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage
 * [Install on a different drive](https://helloglenn.me/blog/installing-wsl-distro-in-different-drive-win/)
 * [apt vs apt-get](https://phoenixnap.com/kb/apt-vs-apt-get)
 * [Adding ZSH and setting up Windows Terminal](https://youtu.be/235G6X5EAvM) specifically for WSL. Uses [Nerdfonts](https://nerdfonts.com/)
 * [Get snap working with WSL 2](https://snapcraft.ninja/2020/07/29/systemd-snap-packages-wsl2/) which requires getting systemd to work too.


## Videos

{{< yt _fntjriRe48 >}}