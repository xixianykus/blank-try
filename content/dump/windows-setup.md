---
title: "Windows Setup"
date: 2022-08-31T12:26:21+01:00
draft: true
summary: Ideas for setting up Windows for development by Nick Janetakis
tags: [Windows]
---

Setup by [Nick Janetakis](https://nickjanetakis.com/)


1. Windows **Powertools** with **Powertoys**.
2. Windows **Terminal** (from the app store)
3. **Docker** ???
4. **Dexpot** - virtual workspaces
5. **fzf** - fuzzy reverse seach of your terminal.
6. **Ditto** - mulitple clipboards that are searchable.
7. [Auto Hot key](https://autohotkey.com) - create a config file to switch or create hot key shortcuts.


- Google hangouts and zoom to chat to clients.
- **Momentum** - Chrome extension looks nice, defeat procrastination.
- Hexchat - an alternative to using Slack, using IRC
- [Pass](https://www.passwordstore.org/) - a CLI password manager for Linux
- **Foobar2000** - a very lightweight mp3 player, uses 5mbs of RAM. 
- **IrfanView** - good for bulk resizing etc..
- **GnuCash** - for finance and accounting
- **Running in Produciton** his podcast


## WSL config file

WSL can be memory intensive. You can cap this in a config file.

You can also say where the swap file should be: so not on the SSD perhaps.

See Nick's [setup on GitHub](https://github.com/nickjj/dotfiles)

## VcXsrv Windows X server

Used for lots of things but clipboard access between Windows and Linux.

## htop

Is a bit like Windows Task Manager but on the command line.