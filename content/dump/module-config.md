---
title: "Module Config"
date: 2022-08-25T12:22:53+01:00
draft: true
summary: The config file code for the second part of The New Dynamic article on modules
tags: [Hugo, Hugo modules]
---

Two versions of the config file for the [hugo-icons module project](https://www.thenewdynamic.com/article/hugo-modules-everything-from-imports-to-create/#create-a-hugo-module "from The New Dynamic").

### YAML verion

```yaml
module:
  hugoVersion:
    min: 0.64.0
  mounts:
    - source: page/index.md
      target: hugo-icons-listing.md
      lang: en
    - source: page/template.html
      target: layouts/_default/hugo-icons-listing.html
    - source: partials
      target: layouts/partials/hugo-icons
  imports:
   - path: github.com/twbs/icons
     mounts:
      - source: icons
        target: assets/hugo-icons/icons
```

### TOML version


```toml
[module.hugoVersion]
min = "0.64.0"

    [[module.mounts]]
    source = "page/index.md"
    target = "hugo-icons-listing.md"
    lang = "en"

    [[module.mounts]]
    source = "page/template.html"
    target = "layouts/_default/hugo-icons-listing.html"

    [[module.mounts]]
    source = "partials"
    target = "layouts/partials/hugo-icons"

    [[module.imports]]
    path = "github.com/twbs/icons"

      [[module.imports.mounts]]
      source = "icons"
      target = "assets/hugo-icons/icons"
```