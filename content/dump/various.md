---
title: "Various"
date: 2022-08-26T01:12:46+01:00
summary: Various random notes on things that interest me.
tags: [bash, Linux, Hugo, CSS, SVG, backgrounds]
---



1. `hugo server --notHTTPCache` always refreshed. :smile:
2. Horizontal margins apply to inline elements but vertical margins do not
3. The home page object: `{{ site.Home }}`
4. `mkdir -p dir4/dir5/dir6` the -p switch means create *parent* too.
5. `rmdir -p dir1/dir2/dir3` deletes `dir3`, then `dir2` then `dir1` as long as they are empty.
6. `rm -i` means *interactive*. `rm -i *.md` will prompt you for each file to delete. Use `y` or `n` to decide.
7. `wc` for word count. This counts no. of lines, no. of words and no. of characters. To get just one of these use the switches `-l` for lines, `-w` for words and `-c` for characters.
8. You can use `wc` to count the number of items in a directory. `ls /etc|wc -l` list the items in the `etc` directory and count the number of lines. One on each line so..
9. The `less` command is a modern version of `more`. `ls /etc | less` will mean not more than one page's worth of items will be displayed. Pressing `enter` gets more but you can scroll back up  too. Press `q` to *quit*.
10. To get help with a command use `man` for *manual*. Eg. `man uniq`.


## SVG Generators

There are many of these but some require a sign up and can be a bit of a pain.

1. [Haikei.app](https://haikei.app/ "Great generator with multiple patterns") is one of my favourites, well made, nice and dark too.
2. [SVG Wave.in](https://svgwave.in/) is another good one.
3. [Public Domain Vectors](https://publicdomainvectors.org/svg-generator.php) is old and bit different which is what makes it interesting. You can even generate patterns from letters and characters as well as words and phrases. Another version is [here](https://svg-generator.netlify.app/).
4. [ffflux](https://fffuel.co/ffflux/) good for creating some interesting gradient effects.
5. [SVG Generator: Wavy Transitions Between Sections](https://smooth.ie/blogs/news/svg-wavey-transitions-between-sections) Although very simple the nice thing is that it's very simple to get a transition section with very succinct code.
6. [SVG Sprite Generator](https://svgsprite.com/tools/svg-sprite-generator/) a useful tool when you have multiple SVG's.
7. [Monogram Maker](https://www.makemonogram.com/monogram-maker) quckly makes up 1, 2 or 3 letter monograms that can be sized and downloaded in various formats inc. SVG.
8. [SVG Circus](https://svgcircus.com/) for quickly making animated pattern SVG's.

## Favicon Generators

1. [Real Favicon Generator](https://realfavicongenerator.net/) creates a set of favicons from an image in different sizes, dark and light modes, vector and rastor formats.
2. [Formito](https://formito.com/tools/favicon) quickly create a `<link>` containing an inline SVG. Choose a letter or letters with any google font.



## Links

1. [Great article](https://ubuntu.com/tutorials/command-line-for-beginners) on the Linux CLI by Ubuntu