---
title: "Youtube Thumb Swap"
date: 2022-09-29T12:23:09+01:00
draft: false
summary: By using just a thumbnail image of a video you can significantly speed up page load times on pages with multiple embedded Youtube videos. Here’s how.
tags: [Hugo, Youtube, Javascript]
---


You can get the thumbnail image of most Youtube videos if you have their number.

```go-html-template
{{ $img_url := printf "http://img.youtube.com/vi/%s/maxresdefault.jpg" . }}
```

Possibly a good solution here from [Stack Overflow](https://stackoverflow.com/questions/7199624/youtube-embedded-video-set-different-thumbnail):

```html
<div onclick="this.nextElementSibling.style.display='block'; this.style.display='none'">
   <img src="my_thumbnail.png" style="cursor:pointer" />
</div>
<div style="display:none">
    <!-- Embed code here -->
</div>
```

This works exactly as the example. If you use `display: none` in an embedded stylesheet instead of inline it requires two clicks to get the video up.

Although the iframe is set to `autoplay` videos don't always start. This is probably due to my system and having Youtube vids pause by default.

## Links

1 [Stackoverflow: how to replace youtube videos with a click to play thumbnail](https://wordpress.stackexchange.com/questions/73996/how-to-replace-youtube-videos-with-a-click-to-play-thumbnail)