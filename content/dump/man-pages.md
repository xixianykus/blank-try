---
title: "Man Pages"
date: 2022-09-01T22:10:15+01:00
draft: true
tags: [linux]
---

The *man* pages from the commands introduced in [The Linux Command Line for Beginners](https://ubuntu.com/tutorials/command-line-for-beginners)

- [ls](#ls)
- [mkdir](#mkdir)
- [less](#less)
- [rm](#rm)
- [wc](#wc)
- [uniq](#uniq)
- [man](#man)

## ls

```bash
LS(1)                                               User Commands                                               LS(1)

NAME
       ls - list directory contents

SYNOPSIS
       ls [OPTION]... [FILE]...

DESCRIPTION
       List  information  about the FILEs (the current directory by default).  Sort entries alphabetically if none of
       -cftuvSUX nor --sort is specified.

       Mandatory arguments to long options are mandatory for short options too.

       -a, --all
              do not ignore entries starting with .

       -A, --almost-all
              do not list implied . and ..

       --author
              with -l, print the author of each file

       -b, --escape
              print C-style escapes for nongraphic characters

       --block-size=SIZE
              with -l, scale sizes by SIZE when printing them; e.g., '--block-size=M'; see SIZE format below

       -B, --ignore-backups
              do not list implied entries ending with ~

       -c     with -lt: sort by, and show, ctime (time of last modification of file  status  information);  with  -l:
              show ctime and sort by name; otherwise: sort by ctime, newest first

       -C     list entries by columns

       --color[=WHEN]
              colorize the output; WHEN can be 'always' (default if omitted), 'auto', or 'never'; more info below

       -d, --directory
              list directories themselves, not their contents

       -D, --dired
              generate output designed for Emacs' dired mode

       -f     do not sort, enable -aU, disable -ls --color

       -F, --classify
              append indicator (one of */=>@|) to entries

       --file-type
              likewise, except do not append '*'

       --format=WORD
              across -x, commas -m, horizontal -x, long -l, single-column -1, verbose -l, vertical -C

       --full-time
              like -l --time-style=full-iso

       -g     like -l, but do not list owner

       --group-directories-first
              group directories before files;

              can be augmented with a --sort option, but any use of --sort=none (-U) disables grouping

       -G, --no-group
              in a long listing, don't print group names

       -h, --human-readable
              with -l and -s, print sizes like 1K 234M 2G etc.

       --si   likewise, but use powers of 1000 not 1024

       -H, --dereference-command-line
              follow symbolic links listed on the command line

       --dereference-command-line-symlink-to-dir
              follow each command line symbolic link

              that points to a directory

       --hide=PATTERN
              do not list implied entries matching shell PATTERN (overridden by -a or -A)

       --hyperlink[=WHEN]
              hyperlink file names; WHEN can be 'always' (default if omitted), 'auto', or 'never'

       --indicator-style=WORD
              append  indicator  with style WORD to entry names: none (default), slash (-p), file-type (--file-type),
              classify (-F)

       -i, --inode
              print the index number of each file

       -I, --ignore=PATTERN
              do not list implied entries matching shell PATTERN

       -k, --kibibytes
              default to 1024-byte blocks for disk usage; used only with -s and per directory totals

       -l     use a long listing format

       -L, --dereference
              when showing file information for a symbolic link, show information for the file  the  link  references
              rather than for the link itself

       -m     fill width with a comma separated list of entries

       -n, --numeric-uid-gid
              like -l, but list numeric user and group IDs

       -N, --literal
              print entry names without quoting

       -o     like -l, but do not list group information

       -p, --indicator-style=slash
              append / indicator to directories

       -q, --hide-control-chars
              print ? instead of nongraphic characters

       --show-control-chars
              show nongraphic characters as-is (the default, unless program is 'ls' and output is a terminal)

       -Q, --quote-name
              enclose entry names in double quotes

       --quoting-style=WORD
              use  quoting  style WORD for entry names: literal, locale, shell, shell-always, shell-escape, shell-es‐
              cape-always, c, escape (overrides QUOTING_STYLE environment variable)

       -r, --reverse
              reverse order while sorting

       -R, --recursive
              list subdirectories recursively

       -s, --size
              print the allocated size of each file, in blocks

       -S     sort by file size, largest first

       --sort=WORD
              sort by WORD instead of name: none (-U), size (-S), time (-t), version (-v), extension (-X)

       --time=WORD
              with -l, show time as WORD instead of default modification time: atime or access or use (-u); ctime  or
              status (-c); also use specified time as sort key if --sort=time (newest first)

       --time-style=TIME_STYLE
              time/date format with -l; see TIME_STYLE below

       -t     sort by modification time, newest first

       -T, --tabsize=COLS
              assume tab stops at each COLS instead of 8

       -u     with  -lt:  sort by, and show, access time; with -l: show access time and sort by name; otherwise: sort
              by access time, newest first

       -U     do not sort; list entries in directory order

       -v     natural sort of (version) numbers within text

       -w, --width=COLS
              set output width to COLS.  0 means no limit

       -x     list entries by lines instead of by columns

       -X     sort alphabetically by entry extension

       -Z, --context
              print any security context of each file

       -1     list one file per line.  Avoid '\n' with -q or -b

       --help display this help and exit

       --version
              output version information and exit

       The SIZE argument is an integer and optional unit (example: 10K is 10*1024).  Units are K,M,G,T,P,E,Z,Y  (pow‐
       ers of 1024) or KB,MB,... (powers of 1000).

       The  TIME_STYLE  argument  can  be full-iso, long-iso, iso, locale, or +FORMAT.  FORMAT is interpreted like in
       date(1).  If FORMAT is FORMAT1<newline>FORMAT2, then FORMAT1 applies to non-recent files and FORMAT2 to recent
       files.  TIME_STYLE prefixed with 'posix-' takes effect only outside the POSIX locale.  Also the TIME_STYLE en‐
       vironment variable sets the default style to use.

       Using color to distinguish file types is disabled both by default and with --color=never.  With  --color=auto,
       ls emits color codes only when standard output is connected to a terminal.  The LS_COLORS environment variable
       can change the settings.  Use the dircolors command to set it.

   Exit status:
       0      if OK,

       1      if minor problems (e.g., cannot access subdirectory),

       2      if serious trouble (e.g., cannot access command-line argument).

AUTHOR
       Written by Richard M. Stallman and David MacKenzie.

REPORTING BUGS
       GNU coreutils online help: <https://www.gnu.org/software/coreutils/>
       Report ls translation bugs to <https://translationproject.org/team/>

COPYRIGHT
       Copyright  ©  2018  Free  Software  Foundation,  Inc.   License  GPLv3+:  GNU   GPL   version   3   or   later
       <https://gnu.org/licenses/gpl.html>.
       This  is  free software: you are free to change and redistribute it.  There is NO WARRANTY, to the extent per‐
       mitted by law.

SEE ALSO
       Full documentation at: <https://www.gnu.org/software/coreutils/ls>
       or available locally via: info '(coreutils) ls invocation'

GNU coreutils 8.30                                  September 2019                                              LS(1)
```

## mkdir

```bash
MKDIR(1)                                            User Commands                                            MKDIR(1)

NAME
       mkdir - make directories

SYNOPSIS
       mkdir [OPTION]... DIRECTORY...

DESCRIPTION
       Create the DIRECTORY(ies), if they do not already exist.

       Mandatory arguments to long options are mandatory for short options too.

       -m, --mode=MODE
              set file mode (as in chmod), not a=rwx - umask

       -p, --parents
              no error if existing, make parent directories as needed

       -v, --verbose
              print a message for each created directory

       -Z     set SELinux security context of each created directory to the default type

       --context[=CTX]
              like -Z, or if CTX is specified then set the SELinux or SMACK security context to CTX

       --help display this help and exit

       --version
              output version information and exit

AUTHOR
       Written by David MacKenzie.

REPORTING BUGS
       GNU coreutils online help: <https://www.gnu.org/software/coreutils/>
       Report mkdir translation bugs to <https://translationproject.org/team/>

COPYRIGHT
       Copyright   ©   2018   Free   Software   Foundation,  Inc.   License  GPLv3+:  GNU  GPL  version  3  or  later
       <https://gnu.org/licenses/gpl.html>.
       This is free software: you are free to change and redistribute it.  There is NO WARRANTY, to the  extent  per‐
       mitted by law.

SEE ALSO
       mkdir(2)

       Full documentation at: <https://www.gnu.org/software/coreutils/mkdir>
       or available locally via: info '(coreutils) mkdir invocation'

GNU coreutils 8.30                                  September 2019                                           MKDIR(1)
RMDIR(1)                                            User Commands                                            RMDIR(1)

NAME
       rmdir - remove empty directories

SYNOPSIS
       rmdir [OPTION]... DIRECTORY...

DESCRIPTION
       Remove the DIRECTORY(ies), if they are empty.

       --ignore-fail-on-non-empty

              ignore each failure that is solely because a directory

              is non-empty

       -p, --parents
              remove DIRECTORY and its ancestors; e.g., 'rmdir -p a/b/c' is similar to 'rmdir a/b/c a/b a'

       -v, --verbose
              output a diagnostic for every directory processed

       --help display this help and exit

       --version
              output version information and exit

AUTHOR
       Written by David MacKenzie.

REPORTING BUGS
       GNU coreutils online help: <https://www.gnu.org/software/coreutils/>
       Report rmdir translation bugs to <https://translationproject.org/team/>

COPYRIGHT
       Copyright   ©   2018   Free   Software   Foundation,  Inc.   License  GPLv3+:  GNU  GPL  version  3  or  later
       <https://gnu.org/licenses/gpl.html>.
       This is free software: you are free to change and redistribute it.  There is NO WARRANTY, to the  extent  per‐
       mitted by law.

SEE ALSO
       rmdir(2)

       Full documentation at: <https://www.gnu.org/software/coreutils/rmdir>
       or available locally via: info '(coreutils) rmdir invocation'

GNU coreutils 8.30                                  September 2019                                           RMDIR(1)
```

## less

```bash
LESS(1)                                        General Commands Manual                                        LESS(1)

NAME
       less - opposite of more

SYNOPSIS
       less -?
       less --help
       less -V
       less --version
       less [-[+]aABcCdeEfFgGiIJKLmMnNqQrRsSuUVwWX~]
            [-b space] [-h lines] [-j line] [-k keyfile]
            [-{oO} logfile] [-p pattern] [-P prompt] [-t tag]
            [-T tagsfile] [-x tab,...] [-y lines] [-[z] lines]
            [-# shift] [+[+]cmd] [--] [filename]...
       (See the OPTIONS section for alternate option syntax with long option names.)

DESCRIPTION
       Less  is  a program similar to more (1), but it has many more features.  Less does not have to read the entire
       input file before starting, so with large input files it starts up faster than text editors like vi (1).  Less
       uses  termcap  (or  terminfo on some systems), so it can run on a variety of terminals.  There is even limited
       support for hardcopy terminals.  (On a hardcopy terminal, lines which should be printed  at  the  top  of  the
       screen are prefixed with a caret.)

       Commands  are  based  on  both more and vi.  Commands may be preceded by a decimal number, called N in the de‐
       scriptions below.  The number is used by some commands, as indicated.

COMMANDS
       In the following descriptions, ^X means control-X.  ESC stands for the ESCAPE key; for example ESC-v means the
       two character sequence "ESCAPE", then "v".

       h or H Help: display a summary of these commands.  If you forget all the other commands, remember this one.

       SPACE or ^V or f or ^F
              Scroll  forward  N lines, default one window (see option -z below).  If N is more than the screen size,
              only the final screenful is displayed.  Warning: some systems use ^V as a special literalization  char‐
              acter.

       z      Like SPACE, but if N is specified, it becomes the new window size.

       ESC-SPACE
              Like SPACE, but scrolls a full screenful, even if it reaches end-of-file in the process.

       ENTER or RETURN or ^N or e or ^E or j or ^J
              Scroll forward N lines, default 1.  The entire N lines are displayed, even if N is more than the screen
              size.

       d or ^D
              Scroll forward N lines, default one half of the screen size.  If N is specified, it becomes the new de‐
              fault for subsequent d and u commands.

       b or ^B or ESC-v
              Scroll  backward N lines, default one window (see option -z below).  If N is more than the screen size,
              only the final screenful is displayed.

       w      Like ESC-v, but if N is specified, it becomes the new window size.

       y or ^Y or ^P or k or ^K
              Scroll backward N lines, default 1.  The entire N lines are displayed, even  if  N  is  more  than  the
              screen size.  Warning: some systems use ^Y as a special job control character.

       u or ^U
              Scroll  backward  N  lines, default one half of the screen size.  If N is specified, it becomes the new
              default for subsequent d and u commands.

       J      Like j, but continues to scroll beyond the end of the file.

       K or Y Like k, but continues to scroll beyond the beginning of the file.

       ESC-) or RIGHTARROW
              Scroll horizontally right N characters, default half the screen width (see the -# option).  If a number
              N is specified, it becomes the default for future RIGHTARROW and LEFTARROW commands.  While the text is
              scrolled, it acts as though the -S option (chop lines) were in effect.

       ESC-( or LEFTARROW
              Scroll horizontally left N characters, default half the screen width (see the -# option).  If a  number
              N is specified, it becomes the default for future RIGHTARROW and LEFTARROW commands.

       ESC-} or ^RIGHTARROW
              Scroll horizontally right to show the end of the longest displayed line.

       ESC-{ or ^LEFTARROW
              Scroll horizontally left back to the first column.

       r or ^R or ^L
              Repaint the screen.

       R      Repaint  the  screen,  discarding any buffered input.  Useful if the file is changing while it is being
              viewed.

       F      Scroll forward, and keep trying to read when the end of file is reached.  Normally this  command  would
              be  used when already at the end of the file.  It is a way to monitor the tail of a file which is grow‐
              ing while it is being viewed.  (The behavior is similar to the "tail -f" command.)

       ESC-F  Like F, but as soon as a line is found which matches the last search pattern, the terminal bell is rung
              and forward scrolling stops.

       g or < or ESC-<
              Go to line N in the file, default 1 (beginning of file).  (Warning: this may be slow if N is large.)

       G or > or ESC->
              Go to line N in the file, default the end of the file.  (Warning: this may be slow if N is large, or if
              N is not specified and standard input, rather than a file, is being read.)

       ESC-G  Same as G, except if no number N is specified and the input is standard input, goes to  the  last  line
              which is currently buffered.

       p or % Go  to  a  position  N percent into the file.  N should be between 0 and 100, and may contain a decimal
              point.

       P      Go to the line containing byte offset N in the file.

       {      If a left curly bracket appears in the top line displayed on the screen, the { command will go  to  the
              matching right curly bracket.  The matching right curly bracket is positioned on the bottom line of the
              screen.  If there is more than one left curly bracket on the top line, a number N may be used to  spec‐
              ify the N-th bracket on the line.

       }      If  a  right curly bracket appears in the bottom line displayed on the screen, the } command will go to
              the matching left curly bracket.  The matching left curly bracket is positioned on the top line of  the
              screen.  If there is more than one right curly bracket on the top line, a number N may be used to spec‐
              ify the N-th bracket on the line.

       (      Like {, but applies to parentheses rather than curly brackets.

       )      Like }, but applies to parentheses rather than curly brackets.

       [      Like {, but applies to square brackets rather than curly brackets.

       ]      Like }, but applies to square brackets rather than curly brackets.

       ESC-^F Followed by two characters, acts like {, but uses the two characters as open and  close  brackets,  re‐
              spectively.   For example, "ESC ^F < >" could be used to go forward to the > which matches the < in the
              top displayed line.

       ESC-^B Followed by two characters, acts like }, but uses the two characters as open and  close  brackets,  re‐
              spectively.  For example, "ESC ^B < >" could be used to go backward to the < which matches the > in the
              bottom displayed line.

       m      Followed by any lowercase or uppercase letter, marks the first displayed line with that letter.  If the
              status column is enabled via the -J option, the status column shows the marked line.

       M      Acts like m, except the last displayed line is marked rather than the first displayed line.

       '      (Single  quote.)  Followed by any lowercase or uppercase letter, returns to the position which was pre‐
              viously marked with that letter.  Followed by another single quote, returns to the  position  at  which
              the last "large" movement command was executed.  Followed by a ^ or $, jumps to the beginning or end of
              the file respectively.  Marks are preserved when a new file is examined, so the ' command can  be  used
              to switch between input files.

       ^X^X   Same as single quote.

       ESC-m  Followed by any lowercase or uppercase letter, clears the mark identified by that letter.

       /pattern
              Search  forward in the file for the N-th line containing the pattern.  N defaults to 1.  The pattern is
              a regular expression, as recognized by the regular expression library supplied  by  your  system.   The
              search starts at the first line displayed (but see the -a and -j options, which change this).

              Certain  characters  are  special  if  entered at the beginning of the pattern; they modify the type of
              search rather than become part of the pattern:

              ^N or !
                     Search for lines which do NOT match the pattern.

              ^E or *
                     Search multiple files.  That is, if the search reaches the END of the current file without find‐
                     ing a match, the search continues in the next file in the command line list.

              ^F or @
                     Begin  the  search  at  the first line of the FIRST file in the command line list, regardless of
                     what is currently displayed on the screen or the settings of the -a or -j options.

              ^K     Highlight any text which matches the pattern on the current screen, but don't move to the  first
                     match (KEEP current position).

              ^R     Don't interpret regular expression metacharacters; that is, do a simple textual comparison.

       ?pattern
              Search  backward  in  the file for the N-th line containing the pattern.  The search starts at the last
              line displayed (but see the -a and -j options, which change this).

              Certain characters are special as in the / command:

              ^N or !
                     Search for lines which do NOT match the pattern.

              ^E or *
                     Search multiple files.  That is, if the search reaches the beginning of the current file without
                     finding a match, the search continues in the previous file in the command line list.

              ^F or @
                     Begin  the search at the last line of the last file in the command line list, regardless of what
                     is currently displayed on the screen or the settings of the -a or -j options.

              ^K     As in forward searches.

              ^R     As in forward searches.

       ESC-/pattern
              Same as "/*".

       ESC-?pattern
              Same as "?*".

       n      Repeat previous search, for N-th line containing the last pattern.  If the previous search was modified
              by  ^N,  the  search  is made for the N-th line NOT containing the pattern.  If the previous search was
              modified by ^E, the search continues in the next (or previous) file if not  satisfied  in  the  current
              file.  If the previous search was modified by ^R, the search is done without using regular expressions.
              There is no effect if the previous search was modified by ^F or ^K.

       N      Repeat previous search, but in the reverse direction.

       ESC-n  Repeat previous search, but crossing file boundaries.  The effect is as if  the  previous  search  were
              modified by *.

       ESC-N  Repeat previous search, but in the reverse direction and crossing file boundaries.

       ESC-u  Undo  search  highlighting.   Turn off highlighting of strings matching the current search pattern.  If
              highlighting is already off because of a previous ESC-u command, turn highlighting back on.  Any search
              command will also turn highlighting back on.  (Highlighting can also be disabled by toggling the -G op‐
              tion; in that case search commands do not turn highlighting back on.)

       &pattern
              Display only lines which match the pattern; lines which do not match the pattern are not displayed.  If
              pattern  is  empty  (if you type & immediately followed by ENTER), any filtering is turned off, and all
              lines are displayed.  While filtering is in effect, an ampersand is displayed at the beginning  of  the
              prompt, as a reminder that some lines in the file may be hidden.

              Certain characters are special as in the / command:

              ^N or !
                     Display only lines which do NOT match the pattern.

              ^R     Don't interpret regular expression metacharacters; that is, do a simple textual comparison.

       :e [filename]
              Examine  a new file.  If the filename is missing, the "current" file (see the :n and :p commands below)
              from the list of files in the command line is re-examined.  A percent sign (%) in the filename  is  re‐
              placed by the name of the current file.  A pound sign (#) is replaced by the name of the previously ex‐
              amined file.  However, two consecutive percent signs are simply replaced with a  single  percent  sign.
              This  allows you to enter a filename that contains a percent sign in the name.  Similarly, two consecu‐
              tive pound signs are replaced with a single pound sign.  The filename is inserted into the command line
              list  of  files  so  that it can be seen by subsequent :n and :p commands.  If the filename consists of
              several files, they are all inserted into the list of files and the first  one  is  examined.   If  the
              filename contains one or more spaces, the entire filename should be enclosed in double quotes (also see
              the -" option).

       ^X^V or E
              Same as :e.  Warning: some systems use ^V as a special literalization character.  On such systems,  you
              may not be able to use ^V.

       :n     Examine  the next file (from the list of files given in the command line).  If a number N is specified,
              the N-th next file is examined.

       :p     Examine the previous file in the command line list.  If a number N is specified, the N-th previous file
              is examined.

       :x     Examine the first file in the command line list.  If a number N is specified, the N-th file in the list
              is examined.

       :d     Remove the current file from the list of files.

       t      Go to the next tag, if there were more than one matches for the current tag.  See  the  -t  option  for
              more details about tags.

       T      Go to the previous tag, if there were more than one matches for the current tag.

       = or ^G or :f
              Prints  some  information  about the file being viewed, including its name and the line number and byte
              offset of the bottom line being displayed.  If possible, it also prints the length  of  the  file,  the
              number of lines in the file and the percent of the file above the last displayed line.

       -      Followed by one of the command line option letters (see OPTIONS below), this will change the setting of
              that option and print a message describing the new setting.  If a ^P (CONTROL-P) is entered immediately
              after  the  dash, the setting of the option is changed but no message is printed.  If the option letter
              has a numeric value (such as -b or -h), or a string value (such as -P or -t), a new value  may  be  en‐
              tered after the option letter.  If no new value is entered, a message describing the current setting is
              printed and nothing is changed.

       --     Like the - command, but takes a long option name (see OPTIONS below) rather than a single  option  let‐
              ter.   You  must press ENTER or RETURN after typing the option name.  A ^P immediately after the second
              dash suppresses printing of a message describing the new setting, as in the - command.

       -+     Followed by one of the command line option letters this will reset the option to  its  default  setting
              and print a message describing the new setting.  (The "-+X" command does the same thing as "-+X" on the
              command line.)  This does not work for string-valued options.

       --+    Like the -+ command, but takes a long option name rather than a single option letter.

       -!     Followed by one of the command line option letters, this will reset the option to the "opposite" of its
              default  setting  and  print  a  message describing the new setting.  This does not work for numeric or
              string-valued options.

       --!    Like the -! command, but takes a long option name rather than a single option letter.

       _      (Underscore.)  Followed by one of the command line option letters, this will print a message describing
              the current setting of that option.  The setting of the option is not changed.

       __     (Double  underscore.)  Like the _ (underscore) command, but takes a long option name rather than a sin‐
              gle option letter.  You must press ENTER or RETURN after typing the option name.

       +cmd   Causes the specified cmd to be executed each time a new file is examined.  For example, +G causes  less
              to initially display each file starting at the end rather than the beginning.

       V      Prints the version number of less being run.

       q or Q or :q or :Q or ZZ
              Exits less.

       The following four commands may or may not be valid, depending on your particular installation.

       v      Invokes  an  editor  to  edit  the current file being viewed.  The editor is taken from the environment
              variable VISUAL if defined, or EDITOR if VISUAL is not defined, or defaults to "vi" if  neither  VISUAL
              nor EDITOR is defined.  See also the discussion of LESSEDIT under the section on PROMPTS below.

       ! shell-command
              Invokes  a  shell to run the shell-command given.  A percent sign (%) in the command is replaced by the
              name of the current file.  A pound sign (#) is replaced by the name of the  previously  examined  file.
              "!!"  repeats  the last shell command.  "!" with no shell command simply invokes a shell.  On Unix sys‐
              tems, the shell is taken from the environment variable SHELL, or defaults to "sh".  On MS-DOS and  OS/2
              systems, the shell is the normal command processor.

       | <m> shell-command
              <m>  represents  any  mark  letter.  Pipes a section of the input file to the given shell command.  The
              section of the file to be piped is between the position marked by the letter and  the  current  screen.
              The entire current screen is included, regardless of whether the marked position is before or after the
              current screen.  <m> may also be ^ or $ to indicate beginning or end of file respectively.  If <m> is .
              or newline, the current screen is piped.

       s filename
              Save the input to a file.  This only works if the input is a pipe, not an ordinary file.

OPTIONS
       Command line options are described below.  Most options may be changed while less is running, via the "-" com‐
       mand.

       Most options may be given in one of two forms: either a dash followed by a single letter, or two  dashes  fol‐
       lowed  by  a long option name.  A long option name may be abbreviated as long as the abbreviation is unambigu‐
       ous.  For example, --quit-at-eof may be abbreviated --quit,  but  not  --qui,  since  both  --quit-at-eof  and
       --quiet  begin  with  --qui.  Some long option names are in uppercase, such as --QUIT-AT-EOF, as distinct from
       --quit-at-eof.  Such option names need only have their first letter capitalized; the remainder of the name may
       be in either case.  For example, --Quit-at-eof is equivalent to --QUIT-AT-EOF.

       Options are also taken from the environment variable "LESS".  For example, to avoid typing "less -options ..."
       each time less is invoked, you might tell csh:

       setenv LESS "-options"

       or if you use sh:

       LESS="-options"; export LESS

       On MS-DOS, you don't need the quotes, but you should replace any percent signs in the options string by double
       percent signs.

       The environment variable is parsed before the command line, so command line options override the LESS environ‐
       ment variable.  If an option appears in the LESS variable, it can be reset to its default value on the command
       line by beginning the command line option with "-+".

       Some  options  like -k or -D require a string to follow the option letter.  The string for that option is con‐
       sidered to end when a dollar sign ($) is found.  For example, you can set two -D options on MS-DOS like this:

       LESS="Dn9.1$Ds4.1"

       If the --use-backslash option appears earlier in the options, then a dollar sign or backslash may be  included
       literally  in  an option string by preceding it with a backslash.  If the --use-backslash option is not in ef‐
       fect, then backslashes are not treated specially, and there is no way to include a dollar sign in  the  option
       string.

       -? or --help
              This  option displays a summary of the commands accepted by less (the same as the h command).  (Depend‐
              ing on how your shell interprets the question mark, it may be necessary to  quote  the  question  mark,
              thus: "-\?".)

       -a or --search-skip-screen
              By  default,  forward searches start at the top of the displayed screen and backwards searches start at
              the bottom of the displayed screen (except for repeated searches invoked by the n or N commands,  which
              start  after  or  before  the  "target"  line respectively; see the -j option for more about the target
              line).  The -a option causes forward searches to instead start at the bottom of the screen and backward
              searches to start at the top of the screen, thus skipping all lines displayed on the screen.

       -A or --SEARCH-SKIP-SCREEN
              Causes  all  forward searches (not just non-repeated searches) to start just after the target line, and
              all backward searches to start just before the target line.  Thus, forward searches will skip  part  of
              the  displayed  screen  (from the first line up to and including the target line).  Similarly backwards
              searches will skip the displayed screen from the last line up to and including the target  line.   This
              was the default behavior in less versions prior to 441.

       -bn or --buffers=n
              Specifies  the  amount of buffer space less will use for each file, in units of kilobytes (1024 bytes).
              By default 64 K of buffer space is used for each file (unless the file is a pipe; see the  -B  option).
              The -b option specifies instead that n kilobytes of buffer space should be used for each file.  If n is
              -1, buffer space is unlimited; that is, the entire file can be read into memory.

       -B or --auto-buffers
              By default, when data is read from a pipe, buffers are allocated automatically as needed.  If  a  large
              amount  of data is read from the pipe, this can cause a large amount of memory to be allocated.  The -B
              option disables this automatic allocation of buffers for pipes, so that only 64 K  (or  the  amount  of
              space  specified  by  the  -b option) is used for the pipe.  Warning: use of -B can result in erroneous
              display, since only the most recently viewed part of the piped data is kept in memory; any earlier data
              is lost.

       -c or --clear-screen
              Causes full screen repaints to be painted from the top line down.  By default, full screen repaints are
              done by scrolling from the bottom of the screen.

       -C or --CLEAR-SCREEN
              Same as -c, for compatibility with older versions of less.

       -d or --dumb
              The -d option suppresses the error message normally displayed if the terminal is dumb; that  is,  lacks
              some  important  capability, such as the ability to clear the screen or scroll backward.  The -d option
              does not otherwise change the behavior of less on a dumb terminal.

       -Dxcolor or --color=xcolor
              [MS-DOS only] Sets the color of the text displayed.  x is a single character which selects the type  of
              text whose color is being set: n=normal, s=standout, d=bold, u=underlined, k=blink.  color is a pair of
              numbers separated by a period.  The first number selects the foreground color and  the  second  selects
              the  background  color  of the text.  A single number N is the same as N.M, where M is the normal back‐
              ground color.  The color may start or end with u to use underline (with the normal  color,  if  by  it‐
              self),  if  the system supports it (Windows only).  x may also be a to toggle strict ANSI sequence ren‐
              dering (SGR mode).

       -e or --quit-at-eof
              Causes less to automatically exit the second time it reaches end-of-file.  By default, the only way  to
              exit less is via the "q" command.

       -E or --QUIT-AT-EOF
              Causes less to automatically exit the first time it reaches end-of-file.

       -f or --force
              Forces  non-regular  files to be opened.  (A non-regular file is a directory or a device special file.)
              Also suppresses the warning message when a binary file is opened.  By default, less will refuse to open
              non-regular  files.  Note that some operating systems will not allow directories to be read, even if -f
              is set.

       -F or --quit-if-one-screen
              Causes less to automatically exit if the entire file can be displayed on the first screen.

       -g or --hilite-search
              Normally, less will highlight ALL strings which match the last search command.  The -g  option  changes
              this behavior to highlight only the particular string which was found by the last search command.  This
              can cause less to run somewhat faster than the default.

       -G or --HILITE-SEARCH
              The -G option suppresses all highlighting of strings found by search commands.

       -hn or --max-back-scroll=n
              Specifies a maximum number of lines to scroll backward.  If it is necessary  to  scroll  backward  more
              than  n  lines, the screen is repainted in a forward direction instead.  (If the terminal does not have
              the ability to scroll backward, -h0 is implied.)

       -i or --ignore-case
              Causes searches to ignore case; that is, uppercase and lowercase are considered identical.  This option
              is ignored if any uppercase letters appear in the search pattern; in other words, if a pattern contains
              uppercase letters, then that search does not ignore case.

       -I or --IGNORE-CASE
              Like -i, but searches ignore case even if the pattern contains uppercase letters.

       -jn or --jump-target=n
              Specifies a line on the screen where the "target" line is to be positioned.  The  target  line  is  the
              line specified by any command to search for a pattern, jump to a line number, jump to a file percentage
              or jump to a tag.  The screen line may be specified by a number: the top line on the screen is  1,  the
              next  is  2,  and  so  on.   The number may be negative to specify a line relative to the bottom of the
              screen: the bottom line on the screen is -1, the second to the bottom is -2, and so  on.   Alternately,
              the  screen  line  may  be specified as a fraction of the height of the screen, starting with a decimal
              point: .5 is in the middle of the screen, .3 is three tenths down from the first line, and so  on.   If
              the  line  is specified as a fraction, the actual line number is recalculated if the terminal window is
              resized, so that the target line remains at the specified fraction of the screen height.  If  any  form
              of the -j option is used, repeated forward searches (invoked with "n" or "N") begin at the line immedi‐
              ately after the target line, and repeated backward searches begin at the target line, unless changed by
              -a  or -A.  For example, if "-j4" is used, the target line is the fourth line on the screen, so forward
              searches begin at the fifth line on the screen.  However nonrepeated searches (invoked with "/" or "?")
              always begin at the start or end of the current screen respectively.

       -J or --status-column
              Displays  a  status  column  at  the  left  edge of the screen.  The status column shows the lines that
              matched the current search, and any lines that are marked (via the m or M command).  The status  column
              is also used if the -w or -W option is in effect.

       -kfilename or --lesskey-file=filename
              Causes  less  to  open  and interpret the named file as a lesskey (1) file.  Multiple -k options may be
              specified.  If the LESSKEY or LESSKEY_SYSTEM environment variable is set, or if a lesskey file is found
              in a standard place (see KEY BINDINGS), it is also used as a lesskey file.

       -K or --quit-on-intr
              Causes  less  to  exit  immediately  (with status 2) when an interrupt character (usually ^C) is typed.
              Normally, an interrupt character causes less to stop whatever it is doing and  return  to  its  command
              prompt.   Note that use of this option makes it impossible to return to the command prompt from the "F"
              command.

       -L or --no-lessopen
              Ignore the LESSOPEN environment variable (see the INPUT PREPROCESSOR section below).  This  option  can
              be  set from within less, but it will apply only to files opened subsequently, not to the file which is
              currently open.

       -m or --long-prompt
              Causes less to prompt verbosely (like more), with the percent into the file.  By default, less  prompts
              with a colon.

       -M or --LONG-PROMPT
              Causes less to prompt even more verbosely than more.

       -n or --line-numbers
              Suppresses  line  numbers.  The default (to use line numbers) may cause less to run more slowly in some
              cases, especially with a very large input file.  Suppressing line numbers with the -n option will avoid
              this problem.  Using line numbers means: the line number will be displayed in the verbose prompt and in
              the = command, and the v command will pass the current line number to the editor (see also the  discus‐
              sion of LESSEDIT in PROMPTS below).

       -N or --LINE-NUMBERS
              Causes a line number to be displayed at the beginning of each line in the display.

       -ofilename or --log-file=filename
              Causes  less to copy its input to the named file as it is being viewed.  This applies only when the in‐
              put file is a pipe, not an ordinary file.  If the file already exists, less will ask  for  confirmation
              before overwriting it.

       -Ofilename or --LOG-FILE=filename
              The -O option is like -o, but it will overwrite an existing file without asking for confirmation.

              If  no log file has been specified, the -o and -O options can be used from within less to specify a log
              file.  Without a file name, they will simply report the name of the  log  file.   The  "s"  command  is
              equivalent to specifying -o from within less.

       -ppattern or --pattern=pattern
              The  -p  option  on  the  command line is equivalent to specifying +/pattern; that is, it tells less to
              start at the first occurrence of pattern in the file.

       -Pprompt or --prompt=prompt
              Provides a way to tailor the three prompt styles to your own preference.  This option would normally be
              put  in  the LESS environment variable, rather than being typed in with each less command.  Such an op‐
              tion must either be the last option in the LESS variable, or be terminated by a dollar sign.
               -Ps followed by a string changes the default (short) prompt to that string.
               -Pm changes the medium (-m) prompt.
               -PM changes the long (-M) prompt.
               -Ph changes the prompt for the help screen.
               -P= changes the message printed by the = command.
               -Pw changes the message printed while waiting for data (in the F command).

              All prompt strings consist of a sequence of letters and special escape sequences.  See the  section  on
              PROMPTS for more details.

       -q or --quiet or --silent
              Causes moderately "quiet" operation: the terminal bell is not rung if an attempt is made to scroll past
              the end of the file or before the beginning of the file.  If the terminal has a "visual  bell",  it  is
              used instead.  The bell will be rung on certain other errors, such as typing an invalid character.  The
              default is to ring the terminal bell in all such cases.

       -Q or --QUIET or --SILENT
              Causes totally "quiet" operation: the terminal bell is never rung.   If  the  terminal  has  a  "visual
              bell", it is used in all cases where the terminal bell would have been rung.

       -r or --raw-control-chars
              Causes  "raw"  control  characters to be displayed.  The default is to display control characters using
              the caret notation; for example, a control-A (octal 001) is displayed as "^A".  Warning:  when  the  -r
              option  is  used,  less cannot keep track of the actual appearance of the screen (since this depends on
              how the screen responds to each type of control character).  Thus, various display problems may result,
              such as long lines being split in the wrong place.

       -R or --RAW-CONTROL-CHARS
              Like  -r,  but  only ANSI "color" escape sequences are output in "raw" form.  Unlike -r, the screen ap‐
              pearance is maintained correctly in most cases.  ANSI "color" escape sequences  are  sequences  of  the
              form:

                   ESC [ ... m

              where  the  "..."  is  zero  or more color specification characters For the purpose of keeping track of
              screen appearance, ANSI color escape sequences are assumed to not move the cursor.  You can  make  less
              think  that  characters  other  than "m" can end ANSI color escape sequences by setting the environment
              variable LESSANSIENDCHARS to the list of characters which can end a color escape sequence.  And you can
              make  less  think  that characters other than the standard ones may appear between the ESC and the m by
              setting the environment variable LESSANSIMIDCHARS to the list of characters which can appear.

       -s or --squeeze-blank-lines
              Causes consecutive blank lines to be squeezed into a single blank line.  This is  useful  when  viewing
              nroff output.

       -S or --chop-long-lines
              Causes  lines longer than the screen width to be chopped (truncated) rather than wrapped.  That is, the
              portion of a long line that does not fit in the screen width is not shown.  The default is to wrap long
              lines; that is, display the remainder on the next line.

       -ttag or --tag=tag
              The  -t  option,  followed  immediately  by a TAG, will edit the file containing that tag.  For this to
              work, tag information must be available; for example, there may be a  file  in  the  current  directory
              called  "tags",  which  was previously built by ctags (1) or an equivalent command.  If the environment
              variable LESSGLOBALTAGS is set, it is taken to be the name of a command compatible with global (1), and
              that  command  is executed to find the tag.  (See http://www.gnu.org/software/global/global.html).  The
              -t option may also be specified from within less (using the - command) as a  way  of  examining  a  new
              file.  The command ":t" is equivalent to specifying -t from within less.

       -Ttagsfile or --tag-file=tagsfile
              Specifies a tags file to be used instead of "tags".

       -u or --underline-special
              Causes backspaces and carriage returns to be treated as printable characters; that is, they are sent to
              the terminal when they appear in the input.

       -U or --UNDERLINE-SPECIAL
              Causes backspaces, tabs, carriage returns and "formatting characters" (as defined  by  Unicode)  to  be
              treated as control characters; that is, they are handled as specified by the -r option.

              By  default, if neither -u nor -U is given, backspaces which appear adjacent to an underscore character
              are treated specially: the underlined text is displayed using the terminal's hardware underlining capa‐
              bility.   Also,  backspaces  which  appear  between two identical characters are treated specially: the
              overstruck text is printed using the terminal's hardware boldface  capability.   Other  backspaces  are
              deleted,  along  with  the preceding character.  Carriage returns immediately followed by a newline are
              deleted.  Other carriage returns are handled as specified by the -r option.  Text which  is  overstruck
              or underlined can be searched for if neither -u nor -U is in effect.

       -V or --version
              Displays the version number of less.

       -w or --hilite-unread
              Temporarily  highlights  the first "new" line after a forward movement of a full page.  The first "new"
              line is the line immediately following the line previously at the bottom of  the  screen.   Also  high‐
              lights  the  target  line  after  a g or p command.  The highlight is removed at the next command which
              causes movement.  The entire line is highlighted, unless the -J option is in effect, in which case only
              the status column is highlighted.

       -W or --HILITE-UNREAD
              Like  -w,  but temporarily highlights the first new line after any forward movement command larger than
              one line.

       -xn,... or --tabs=n,...
              Sets tab stops.  If only one n is specified, tab stops are set at multiples of n.  If  multiple  values
              separated  by  commas  are  specified, tab stops are set at those positions, and then continue with the
              same spacing as the last two.  For example, -x9,17 will set tabs at positions 9, 17, 25, 33, etc.   The
              default for n is 8.

       -X or --no-init
              Disables  sending  the  termcap  initialization  and deinitialization strings to the terminal.  This is
              sometimes desirable if the deinitialization  string  does  something  unnecessary,  like  clearing  the
              screen.

       -yn or --max-forw-scroll=n
              Specifies  a maximum number of lines to scroll forward.  If it is necessary to scroll forward more than
              n lines, the screen is repainted instead.  The -c or -C option may be used to repaint from the  top  of
              the screen if desired.  By default, any forward movement causes scrolling.

       -zn or --window=n or -n
              Changes  the default scrolling window size to n lines.  The default is one screenful.  The z and w com‐
              mands can also be used to change the window size.  The "z" may be omitted for compatibility  with  some
              versions of more.  If the number n is negative, it indicates n lines less than the current screen size.
              For example, if the screen is 24 lines, -z-4 sets the scrolling window to 20 lines.  If the  screen  is
              resized to 40 lines, the scrolling window automatically changes to 36 lines.

       -"cc or --quotes=cc
              Changes  the  filename quoting character.  This may be necessary if you are trying to name a file which
              contains both spaces and quote characters.  Followed by a single  character,  this  changes  the  quote
              character  to that character.  Filenames containing a space should then be surrounded by that character
              rather than by double quotes.  Followed by two characters, changes the open quote to the first  charac‐
              ter, and the close quote to the second character.  Filenames containing a space should then be preceded
              by the open quote character and followed by the close quote character.  Note that even after the  quote
              characters are changed, this option remains -" (a dash followed by a double quote).

       -~ or --tilde
              Normally  lines  after end of file are displayed as a single tilde (~).  This option causes lines after
              end of file to be displayed as blank lines.

       -# or --shift
              Specifies the default number of positions to scroll horizontally in the RIGHTARROW and  LEFTARROW  com‐
              mands.   If  the  number  specified is zero, it sets the default number of positions to one half of the
              screen width.  Alternately, the number may be specified as a fraction  of  the  width  of  the  screen,
              starting  with a decimal point: .5 is half of the screen width, .3 is three tenths of the screen width,
              and so on.  If the number is specified as a fraction, the actual number of scroll positions is recalcu‐
              lated if the terminal window is resized, so that the actual scroll remains at the specified fraction of
              the screen width.

       --follow-name
              Normally, if the input file is renamed while an F command is executing, less will continue  to  display
              the  contents of the original file despite its name change.  If --follow-name is specified, during an F
              command less will periodically attempt to reopen the file by name.  If the reopen succeeds and the file
              is  a different file from the original (which means that a new file has been created with the same name
              as the original (now renamed) file), less will display the contents of that new file.

       --mouse
              Enables mouse input: scrolling the mouse wheel down moves forward in  the  file,  scrolling  the  mouse
              wheel  up  moves  backwards in the file, and clicking the mouse sets the "#" mark to the line where the
              mouse is clicked.  The number of lines to scroll when the wheel is moved can be  set  by  the  --wheel-
              lines  option.   Mouse input works only on terminals which support X11 mouse reporting, and on the Win‐
              dows version of less.

       --MOUSE
              Like --mouse, except the direction scrolled on mouse wheel movement is reversed.

       --no-keypad
              Disables sending the keypad initialization and deinitialization strings to the terminal.  This is some‐
              times useful if the keypad strings make the numeric keypad behave in an undesirable manner.

       --no-histdups
              This  option  changes  the  behavior  so that if a search string or file name is typed in, and the same
              string is already in the history list, the existing copy is removed from the history  list  before  the
              new  one is added.  Thus, a given string will appear only once in the history list.  Normally, a string
              may appear multiple times.

       --rscroll
              This option changes the character used to mark truncated lines.  It may begin with a two-character  at‐
              tribute  indicator like LESSBINFMT does.  If there is no attribute indicator, standout is used.  If set
              to "-", truncated lines are not marked.

       --save-marks
              Save marks in the history file, so marks are retained across different invocations of less.

       --use-backslash
              This option changes the interpretations of options which follow this one.   After  the  --use-backslash
              option,  any  backslash  in an option string is removed and the following character is taken literally.
              This allows a dollar sign to be included in option strings.

       --wheel-lines=n
              Set the number of lines to scroll when the mouse wheel is scrolled and the --mouse or --MOUSE option is
              in effect.  The default is 1 line.

       --     A  command  line  argument of "--" marks the end of option arguments.  Any arguments following this are
              interpreted as filenames.  This can be useful when viewing a file whose name begins with a "-" or "+".

       +      If a command line option begins with +, the remainder of that option is taken to be an initial  command
              to  less.   For  example,  +G tells less to start at the end of the file rather than the beginning, and
              +/xyz tells it to start at the first occurrence of "xyz" in the file.  As  a  special  case,  +<number>
              acts  like  +<number>g;  that  is, it starts the display at the specified line number (however, see the
              caveat under the "g" command above).  If the option starts with ++, the initial command applies to  ev‐
              ery  file being viewed, not just the first one.  The + command described previously may also be used to
              set (or change) an initial command for every file.

LINE EDITING
       When entering command line at the bottom of the screen (for example, a filename for the  :e  command,  or  the
       pattern for a search command), certain keys can be used to manipulate the command line.  Most commands have an
       alternate form in [ brackets ] which can be used if a key does not exist on a particular keyboard.  (Note that
       the  forms  beginning  with  ESC  do not work in some MS-DOS and Windows systems because ESC is the line erase
       character.)  Any of these special keys may be entered literally by preceding it with the "literal"  character,
       either ^V or ^A.  A backslash itself may also be entered literally by entering two backslashes.

       LEFTARROW [ ESC-h ]
              Move the cursor one space to the left.

       RIGHTARROW [ ESC-l ]
              Move the cursor one space to the right.

       ^LEFTARROW [ ESC-b or ESC-LEFTARROW ]
              (That is, CONTROL and LEFTARROW simultaneously.)  Move the cursor one word to the left.

       ^RIGHTARROW [ ESC-w or ESC-RIGHTARROW ]
              (That is, CONTROL and RIGHTARROW simultaneously.)  Move the cursor one word to the right.

       HOME [ ESC-0 ]
              Move the cursor to the beginning of the line.

       END [ ESC-$ ]
              Move the cursor to the end of the line.

       BACKSPACE
              Delete the character to the left of the cursor, or cancel the command if the command line is empty.

       DELETE or [ ESC-x ]
              Delete the character under the cursor.

       ^BACKSPACE [ ESC-BACKSPACE ]
              (That is, CONTROL and BACKSPACE simultaneously.)  Delete the word to the left of the cursor.

       ^DELETE [ ESC-X or ESC-DELETE ]
              (That is, CONTROL and DELETE simultaneously.)  Delete the word under the cursor.

       UPARROW [ ESC-k ]
              Retrieve  the  previous command line.  If you first enter some text and then press UPARROW, it will re‐
              trieve the previous command which begins with that text.

       DOWNARROW [ ESC-j ]
              Retrieve the next command line.  If you first enter some text and then press  DOWNARROW,  it  will  re‐
              trieve the next command which begins with that text.

       TAB    Complete  the  partial  filename  to the left of the cursor.  If it matches more than one filename, the
              first match is entered into the command line.  Repeated TABs will cycle thru the other  matching  file‐
              names.   If  the completed filename is a directory, a "/" is appended to the filename.  (On MS-DOS sys‐
              tems, a "\" is appended.)  The environment variable LESSSEPARATOR can be used to  specify  a  different
              character to append to a directory name.

       BACKTAB [ ESC-TAB ]
              Like, TAB, but cycles in the reverse direction thru the matching filenames.

       ^L     Complete  the  partial  filename  to the left of the cursor.  If it matches more than one filename, all
              matches are entered into the command line (if they fit).

       ^U (Unix and OS/2) or ESC (MS-DOS)
              Delete the entire command line, or cancel the command if the  command  line  is  empty.   If  you  have
              changed  your line-kill character in Unix to something other than ^U, that character is used instead of
              ^U.

       ^G     Delete the entire command line and return to the main prompt.

KEY BINDINGS
       You may define your own less commands by using the program lesskey (1) to create a lesskey  file.   This  file
       specifies  a  set  of command keys and an action associated with each key.  You may also use lesskey to change
       the line-editing keys (see LINE EDITING), and to set  environment  variables.   If  the  environment  variable
       LESSKEY is set, less uses that as the name of the lesskey file.  Otherwise, less looks in a standard place for
       the lesskey file: On Unix systems, less looks for a lesskey file called "$HOME/.less".  On MS-DOS and  Windows
       systems,  less  looks  for a lesskey file called "$HOME/_less", and if it is not found there, then looks for a
       lesskey file called "_less" in any directory specified in the PATH environment  variable.   On  OS/2  systems,
       less  looks  for a lesskey file called "$HOME/less.ini", and if it is not found, then looks for a lesskey file
       called "less.ini" in any directory specified in the INIT environment variable, and if it not found there, then
       looks  for  a lesskey file called "less.ini" in any directory specified in the PATH environment variable.  See
       the lesskey manual page for more details.

       A system-wide lesskey file may also be set up to provide key bindings.  If a key is defined in  both  a  local
       lesskey  file  and  in  the system-wide file, key bindings in the local file take precedence over those in the
       system-wide file.  If the environment variable LESSKEY_SYSTEM is set, less uses that as the name of  the  sys‐
       tem-wide  lesskey  file.   Otherwise, less looks in a standard place for the system-wide lesskey file: On Unix
       systems, the system-wide lesskey file is /usr/local/etc/sysless.  (However, if less was built with a different
       sysconf directory than /usr/local/etc, that directory is where the sysless file is found.)  On MS-DOS and Win‐
       dows systems, the system-wide lesskey file is c:\_sysless.  On OS/2 systems, the system-wide lesskey  file  is
       c:\sysless.ini.

INPUT PREPROCESSOR
       You  may  define  an  "input preprocessor" for less.  Before less opens a file, it first gives your input pre‐
       processor a chance to modify the way the contents of the file are displayed.  An input preprocessor is  simply
       an executable program (or shell script), which writes the contents of the file to a different file, called the
       replacement file.  The contents of the replacement file are then displayed in place of  the  contents  of  the
       original file.  However, it will appear to the user as if the original file is opened; that is, less will dis‐
       play the original filename as the name of the current file.

       An input preprocessor receives one command line argument, the original filename, as entered by the  user.   It
       should  create the replacement file, and when finished, print the name of the replacement file to its standard
       output.  If the input preprocessor does not output a replacement filename, less uses  the  original  file,  as
       normal.   The  input preprocessor is not called when viewing standard input.  To set up an input preprocessor,
       set the LESSOPEN environment variable to a command line which will invoke your input preprocessor.  This  com‐
       mand  line  should  include one occurrence of the string "%s", which will be replaced by the filename when the
       input preprocessor command is invoked.

       When less closes a file opened in such a way, it will call another program, called  the  input  postprocessor,
       which  may  perform  any  desired clean-up action (such as deleting the replacement file created by LESSOPEN).
       This program receives two command line arguments, the original filename as entered by the user, and  the  name
       of  the  replacement file.  To set up an input postprocessor, set the LESSCLOSE environment variable to a com‐
       mand line which will invoke your input postprocessor.  It may include two occurrences of the string "%s";  the
       first  is  replaced  with  the original name of the file and the second with the name of the replacement file,
       which was output by LESSOPEN.

       For example, on many Unix systems, these two scripts will allow you to keep files in  compressed  format,  but
       still let less view them directly:

       lessopen.sh:
            #! /bin/sh
            case "$1" in
            *.Z) TEMPFILE=$(mktemp)
                 uncompress -c $1  >$TEMPFILE  2>/dev/null
                 if [ -s $TEMPFILE ]; then
                      echo $TEMPFILE
                 else
                      rm -f $TEMPFILE
                 fi
                 ;;
            esac

       lessclose.sh:
            #! /bin/sh
            rm $2

       To  use  these  scripts, put them both where they can be executed and set LESSOPEN="lessopen.sh %s", and LESS‐
       CLOSE="lessclose.sh %s %s".  More complex LESSOPEN and LESSCLOSE scripts may be written to accept other  types
       of compressed files, and so on.

       It  is  also  possible  to  set  up  an input preprocessor to pipe the file data directly to less, rather than
       putting the data into a replacement file.  This avoids the need to decompress the entire file before  starting
       to  view  it.   An  input preprocessor that works this way is called an input pipe.  An input pipe, instead of
       writing the name of a replacement file on its standard output, writes the entire contents of  the  replacement
       file  on  its  standard  output.  If the input pipe does not write any characters on its standard output, then
       there is no replacement file and less uses the original file, as normal.  To use an input pipe, make the first
       character in the LESSOPEN environment variable a vertical bar (|) to signify that the input preprocessor is an
       input pipe.  As with non-pipe input preprocessors, the command string must contain one occurrence of %s, which
       is replaced with the filename of the input file.

       For example, on many Unix systems, this script will work like the previous example scripts:

       lesspipe.sh:
            #! /bin/sh
            case "$1" in
            *.Z) uncompress -c $1  2>/dev/null
                 ;;
            *)   exit 1
                 ;;
            esac
            exit $?

       To use this script, put it where it can be executed and set LESSOPEN="|lesspipe.sh %s".

       Note  that  a  preprocessor  cannot output an empty file, since that is interpreted as meaning there is no re‐
       placement, and the original file is used.  To avoid this, if LESSOPEN starts with two vertical bars, the  exit
       status  of the script becomes meaningful.  If the exit status is zero, the output is considered to be replace‐
       ment text, even if it is empty.  If the exit status is nonzero, any output is ignored and the original file is
       used.   For  compatibility  with previous versions of less, if LESSOPEN starts with only one vertical bar, the
       exit status of the preprocessor is ignored.

       When an input pipe is used, a LESSCLOSE postprocessor can be used, but it is usually not necessary since there
       is  no replacement file to clean up.  In this case, the replacement file name passed to the LESSCLOSE postpro‐
       cessor is "-".

       For compatibility with previous versions of less, the input preprocessor or pipe is not used if less is  view‐
       ing standard input.  However, if the first character of LESSOPEN is a dash (-), the input preprocessor is used
       on standard input as well as other files.  In this case, the dash is not considered to be  part  of  the  pre‐
       processor command.  If standard input is being viewed, the input preprocessor is passed a file name consisting
       of a single dash.  Similarly, if the first two characters of LESSOPEN are vertical bar and dash  (|-)  or  two
       vertical  bars  and  a dash (||-), the input pipe is used on standard input as well as other files.  Again, in
       this case the dash is not considered to be part of the input pipe command.

NATIONAL CHARACTER SETS
       There are three types of characters in the input file:

       normal characters
              can be displayed directly to the screen.

       control characters
              should not be displayed directly, but are expected  to  be  found  in  ordinary  text  files  (such  as
              backspace and tab).

       binary characters
              should not be displayed directly and are not expected to be found in text files.

       A  "character  set"  is simply a description of which characters are to be considered normal, control, and bi‐
       nary.  The LESSCHARSET environment variable may be used to select a character set.  Possible values for  LESS‐
       CHARSET are:

       ascii  BS, TAB, NL, CR, and formfeed are control characters, all chars with values between 32 and 126 are nor‐
              mal, and all others are binary.

       iso8859
              Selects an ISO 8859 character set.  This is the same as ASCII, except characters between  160  and  255
              are treated as normal characters.

       latin1 Same as iso8859.

       latin9 Same as iso8859.

       dos    Selects a character set appropriate for MS-DOS.

       ebcdic Selects an EBCDIC character set.

       IBM-1047
              Selects  an  EBCDIC character set used by OS/390 Unix Services.  This is the EBCDIC analogue of latin1.
              You get similar results by setting either LESSCHARSET=IBM-1047 or LC_CTYPE=en_US in your environment.

       koi8-r Selects a Russian character set.

       next   Selects a character set appropriate for NeXT computers.

       utf-8  Selects the UTF-8 encoding of the ISO 10646 character set.  UTF-8 is special in that it supports multi-
              byte characters in the input file.  It is the only character set that supports multi-byte characters.

       windows
              Selects a character set appropriate for Microsoft Windows (cp 1251).

       In  rare cases, it may be desired to tailor less to use a character set other than the ones definable by LESS‐
       CHARSET.  In this case, the environment variable LESSCHARDEF can be used to define a character set.  It should
       be  set  to  a  string  where each character in the string represents one character in the character set.  The
       character "." is used for a normal character, "c" for control, and "b" for binary.  A decimal  number  may  be
       used for repetition.  For example, "bccc4b." would mean character 0 is binary, 1, 2 and 3 are control, 4, 5, 6
       and 7 are binary, and 8 is normal.  All characters after the last are taken to be the same  as  the  last,  so
       characters  9  through  255 would be normal.  (This is an example, and does not necessarily represent any real
       character set.)

       This table shows the value of LESSCHARDEF which is equivalent to each of the possible values for LESSCHARSET:

            ascii     8bcccbcc18b95.b
            dos       8bcccbcc12bc5b95.b.
            ebcdic    5bc6bcc7bcc41b.9b7.9b5.b..8b6.10b6.b9.7b
                      9.8b8.17b3.3b9.7b9.8b8.6b10.b.b.b.
            IBM-1047  4cbcbc3b9cbccbccbb4c6bcc5b3cbbc4bc4bccbc
                      191.b
            iso8859   8bcccbcc18b95.33b.
            koi8-r    8bcccbcc18b95.b128.
            latin1    8bcccbcc18b95.33b.
            next      8bcccbcc18b95.bb125.bb

       If neither LESSCHARSET nor LESSCHARDEF is set, but any of the strings "UTF-8", "UTF8", "utf-8"  or  "utf8"  is
       found in the LC_ALL, LC_CTYPE or LANG environment variables, then the default character set is utf-8.

       If  that string is not found, but your system supports the setlocale interface, less will use setlocale to de‐
       termine the character set.  setlocale is controlled by setting the LANG or LC_CTYPE environment variables.

       Finally, if the setlocale interface is also not available, the default character set is latin1.

       Control and binary characters are displayed in standout (reverse video).  Each such character is displayed  in
       caret notation if possible (e.g. ^A for control-A).  Caret notation is used only if inverting the 0100 bit re‐
       sults in a normal printable character.  Otherwise, the character is displayed as a hex number in angle  brack‐
       ets.   This format can be changed by setting the LESSBINFMT environment variable.  LESSBINFMT may begin with a
       "*" and one character to select the display attribute: "*k" is blinking, "*d" is  bold,  "*u"  is  underlined,
       "*s"  is  standout, and "*n" is normal.  If LESSBINFMT does not begin with a "*", normal attribute is assumed.
       The remainder of LESSBINFMT is a string which may include one printf-style escape sequence (a % followed by x,
       X,  o,  d, etc.).  For example, if LESSBINFMT is "*u[%x]", binary characters are displayed in underlined hexa‐
       decimal surrounded by brackets.  The default if no LESSBINFMT is specified is "*s<%02X>".  Warning: the result
       of expanding the character via LESSBINFMT must be less than 31 characters.

       When  the  character  set is utf-8, the LESSUTFBINFMT environment variable acts similarly to LESSBINFMT but it
       applies to Unicode code points that were successfully decoded but are unsuitable for display (e.g., unassigned
       code  points).   Its default value is "<U+%04lX>".  Note that LESSUTFBINFMT and LESSBINFMT share their display
       attribute setting ("*x") so specifying one will affect both; LESSUTFBINFMT is read  after  LESSBINFMT  so  its
       setting,  if  any,  will  have  priority.  Problematic octets in a UTF-8 file (octets of a truncated sequence,
       octets of a complete but non-shortest form sequence, illegal octets, and stray trailing octets) are  displayed
       individually using LESSBINFMT so as to facilitate diagnostic of how the UTF-8 file is ill-formed.

PROMPTS
       The  -P option allows you to tailor the prompt to your preference.  The string given to the -P option replaces
       the specified prompt string.  Certain characters in the string are interpreted specially.  The  prompt  mecha‐
       nism  is  rather  complicated to provide flexibility, but the ordinary user need not understand the details of
       constructing personalized prompt strings.

       A percent sign followed by a single character is expanded according to what the following character is:

       %bX    Replaced by the byte offset into the current input file.  The b  is  followed  by  a  single  character
              (shown  as  X  above)  which specifies the line whose byte offset is to be used.  If the character is a
              "t", the byte offset of the top line in the display is used, an "m" means use the middle  line,  a  "b"
              means use the bottom line, a "B" means use the line just after the bottom line, and a "j" means use the
              "target" line, as specified by the -j option.

       %B     Replaced by the size of the current input file.

       %c     Replaced by the column number of the text appearing in the first column of the screen.

       %dX    Replaced by the page number of a line in the input file.  The line to be used is determined by  the  X,
              as with the %b option.

       %D     Replaced by the number of pages in the input file, or equivalently, the page number of the last line in
              the input file.

       %E     Replaced by the name of the editor (from the VISUAL environment variable,  or  the  EDITOR  environment
              variable if VISUAL is not defined).  See the discussion of the LESSEDIT feature below.

       %f     Replaced by the name of the current input file.

       %F     Replaced by the last component of the name of the current input file.

       %g     Replaced  by the shell-escaped name of the current input file.  This is useful when the expanded string
              will be used in a shell command, such as in LESSEDIT.

       %i     Replaced by the index of the current file in the list of input files.

       %lX    Replaced by the line number of a line in the input file.  The line to be used is determined by  the  X,
              as with the %b option.

       %L     Replaced by the line number of the last line in the input file.

       %m     Replaced by the total number of input files.

       %pX    Replaced  by  the  percent into the current input file, based on byte offsets.  The line used is deter‐
              mined by the X as with the %b option.

       %PX    Replaced by the percent into the current input file, based on line numbers.  The line  used  is  deter‐
              mined by the X as with the %b option.

       %s     Same as %B.

       %t     Causes  any  trailing spaces to be removed.  Usually used at the end of the string, but may appear any‐
              where.

       %T     Normally expands to the word "file".  However if viewing files via a tags list using the -t option,  it
              expands to the word "tag".

       %x     Replaced by the name of the next input file in the list.

       If any item is unknown (for example, the file size if input is a pipe), a question mark is printed instead.

       The format of the prompt string can be changed depending on certain conditions.  A question mark followed by a
       single character acts like an "IF": depending on the following character, a condition is  evaluated.   If  the
       condition is true, any characters following the question mark and condition character, up to a period, are in‐
       cluded in the prompt.  If the condition is false, such characters are not included.  A colon appearing between
       the  question mark and the period can be used to establish an "ELSE": any characters between the colon and the
       period are included in the string if and only if the IF condition is false.  Condition characters (which  fol‐
       low a question mark) may be:

       ?a     True if any characters have been included in the prompt so far.

       ?bX    True if the byte offset of the specified line is known.

       ?B     True if the size of current input file is known.

       ?c     True if the text is horizontally shifted (%c is not zero).

       ?dX    True if the page number of the specified line is known.

       ?e     True if at end-of-file.

       ?f     True if there is an input filename (that is, if input is not a pipe).

       ?lX    True if the line number of the specified line is known.

       ?L     True if the line number of the last line in the file is known.

       ?m     True if there is more than one input file.

       ?n     True if this is the first prompt in a new input file.

       ?pX    True if the percent into the current input file, based on byte offsets, of the specified line is known.

       ?PX    True if the percent into the current input file, based on line numbers, of the specified line is known.

       ?s     Same as "?B".

       ?x     True if there is a next input file (that is, if the current input file is not the last one).

       Any  characters other than the special ones (question mark, colon, period, percent, and backslash) become lit‐
       erally part of the prompt.  Any of the special characters may be included in the prompt literally by preceding
       it with a backslash.

       Some examples:

       ?f%f:Standard input.

       This prompt prints the filename, if known; otherwise the string "Standard input".

       ?f%f .?ltLine %lt:?pt%pt\%:?btByte %bt:-...

       This prompt would print the filename, if known.  The filename is followed by the line number, if known, other‐
       wise the percent if known, otherwise the byte offset if known.  Otherwise, a dash is printed.  Notice how each
       question  mark  has a matching period, and how the % after the %pt is included literally by escaping it with a
       backslash.

       ?n?f%f .?m(%T %i of %m) ..?e(END) ?x- Next\: %x..%t";

       This prints the filename if this is the first prompt in a file, followed by the "file N of N" message if there
       is  more  than  one input file.  Then, if we are at end-of-file, the string "(END)" is printed followed by the
       name of the next file, if there is one.  Finally, any trailing spaces are  truncated.   This  is  the  default
       prompt.   For  reference,  here  are the defaults for the other two prompts (-m and -M respectively).  Each is
       broken into two lines here for readability only.

       ?n?f%f .?m(%T %i of %m) ..?e(END) ?x- Next\: %x.:
            ?pB%pB\%:byte %bB?s/%s...%t

       ?f%f .?n?m(%T %i of %m) ..?ltlines %lt-%lb?L/%L. :
            byte %bB?s/%s. .?e(END) ?x- Next\: %x.:?pB%pB\%..%t

       And here is the default message produced by the = command:

       ?f%f .?m(%T %i of %m) .?ltlines %lt-%lb?L/%L. .
            byte %bB?s/%s. ?e(END) :?pB%pB\%..%t

       The prompt expansion features are also used for another purpose: if an environment variable  LESSEDIT  is  de‐
       fined,  it  is  used  as the command to be executed when the v command is invoked.  The LESSEDIT string is ex‐
       panded in the same way as the prompt strings.  The default value for LESSEDIT is:

            %E ?lm+%lm. %g

       Note that this expands to the editor name, followed by a + and the line number, followed by the  shell-escaped
       file  name.   If  your editor does not accept the "+linenumber" syntax, or has other differences in invocation
       syntax, the LESSEDIT variable can be changed to modify this default.

SECURITY
       When the environment variable LESSSECURE is set to 1, less runs in a "secure" mode.  This means these features
       are disabled:

              !      the shell command

              |      the pipe command

              :e     the examine command.

              v      the editing command

              s  -o  log files

              -k     use of lesskey files

              -t     use of tags files

                     metacharacters in filenames, such as *

                     filename completion (TAB, ^L)

       Less can also be compiled to be permanently in "secure" mode.

COMPATIBILITY WITH MORE
       If  the  environment  variable  LESS_IS_MORE  is  set to 1, or if the program is invoked via a file link named
       "more", less behaves (mostly) in conformance with the POSIX "more" command specification.  In this mode,  less
       behaves differently in these ways:

       The  -e option works differently.  If the -e option is not set, less behaves as if the -e option were set.  If
       the -e option is set, less behaves as if the -E option were set.

       The -m option works differently.  If the -m option is not set, the medium prompt is used, and it  is  prefixed
       with the string "--More--".  If the -m option is set, the short prompt is used.

       The -n option acts like the -z option.  The normal behavior of the -n option is unavailable in this mode.

       The parameter to the -p option is taken to be a less command rather than a search pattern.

       The LESS environment variable is ignored, and the MORE environment variable is used in its place.

ENVIRONMENT VARIABLES
       Environment  variables  may  be specified either in the system environment as usual, or in a lesskey (1) file.
       If environment variables are defined in more than one place, variables defined in a local  lesskey  file  take
       precedence  over  variables defined in the system environment, which take precedence over variables defined in
       the system-wide lesskey file.

       COLUMNS
              Sets the number of columns on the screen.  Takes precedence over the number of columns specified by the
              TERM  variable.   (But if you have a windowing system which supports TIOCGWINSZ or WIOCGETD, the window
              system's idea of the screen size takes precedence over the LINES and COLUMNS environment variables.)

       EDITOR The name of the editor (used for the v command).

       HOME   Name of the user's home directory (used to find a lesskey file on Unix and OS/2 systems).

       HOMEDRIVE, HOMEPATH
              Concatenation of the HOMEDRIVE and HOMEPATH environment variables is the name of the user's home direc‐
              tory if the HOME variable is not set (only in the Windows version).

       INIT   Name of the user's init directory (used to find a lesskey file on OS/2 systems).

       LANG   Language for determining the character set.

       LC_CTYPE
              Language for determining the character set.

       LESS   Options which are passed to less automatically.

       LESSANSIENDCHARS
              Characters which may end an ANSI color escape sequence (default "m").

       LESSANSIMIDCHARS
              Characters which may appear between the ESC character and the end character in an ANSI color escape se‐
              quence (default "0123456789:;[?!"'#%()*+ ".

       LESSBINFMT
              Format for displaying non-printable, non-control characters.

       LESSCHARDEF
              Defines a character set.

       LESSCHARSET
              Selects a predefined character set.

       LESSCLOSE
              Command line to invoke the (optional) input-postprocessor.

       LESSECHO
              Name of the lessecho program (default "lessecho").  The lessecho program is needed to expand  metachar‐
              acters, such as * and ?, in filenames on Unix systems.

       LESSEDIT
              Editor prototype string (used for the v command).  See discussion under PROMPTS.

       LESSGLOBALTAGS
              Name  of  the command used by the -t option to find global tags.  Normally should be set to "global" if
              your system has the global (1) command.  If not set, global tags are not used.

       LESSHISTFILE
              Name of the history file used to remember search commands and shell  commands  between  invocations  of
              less.   If  set  to "-" or "/dev/null", a history file is not used.  The default is "$HOME/.lesshst" on
              Unix  systems,   "$HOME/_lesshst"   on   DOS   and   Windows   systems,   or   "$HOME/lesshst.ini"   or
              "$INIT/lesshst.ini" on OS/2 systems.

       LESSHISTSIZE
              The maximum number of commands to save in the history file.  The default is 100.

       LESSKEY
              Name of the default lesskey(1) file.

       LESSKEY_SYSTEM
              Name of the default system-wide lesskey(1) file.

       LESSMETACHARS
              List of characters which are considered "metacharacters" by the shell.

       LESSMETAESCAPE
              Prefix which less will add before each metacharacter in a command sent to the shell.  If LESSMETAESCAPE
              is an empty string, commands containing metacharacters will not be passed to the shell.

       LESSOPEN
              Command line to invoke the (optional) input-preprocessor.

       LESSSECURE
              Runs less in "secure" mode.  See discussion under SECURITY.

       LESSSEPARATOR
              String to be appended to a directory name in filename completion.

       LESSUTFBINFMT
              Format for displaying non-printable Unicode code points.

       LESS_IS_MORE
              Emulate the more (1) command.

       LINES  Sets the number of lines on the screen.  Takes precedence over the number of  lines  specified  by  the
              TERM  variable.   (But if you have a windowing system which supports TIOCGWINSZ or WIOCGETD, the window
              system's idea of the screen size takes precedence over the LINES and COLUMNS environment variables.)

       MORE   Options which are passed to less automatically when running in more compatible mode.

       PATH   User's search path (used to find a lesskey file on MS-DOS and OS/2 systems).

       SHELL  The shell used to execute the ! command, as well as to expand filenames.

       TERM   The type of terminal on which less is being run.

       VISUAL The name of the editor (used for the v command).

SEE ALSO
       lesskey(1)

COPYRIGHT
       Copyright (C) 1984-2019  Mark Nudelman

       less is part of the GNU project and is free software.  You can redistribute it  and/or  modify  it  under  the
       terms  of  either  (1) the GNU General Public License as published by the Free Software Foundation; or (2) the
       Less License.  See the file README in the less distribution for more details  regarding  redistribution.   You
       should  have  received  a  copy of the GNU General Public License along with the source for less; see the file
       COPYING.  If not, write to the Free Software Foundation, 59 Temple Place, Suite 330, Boston,  MA   02111-1307,
       USA.  You should also have received a copy of the Less License; see the file LICENSE.

       less  is  distributed  in  the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
       warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for  more
       details.

AUTHOR
       Mark Nudelman
       Report bugs at https://github.com/gwsw/less/issues.
       For more information, see the less homepage at
       http://www.greenwoodsoftware.com/less.

                                               Version 551: 11 Jun 2019                                       LESS(1)

```

## rm

```bash
RM(1)                                               User Commands                                               RM(1)

NAME
       rm - remove files or directories

SYNOPSIS
       rm [OPTION]... [FILE]...

DESCRIPTION
       This  manual  page  documents the GNU version of rm.  rm removes each specified file.  By default, it does not
       remove directories.

       If the -I or --interactive=once option is given, and there are more than three files or the -r, -R,  or  --re‐
       cursive are given, then rm prompts the user for whether to proceed with the entire operation.  If the response
       is not affirmative, the entire command is aborted.

       Otherwise, if a file is unwritable, standard input is a terminal, and the -f or --force option is  not  given,
       or the -i or --interactive=always option is given, rm prompts the user for whether to remove the file.  If the
       response is not affirmative, the file is skipped.

OPTIONS
       Remove (unlink) the FILE(s).

       -f, --force
              ignore nonexistent files and arguments, never prompt

       -i     prompt before every removal

       -I     prompt once before removing more than three files, or when removing recursively;  less  intrusive  than
              -i, while still giving protection against most mistakes

       --interactive[=WHEN]
              prompt according to WHEN: never, once (-I), or always (-i); without WHEN, prompt always

       --one-file-system
              when  removing a hierarchy recursively, skip any directory that is on a file system different from that
              of the corresponding command line argument

       --no-preserve-root
              do not treat '/' specially

       --preserve-root[=all]
              do not remove '/' (default); with 'all', reject any command line argument on a separate device from its
              parent

       -r, -R, --recursive
              remove directories and their contents recursively

       -d, --dir
              remove empty directories

       -v, --verbose
              explain what is being done

       --help display this help and exit

       --version
              output version information and exit

       By  default,  rm does not remove directories.  Use the --recursive (-r or -R) option to remove each listed di‐
       rectory, too, along with all of its contents.

       To remove a file whose name starts with a '-', for example '-foo', use one of these commands:

              rm -- -foo

              rm ./-foo

       Note that if you use rm to remove a file, it might be possible to recover some of its contents,  given  suffi‐
       cient  expertise and/or time.  For greater assurance that the contents are truly unrecoverable, consider using
       shred.

AUTHOR
       Written by Paul Rubin, David MacKenzie, Richard M. Stallman, and Jim Meyering.

REPORTING BUGS
       GNU coreutils online help: <https://www.gnu.org/software/coreutils/>
       Report rm translation bugs to <https://translationproject.org/team/>

COPYRIGHT
       Copyright  ©  2018  Free  Software  Foundation,  Inc.   License  GPLv3+:  GNU   GPL   version   3   or   later
       <https://gnu.org/licenses/gpl.html>.
       This  is  free software: you are free to change and redistribute it.  There is NO WARRANTY, to the extent per‐
       mitted by law.

SEE ALSO
       unlink(1), unlink(2), chattr(1), shred(1)

       Full documentation at: <https://www.gnu.org/software/coreutils/rm>
       or available locally via: info '(coreutils) rm invocation'

GNU coreutils 8.30                                  September 2019                                              RM(1)
```


## wc

```bash
WC(1)                                               User Commands                                               WC(1)

NAME
       wc - print newline, word, and byte counts for each file

SYNOPSIS
       wc [OPTION]... [FILE]...
       wc [OPTION]... --files0-from=F

DESCRIPTION
       Print  newline,  word,  and byte counts for each FILE, and a total line if more than one FILE is specified.  A
       word is a non-zero-length sequence of characters delimited by white space.

       With no FILE, or when FILE is -, read standard input.

       The options below may be used to select which counts are printed, always  in  the  following  order:  newline,
       word, character, byte, maximum line length.

       -c, --bytes
              print the byte counts

       -m, --chars
              print the character counts

       -l, --lines
              print the newline counts

       --files0-from=F
              read  input  from the files specified by NUL-terminated names in file F; If F is - then read names from
              standard input

       -L, --max-line-length
              print the maximum display width

       -w, --words
              print the word counts

       --help display this help and exit

       --version
              output version information and exit

AUTHOR
       Written by Paul Rubin and David MacKenzie.

REPORTING BUGS
       GNU coreutils online help: <https://www.gnu.org/software/coreutils/>
       Report wc translation bugs to <https://translationproject.org/team/>

COPYRIGHT
       Copyright  ©  2018  Free  Software  Foundation,  Inc.   License  GPLv3+:  GNU   GPL   version   3   or   later
       <https://gnu.org/licenses/gpl.html>.
       This  is  free software: you are free to change and redistribute it.  There is NO WARRANTY, to the extent per‐
       mitted by law.

SEE ALSO
       Full documentation at: <https://www.gnu.org/software/coreutils/wc>
       or available locally via: info '(coreutils) wc invocation'

GNU coreutils 8.30                                  September 2019                                              WC(1)
```

## uniq

```bash
UNIQ(1)                                             User Commands                                             UNIQ(1)

NAME
       uniq - report or omit repeated lines

SYNOPSIS
       uniq [OPTION]... [INPUT [OUTPUT]]

DESCRIPTION
       Filter adjacent matching lines from INPUT (or standard input), writing to OUTPUT (or standard output).

       With no options, matching lines are merged to the first occurrence.

       Mandatory arguments to long options are mandatory for short options too.

       -c, --count
              prefix lines by the number of occurrences

       -d, --repeated
              only print duplicate lines, one for each group

       -D     print all duplicate lines

       --all-repeated[=METHOD]
              like -D, but allow separating groups with an empty line; METHOD={none(default),prepend,separate}

       -f, --skip-fields=N
              avoid comparing the first N fields

       --group[=METHOD]
              show all items, separating groups with an empty line; METHOD={separate(default),prepend,append,both}

       -i, --ignore-case
              ignore differences in case when comparing

       -s, --skip-chars=N
              avoid comparing the first N characters

       -u, --unique
              only print unique lines

       -z, --zero-terminated
              line delimiter is NUL, not newline

       -w, --check-chars=N
              compare no more than N characters in lines

       --help display this help and exit

       --version
              output version information and exit

       A field is a run of blanks (usually spaces and/or TABs), then non-blank characters.  Fields are skipped before
       chars.

       Note: 'uniq' does not detect repeated lines unless they are adjacent.  You may want to sort the  input  first,
       or use 'sort -u' without 'uniq'.  Also, comparisons honor the rules specified by 'LC_COLLATE'.

AUTHOR
       Written by Richard M. Stallman and David MacKenzie.

REPORTING BUGS
       GNU coreutils online help: <https://www.gnu.org/software/coreutils/>
       Report uniq translation bugs to <https://translationproject.org/team/>

COPYRIGHT
       Copyright   ©   2018   Free   Software   Foundation,  Inc.   License  GPLv3+:  GNU  GPL  version  3  or  later
       <https://gnu.org/licenses/gpl.html>.
       This is free software: you are free to change and redistribute it.  There is NO WARRANTY, to the  extent  per‐
       mitted by law.

SEE ALSO
       comm(1), join(1), sort(1)

       Full documentation at: <https://www.gnu.org/software/coreutils/uniq>
       or available locally via: info '(coreutils) uniq invocation'

```

## man

```bash
GNU coreutils 8.30                                  September 2019                                            UNIQ(1)
MAN(1)                                            Manual pager utils                                           MAN(1)

NAME
       man - an interface to the system reference manuals

SYNOPSIS
       man [man options] [[section] page ...] ...
       man -k [apropos options] regexp ...
       man -K [man options] [section] term ...
       man -f [whatis options] page ...
       man -l [man options] file ...
       man -w|-W [man options] page ...

DESCRIPTION
       man  is the system's manual pager.  Each page argument given to man is normally the name of a program, utility
       or function.  The manual page associated with each of these arguments is then found and displayed.  A section,
       if  provided,  will direct man to look only in that section of the manual.  The default action is to search in
       all of the available sections following a pre-defined order (see DEFAULTS), and to show only  the  first  page
       found, even if page exists in several sections.

       The table below shows the section numbers of the manual followed by the types of pages they contain.

       1   Executable programs or shell commands
       2   System calls (functions provided by the kernel)
       3   Library calls (functions within program libraries)
       4   Special files (usually found in /dev)
       5   File formats and conventions, e.g. /etc/passwd
       6   Games
       7   Miscellaneous (including macro packages and conventions), e.g. man(7), groff(7)
       8   System administration commands (usually only for root)
       9   Kernel routines [Non standard]

       A manual page consists of several sections.

       Conventional  section  names  include  NAME,  SYNOPSIS,  CONFIGURATION, DESCRIPTION, OPTIONS, EXIT STATUS, RE‐
       TURN VALUE, ERRORS, ENVIRONMENT, FILES, VERSIONS, CONFORMING TO, NOTES, BUGS, EXAMPLE, AUTHORS, and SEE ALSO.

       The following conventions apply to the SYNOPSIS section and can be used as a guide in other sections.

       bold text          type exactly as shown.
       italic text        replace with appropriate argument.
       [-abc]             any or all arguments within [ ] are optional.
       -a|-b              options delimited by | cannot be used together.
       argument ...       argument is repeatable.
       [expression] ...   entire expression within [ ] is repeatable.

       Exact rendering may vary depending on the output device.  For instance, man will usually not be able to render
       italics when running in a terminal, and will typically use underlined or coloured text instead.

       The  command  or function illustration is a pattern that should match all possible invocations.  In some cases
       it is advisable to illustrate several exclusive invocations as is shown in the SYNOPSIS section of this manual
       page.

EXAMPLES
       man ls
           Display the manual page for the item (program) ls.

       man man.7
           Display  the manual page for macro package man from section 7.  (This is an alternative spelling of "man 7
           man".)

       man 'man(7)'
           Display the manual page for macro package man from section 7.  (This is another  alternative  spelling  of
           "man  7  man".  It may be more convenient when copying and pasting cross-references to manual pages.  Note
           that the parentheses must normally be quoted to protect them from the shell.)

       man -a intro
           Display, in succession, all of the available intro manual pages contained within the manual.  It is possi‐
           ble to quit between successive displays or skip any of them.

       man -t bash | lpr -Pps
           Format  the  manual  page for bash into the default troff or groff format and pipe it to the printer named
           ps.  The default output for groff is usually PostScript.  man --help should advise as to  which  processor
           is bound to the -t option.

       man -l -Tdvi ./foo.1x.gz > ./foo.1x.dvi
           This command will decompress and format the nroff source manual page ./foo.1x.gz into a device independent
           (dvi) file.  The redirection is necessary as the -T flag causes output to be directed to  stdout  with  no
           pager.   The output could be viewed with a program such as xdvi or further processed into PostScript using
           a program such as dvips.

       man -k printf
           Search the short descriptions and manual page names for the keyword printf as regular  expression.   Print
           out any matches.  Equivalent to apropos printf.

       man -f smail
           Lookup the manual pages referenced by smail and print out the short descriptions of any found.  Equivalent
           to whatis smail.

OVERVIEW
       Many options are available to man in order to give as much flexibility as possible to the user.   Changes  can
       be  made to the search path, section order, output processor, and other behaviours and operations detailed be‐
       low.

       If set, various environment variables are interrogated to determine the operation of man.  It is  possible  to
       set  the "catch-all" variable $MANOPT to any string in command line format, with the exception that any spaces
       used as part of an option's argument must be escaped (preceded by a backslash).  man will parse $MANOPT  prior
       to  parsing  its own command line.  Those options requiring an argument will be overridden by the same options
       found on the command line.  To reset all of the options set in $MANOPT, -D can be  specified  as  the  initial
       command  line  option.   This will allow man to "forget" about the options specified in $MANOPT, although they
       must still have been valid.

       Manual pages are normally stored in nroff(1) format under a directory such as /usr/share/man.  In some instal‐
       lations, there may also be preformatted cat pages to improve performance.  See manpath(5) for details of where
       these files are stored.

       This package supports manual pages in multiple languages, controlled by your locale.  If your system  did  not
       set  this  up for you automatically, then you may need to set $LC_MESSAGES, $LANG, or another system-dependent
       environment variable to indicate your preferred locale, usually specified in the POSIX format:

       <language>[_<territory>[.<character-set>[,<version>]]]

       If the desired page is available in your locale, it will be displayed in lieu of the standard (usually  Ameri‐
       can English) page.

       If you find that the translations supplied with this package are not available in your native language and you
       would like to supply them, please contact the maintainer who will be coordinating such activity.

       Individual manual pages are normally written and maintained by the maintainers of the  program,  function,  or
       other  topic  that  they  document, and are not included with this package.  If you find that a manual page is
       missing or inadequate, please report that to the maintainers of the package in question.

       For information regarding other features and extensions available with this manual pager, please read the doc‐
       uments supplied with the package.

DEFAULTS
       The  order  of sections to search may be overridden by the environment variable $MANSECT or by the SECTION di‐
       rective in /etc/manpath.config.  By default it is as follows:

              1 n l 8 3 2 3posix 3pm 3perl 3am 5 4 9 6 7

       The formatted manual page is displayed using a pager.  This can be specified in a number of ways, or else will
       fall back to a default (see option -P for details).

       The filters are deciphered by a number of means.  Firstly, the command line option -p or the environment vari‐
       able $MANROFFSEQ is interrogated.  If -p was not used and the environment variable was not  set,  the  initial
       line of the nroff file is parsed for a preprocessor string.  To contain a valid preprocessor string, the first
       line must resemble

       '\" <string>

       where string can be any combination of letters described by option -p below.

       If none of the above methods provide any filter information, a default set is used.

       A formatting pipeline is formed from the filters and the primary formatter (nroff or [tg]roff with -t) and ex‐
       ecuted.   Alternatively,  if  an  executable program mandb_nfmt (or mandb_tfmt with -t) exists in the man tree
       root, it is executed instead.  It gets passed the manual source file, the preprocessor string, and  optionally
       the device specified with -T or -E as arguments.

OPTIONS
       Non-argument  options  that  are  duplicated either on the command line, in $MANOPT, or both, are not harmful.
       For options that require an argument, each duplication will override the previous argument value.

   General options
       -C file, --config-file=file
              Use this user configuration file rather than the default of ~/.manpath.

       -d, --debug
              Print debugging information.

       -D, --default
              This option is normally issued as the very first option and resets man's behaviour to its default.  Its
              use  is to reset those options that may have been set in $MANOPT.  Any options that follow -D will have
              their usual effect.

       --warnings[=warnings]
              Enable warnings from groff.  This may be used to perform sanity checks on the  source  text  of  manual
              pages.   warnings  is  a  comma-separated  list of warning names; if it is not supplied, the default is
              "mac".  See the “Warnings” node in info groff for a list of available warning names.

   Main modes of operation
       -f, --whatis
              Equivalent to whatis.  Display a short description from the manual page, if available.   See  whatis(1)
              for details.

       -k, --apropos
              Equivalent to apropos.  Search the short manual page descriptions for keywords and display any matches.
              See apropos(1) for details.

       -K, --global-apropos
              Search for text in all manual pages.  This is a brute-force search, and is likely to take some time; if
              you  can,  you should specify a section to reduce the number of pages that need to be searched.  Search
              terms may be simple strings (the default), or regular expressions if the --regex option is used.

              Note that this searches the sources of the manual pages, not the rendered  text,  and  so  may  include
              false positives due to things like comments in source files.  Searching the rendered text would be much
              slower.

       -l, --local-file
              Activate "local" mode.  Format and display local manual files instead of searching through the system's
              manual  collection.   Each manual page argument will be interpreted as an nroff source file in the cor‐
              rect format.  No cat file is produced.  If '-' is listed as one of the arguments, input will  be  taken
              from  stdin.   When this option is not used, and man fails to find the page required, before displaying
              the error message, it attempts to act as if this option was supplied, using the name as a filename  and
              looking for an exact match.

       -w, --where, --path, --location
              Don't  actually  display the manual page, but do print the location of the source nroff file that would
              be formatted.  If the -a option is also used, then print the locations of all source files  that  match
              the search criteria.

       -W, --where-cat, --location-cat
              Don't  actually  display  the  manual page, but do print the location of the preformatted cat file that
              would be displayed.  If the -a option is also used, then print the locations of  all  preformatted  cat
              files that match the search criteria.

              If  -w  and -W are both used, then print both source file and cat file separated by a space.  If all of
              -w, -W, and -a are used, then do this for each possible match.

       -c, --catman
              This option is not for general use and should only be used by the catman program.

       -R encoding, --recode=encoding
              Instead of formatting the manual page in the usual way, output its source converted  to  the  specified
              encoding.   If  you already know the encoding of the source file, you can also use manconv(1) directly.
              However, this option allows you to convert several manual pages to a single encoding without having  to
              explicitly state the encoding of each, provided that they were already installed in a structure similar
              to a manual page hierarchy.

              Consider using man-recode(1) instead for converting multiple manual pages, since it  has  an  interface
              designed for bulk conversion and so can be much faster.

   Finding manual pages
       -L locale, --locale=locale
              man will normally determine your current locale by a call to the C function setlocale(3) which interro‐
              gates various environment variables, possibly including $LC_MESSAGES and $LANG.  To  temporarily  over‐
              ride  the  determined  value,  use this option to supply a locale string directly to man.  Note that it
              will not take effect until the search for pages actually begins.  Output such as the help message  will
              always be displayed in the initially determined locale.

       -m system[,...], --systems=system[,...]
              If this system has access to other operating system's manual pages, they can be accessed using this op‐
              tion.  To search for a manual page from NewOS's manual page collection, use the option -m NewOS.

              The system specified can be a combination of comma delimited operating  system  names.   To  include  a
              search  of  the  native  operating  system's  manual pages, include the system name man in the argument
              string.  This option will override the $SYSTEM environment variable.

       -M path, --manpath=path
              Specify an alternate manpath to use.  By default, man uses manpath derived code to determine  the  path
              to search.  This option overrides the $MANPATH environment variable and causes option -m to be ignored.

              A  path  specified as a manpath must be the root of a manual page hierarchy structured into sections as
              described in the man-db manual (under "The manual page system").  To view manual pages outside such hi‐
              erarchies, see the -l option.

       -S list, -s list, --sections=list
              The given list is a colon- or comma-separated list of sections, used to determine which manual sections
              to search and in what order.  This option overrides the $MANSECT environment variable.  (The -s  spell‐
              ing is for compatibility with System V.)

       -e sub-extension, --extension=sub-extension
              Some  systems incorporate large packages of manual pages, such as those that accompany the Tcl package,
              into the main manual page hierarchy.  To get around the problem of having two  manual  pages  with  the
              same  name  such as exit(3), the Tcl pages were usually all assigned to section l.  As this is unfortu‐
              nate, it is now possible to put the pages in the correct section, and to assign a specific  "extension"
              to  them,  in this case, exit(3tcl).  Under normal operation, man will display exit(3) in preference to
              exit(3tcl).  To negotiate this situation and to avoid having to know which section the page you require
              resides  in,  it  is  now possible to give man a sub-extension string indicating which package the page
              must belong to.  Using the above example, supplying the option -e tcl to man will restrict  the  search
              to pages having an extension of *tcl.

       -i, --ignore-case
              Ignore case when searching for manual pages.  This is the default.

       -I, --match-case
              Search for manual pages case-sensitively.

       --regex
              Show all pages with any part of either their names or their descriptions matching each page argument as
              a regular expression, as with apropos(1).  Since there is usually no reasonable way to  pick  a  "best"
              page when searching for a regular expression, this option implies -a.

       --wildcard
              Show  all  pages  with any part of either their names or their descriptions matching each page argument
              using shell-style wildcards, as with apropos(1) --wildcard.  The page argument must  match  the  entire
              name or description, or match on word boundaries in the description.  Since there is usually no reason‐
              able way to pick a "best" page when searching for a wildcard, this option implies -a.

       --names-only
              If the --regex or --wildcard option is used, match only page names,  not  page  descriptions,  as  with
              whatis(1).  Otherwise, no effect.

       -a, --all
              By  default,  man will exit after displaying the most suitable manual page it finds.  Using this option
              forces man to display all the manual pages with names that match the search criteria.

       -u, --update
              This option causes man to update its database caches of installed manual pages.  This is only needed in
              rare situations, and it is normally better to run mandb(8) instead.

       --no-subpages
              By  default,  man will try to interpret pairs of manual page names given on the command line as equiva‐
              lent to a single manual page name containing a hyphen or an underscore.  This supports the common  pat‐
              tern of programs that implement a number of subcommands, allowing them to provide manual pages for each
              that can be accessed using similar syntax as would be used to invoke the subcommands  themselves.   For
              example:

                $ man -aw git diff
                /usr/share/man/man1/git-diff.1.gz

              To disable this behaviour, use the --no-subpages option.

                $ man -aw --no-subpages git diff
                /usr/share/man/man1/git.1.gz
                /usr/share/man/man3/Git.3pm.gz
                /usr/share/man/man1/diff.1.gz

   Controlling formatted output
       -P pager, --pager=pager
              Specify  which  output  pager  to use.  By default, man uses pager, falling back to cat if pager is not
              found or is not executable.  This option overrides the $MANPAGER environment variable,  which  in  turn
              overrides the $PAGER environment variable.  It is not used in conjunction with -f or -k.

              The  value  may  be a simple command name or a command with arguments, and may use shell quoting (back‐
              slashes, single quotes, or double quotes).  It may not use pipes to connect multiple commands;  if  you
              need  that,  use a wrapper script, which may take the file to display either as an argument or on stan‐
              dard input.

       -r prompt, --prompt=prompt
              If a recent version of less is used as the pager, man will attempt to set its prompt and some  sensible
              options.  The default prompt looks like

               Manual page name(sec) line x

              where  name  denotes the manual page name, sec denotes the section it was found under and x the current
              line number.  This is achieved by using the $LESS environment variable.

              Supplying -r with a string will override this default.  The string may contain the text  $MAN_PN  which
              will be expanded to the name of the current manual page and its section name surrounded by "(" and ")".
              The string used to produce the default could be expressed as

              \ Manual\ page\ \$MAN_PN\ ?ltline\ %lt?L/%L.:
              byte\ %bB?s/%s..?\ (END):?pB\ %pB\\%..
              (press h for help or q to quit)

              It is broken into three lines here for the sake of readability only.  For its meaning see  the  less(1)
              manual  page.   The  prompt string is first evaluated by the shell.  All double quotes, back-quotes and
              backslashes in the prompt must be escaped by a preceding backslash.  The prompt string may  end  in  an
              escaped $ which may be followed by further options for less.  By default man sets the -ix8 options.

              The $MANLESS environment variable described below may be used to set a default prompt string if none is
              supplied on the command line.

       -7, --ascii
              When viewing a pure ascii(7) manual page on a 7 bit terminal or terminal emulator, some characters  may
              not  display  correctly when using the latin1(7) device description with GNU nroff.  This option allows
              pure ascii manual pages to be displayed in ascii with the latin1 device.  It  will  not  translate  any
              latin1  text.   The following table shows the translations performed: some parts of it may only be dis‐
              played properly when using GNU nroff's latin1(7) device.

              Description           Octal   latin1   ascii
              ─────────────────────────────────────────────
              continuation hyphen    255      ‐        -
              bullet (middle dot)    267      •        o
              acute accent           264      ´        '
              multiplication sign    327      ×        x

              If the latin1 column displays correctly, your terminal may be set up for latin1 characters and this op‐
              tion  is not necessary.  If the latin1 and ascii columns are identical, you are reading this page using
              this option or man did not format this page using the latin1 device description.  If the latin1  column
              is missing or corrupt, you may need to view manual pages with this option.

              This  option  is  ignored  when using options -t, -H, -T, or -Z and may be useless for nroff other than
              GNU's.

       -E encoding, --encoding=encoding
              Generate output for a character encoding other than the default.  For backward compatibility,  encoding
              may  be  an  nroff  device  such as ascii, latin1, or utf8 as well as a true character encoding such as
              UTF-8.

       --no-hyphenation, --nh
              Normally, nroff will automatically hyphenate text at line breaks even in words that do not contain  hy‐
              phens,  if  it is necessary to do so to lay out words on a line without excessive spacing.  This option
              disables automatic hyphenation, so words will only be hyphenated if they already contain hyphens.

              If you are writing a manual page and simply want to prevent nroff from hyphenating a word at  an  inap‐
              propriate point, do not use this option, but consult the nroff documentation instead; for instance, you
              can put "\%" inside a word to indicate that it may be hyphenated at that point,  or  put  "\%"  at  the
              start of a word to prevent it from being hyphenated.

       --no-justification, --nj
              Normally,  nroff will automatically justify text to both margins.  This option disables full justifica‐
              tion, leaving justified only to the left margin, sometimes called "ragged-right" text.

              If you are writing a manual page and simply want to prevent nroff from justifying  certain  paragraphs,
              do  not  use  this  option,  but consult the nroff documentation instead; for instance, you can use the
              ".na", ".nf", ".fi", and ".ad" requests to temporarily disable adjusting and filling.

       -p string, --preprocessor=string
              Specify the sequence of preprocessors to run before nroff or troff/groff.  Not all  installations  will
              have  a  full  set  of preprocessors.  Some of the preprocessors and the letters used to designate them
              are: eqn (e), grap (g), pic (p), tbl (t), vgrind (v), refer (r).  This option overrides the $MANROFFSEQ
              environment variable.  zsoelim is always run as the very first preprocessor.

       -t, --troff
              Use groff -mandoc to format the manual page to stdout.  This option is not required in conjunction with
              -H, -T, or -Z.

       -T[device], --troff-device[=device]
              This option is used to change groff (or possibly troff's) output to be suitable for a device other than
              the  default.   It  implies -t.  Examples (provided with Groff-1.17) include dvi, latin1, ps, utf8, X75
              and X100.

       -H[browser], --html[=browser]
              This option will cause groff to produce HTML output, and will display that output  in  a  web  browser.
              The  choice  of  browser  is  determined  by  the  optional browser argument if one is provided, by the
              $BROWSER environment variable, or by a compile-time default if that is unset (usually lynx).  This  op‐
              tion implies -t, and will only work with GNU troff.

       -X[dpi], --gxditview[=dpi]
              This  option  displays  the output of groff in a graphical window using the gxditview program.  The dpi
              (dots per inch) may be 75, 75-12, 100, or 100-12, defaulting to 75; the -12  variants  use  a  12-point
              base font.  This option implies -T with the X75, X75-12, X100, or X100-12 device respectively.

       -Z, --ditroff
              groff will run troff and then use an appropriate post-processor to produce output suitable for the cho‐
              sen device.  If groff -mandoc is groff, this option is passed to groff and will suppress the use  of  a
              post-processor.  It implies -t.

   Getting help
       -?, --help
              Print a help message and exit.

       --usage
              Print a short usage message and exit.

       -V, --version
              Display version information.

EXIT STATUS
       0      Successful program execution.

       1      Usage, syntax or configuration file error.

       2      Operational error.

       3      A child process returned a non-zero exit status.

       16     At least one of the pages/files/keywords didn't exist or wasn't matched.

ENVIRONMENT
       MANPATH
              If $MANPATH is set, its value is used as the path to search for manual pages.

       MANROFFOPT
              Every  time  man invokes the formatter (nroff, troff, or groff), it adds the contents of $MANROFFOPT to
              the formatter's command line.

       MANROFFSEQ
              If $MANROFFSEQ is set, its value is used to determine the set of preprocessors to pass each manual page
              through.  The default preprocessor list is system dependent.

       MANSECT
              If  $MANSECT  is set, its value is a colon-delimited list of sections and it is used to determine which
              manual sections to search and in what order.  The default is "1 n l 8 3 2 3posix 3pm 3perl 3am 5 4 9  6
              7", unless overridden by the SECTION directive in /etc/manpath.config.

       MANPAGER, PAGER
              If  $MANPAGER  or $PAGER is set ($MANPAGER is used in preference), its value is used as the name of the
              program used to display the manual page.  By default, pager is used, falling back to cat  if  pager  is
              not found or is not executable.

              The  value  may  be a simple command name or a command with arguments, and may use shell quoting (back‐
              slashes, single quotes, or double quotes).  It may not use pipes to connect multiple commands;  if  you
              need  that,  use a wrapper script, which may take the file to display either as an argument or on stan‐
              dard input.

       MANLESS
              If $MANLESS is set, its value will be used as the default prompt string for the less pager,  as  if  it
              had  been  passed  using  the -r option (so any occurrences of the text $MAN_PN will be expanded in the
              same way).  For example, if you want to set the prompt string unconditionally to  “my  prompt  string”,
              set $MANLESS to ‘-Psmy prompt string’.  Using the -r option overrides this environment variable.

       BROWSER
              If  $BROWSER  is set, its value is a colon-delimited list of commands, each of which in turn is used to
              try to start a web browser for man --html.  In each command, %s is replaced by  a  filename  containing
              the  HTML output from groff, %% is replaced by a single percent sign (%), and %c is replaced by a colon
              (:).

       SYSTEM If $SYSTEM is set, it will have the same effect as if it had been specified as the argument to  the  -m
              option.

       MANOPT If  $MANOPT  is  set,  it will be parsed prior to man's command line and is expected to be in a similar
              format.  As all of the other man specific environment variables can be expressed as  command  line  op‐
              tions, and are thus candidates for being included in $MANOPT it is expected that they will become obso‐
              lete.  N.B.  All spaces that should be interpreted as part of an option's argument must be escaped.

       MANWIDTH
              If $MANWIDTH is set, its value is used as the line length for which manual pages should  be  formatted.
              If it is not set, manual pages will be formatted with a line length appropriate to the current terminal
              (using the value of $COLUMNS, and ioctl(2) if available, or falling back to 80 characters if neither is
              available).   Cat  pages  will  only be saved when the default formatting can be used, that is when the
              terminal line length is between 66 and 80 characters.

       MAN_KEEP_FORMATTING
              Normally, when output is not being directed to a terminal (such as to a file  or  a  pipe),  formatting
              characters  are  discarded  to  make  it  easier to read the result without special tools.  However, if
              $MAN_KEEP_FORMATTING is set to any non-empty value, these formatting characters are retained.  This may
              be useful for wrappers around man that can interpret formatting characters.

       MAN_KEEP_STDERR
              Normally,  when  output is being directed to a terminal (usually to a pager), any error output from the
              command used to produce formatted versions of manual pages is discarded to avoid interfering  with  the
              pager's display.  Programs such as groff often produce relatively minor error messages about typograph‐
              ical problems such as poor alignment, which are unsightly and generally confusing when displayed  along
              with  the  manual page.  However, some users want to see them anyway, so, if $MAN_KEEP_STDERR is set to
              any non-empty value, error output will be displayed as usual.

       LANG, LC_MESSAGES
              Depending on system and implementation, either or both of $LANG and $LC_MESSAGES will  be  interrogated
              for the current message locale.  man will display its messages in that locale (if available).  See set‐
              locale(3) for precise details.

FILES
       /etc/manpath.config
              man-db configuration file.

       /usr/share/man
              A global manual page hierarchy.

SEE ALSO
       apropos(1), groff(1), less(1), manpath(1), nroff(1), troff(1), whatis(1), zsoelim(1), manpath(5), man(7), cat‐
       man(8), mandb(8)

       Documentation for some packages may be available in other formats, such as info(1) or HTML.

HISTORY
       1990, 1991 – Originally written by John W. Eaton (jwe@che.utexas.edu).

       Dec   23   1992:   Rik   Faith   (faith@cs.unc.edu)   applied   bug   fixes   supplied   by   Willem   Kasdorp
       (wkasdo@nikhefk.nikef.nl).

       30th April 1994 – 23rd February 2000: Wilf. (G.Wilford@ee.surrey.ac.uk) has been  developing  and  maintaining
       this package with the help of a few dedicated people.

       30th October 1996 – 30th March 2001: Fabrizio Polacco <fpolacco@debian.org> maintained and enhanced this pack‐
       age for the Debian project, with the help of all the community.

       31st March 2001 – present day: Colin Watson <cjwatson@debian.org> is now developing and maintaining man-db.

2.9.1                                                 2020-02-25                                               MAN(1)
```