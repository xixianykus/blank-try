---
title: "Laptop Notes"
date: 2022-09-26T07:10:16+01:00
draft: false
summary: Some brief notes on getting a new laptop.
tags: [intel, laptops]
---

- [Specs](#specs)
  - [screen and graphics](#screen-and-graphics)
  - [Minimal specs](#minimal-specs)
- [Factors to consider when buying used](#factors-to-consider-when-buying-used)
- [Other related info](#other-related-info)
  - [SSD prices](#ssd-prices)
  - [Intel CPU's](#intel-cpus)
- [intel Core CPU suffix meanings](#intel-core-cpu-suffix-meanings)
- [Add on costs](#add-on-costs)
- [Useful Links](#useful-links)

Most prob use will be plugged in so battery is not so important.

## Specs

Generally these are [about](https://www.cclonline.com/laptops/core-i5-laptops/pricerange/400-500/attributerange/26/15-17/ "CCL Online page") £400 - £500 new (though IPS is likely to cost more).

**Ebuyer**: has an i5 FHD, 8GB, 256SSD, Win 11 Home for £350. (Model Medion Akoya)

<!-- 1. 8Gb RAM or more
2. 15.6 inch screen or even 17 inch.
3. Resolution ? 1366x758 vs 1920x1080.
4. display-tech: IPS
5. Battery (replaceable)
6. SSD - or upgradable, prob min of 500Gb
7. CPU ?? core i5 or AMD
8. bluetooth for external speaker
9. SD card slot for photos from camera
10. screen - scratched. -->


1. **RAM** 8GB seems like a must. But a laptop with 4GBs could be OK if it has a spare memory slot to upgrade.
2. **Screen size** 17 inch screens are rare so 15.6 inch is gonna have to be it.
3. **Resolution** 1920x1080 would be nice and more future proof. But more expensive. Might be possible to put a higher res screen in for around 45 - 60 quid, esp if I want an IPS one. But as I won't be using it that much it's probably not worth the cost and hassle.
4. **IPS** Unfortunately these seem hard to come across so prob have to accept a TN one.
5. **Battery** These seem cheap to buy as needed - considered consumable items, so not worth worrying about.
6. **SSD** Really a must these days but a SATA 3 connection is required, ruling out really old laptops. Size wise 500GBs for £35 should be fine or 1TB for an extra £18 is good value but probably not needed in the laptop. If I got a new one for my desktop could use the old one out of that (240GBs).
7. **CPU** Intel are far more common than AMD. Minimum should be a Core i3 but ideally an i7 would be better. Probably not older than 3rd gen either. Should look at what the newer generations offer.
8. **Bluetooth** Not sure how important it is to get recent version. Needed for the external speaker and that could even be connected by cable if needs be.
9. **SD Card slot** check, but this seems standard on most.
10. **CD ROM** not needed but the presence indicates room and connection for another HDD or SSD.
11. **Screen** - check condition. A badly scratched one would be a bit shit.


Also *graphics*: separate card, or uses the cpu?

### screen and graphics

**Resolution:** either 1280x720 (HD) 1366x768 or 1920x1080 (FHD). The latter implies better graphics w more memory. Old laptop would have been 1366.

**Display tech:** IPS or TFT.

**Graphics:** built in or separate graphics card?

> notebooks can run with either integrated graphics – that means a GPU (Graphics Processing Unit) built into the processor – or discrete graphics, which denotes that the graphics card is a separate entity to the CPU, and therefore bigger and more powerful.
>
>As with a desktop PC, if you want a gaming laptop, it should ideally run with a discrete GPU (although integrated graphics are getting more powerful these days, and can produce solid results with many games – more on that later). If you use your machine for just general computing and browsing the web, then you’ll get by with an integrated GPU just fine. [^1]


### Minimal specs

1. SATA 3 connector plus 1 SATA 2
2. i3 or i5 processor, pref 3rd gen or above
3. 8GB RAM
4. 1366x768 res, preferably 1920x1080
5. 3 x USB (one mouse, one keyboard, one pen drive)


## Factors to consider when buying used

1. Knackered battery
2. Looking shoddy
3. Poor keyboard
4. Worn out SSD / HDD
5. Warranty


## Other related info

### SSD prices

Could get a new SSD if it as an HDD or a low capacity SSD.

Cost at ebuyer are about £35 for 500GB and from £57 for 1TB.

If it has an optical drive could use for a spare HDD (depending on mobo).

### Intel CPU's

The first number or two is the CPU generation. So Core i5 3550 would be 3rd generation. Last 3 digits are the SKU, roughly represent the order they were developed and the higher the number the more features it will have.

Core suffix meanings:

- C: Desktop processor with high-end graphics
- F: High-performance processor used with discrete graphics cards (ex. Gaming)
- H: High-performance graphics
- K: Unlocked for overclocking
- **M: Mobile**
- Q: Quad-core
- R: Desktop processor, BGA1364 (mobile) package, high-end graphics
- S: Performance-optimized lifestyle
- T: Power-optimized for best desktop computing
- **U: Ultra-low power for laptop efficiency**
- X: Extreme unlocked for high desktop performance
- Y: Extreme low power

From [HP.com](https://www.hp.com/us-en/shop/tech-takes/intel-processor-guide)

## intel Core CPU suffix meanings

The suffix can indicate big differences such as twice as many cores.

See [Wikipedia page](https://en.wikipedia.org/wiki/Intel_Core) for more details and understanding as the precise meaning differs from one generation to the next.

| Suffix | Meaning                                                                                     | Notes                                  |
| :----- | :------------------------------------------------------------------------------------------ | :------------------------------------- |
| G1-G7  | Graphics level (processors with new integrated graphics technology only, higher is better.) |                                        |
| E      | Embedded                                                                                    |                                        |
| F      | Requires discrete graphics                                                                  |                                        |
| G      | Includes discrete graphics on package                                                       |                                        |
| H      | High performance optimized for mobile                                                       | Best mobile with typically more cores. |
| HK     | High performance optimized for mobile, unlocked                                             |                                        |
| HQ     | High performance optimized for mobile, quad core                                            | Many mobile CPU's are only duel core   |
| K      | Unlocked                                                                                    |                                        |
| S      | Special edition                                                                             |                                        |
| T      | Power-optimized lifestyle                                                                   |                                        |
| U      | Mobile power efficient                                                                      |                                        |
| Y      | Mobile extremely low power                                                                  |                                        |
| X/XE   | Unlocked, High End                                                                          |                                        |
| B      | Ball Grid Array (BGA)                                                                       |                                        |

From [intel.com](https://www.intel.com/content/www/us/en/processors/processor-numbers.html)


## Add on costs

1. Charger £10 - £15
2. Battery £11 - £15
3. Screen - Brand new 1920x1080 15.6 inch cost £56, OR £62 for an IPS FHD


## Useful Links

1. [Laptop Media](https://laptopmedia.com/) has reviews of many of the laptops.
2. [Intel Product Specs](https://ark.intel.com/content/www/us/en/ark.html#@Processors) view details on any CPU
3. [Wikipedia Intel Core page](https://en.wikipedia.org/wiki/Intel_Core) Look up specs of specific CPU's
4. [Notebook Check](https://www.notebookcheck.net/) useful info such as AMD processor equivalents.


[^1]: From [Techradar](https://www.techradar.com/news/laptop-graphics-cards-explained)