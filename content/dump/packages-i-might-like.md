---
title: "Packages I Might Like"
date: 2022-09-29T21:17:15+01:00
draft: false
summary: Packages from Chocolatey community repo that I might wanna try sometime
tags: [chocolatey]
---

[Chocolatey community Package Repository](https://chocolatey.org/packages)


1. [Nordpass](https://nordpass.com/) password manager. There's also a [ton of other password managers](https://community.chocolatey.org/packages?q=tag%3Apassword).
2. [Gnucash](https://gnucash.org/) financial accounting software[^1] for personal and small business. Very mature and available for Linux, MacOS and Windows
3. [Homebank](http://homebank.free.fr/en/)
4. [Google Earth Pro](https://community.chocolatey.org/packages/googleearthpro) -- hmmm: Google Earth Pro on desktop is free for users with advanced feature needs. Import and export GIS data, and go back in time with historical imagery.

   Google Earth lets you fly anywhere on Earth to view satellite imagery, maps, terrain, 3D buildings, from galaxies in outer space to the canyons of the ocean.
5. [Minify](https://community.chocolatey.org/packages/minify) provides HTML5, CSS3, JS, JSON, SVG and XML minification
6. [ImageMajick](https://imagemagick.org/) create, edit, compose, or convert digital images. It can read and write images in a variety of formats (over 200) including PNG, JPEG, GIF, WebP, HEIC, SVG, PDF, DPX, EXR and TIFF. ImageMagick can resize, flip, mirror, rotate, distort, shear and transform images, adjust image colors, apply various special effects, or draw text, lines, polygons, ellipses and Bézier curves.
7. [Only Office](https://personal.onlyoffice.com/en-GB) cloud based office software. Over 10 years old.
8. NodeJS LTS
9. A [softphone](https://community.chocolatey.org/packages?q=tag%3Asoftphone) of some sort
10. [Betterbird](https://www.betterbird.eu/) improved Thunderbird
11. [ElectronMail](https://community.chocolatey.org/packages/electron-mail) is an Electron-based unofficial desktop client for ProtonMail. The app aims to provide enhanced desktop user experience enabling features that are not supported by the official in-browser web clients.
12. [gyazo](https://gyazo.com/) Screen caputre and share app, using free cloud storage. Paid service allows more browsing and searching. 
   > After you capture we upload and copy a unique link that's private but ready to paste & share instantly."
13. [CCleaner](https://community.chocolatey.org/packages/ccleaner)
14. [Progressive Web Apps for Firefox](https://community.chocolatey.org/packages/firefoxpwa) - A tool to install, manage and use Progressive Web Apps (PWAs) in Mozilla Firefox (native component)
15. [ToDo list](https://community.chocolatey.org/packages?q=tag%3Atodo) - there's a choice of about 7.
16. [ExifTool](https://exiftool.org/)[^2] CLI tool to edit EXIF and other metadata.
17. [Metadata++](https://logipole.com/metadata++-en.htm) a powerful yet easy to use freeware tool to view, add, edit, modify, extract, copy metadata of various files formats (images, videos, texts, audios). Not a CLI tool.
18. [Calibre](https://calibre-ebook.com/) always needs updating.
19. [Cyberduck](https://cyberduck.io/) is a libre FTP, SFTP, WebDAV, S3, Backblaze B2, Azure & OpenStack Swift browser
20. [Responsively App](https://responsively.app/) - A must-have DevTool for all Front-End developers that will make your job easier.
21. [Haroopad](http://pad.haroopress.com) edit in markdown and export to various web formats like PDF and HTML. Looks interesting though much of the documentation is in Japanese (or summat).
22. [Emacs](https://www.gnu.org/software/emacs/tour/index.html) text editor that servers many purposes such as news reader, mail, RSS.
23. [CrystalDiskInfo](https://community.chocolatey.org/packages/crystaldiskinfo.install) [portable](https://community.chocolatey.org/packages/crystaldiskinfo.portable) is a HDD/SSD utility software which shows the health status much more clearly than similar tools.
24. [CLI text editors](https://community.chocolatey.org/packages?q=tag%3Anano) Nano or Micro
25. [Spotube](https://spotube.netlify.app/) A lightweight free Spotify 🎧 crossplatform-client 🖥📱 which handles playback manually, streams music using Youtube & no Spotify premium. Open Source, privacy respecting & No ads
26. Produkey Recover lost product key (CD-Key) of Windows/MS-Office/SQL Server. Useful for a new machine.
27. [Windows Repair](https://community.chocolatey.org/packages/windowsrepair.install) All-in-one repair tool to help fix known Windows issues
28. QuiteRSS or 
    - [another RSS reader](https://community.chocolatey.org/packages?q=tag%3Arss)
    - [TuiFeed](https://github.com/veeso/tuifeed) a CLI feed reader
29. [Power ISO](http://www.poweriso.com/) for burning CDs, DVDs, BDs. 



Others to update: Tor (automatic), qBittorrent, pandoc

[^1]: [Alternatives](https://community.chocolatey.org/packages?q=tag%3Aaccounting)
[^2]: Note: If exiftool.org goes down, it is because of the crappy DreamHost web hosting which disables an "unlimited traffic" web site if a single bot hammers the site with a moderate load. An alternate ExifTool homepage is available at http://exiftool.sourceforge.net/