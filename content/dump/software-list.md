---
title: "Software List"
date: 2022-09-28T13:55:14+01:00
draft: true
summary: About winget, Windows Package Manager and a snapshot of my software.
tags: [winget, chocolatey, Windows, package managers]
---


Windows App Installer or package manager comes with later versions of Windows 10 and Windows 11.

Use `winget -v` to check you have it and see which version. It will update as part of Windows updates.

What's good about it is that it can show ALL installed programs on the system and update many of them, even if they weren't installed with winget.

```bash
winget list
```

This will show all installed apps along with the version, whether there is a newer version and the source (if it says winget you can update the apps from there).

To see just the outdated apps:

```bash
winget upgrade
```

To update an app use:

```bash
winget upgrade appname
winget upgrade "my lovely app"
```

## winget --help

Package Manager v1.3.2091  
Copyright (c) Microsoft Corporation. All rights reserved.

The winget command line utility enables installing applications and other packages from the command line.

usage: winget [<command>] [<options>]

The following commands are available:

| Command   | Description                                 |
| :-------- | :------------------------------------------ |
| install   | Installs the given package                  |
| show      | Shows information about a package           |
| source    | Manage sources of packages                  |
| search    | Find and show basic info of packages        |
| list      | Display installed packages                  |
| upgrade   | Shows and performs available upgrades       |
| uninstall | Uninstalls the given package                |
| hash      | Helper to hash installer files              |
| validate  | Validates a manifest file                   |
| settings  | Open settings or set administrator settings |
| features  | Shows the status of experimental features   |
| export    | Exports a list of the installed packages    |
| import    | Installs all the packages in a file         |

For more details on a specific command, pass it the help argument. [-?]

The following options are available:
  -v,--version  Display the version of the tool
  --info        Display general info of the tool

More help can be found at: https://aka.ms/winget-command-help


The **export** command creates a JSON file that can be used on another machine to import all the packages there.


## Installing Online

You can go to [winstall.app](https://winstall.app/) and install packages from there.


## Chocolatey

Installations are listed in the post-install message. For example:

```bash
The install of keepass was successful.
  Software installed to 'C:\ProgramData\chocolatey\lib\keepass'
```

NB `ProgramData` maybe a hidden folder.


## Video

Video by TechGuru

{{< yt y2PbdOueUNQ >}}


Using `winget list >> file.txt`
  
| Name                                    | Id                                       | Version             | Available   | Source |
| :-------------------------------------- | :--------------------------------------- | :------------------ | :---------- | :----- |
| 7-Zip 22.01 (x64)                       | 7zip.7zip                                | 22.01               |             | winget |
| AMD Software                            | AMD Catalyst Install Manager             | 21.4.1              |             |        |
| Adobe Creative Cloud                    | Adobe Creative Cloud                     | 3.9.0.327           |             |        |
| Brave                                   | BraveSoftware.BraveBrowser               | 106.1.44.101        |             | winget |
| Ubuntu on Windows                       | Canonical.Ubuntu                         | 2004.2022.1.0       |             | winget |
| EdgeDeflector                           | EdgeDeflector                            | Unknown             |             |        |
| FileZilla Client 3.56.2                 | FileZilla Client                         | 3.56.2              |             |        |
| f.lux                                   | flux.flux                                | Unknown             | 4.120       | winget |
| Free Video Volume Booster v3.0          | Free Video Volume Booster_is1            | 3.0.0.0             |             |        |
| GPL Ghostscript                         | ArtifexSoftware.GhostScript              | 9.50                | 10.00.0     | winget |
| Gajim 1.4.5 (64-Bit)                    | Gajim.Gajim                              | 1.4.5               |             | winget |
| Git                                     | Git.Git                                  | 2.37.2.2            | 2.37.3      | winget |
| GlassWire 2.1 (remove only)             | GlassWire 2.1                            | 2.1.152             |             |        |
| Google Chrome                           | Google.Chrome                            | 105.0.5195.127      |             | winget |
| Gramblr                                 | Gramblr                                  | 2.9.209             |             |        |
| Adobe Illustrator CC 2017               | ILST_21_0_0                              | 21.0.0              |             |        |
| ImgBurn                                 | LIGHTNINGUK.ImgBurn                      | 2.5.8.0             |             | winget |
| VIA Platform Device Manager             | InstallShield_{20D4A895-748C-4D88-871C-… | 1.43                |             |        |
| IrfanView 4.50 (32-bit)                 | IrfanSkiljan.IrfanView                   | 4.50                | 4.60        | winget |
| IrfanView 4.57 (64-bit)                 | IrfanSkiljan.IrfanView                   | 4.57                | 4.60        | winget |
| Adobe Lightroom Classic CC              | LTRM_8_2_1                               | 8.2.1               |             |        |
| MediaMonkey 5                           | MediaMonkey 5_is1                        | 5                   |             |        |
| MediaMonkey 4.1                         | VentisMedia.MediaMonkey                  | 4.1                 | 5           | winget |
| Microsoft Edge                          | Microsoft.Edge                           | 105.0.1343.53       |             | winget |
| Microsoft Edge Update                   | Microsoft Edge Update                    | 1.3.167.21          |             |        |
| Microsoft Edge WebView2 Runtime         | Microsoft.EdgeWebView2Runtime            | 105.0.1343.50       |             | winget |
| Microsoft Mouse and Keyboard Center     | Microsoft.MouseandKeyboardCenter         | 13.222.137.0        | 14.41.137.0 | winget |
| Cortana                                 | Microsoft.549981C3F5F10_8wekyb3d8bbwe    | 4.2204.13303.0      |             |        |
| MSN Weather                             | Microsoft.BingWeather_8wekyb3d8bbwe      | 4.53.42682.0        |             |        |
| App Installer                           | Microsoft.DesktopAppInstaller_8wekyb3d8… | 1.18.2091.0         |             |        |
| Get Help                                | Microsoft.GetHelp_8wekyb3d8bbwe          | 10.2206.2011.0      |             |        |
| Microsoft Tips                          | Microsoft.Getstarted_8wekyb3d8bbwe       | 10.2206.2.0         |             |        |
| HEIF Image Extensions                   | Microsoft.HEIFImageExtension_8wekyb3d8b… | 1.0.43012.0         |             |        |
| English (United Kingdom) Local Experie… | Microsoft.LanguageExperiencePacken-GB_8… | 19041.39.123.0      |             |        |
| Paint 3D                                | Microsoft.MSPaint_8wekyb3d8bbwe          | 6.2203.1037.0       |             |        |
| 3D Viewer                               | Microsoft.Microsoft3DViewer_8wekyb3d8bb… | 7.2107.7012.0       |             |        |
| Microsoft Edge                          | Microsoft.MicrosoftEdge.Stable_8wekyb3d… | 105.0.1343.53       |             |        |
| Microsoft Sticky Notes                  | Microsoft.MicrosoftStickyNotes_8wekyb3d… | 4.5.6.0             |             |        |
| Mixed Reality Portal                    | Microsoft.MixedReality.Portal_8wekyb3d8… | 2000.21051.1282.0   |             |        |
| OneNote for Windows 10                  | Microsoft.Office.OneNote_8wekyb3d8bbwe   | 16001.14326.21138.0 |             |        |
| Microsoft People                        | Microsoft.People_8wekyb3d8bbwe           | 10.2105.4.0         |             |        |
| Print 3D                                | Microsoft.Print3D_8wekyb3d8bbwe          | 3.3.791.0           |             |        |
| Snip & Sketch                           | Microsoft.ScreenSketch_8wekyb3d8bbwe     | 10.2008.2277.0      |             |        |
| Store Experience Host                   | Microsoft.StorePurchaseApp_8wekyb3d8bbwe | 12203.44.0.0        |             |        |
| VP9 Video Extensions                    | Microsoft.VP9VideoExtensions_8wekyb3d8b… | 1.0.51171.0         |             |        |
| Microsoft Pay                           | Microsoft.Wallet_8wekyb3d8bbwe           | 2.4.18324.0         |             |        |
| Web Media Extensions                    | Microsoft.WebMediaExtensions_8wekyb3d8b… | 1.0.42192.0         |             |        |
| Webp Image Extensions                   | Microsoft.WebpImageExtension_8wekyb3d8b… | 1.0.52351.0         |             |        |
| Microsoft Whiteboard                    | Microsoft.Whiteboard_8wekyb3d8bbwe       | 52.10801.429.0      |             |        |
| Microsoft Photos                        | Microsoft.Windows.Photos_8wekyb3d8bbwe   | 2022.30070.26007.0  |             |        |
| Windows Clock                           | Microsoft.WindowsAlarms_8wekyb3d8bbwe    | 11.2208.5.0         |             |        |
| Windows Calculator                      | Microsoft.WindowsCalculator_8wekyb3d8bb… | 10.2103.8.0         |             |        |
| Windows Camera                          | Microsoft.WindowsCamera_8wekyb3d8bbwe    | 2021.105.10.0       |             |        |
| Feedback Hub                            | Microsoft.WindowsFeedbackHub_8wekyb3d8b… | 1.2203.761.0        |             |        |
| Windows Maps                            | Microsoft.WindowsMaps_8wekyb3d8bbwe      | 11.2208.6.0         |             |        |
| Windows Voice Recorder                  | Microsoft.WindowsSoundRecorder_8wekyb3d… | 10.2103.28.0        |             |        |
| Microsoft Store                         | Microsoft.WindowsStore_8wekyb3d8bbwe     | 22207.1401.9.0      |             |        |
| Windows Terminal                        | Microsoft.WindowsTerminal                | 1.14.2281.0         | 1.15.2524.0 | winget |
| Windows Package Manager Source (winget) | Microsoft.Winget.Source_8wekyb3d8bbwe    | 2022.927.2438.109   |             |        |
| Phone Link                              | Microsoft.YourPhone_8wekyb3d8bbwe        | 1.22072.207.0       |             |        |
| Films & TV                              | Microsoft.ZuneVideo_8wekyb3d8bbwe        | 10.22041.10091.0    |             |        |
| Mozilla Firefox (x64 en-GB)             | Mozilla Firefox 105.0.1 (x64 en-GB)      | 105.0.1             |             |        |
| Mozilla Thunderbird (x86 en-US)         | Mozilla.Thunderbird                      | 91.13.1             | 102.3.0     | winget |
| Mozilla Maintenance Service             | MozillaMaintenanceService                | 56.0.2              |             |        |
| Nik Collection                          | Nik Collection                           | 2.3.0               |             |        |
| NoteTab 7 (Remove only)                 | NoteTab 7_is1                            | 7.1                 |             |        |
| Adobe Photoshop CC 2019                 | PHSP_20_0_4                              | 20.0.4              |             |        |
| Postman x86_64 9.13.0                   | Postman.Postman                          | 9.13.0              | 9.29.0      | winget |
| ProtonVPN                               | ProtonTechnologies.ProtonVPN             | 1.25.2              | 2.0.6       | winget |
| Topaz Adjust 5                          | Topaz Adjust 5                           | 5.1.0               |             |        |
| Topaz B&W Effects                       | Topaz BW Effects 2                       | 2.1.0               |             |        |
| Topaz Clarity                           | Topaz Clarity                            | 1.0.0               |             |        |
| Topaz Clean 3                           | Topaz Clean 3                            | 3.1.0               |             |        |
| Topaz DeJpeg 4                          | Topaz DeJpeg 4                           | 4.0.2               |             |        |
| Topaz DeNoise 6                         | Topaz DeNoise 6                          | 6.0.1               |             |        |
| Topaz Detail 3                          | Topaz Detail 3                           | 3.2.0               |             |        |
| Topaz Fusion Express 2                  | Topaz Fusion Express 2                   | 2.1.3               |             |        |
| Topaz InFocus                           | Topaz InFocus                            | 1.0.0               |             |        |
| Topaz Lens Effects                      | Topaz Lens Effects                       | 1.2.0               |             |        |
| Topaz ReMask 5                          | Topaz ReMask 5                           | 5.0.1               |             |        |
| Topaz ReStyle                           | Topaz ReStyle                            | 1.0.0               |             |        |
| Topaz Simplify 4                        | Topaz Simplify 4                         | 4.1.1               |             |        |
| Topaz Star Effects                      | Topaz Star Effects                       | 1.1.0               |             |        |
| Universal Extractor 1.6.1               | jbreland.uniextract                      | 1.6.1               |             | winget |
| VLC media player                        | VLC media player                         | 3.0.16              |             |        |
| Obsidian                                | Obsidian.Obsidian                        | 0.15.9              |             | winget |
| FontBase 2.17.5                         | Levitsky.FontBase                        | 2.17.5              |             | winget |
| get_iplayer 3.21.0                      | get_iplayer_is1                          | 3.21.0              |             |        |
| Mail and Calendar                       | microsoft.windowscommunicationsapps_8we… | 16005.14326.20970.0 |             |        |
| photoFXlab                              | photoFXlab                               | 1.2.10              |             |        |
| qBittorrent 4.4.5                       | qBittorrent.qBittorrent                  | 4.4.5               |             | winget |
| Lunacy                                  | Icons8.Lunacy                            | 8.6.2               |             | winget |
| calibre 64bit                           | calibre.calibre                          | 6.3.0               | 6.5.0       | winget |
| Microsoft Visual C++ 2013 Redistributa… | Microsoft.VC++2013Redist-x64             | 12.0.30501.0        | 12.0.40664… | winget |
| Microsoft Visual C++ 2005 Redistributa… | {071c9b48-7c32-4621-a0ac-3f809523288f}   | 8.0.56336           |             |        |
| Go Programming Language amd64 go1.19.1  | GoLang.Go.1.19                           | 1.19.1              |             | winget |
| PuTTY release 0.74 (64-bit)             | PuTTY.PuTTY                              | 0.74.0.0            | 0.77.0.0    | winget |
| Microsoft Visual C++ 2010  x64 Redistr… | Microsoft.VC++2010Redist-x64             | 10.0.40219          |             | winget |
| Microsoft Visual C++ 2008 Redistributa… | {1F1C2DFC-2D24-3E06-BCB8-725134ADF989}   | 9.0.30729.4148      |             |        |
| Apowersoft Online Launcher version 1.8… | {20BF67A8-D81A-4489-8225-FABAA0896E2D}_… | 1.8.1               |             |        |
| MPC-HC 1.7.13 (64-bit)                  | clsid2.mpc-hc                            | 1.7.13              | 1.9.23      | winget |
| DxO PhotoLab 2 plug-in for Adobe Light… | {2E2FA6EE-39A0-4022-B125-DD0036195E46}   | 1.0.46              |             |        |
| LucisArt 3.0.5 ED/SE (64-bit)           | {2FD27DF3-3BEF-48A1-8A87-378DA085E472}   | 3.0.5.0             |             |        |
| Microsoft Visual C++ 2012 Redistributa… | Microsoft.VC++2012Redist-x86             | 11.0.61030.0        |             | winget |
| Microsoft Visual C++ 2015-2022 Redistr… | {3746f21b-c990-4045-bb33-1cf98cff7a68}   | 14.32.31332.0       |             |        |
| Python 3.8.0 (32-bit)                   | {37ec7371-0827-49f1-be8a-63c158184b9c}   | 3.8.150.0           |             |        |
| PowerShell 7-x64                        | Microsoft.PowerShell                     | 7.2.6.0             |             | winget |
| Microsoft Visual C++ 2008 Redistributa… | {4B6C7001-C7D6-3710-913E-5BC23FCE91E6}   | 9.0.30729.4148      |             |        |
| OpenShot Video Editor version 2.5.1     | OpenShot.OpenShot                        | 2.5.1               | 2.6.1       | winget |
| ProtonVPNTap                            | {5DA710E2-1B81-4675-BFC5-76BAF63AE1F6}   | 1.1.3               |             |        |
| Microsoft Visual C++ 2008 Redistributa… | {5FCE6D76-F5DC-37AB-B2B8-22AB8CEDB1D4}   | 9.0.30729.6161      |             |        |
| Microsoft Visual C++ 2013 Redistributa… | Microsoft.VC++2013Redist-x86             | 12.0.40660.0        | 12.0.40664… | winget |
| pCloud Drive                            | {6232a5bd-25fa-42e8-baee-a284e25b1734}   | 3.11.13.0           |             |        |
| Windows PC Health Check                 | Microsoft.WindowsPCHealthCheck           | 3.6.2204.08001      |             | winget |
| Microsoft Visual C++ 2005 Redistributa… | Microsoft.VCRedist.2005.x86              | 8.0.61001           |             | winget |
| Microsoft Visual C++ 2005 Redistributa… | {7299052b-02a4-4627-81f2-1818da5d550d}   | 8.0.56336           |             |        |
| Microsoft Visual Studio Code (User)     | Microsoft.VisualStudioCode               | 1.71.2              |             | winget |
| Microsoft Update Health Tools           | {7B1FCD52-8F6B-4F12-A143-361EA39F5E7C}   | 3.67.0.0            |             |        |
| Python Launcher                         | {7DBA9B7D-924F-4CE8-8AE8-65977EF62744}   | 3.8.6860.0          |             |        |
| Brother iPrint&Scan                     | {80e3f154-59fa-491e-911b-98caf1c71120}   | 3.0.0.10            |             |        |
| Microsoft Visual C++ 2008 Redistributa… | {8220EEFE-38CD-377E-8595-13398D740ACE}   | 9.0.30729           |             |        |
| Node.js                                 | OpenJS.NodeJS                            | 18.9.0              | 18.9.1      | winget |
| Microsoft Visual C++ 2008 Redistributa… | {9A25302D-30C0-39D9-BD6F-21E6EC160475}   | 9.0.30729           |             |        |
| Microsoft Visual C++ 2008 Redistributa… | {9BE518E6-ECC6-35A9-88E4-87755C07200F}   | 9.0.30729.6161      |             |        |
| PDF-Viewer                              | {A278382D-4F1B-4D47-9885-8523F7261E8D}_… | 2.5.322.10          |             |        |
| Update for Windows 10 for x64-based Sy… | {B2E25355-C24E-4E7D-8AD3-455D59810838}   | 2.57.0.0            |             |        |
| ADATA SSD ToolBox version 3.0.3         | {C0991D3E-8786-48E7-A5DB-57FBACB0A03A}_… | 3.0.3               |             |        |
| ProtonVPNTun                            | {C953D354-0C14-4CB5-AB42-0A9E40F55857}   | 0.13.0              |             |        |
| LibreOffice 6.0.4.2                     | TheDocumentFoundation.LibreOffice        | 6.0.4.2             | 7.4.1.2     | winget |
| DxO PhotoLab 2                          | {E9CD9A75-02FC-4921-83A6-F57C892C2250}   | 2.3.2               |             |        |
| Microsoft Visual C++ 2010  x86 Redistr… | Microsoft.VC++2010Redist-x86             | 10.0.40219          |             | winget |
| Windows Subsystem for Linux Update      | {F8474A47-8B5D-4466-ACE3-78EAB3BF21A8}   | 5.10.102.1          |             |        |
| Pandoc 2.9.2.1                          | JohnMacFarlane.Pandoc                    | 2.9.2.1             | 2.19.2      | winget |
| Microsoft Visual C++ 2015-2022 Redistr… | {a98dc6ff-d360-4878-9f0a-915eba86eaf3}   | 14.32.31332.0       |             |        |
| Microsoft Visual C++ 2005 Redistributa… | {ad8a2fa1-06e7-4b0d-927d-6e54b3d31028}   | 8.0.61000           |             |        |
| Microsoft Visual C++ 2012 Redistributa… | Microsoft.VC++2012Redist-x64             | 11.0.61030.0        |             | winget |
| Microsoft Visual C++ 2013 Redistributa… | Microsoft.VC++2013Redist-x64             | 12.0.40660.0        | 12.0.40664… | winget |
| Microsoft Visual C++ 2013 Redistributa… | Microsoft.VC++2013Redist-x86             | 12.0.30501.0        | 12.0.40664… | winget | Windows |



