---
title: "Windows Chronology"
date: 2022-09-28T12:30:15+01:00
draft: true
summary: A list of every version of Windows and the year it came out. Also some stuff on Windows 11.
tags: [Windows]
---

|Windows Version | Release date|
|:--|:--|
| Windows 2 | 1987 |
| Windows 3 | 1990 |
| Windows 3.1 | March 1992 |
| Windows 95 | August 1995 |
| Windows 98 | June 1998 |
| Windows ME | Septmber 2000 |
| Windows XP | October 2001 |
| Windows Vista | November 2006 |
| Windows 7 | October 2009 |
| Windows 8 | October 2012 |
| Windows 10 | July 2015 |
| Windows 11 | October 2021 |


## Windows 11

Is not supported for machines older than iCore 8th generation CPU or equivalent. It is said these CPU mitigate issues of Meltdown and Spectre. However you can still install Windows 11 via a fresh install at you're own risk.

### Downsides

- Apps must be installed via MS Store
- Sluggish compared to Win 10

### Upsides

- WSL now supports desktop apps
- WSA or Windows Subsystem for Android