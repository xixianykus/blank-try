---
title: "Render Image"
date: 2022-08-26T19:50:28+01:00
type: nu
summary: How Hugo's render hooks work with images
tags: [Hugo]
---

![honbike](/img/honbike.avif "The Honbike is an electrically assisted belt drive bike from the US costing only $1600")


Above are the variables available in the `render-image.html` file from a markdown image.

```md
![honbike](/img/honbike.avif "The Honbike is an electrically assisted belt drive bike from the US costing only $1600")
```

As you can see above there are 3 in the image itself: `alt` value, the `src` value and the `title` value, each respectively grabbed using `.Text`, `.Destination` and `.Title` respectively.

Additionally you can get any of the current page's variables too.


## How it's done..

1. Create a folder in your `/layouts/_default/` folder called `_markup`. (NB if you only want to use this on certain pages create a new folder off of `/layouts/` and create the `_markup` folder there.)
2. Create a new file called `render-image.html`. All the code previously used for an image in markdown will be replaced with the code in this file. If there is no code then your images will disappear.
3. Add some code, at the very least an img tag with a variable of `{{ .Destination }}` for the image link.

If you created a new folder in step 1 for your `_markup` folder then to get this to work in the page you need to add `type: name-of-folder` in your frontmatter.




