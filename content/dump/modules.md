---
title: "Modules"
date: 2022-08-22T16:21:48+01:00
draft: false
summary: Adding to a Hugo theme with modules.
tags: [Hugo, Hugo modules]
---

To add something to a theme using Hugo modules you need to:

1. Make your site a module (this process just adds a couple of files: `go.mod` and `go.sum`) using `hugo mod init` [see gendoc]. 
2. Add some code, like that below, to your config file.

Next you can either:

1. Start Hugo server and see the content pulled from elsewhere
2. Build the site using `hugo` and pull the content into the build site.
3. Upload the site


```toml
[module]

[[module.imports]]
    path = "github.com/craftsmandigital/markdown-content-repo"
    disabled = false

[[module.imports.mounts]]
    source = "testing-hugo-modules"
    target = "content/new-stuff"
```

## Update

```bash
hugo mod get -u github.com/twbs/icons
```

## Fail

Trying to get some images from Mega.nz

```bash
$ hugo -D
hugo: collected modules in 960 ms
Error: module "https://mega.nz/folder/7JYihLTB#FclpuFHngNKPNyXnRul2_Q" not found; either add 
it as a Hugo Module or store it in "T:\\HTML-extras\\TRYOUTS\\hugo-test-1\\themes".: module does not exist
```

## Comment from Bep

> A related hint from me (which should get a more prominent place in the docs):
>
> Hugo uses a `/tmp dir` for the cache if cace dir is not set (in config.toml or environment)  
> Go downloads the modules and write protects all directories  
> On MacOS I have experienced some occasional issues which I guess relates to MacOS trying to clean out these directories, but only partly succeeds.  
> Setting `HUGO_CACHEDIR` os env variable to something outside `/tmp` is the solution to the above. It’s also in general a good thing.

## Links

1. [The New Dynamic](https://www.thenewdynamic.com/article/hugo-modules-everything-from-imports-to-create/)
2. [Hugo Modules for Dummies](https://discourse.gohugo.io/t/hugo-modules-for-dummies/20758) on the Hugo forums.
4. [Hugo docs on modules](https://gohugo.io/hugo-modules/)
3. [Hugo mod help page](/commands/hugo_mod/)

[see gendoc]: /commands/hugo_mod_init/