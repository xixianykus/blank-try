---
title: "What to Make"
date: 2022-09-16T22:53:05+01:00
draft: false
tags: [css, Kevin Powell, video]
summary: A list of places to find projects to work on to improve your CSS.
---


From *Escape Tutorial Hell* vid by Kevin Powell


## Resources

1. [Front End Mentor](https://www.frontendmentor.io) challenges with pre-made designs listed in order of difficulty. Premium and free tiers.
2. [Style Stage.dev](https://stylestage.dev/) a modern version of CSS Zen Garden. Submit via git. By Stephanie Eckles
3. [CSS Battle]
4. [Code Wars](https://codewars.com)
5. [Frontend Practice](https://www.frontendpractice.com/) copy various sites. Difficulty ratings from 1 to 3 - you learn a lot from this.
6. [Contribute to Real projects] - open source projects. See [First Timers Only](https://firsttimersonly.com/)

{{< yt QqDH5sYzDS8 >}}

## Other ideas

1. Makes sites for friends
2. for charitable projects
3. For Fivrr and Upwork etc..


