---
title: "Other Windows Stuff"
date: 2022-09-02T06:06:58+01:00
summary: notes and links on Windows..
tags: [Windows]
---

## Windows debloater

Run a command from powershell to install and run this app. Creates a sytem restore in case anything goes wrong. Select what you don't want: eg. Cortana.

[techtools thread](https://thewindowsforum.com/threads/debloat-windows-11-from-unnecessary-apps-and-services.83429/)


## Windows 11 activation

See [techtools thread](http://techtools.net/forum/viewforum.php?f=12)