---
title: "Using Linux"
date: 2022-09-02T05:26:31+01:00
summary: Notes on using Linux from the CLI
draft: true
tags: [linux, sudo, apt]
---

## su and sudo

`su` means *switch user* OR *superuser*. It's like using a windows cmd shell with administrator priviliges. In Linux this is disabled by default and instead you used `sudo` which means *switch user and do this*. You have to give your password which is cached for 15 minutes. Each command with `sudo` is done with admin priviliges but you are not logged in as a *superuser*.

## superuser vs admin

The superuser is considered to be the first person setting up an account on the OS. Subsequent users can be either *standard* or *administrator*. An administrator has

## apt

Linux's package manager. It downloads from the distro's repo.

There is also `snap` which installs from elsewhere, eg. `snap install hugo`.


## The Linux Command Line

This is [a book](http://linuxcommand.org/index.php) that's online and free to read.