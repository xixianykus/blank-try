---
title: "More Bash Commands"
date: 2022-09-20T06:56:03+01:00
draft: true
summary: Bash commands I don't know well
tags: [bash, linux]
---

## wc

Not water closet but *word count*.

```bash
wc file.txt

25  132 1467 file.txt
```

The first number, 25, is the number of lines.

The second number, 132 is the number of words.

THe third is the number of characters.



You can use this to get the number of files in a folder too like this:

```bash
ls path/to/file.txt | wc -l
```

The `-l` switch says count the number of lines only.

Other switches are:

- `-m` or `--chars` for characters only
- `-c` or `--bytes` for bytes only
- `-w` or `--words` for words only

## type

Gives information about a command eg.

```bash
type pwd

pwd is a shell builtin
```

If there are several versions you can find out *which* version you're using, and the path to it, using the `which` command.

```bash
which ls
```

## find


## alias

`alias` by itself is a great way to print out a list of aliases.

You can also use it to create temporary aliases:

```bash
alias cls='clear'
```

By typing `cls` you can run the `clear` command.

## unalias

Makes the created aliases non-functional.

```bash
unalias cls
```

You can add the `-a` option to unalias all aliases.

## sh

The shell command.

## chmod

You can see the file permissions using the *long* form of the `ls` command: `ls -l`.

## chown

Use to change the owner and user of any file or folder in bash.

## grep

## Linux only commands

The following don't work with Windows git-bash

### free

The `free` command gives info about the computer resources:

```bash
              total        used        free      shared  buff/cache   available
Mem:       13045664       69360    12912572          68       63732    12792784
Swap:       4194304           0     4194304
```

### man

This is the manual for a command, ie. an in depth help file.

```bash
man ls
```

### passwd

Used to change the login password. You need to type in teh current password before changing it.

### sudo su
### exit


## Links

1. [25-bash-commands-you-should-know/](https://linuxhint.com/25-bash-commands-you-should-know/ "from linuxhint.com")
2. [Command-line-for-beginners](https://ubuntu.com/tutorials/command-line-for-beginners) at ubuntu.com
3. [Linux Command.org](http://linuxcommand.org/lc3_lts0060.php) is the site of the book by William Shots
4. [LearnBash.org](https://learn-bash.org/) explains programming with bash. The site also covers other languages such as [JavaScript](https://learn-javascript.org/).