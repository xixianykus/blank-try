---
title: "New Places"
date: 2022-08-28T17:07:55+01:00
summary: New websites, video channels on the web that could be interesting.
---

1. Selma Alam Naylor aka [whitep4nth3r](https://whitep4nth3r.com/blog/)
2. White P4nth3r [youtube channel](https://www.youtube.com/c/whitep4nth3r)
3. [Code Newbie](https://www.youtube.com/c/CodeNewbie/videos) interesting looking youtube channel.