---
title: "WSL experiences"
date: 2022-09-01T17:02:53+01:00
draft: false
summary: My experience setting up Windows Sub system for Linux v.2, inc installing programs and getting snap to work.
tags: [wsl, bash, windows]
---

## Installing WSL 2

From a command prompt or Powershell type:

```bash
wsl --install
```

This does several things

1. Enables WSL
2. Enables the virtual machine platform
3. downloads and installs ubuntu.

When I tried to start Ubuntu I got an error. I had to enable virtualisation in my BIOS.

It still did not work. So I restarted Windows again and this time tried starting Ubuntu in an elevated prompt. This worked.

## Configuring the terminal

WSL 2 on Windows 10 only works on the command line. A good program to use for this is Microsoft's Terminal which can be installed from the MS Store, or with Chocolatey package manager or Windows package manager, winget.

I used the *one half dark* theme for bash. However some colours had so little contrast (when I did ls many items had a pale green background and pale blue) I had to change them.

This involved opening a JSON file by clicking on the icon at the bottom left of the terminal screen and having the JSON file popping open in your default editor.

The colour schemes look like this and are much easier to change in VS Code where you can see and pick new colours with a colour picker:

```JSON
    {
         "background": "#282C34",
         "black": "#282C34",
         "blue": "#066fc5",
         "brightBlack": "#5A6374",
         "brightBlue": "#61AFEF",
         "brightCyan": "#56B6C2",
         "brightGreen": "#56bf0b",
         "brightPurple": "#C678DD",
         "brightRed": "#E06C75",
         "brightWhite": "#DCDFE4",
         "brightYellow": "#E5C07B",
         "cursorColor": "#FFFFFF",
         "cyan": "#56B6C2",
         "foreground": "#DCDFE4",
         "green": "#98C379",
         "name": "One Half Dark",
         "purple": "#C678DD",
         "red": "#E06C75",
         "selectionBackground": "#FFFFFF",
         "white": "#DCDFE4",
         "yellow": "#E5C07B"
     },
```

The name of the theme is not easy to spot but in this list you'll see it on line 17, randomly mixed with the colours.

## No autocomplete

Also missing was autocomplete. This can fixed by typing in `shopt -s direxpand` and autocomplete works for the session. Add this line to the `.bashrc` file to make it work all the time.


## Custom shells

The main ones are FISH, ZSH and Oh My ZSH..

## Bash home files

| dir                       | function                                            |
| :------------------------ | :-------------------------------------------------- |
| .bash_history             | what it does                                        |
| .bash_logout              | what it does                                        |
| .bashrc                   | bash config file for aliases and default behaviours |
| .config                   | what it does                                        |
| .landscape                | what it does                                        |
| .motd_shown               | what it does                                        |
| .profile                  | what it does                                        |
| .sudo_as_admin_successful | what it does                                        |
| .viminfo                  | what it does                                        |


## VS Code server

So I tried to open a file from the bash shell with vs code. This prompted the installation of **VS Code server for X64**.


## sudo apt upgrade

To update installed packages you first need to update the package database:

```bash
sudo apt-get update
```

There are 3 types of packages: `Get`, `ign` and `Hit`. `Get` packages can be updated. `Ign` packages have been ignored. Nothing to worry about here. And `Hit` packages are already up to date.

To update the packages listed as `Get` use:

```bash
sudo apt-get upgrade
```

This tells you how much disk space will be needed and you can choose `y` to install or `n` to decline.

To upgrade just one specific package use:

```bash
sudo apt-get upgrade <package_name>
```

For more on `apt-get` see the [Its FOSS guide](https://itsfoss.com/apt-get-linux-guide/)



## Using snap

When trying to download hugo with `snap`: `snap install hugo` I got:

```bash
 error: cannot communicate with server: Post http://localhost/v2/snaps/hugo: dial unix /run/snapd.socket: connect: no such file or directory
```

Seems like:

> If you are using Ubuntu inside Windows using WSL, you will have SysV instead of systemd and your system will complain when you run the systemctl command (intended for Linux systems with systemd init system).
> [system-has-not-been-booted-with-systemd](https://linuxhandbook.com/system-has-not-been-booted-with-systemd/)


### The Fix

The problem can be resolved here: [Use this ONE trick to get Systemd and Snap Packages working on Windows Subsystem for Linux!](https://snapcraft.ninja/2020/07/29/systemd-snap-packages-wsl2/)


## Navigating in WSL

All the drives on your machine are found in the `/mnt` directory. So to change to a specific drive and folder, say the `T` drive just use:

```bash
cd /mnt/t/HTML-extras
```


## More

1. [A Beginners Guide to Snaps in Linux – Part 1](https://www.tecmint.com/install-snap-in-linux/)