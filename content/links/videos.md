---
title: "Videos"
date: 2021-09-06T21:59:32+01:00
draft: false
summary: A list of video channels worth watching
---

So there's a ton of video stuff out there. Here are some of my faves:

1. [9elements](https://www.youtube.com/channel/UCgdBBlyrrG237GXn4D3dLJA) has a bunch of talks by top names like Heydon Pickering, Andy Bell and many more.
2. [Kevin Powell](https://www.youtube.com/channel/UCJZv4d5rbIKd4QHMPkcABCw) the CSS master with short tips and longer tutorials too.
3. [brief.video](https://briefs.video/) a great video blog by Heydon Pickering
4. [Self Teach Me](https://www.youtube.com/channel/UChoskCVZiIDTKxusGPPhLOg)
5. [James Q Quick](https://www.youtube.com/channel/UC-T8W79DN6PBnzomelvqJYw) inc. Build a Quiz app, VS Code, Prettier
6. [Mike Dane](https://www.youtube.com/channel/UCvmINlrza7JHB1zkIOuXEbw) inc. JS, CSS, SASS, Hugo, Gatsby, and a ton more.
7. [Design Course](https://www.youtube.com/channel/UCVyRiMvfUNMA1UPlDPzG5Ow) tip and tutorials on web design from shorts like Rapid Redesigns to longer more indepth stuff plus some coding stuff on CSS and even Greensocks animation library.
8. [Programming with Mosh](https://www.youtube.com/c/programmingwithmosh/videos) covers a wide range of topics inc. JS frameworks, JS in 1hr, ES6 in 1hr, a 6 hour JS course, Data structures for beginners, Node, JS shorts, Rest API's, Design Patterns etc..
9. [Web Dev Simplified](https://www.youtube.com/channel/UCFbNIlppjAuEX4znoulh0Cw) another good channel on various front end topics such JSON, Houdini, REST, JS quiz apps, freelancing, Graph QL, React etc..
10. [Florin Pop](https://www.youtube.com/channel/UCeU-1X402kT-JlLdAitxSMA) interesting channel with lots of JS projects, such as [10 JS projects in 1 hour](https://youtu.be/8GPPJpiLqHk).
11. [Code with Ania Kubow](https://www.youtube.com/c/AniaKub%C3%B3w/search?query=javascript%20bootcamp) another good channel with lots of content inc. lots of JS game projects, [a 12 hour bootcamp](https://www.youtube.com/watch?v=Xm4BObh4MhI), [How to use APIs](https://www.youtube.com/watch?v=dfaj4vI8QxE), [Build a web scraper](https://www.youtube.com/watch?v=-3lqUHeZs_0)
12. [Design Course](https://www.youtube.com/channel/UCVyRiMvfUNMA1UPlDPzG5Ow) comprehensive channel of design for the front end. [UI Design for Coders](https://www.youtube.com/watch?v=0JCUH5daCCE) is a 50 min on Traversy Media.

