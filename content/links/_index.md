---
title: "Links"
date: 2019-11-02T10:47:57Z
draft: false
summary: "Links section has several pages!"
toc: true
css:
- ".content {columns: 300px 3; column-gap: 60px; column-rule: solid 1px #ffffff22}"
- ".content :first-child {margin-top: 0}"
- ".content h2 {break-after: avoid}"
- ".content li {break-before: avoid; break-inside: avoid}"
- ".time {margin-bottom: 4em}"
---

## Hugo

1. [Hugo Official site](https://gohugo.io/)
2. [The New Dynamic](https://www.thenewdynamic.com/) great selection of articles and Hugo freebies.
3. [Regis Philibert: From Wordpress to a mindset transition](https://regisphilibert.com/blog/2019/01/from-wordpress-to-hugo-a-mindset-transition/)
4. [Hugo Codex](https://hugocodex.org/) has some good 'add ons' such as breadcrumb nav, sliders, galleries and even chat functionality.
5. [Cusdis comment app](https://cusdis.com/) is a free, open source Disqus alternative.
6. [Hugo Cheatsheet](https://ohhelloana.blog/my-hugo-cheatsheet/) - nice list of useful code snippets for Hugo.
7. [Budd Parr's mega list](https://github.com/theNewDynamic/awesome-hugo/blob/main/README.md#articles) of Hugo articles


## Colour

1. [Colour harmonies](https://www.tigercolor.com/color-lab/color-theory/color-harmonies.htm) is a good and simple guide to ways of mixing colours.
2. [mycolor.space](https://mycolor.space/) Quickly generates colour schemes from one colour.
3. [color.adobe.com](https://color.adobe.com/) online tool has lots of options for creating colour schemes.

## SVG

1. [SVGator](https://www.svgator.com/) free online SVG editor but only for 3 exports pcm.
2. [SVGO](https://github.com/svg/svgo) is a command line SVG compressor tool.
3. [SVGOMG](https://jakearchibald.github.io/svgomg/) is a great web app version of SVGO with a list of button options so you can see your code by enabling and disabling the many options. (uncheck `remove unused defs` and `clean IDs` for sprites)
4. [Sara Soueidan's blog](https://www.sarasoueidan.com/blog/) an expert on all things SVG
5. [SVG Sprites](https://svgsprit.es/) drag'n'drop your SVG's to have them made into a sprite.

## Markdown

1. [HTML to Markdown converter](https://codebeautify.org/html-to-markdown "and loads of other converters too") at Code Beautify
2. [Markdown, a recap](https://roneo.org/markdown/ ) has lots of advanced Markdown practices but starting from basic stuff.

## Other stuff

1. [Emoji Cheatsheet](https://emojicheatsheet.com/)🐷
2. [Best free resources](https://yesimadesigner.com/free-resources-for-graphic-designers-and-illustrators/) by Yes, I'm a Designer
3. [Alt and Unicode codes](https://altcodeunicode.com/) 
4. [DuckDuckGo list of HTML characters](https://duckduckgo.com/?t=ffab&q=html+characters&ia=answer&iax=answer)
5. [How to Favicon in 2021](https://evilmartians.com/chronicles/how-to-favicon-in-2021-six-files-that-fit-most-needs)

## Icons

So I prefer [SVG sprites](https://css-tricks.com/svg-sprites-use-better-icon-fonts/ "CSS Tricks article on how these work") and some Icon sites allow downloading a selection of icons as a sprite which is the best way to go. (See the [Creative Bloq guide](https://www.creativebloq.com/features/the-complete-guide-to-svg/6 "a 2018 article"))

1. [IconFinder](https://www.iconfinder.com/) has a big somewhat random selection of icons. These are free without any issues. Download the SVG or copy the code to the clipboard. You'll probably want to tidy up the code before using as there's quite a bit of unnecessary code in them.
2. [Iconmnstr](https://iconmonstr.com) can download or just grab the code. Copy and pasting is good for creating SVG sprites. Need to disable adblockers to use.
3. [Fontastic.me](https://app.fontastic.me/) excellent easy way to set up SVG sprites hosted on their CDN. See [Cloud based icons](/posts/cloud-based-icons).
4. [IcoMoon](https://icomoon.io/) has 5500 free SVG icons. Launch the app, pick some icons then download the code and CSS. Can be downloaded as sprites.
5. [The Noun Project](https://thenounproject.com/) has a really great selection, though you need to sign up for an account first.
6. [Font Awesome](https://fontawesome.com/) has a complicated set up but easier to add icons using HTML italic tags like this: `<i class="fab fa-adn"></i>`.
7. [Heroicons](https://heroicons.com/) has a nice set of non-corporate icons (no Twitter or Facebook for instance). One click on the icon to copy the SVG code makes it very easy to use.
8. [Zondicons](http://www.zondicons.com/) another set of icons you can [download](http://www.zondicons.com/zondicons.zip) in one foul swoop.
9.  [Open Mojis](https://openmoji.org/library/) open source emoji library.
10. [Icon Icons](https://icon-icons.com/) has a very comprehensive provided by different authors in ICO, ICNS, PNG and SVG formats.
11. [Fontello.com](https://fontello.com/) completely free, non commercial site to create custom icon font files inc. `.woff2` and `svg` fonts.
12. [Icons8](https://icons8.com/) the free tier has limits on format, low res only and requires a link to back to them.
13. [Flaticon](https://www.flaticon.com/) can only download as PNG's on the free tier.

Finally see [Smashing Mag's page](https://www.smashingmagazine.com/2021/08/open-source-icons/) on open source icons with a variety of subjects: weather, people, health, flags etc..



## Inspiration

1. [Style Stage](https://stylestage.dev/) a modern CSS Zen Garden.
2. [Lapa Ninja](https://lapa.ninja) landing page gallery
3. [CSS Nectar](https://cssnectar.com/)
4. [Dribbble](https://www.dribbble.com/) designs and illustrations by the community.
5. [Code My UI](https://codemyui.com/)
6. [Codrops](https://tympanus.net/codrops/) has a regular selection of inspiring sites plus articles on cool effects.

## Illustration

1. [Illustrators Lounge](https://illustratorslounge.com/)


See also the [categories section](/categories/)

