---
title: "Dev blogs"
date: 2021-09-07T17:13:48+01:00
draft: false
summary: Blogs about development
---

## General

1. [Smashing Magazine](https://smashingmagazine.com/)
2. [Frontend Horse](https://frontend.horse/)
3. [Hashnode](https://hashnode.com/)
4. [Dev.to](https://dev.to)
5. [DailyDev](https://daily.dev/blog)

## Personal

1. Tania Rascia - great source of tutorials beautifully written.
2. [Lea Verou](https://lea.verou.me/) lots of great stuff on CSS.
3. [Amit Sheen](https://www.amitsh.com/) - CSS master, animator (like [these steps](https://codepen.io/amit_sheen/pen/vYGdBNo))
4. [CSS In Real Life](https://css-irl.info/) by [Michelle Barker](https://codepen.io/michellebarker "her Codepen")