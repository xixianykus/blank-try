---
title: "CSS"
date: 2021-09-07T11:33:59+01:00
draft: false
summary: "CSS resources and how to's"
---


## CSS Resources

1. [24 Pattern Generators](https://dev.to/kiranrajvjd/the-ultimate-css-background-pattern-resource-20m8)
2. [10 Shape Generators](https://dev.to/kiranrajvjd/10-awesome-svg-shape-generators-for-your-web-projects-2mob). [This one](https://www.softr.io/tools/svg-wave-generator) seems pretty good as is [getwaves.io](https://getwaves.io/). The [Hakai.app](https://app.haikei.app/) can generate blurry svgs and some other fantastic generators. [Waverly](https://wavelry.vercel.app/) is not bad either.
3. [Kiran's list](https://dev.to/kiranrajvjd/the-ultimate-web-developer-resources-list-200-resources-2gf5) of 200 online apps
4. [CSS Author Generators](https://cssauthor.com/svg-pattern-generator/)
5. [Awesome CSS Generators](https://dev.to/kiranrajvjd/awesome-css-generators-3709)
6. [Grainy Gradient Playground](https://grainy-gradients.vercel.app/) use SVG filters on CSS gradients.
7. [BG Jar](https://bgjar.com/) - wave generator straight to CSS, excellent.
8. [Josh Comeau Gradient Generator](https://www.joshwcomeau.com/gradient-generator/) perhaps the best generator because programmatically creates gradients based on colour in different colour modes like HCL, HSV and LAB modes. Gradients thus don't suffer from grey in the middle. Also change the easing of the gradients.
9. [Vivid Gradient Generator](https://learnui.design/tools/gradient-generator.html)
10. [Polychroma Gradient Generator](https://polychroma.app/) another great generator simulating LAB, HSL and Lch modes displaying gradients at full screen height.
11. [CSS Gradient.io](https://cssgradient.io/)
12. [Colllor](http://colllor.com/) great way to produce a range of graduated swatches.
13. [Hamburgers](https://jonsuh.com/hamburgers/) pre-made animated icons that are easy to install.
14. [W3 Schools Color Picker](https://www.w3schools.com/colors/colors_picker.asp) easy way to generate shades, hues and different saturation from one colour.
15. [CSS Triange Generator](http://apps.eky.hk/css-triangle-generator/)

## CSS How to's
1. [Grid by Example](https://gridbyexample.com/) showcase of basic grid layouts by Rachel Andrew.
2. [Quackit Grid templates](https://www.quackit.com/html/templates/css_grid_templates.cfm)
3. [Modern CSS](https://moderncss.dev/) - great list of articles, tips and how to's.
4. [Smol CSS](https://smolcss.dev/) is the snippet version of Modern CSS by Stephanie Eckles
5. [CSS Wizardry](https://csswizardry.com/archive/) mix of performance and CSS posts by performance expert Harry Roberts.
6. [CSS Guidelin.es](https://cssguidelin.es/) also by Harry Roberts is another excellent resource.
7. [CSS-IRL](https://css-irl.info/) CSS In real life

