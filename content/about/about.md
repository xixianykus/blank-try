---
title: About Supplement
date: 2021-05-07T17:31:01.959Z
draft: false
summary: Ideas for where this site is going.
---


## Roadmap

1. ~~Responsive menu based on [W3 Schools code](https://www.w3schools.com/howto/howto_js_side-nav.asp)~~ ✔️
2. [Ton of javascript](https://www.smashingmagazine.com/2021/04/vanilla-javascript-code-snippets/) stuff to try out.
3. Dark mode switch or button.
4. [Image gallery](/posts/image-gallery-research/) more needed.
5. Add animation to page loads and menu/hover effects.
6. Add a class active to the main menu and style.
7. Add image processing to `ps_thumbs.html` (shortcode for producing a group of thumbnails from a leaf branch.)
8. Fix footer issue w Netlify logo. Temp fix was to replace the `auto` row height on `grid-template-rows` to `minmax(7.5em, auto)`. 
9. Add dropdown list of links to main menu. But how???
10. Search box - algolia free tier perhaps?
11. delete unused shortcodes AND any instance of them.
12. modify codepen shortcode, maybe have a width value. Figure out default values
13. Fix rotating close button. Make it work better with chrome.
14. ~~Style tags, sort into an order, probably need own layout page.~~ ✔️
