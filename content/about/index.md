---
title: "About"
date: 2021-05-21T05:30:02+01:00
draft: false
layout: about
tags: [leaf bundle, hugo]
---

This is a rough and ready web site I built for experimenting with but it ended up morphing into a kind of notetaking blog. It's on the dark web meaning it's not registered with Google so it's not designed for visitors but I thought I'd write this just in case.

## Tech

On the technical side the site is built with Hugo static site generator. The files are kept in a GitLab repo and it's built and deployed by Netlify.

Design wise the site is built using a heavily modded version of the Blank theme by Vimux (hence the site title, Blank Try, which refers to the fact that this was trying out of the blank theme.)

## This page

The page is, in Hugo parlence, a leaf bundle. This means in it's pre-built state it has an `index.md` file in a folder along with other files listed below. Two of the files are markdown files and one of these is displayed on the right.

