---
title: "The Docs"
date: 2021-08-07T12:17:36+01:00
draft: false
summary: The dccumentation for using this site
toc: true
css:
- ".list-content > div {background-color: transparent }"
- ".shortcodes ul {list-style-type: none; padding-left: 0;}"
- ".shortcodes li {break-inside: avoid; margin-bottom: .5em;}"
---



This site has an ever growing list of special features. The docs are here as a reminder of how to use them.

## The blank theme

The site is based on the blank theme by Vimux which has a few useful features built in like pagination. Pagination is set in the config file: `paginate = 12` sets the number of articles before a new page is created.

Edits and addtions to this theme should be added to the `/layouts` folder, NOT the `themes/blank` folder.

Emoji can be used coz `emoji = true` is a config.toml variable. 😀😀😀 These are added via the `Windows + ;` keys in Windows 10.

## The home page

The site updates section pulls content from `/headless/index.md`. This is currently set to `headless: false`.

## Navigation

The top menu, `[menu.main]` is set in the config file. No other menu is set. Should this be part of a `[menu]` object? Dunno. 😯

[The sidebar](/code/sidebar-code/) shows the content of the current section if there is one. For the Home, About and Links pages it shows a mixture of the latest content from *Posts*, *Code* and *Music*.

## Adding HTML to Markdown pages

There are two shortcodes to add HTML tags to markdown files: **div** and **html**. These are very similar. Both can take the attributes:
 - id
 - class
 - style

The difference is that the **div** shortcode includes the `markdownify` function to the inner section. This means you carry on writing markdown in between your opening and closing shortcode.

With html you write using HTML instead.

Here is the div shortcode.

```go-html-template
{{ $id := .Get "id" }}
{{ $style := .Get "style" }}

<div 
  class="{{ .Get "class" }}"
  id="{{ $id }}"
  style="{{ $style | safeCSS }}"
  >

    {{ .Inner | markdownify }}
</div>
```

## CSS

If everything is included this would be a big topic. 

Every page can be accessed by by unique body classes. These are all lower case and for regular pages are the file name without the extention. For section pages they use the title in lower case with spaces replaced with hyphens.

```html
<body class="the-docs docs">
```

You can add a page level style sheet to the `<head>` section of any page using a frontmatter array:

```yaml
css:
- "section.content { font-size: 2em; }"
- ".pigbox {background-color: #33333388; font-size: .8em;}"
```

A range function in the `<head>` partial outputs these to safeCSS.

### Inline CSS

You can add class, id and inline values to elements using single curly braces either directly after on headings, or the first line below what will be an HTML element in the markdown. This is because block level elements have been allowed in the config.toml file:

```toml
[markup]
  [markup.goldmark.parser.attribute]
    block = true
    title = true
```

As of Hugo 0.86 these don't work on list items, only full lists, or table rows or cells. You can apply them to lists: `<ul>` or `<ol>` and `<table>` elements however.

## SVG's

SVG images can be accessed through the normal markdown image tag.

SVG sprites can be called using the SVG shortcode. This takes two arguments:

1. the file name (no extention needed)
2. the id (no hash needed). This will also be added to the class names:

```svg
<svg class="icon {{ id-name }}">
  <use xlink:href="/img/svg/{{ file-name }}.svg#{{ id-name }}"></use>
</svg>
```

This can then have CSS applied, for instance in the frontmatter:

```yaml
css:
- ".magnifying-glass {fill: red;}"
```

This will work if there is no fill applied to the svg itself.

NB. This is a CLASS name not the id name. That's intentional. There might be multiple instances of the same icon on one page.

The sprite MUST be in `/img/svg/` folder and MUST have an svg extention.

## The different content sections

These are currently fairly obvious:

- **code** is for short snippets of code on different things.
- **music** is for ... obviously
- **posts** is for any longer articles. Might make sense to split this up at some point, separating computer stuff from more general.

There is some other stuff too inc. and [Learn SVG section](/posts/svg/)

## Table of Contents

This is set up using Hugo's built in `{{ .TableOfContents }}`. All it needs is to set `toc: true` in a pages frontmatter.

The number of levels to be listed is set in the config.toml file.

## shortcodes

The site currently has the following shortcodes

{{< div class="shortcodes" style="columns: 2 450px" >}}
- **arrays**
- **asset_image**
- **baguette**
- **bundle_image**
- **clampsizer**
- **codepen1**
- **codepen2**
- **codepen3**
- **details** - takes one value, the `<summary>`. The dropdown text is written in markdown between the open and closing tags.
- **div** - a div to contain markdown. Can take class, id and style attributes.
- **draft**
- **gitlab_snip**
- **html** - a div that takes class, id or style attributes. For html code (rather than markdown)
- **img**
- **leafletjs**
- **osm-map**
- **picture** - processes images in page bundles with figure and figcaptions. Takes two attributes: `alt=""` and `caption=""`
- **ps_thumbs**
- **render_image** - an image processing shortcode based on BEP's example.
- **search** - for the search page
- **svg** - used to get svgs from svg sets in the image folder. Takes two parameter, the name of the svg file then the name of the ID of the svg to `<use>`.
- **testpage**
- **unfinished** - lists linked files that are marked unfinished.


### folders

**charts**/

- firstchart
- chartjs
  
**experiments**/    

- bag
- css_target
- getpage
  
**music**/  

- **dnb**
- **house**
- **soundcloud**
- **yt** - custom youtube shortcode using `aspect-ratio`         

**svg**/

- hugo-logo
- heart

{{< /div >}}



## Unfinished files

If the frontmatter key `unfinished: true` is set the page will have and *unfinished* heading at the start of the page. This links to the *unfinished* page that has a list of all such pages for quick access. No need to worry about the `draft` setting anymore.

A complete list of files marked both *unfinished* and *draft* are on the [unfinished page](/head-2/)


## External links

These appear at the bottom of a page in the form of buttons. Add to a page using `extlinks` field in the frontmatter. This is an array consisting of:

1. A link
2. The words or phrase linked to
3. Anything else added to the link title tag.

```yaml
extlinks: [https://example.com/, The Example Site, A non existant site perhaps?]
```

## Image galleries

### Photoswipe

This gallery is not working 100% properly yet but anyway here's what you do:

1. Create a leaf bundle (folder with index.md for the page) and add the images you want displayed.
2. Add the relevant links to the head of the page by adding `photoswipe: true` to the frontmatter.
3. This will also add the photoswipe partial to the bottom of the page so it'll be done.

### Baguette Box JS

1. Create a leaf bundle
2. Frontmatter: `baguette: true` places CSS and JS CDN links in the head of the document.
3. Shortcode: `{{ baguette "folder-name" }}` should allow several galleries on one page.

## Graphs

There are currently two types of graphs used, *chart JS* and *chartist JS*. For both you add the respective frontmatter variable:

```yaml
chartjs: true
# or
chartistjs: true
```

Then the shortcode `{{ charts/firstchart }}` will add the finger strength graph using *chartist JS*. For the same *chart JS* graph use `{{ charts/chartjs }}`. So if you want a new chart copy one these shortcodes to a new shortcode file, edit the contents and use it that way.

These could do with adding maybe a frontmatter object or `.Get` array to the shortcode to make them fully functional allowing input into the data. 

Here's a frontmatter example that might work with Chart JS (not yet set up mind you):

```yaml
chartjs:
- [Ben Moon, 101]
- [Magnus Mitbo, 108]
- [Steve McClure, 116]
```

anyway... onwards


## Mermaid

To use the Mermaid JS charts first add `mermaid: true` to a page's frontmatter to add the script link and code to the `<head>` of a page.

You can add a mermaid chart using either:
- a div tag with the `html` shortcode with a class of `mermaid` OR
- use code fences (3 backticks) with a language of `mermaid`

The second method works but the images are rather small and coloured oddly. Possibly OK for some things but not others.

The shortcode:

```go-html-template
{{⪡ html class="mermaid" ⪢}}
// mermaid code goes here
{{⪡ /html ⪢}}
```

The [official Mermaid site](https://mermaid-js.github.io/mermaid/) has clear info on creating flow charts, pie charts and much more.

